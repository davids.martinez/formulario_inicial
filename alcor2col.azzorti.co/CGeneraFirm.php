<?php
// define('FPDF_FONTPATH','/disco1/paginas/intranet2col.azzorti.co/lib_php/pdf7/font/');
define('FPDF_FONTPATH','/disco1/paginas/alcor2col.azzorti.co/fontPdf/');
include("fontPdf/Gilroy_light.php");
include("pdf/fpdf.php");
require("pdf.php");
/*
 * Clase General para generación de PDF firma digital RISK
 * 22-11-2021
 * Ferney Garzón
*/
class CGeneraFirm{
  private $conexion = null;
  private $comunica = null;
  private $root;

  public function __construct($conexion,$comunica)
  {
    require_once ("cadena.php");
    require_once ("Conexion_Pdo.php");
    $fecha_log = date("m-d-Y");
    $stdlog=fopen(RUTA_TEMPORALES.'/ClaseGeneraFirm_'.$comunica.'_'.$fecha_log.'.txt','a');

    // Servidor y conector ambiente Productivo
    // $this->serv_dest = "https://alcor.dupree.co";
    // $this->conexion = new conexion_pdo("Informix","ProduccionColombia",$stdlog, false);

    // Servidor y conector ambiente de Desarrollo
    $this->serv_dest = "https://alcor2col.azzorti.co";
    $this->conexion = new conexion_pdo("Informix","DesarrolloColombia",$stdlog, false);
	$this->root = $_SERVER["DOCUMENT_ROOT"];
  }

  public function gene_docu_prue()
  {
		$nomb_arch = "documentoBordes.pdf";
		$ruta = RUTA_TEMPORALES.$nomb_arch;
		$pdf=new PDF();
        $pdf->AddFont('Gilroy_light','','Gilroy_light.php');
		$pdf->AddPage();
		$pdf->SetFillColor(199);
		$pdf->RoundedRect(10, 17, 90, 7, 2, '135','F');
		$pdf->Output($ruta);
		$urlx_fina = "https://".$_SERVER["SERVER_NAME"].URL_TEMPORALES.$nomb_arch;
		die("aqui voy".$urlx_fina);
		return $urlx_fina;
  }

  /**
   * Genera PDF
   *
   * Este metodo genera el documento pdf para la solicitud de credito
   * @param
   * @param
   * @param
   * @return
  */
  public function gene_docu_firm($nomb_pers,$apel_pers,$fech_naci,$soli_nume,$tipo_docu,$nume_docu,$depa_resi,$ciud_resi,$zona_vinc,$cent_pobl,$barr_soli,$dire_resi,$dire_entr,$celu_soli,$nume_what,$corr_elec,$inic_proc,$nomb_refe,$ciud_refe,$celu_refe,$pare_refe,$refe_pers,$ciud_pers,$celu_pers,$firm_dig1,$firm_dig2,$tran_star,$refe_codi)
  {
    $dia_hoy = date("d");
    $mes_hoy = date("m");
    $ano_hoy = date("Y");

    $nomb_arch = "solicitud_".$nume_docu.".pdf";
		$ruta = RUTA_TEMPORALES.$nomb_arch;
		$pdf=new PDF('P', 'mm', 'letter');
    $pdf->AddFont('Gilroy_light','','Gilroy_light.php');
    $pdf->AddFont('Gilroy_light','B','gilroy_lightb.php');
		$pdf->SetMargins(20, 10, 15);
		$pdf->SetLineWidth(0.0025);
		$pdf->SetFillColor(220, 220, 220);
		$tota_pagi = 13; // Total de pagina definidas para pintar en PDF

		// Formato utf-8 de textos con acentos
		$var0   = iconv("utf-8", "iso-8859-3", "Versión");
		$var1   = iconv("utf-8", "iso-8859-3", "compañia");
		$var2   = iconv("utf-8", "iso-8859-3", "cédula");
		$var3   = iconv("utf-8", "iso-8859-3", "ciudadanía");
		$var4   = iconv("utf-8", "iso-8859-3", "días");
		$var5   = iconv("utf-8", "iso-8859-3", "vinculación");
		$var6   = iconv("utf-8", "iso-8859-3", "Dirección");
		$var7   = iconv("utf-8", "iso-8859-3", "electrónico");
		$var8   = iconv("utf-8", "iso-8859-3", "Número");
		$var9   = iconv("utf-8", "iso-8859-3", "crédito");
		$var10  = iconv("utf-8", "iso-8859-3", "desempeño");
		$var11  = iconv("utf-8", "iso-8859-3", "información");
		$var12  = iconv("utf-8", "iso-8859-3", "así");
		$var13  = iconv("utf-8", "iso-8859-3", "identificación");
		$var14  = iconv("utf-8", "iso-8859-3", "teléfono");
		$var15  = iconv("utf-8", "iso-8859-3", "confirmación");
		$var16  = iconv("utf-8", "iso-8859-3", "suplantación");
		$var17  = iconv("utf-8", "iso-8859-3", "podrá");
		$var18  = iconv("utf-8", "iso-8859-3", "señalado");
		$var19  = iconv("utf-8", "iso-8859-3", "reclamación");
		$var20  = iconv("utf-8", "iso-8859-3", "corrección");
		$var21  = iconv("utf-8", "iso-8859-3", "Parágrafo");
		$var22  = iconv("utf-8", "iso-8859-3", "través");
		$var23  = iconv("utf-8", "iso-8859-3", "vía");
		$var24  = iconv("utf-8", "iso-8859-3", "telefónica");
		$var25  = iconv("utf-8", "iso-8859-3", "afiliación");
		$var26  = iconv("utf-8", "iso-8859-3", "MÁS");
		$var27  = iconv("utf-8", "iso-8859-3", "más");
		$var28  = iconv("utf-8", "iso-8859-3", "POLÍTICA");
		$var29  = iconv("utf-8", "iso-8859-3", "Política");
		$var30  = iconv("utf-8", "iso-8859-3", "Pública");
		$var31  = iconv("utf-8", "iso-8859-3", "Notaría");
		$var32  = iconv("utf-8", "iso-8859-3", "Bogotá");
		$var33  = iconv("utf-8", "iso-8859-3", "página");
		$var34  = iconv("utf-8", "iso-8859-3", "tratarán");
		$var35  = iconv("utf-8", "iso-8859-3", "públicos");
		$var36  = iconv("utf-8", "iso-8859-3", "orientación");
		$var37  = iconv("utf-8", "iso-8859-3", "biométricos");
		$var38  = iconv("utf-8", "iso-8859-3", "técnicas");
		$var39  = iconv("utf-8", "iso-8859-3", "diseñadas");
		$var40  = iconv("utf-8", "iso-8859-3", "parámetros");
		$var41  = iconv("utf-8", "iso-8859-3", "físicos");
		$var42  = iconv("utf-8", "iso-8859-3", "únicos");
		$var43  = iconv("utf-8", "iso-8859-3", "características");
		$var44  = iconv("utf-8", "iso-8859-3", "categoría");
		$var45  = iconv("utf-8", "iso-8859-3", "protección");
		$var46  = iconv("utf-8", "iso-8859-3", "ejecución");
		$var47  = iconv("utf-8", "iso-8859-3", "relación");
		$var48  = iconv("utf-8", "iso-8859-3", "COMPAÑIA");
		$var49  = iconv("utf-8", "iso-8859-3", "explícitas");
		$var50  = iconv("utf-8", "iso-8859-3", "será");
		$var51  = iconv("utf-8", "iso-8859-3", "carácter");
		$var52  = iconv("utf-8", "iso-8859-3", "propósito");
		$var53  = iconv("utf-8", "iso-8859-3", "logísticos");
		$var54  = iconv("utf-8", "iso-8859-3", "efectúa");
		$var55  = iconv("utf-8", "iso-8859-3", "pagaré");
		$var56  = iconv("utf-8", "iso-8859-3", "circulación");
		$var57  = iconv("utf-8", "iso-8859-3", "jurídica");
		$var58  = iconv("utf-8", "iso-8859-3", "físico");
		$var59  = iconv("utf-8", "iso-8859-3", "comunicación");
		$var60  = iconv("utf-8", "iso-8859-3", "envíe");
		$var61  = iconv("utf-8", "iso-8859-3", "efectúe");
		$var62  = iconv("utf-8", "iso-8859-3", "georreferenciación");
		$var63  = iconv("utf-8", "iso-8859-3", "tecnológicas");
		$var64  = iconv("utf-8", "iso-8859-3", "ARTÍCULO");
		$var65  = iconv("utf-8", "iso-8859-3", "artículos");
		$var66  = iconv("utf-8", "iso-8859-3", "autorización");
		$var67  = iconv("utf-8", "iso-8859-3", "niñas");
		$var68  = iconv("utf-8", "iso-8859-3", "niños");
		$var69  = iconv("utf-8", "iso-8859-3", "infracción");
		$var70  = iconv("utf-8", "iso-8859-3", "régimen");
		$var71  = iconv("utf-8", "iso-8859-3", "según");
		$var72  = iconv("utf-8", "iso-8859-3", "ACTUALIZACIÓN");
		$var73  = iconv("utf-8", "iso-8859-3", "atención");
		$var74  = iconv("utf-8", "iso-8859-3", "línea");
		$var75  = iconv("utf-8", "iso-8859-3", "atenderán");
		$var76  = iconv("utf-8", "iso-8859-3", "LEÍDO");
		$var77  = iconv("utf-8", "iso-8859-3", "leída");
		$var78  = ICONV("UTF-8", "ISO-8859-3", "INFORMACIÓN");
		$var79  = iconv("utf-8", "iso-8859-3", "AUTORIZACIÓN");
		$var80  = iconv("utf-8", "iso-8859-3", "República");
		$var81  = iconv("utf-8", "iso-8859-3", "denominará");
		$var82  = iconv("utf-8", "iso-8859-3", "fabricación");
		$var83  = iconv("utf-8", "iso-8859-3", "comercialización");
		$var84  = iconv("utf-8", "iso-8859-3", "autónoma");
		$var85  = iconv("utf-8", "iso-8859-3", "Catálogo");
		$var86  = iconv("utf-8", "iso-8859-3", "Garantía");
		$var87  = iconv("utf-8", "iso-8859-3", "tendrán");
		$var88  = iconv("utf-8", "iso-8859-3", "continuación");
		$var89  = iconv("utf-8", "iso-8859-3", "artículo");
		$var90  = iconv("utf-8", "iso-8859-3", "perderá");
		$var91  = iconv("utf-8", "iso-8859-3", "satisfacción");
		$var92  = iconv("utf-8", "iso-8859-3", "doméstica");
		$var93  = iconv("utf-8", "iso-8859-3", "esté");
		$var94  = iconv("utf-8", "iso-8859-3", "intrínsecamente");
		$var95  = iconv("utf-8", "iso-8859-3", "económica");
		$var96  = iconv("utf-8", "iso-8859-3", "entenderá");
		$var97  = iconv("utf-8", "iso-8859-3", "razón");
		$var98  = iconv("utf-8", "iso-8859-3", "Campaña");
		$var99  = iconv("utf-8", "iso-8859-3", "Período");
		$var100 = iconv("utf-8", "iso-8859-3", "Publicación");
		$var101 = iconv("utf-8", "iso-8859-3", "éstas");
		$var102 = iconv("utf-8", "iso-8859-3", "básicos");
		$var103 = iconv("utf-8", "iso-8859-3", "términos");
		$var104 = iconv("utf-8", "iso-8859-3", "cláusula");
		$var105 = iconv("utf-8", "iso-8859-3", "estarán");
		$var106 = iconv("utf-8", "iso-8859-3", "Duración");
		$var107 = iconv("utf-8", "iso-8859-3", "suscribirá");
		$var108 = iconv("utf-8", "iso-8859-3", "término");
		$var109 = iconv("utf-8", "iso-8859-3", "décima");
		$var110 = iconv("utf-8", "iso-8859-3", "reservándose");
		$var111 = iconv("utf-8", "iso-8859-3", "Órdenes");
		$var112 = iconv("utf-8", "iso-8859-3", "deberá");
		$var113 = iconv("utf-8", "iso-8859-3", "físicamente");
		$var114 = iconv("utf-8", "iso-8859-3", "telefónicamente");
		$var115 = iconv("utf-8", "iso-8859-3", "cuantía");
		$var116 = iconv("utf-8", "iso-8859-3", "mínima");
		$var117 = iconv("utf-8", "iso-8859-3", "recepción");
		$var118 = iconv("utf-8", "iso-8859-3", "éste");
		$var119 = iconv("utf-8", "iso-8859-3", "están");
		$var120 = iconv("utf-8", "iso-8859-3", "políticas");
		$var121 = iconv("utf-8", "iso-8859-3", "serán");
		$var122 = iconv("utf-8", "iso-8859-3", "límites");
		$var123 = iconv("utf-8", "iso-8859-3", "pagará");
		$var124 = iconv("utf-8", "iso-8859-3", "logística");
		$var125 = iconv("utf-8", "iso-8859-3", "envío");
		$var126 = iconv("utf-8", "iso-8859-3", "representación");
		$var127 = iconv("utf-8", "iso-8859-3", "Séptima");
		$var128 = iconv("utf-8", "iso-8859-3", "público");
		$var129 = iconv("utf-8", "iso-8859-3", "emitirá");
		$var130 = iconv("utf-8", "iso-8859-3", "asumirá");
		$var131 = iconv("utf-8", "iso-8859-3", "suministrará");
		$var132 = iconv("utf-8", "iso-8859-3", "facturó");
		$var133 = iconv("utf-8", "iso-8859-3", "pérdida");
		$var134 = iconv("utf-8", "iso-8859-3", "éstos");
		$var135 = iconv("utf-8", "iso-8859-3", "daño");
		$var136 = iconv("utf-8", "iso-8859-3", "ningún");
		$var137 = iconv("utf-8", "iso-8859-3", "implicará");
		$var138 = iconv("utf-8", "iso-8859-3", "financiación");
		$var139 = iconv("utf-8", "iso-8859-3", "garantía");
		$var140 = iconv("utf-8", "iso-8859-3", "económicas");
		$var141 = iconv("utf-8", "iso-8859-3", "única");
		$var142 = iconv("utf-8", "iso-8859-3", "deberán");
		$var143 = iconv("utf-8", "iso-8859-3", "Campañas");
		$var144 = iconv("utf-8", "iso-8859-3", "señal");
		$var145 = iconv("utf-8", "iso-8859-3", "obligación");
		$var146 = iconv("utf-8", "iso-8859-3", "ésta");
		$var147 = iconv("utf-8", "iso-8859-3", "está");
		$var148 = iconv("utf-8", "iso-8859-3", "modificación");
		$var149 = iconv("utf-8", "iso-8859-3", "número");
		$var150 = iconv("utf-8", "iso-8859-3", "telefónico");
		$var151 = iconv("utf-8", "iso-8859-3", "tendrá");
		$var152 = iconv("utf-8", "iso-8859-3", "iniciación");
		$var153 = iconv("utf-8", "iso-8859-3", "diseñado");
		$var154 = iconv("utf-8", "iso-8859-3", "informará");
		$var155 = iconv("utf-8", "iso-8859-3", "aceptará");
		$var156 = iconv("utf-8", "iso-8859-3", "devolución");
		$var157 = iconv("utf-8", "iso-8859-3", "reversará");
		$var158 = iconv("utf-8", "iso-8859-3", "facturación");
		$var159 = iconv("utf-8", "iso-8859-3", "estipulación");
		$var160 = iconv("utf-8", "iso-8859-3", "estén");
		$var161 = iconv("utf-8", "iso-8859-3", "requerirá");
		$var162 = iconv("utf-8", "iso-8859-3", "presentación");
		$var163 = iconv("utf-8", "iso-8859-3", "Décima");
		$var164 = iconv("utf-8", "iso-8859-3", "colocación");
		$var165 = iconv("utf-8", "iso-8859-3", "correrá");
		$var166 = iconv("utf-8", "iso-8859-3", "garantías");
		$var167 = iconv("utf-8", "iso-8859-3", "diseños");
		$var168 = iconv("utf-8", "iso-8859-3", "coordinará");
		$var169 = iconv("utf-8", "iso-8859-3", "ubicación");
		$var170 = iconv("utf-8", "iso-8859-3", "distribución");
		$var171 = iconv("utf-8", "iso-8859-3", "Terminación");
		$var172 = iconv("utf-8", "iso-8859-3", "terminará");
		$var173 = iconv("utf-8", "iso-8859-3", "válidamente");
		$var174 = iconv("utf-8", "iso-8859-3", "hábiles");
		$var175 = iconv("utf-8", "iso-8859-3", "Código");
		$var176 = iconv("utf-8", "iso-8859-3", "investigación");
		$var177 = iconv("utf-8", "iso-8859-3", "vigésima");
		$var178 = iconv("utf-8", "iso-8859-3", "compensación");
		$var179 = iconv("utf-8", "iso-8859-3", "económico");
		$var180 = iconv("utf-8", "iso-8859-3", "terminación");
		$var181 = iconv("utf-8", "iso-8859-3", "vínculo");
		$var182 = iconv("utf-8", "iso-8859-3", "autonomía");
		$var183 = iconv("utf-8", "iso-8859-3", "técnica");
		$var184 = iconv("utf-8", "iso-8859-3", "únicas");
		$var185 = iconv("utf-8", "iso-8859-3", "realizarán");
		$var186 = iconv("utf-8", "iso-8859-3", "Cesión");
		$var187 = iconv("utf-8", "iso-8859-3", "posición");
		$var188 = iconv("utf-8", "iso-8859-3", "dirigirán");
		$var189 = iconv("utf-8", "iso-8859-3", "Teléfono");
		$var190 = iconv("utf-8", "iso-8859-3", "solución");
		$var191 = iconv("utf-8", "iso-8859-3", "reunirán");
		$var192 = iconv("utf-8", "iso-8859-3", "reunión");
		$var193 = iconv("utf-8", "iso-8859-3", "jurisdicción");
		$var194 = iconv("utf-8", "iso-8859-3", "años");
		$var195 = iconv("utf-8", "iso-8859-3", "suscripción");
		$var196 = iconv("utf-8", "iso-8859-3", "Vigésima");
		$var197 = iconv("utf-8", "iso-8859-3", "CLAÚSULA");
		$var198 = iconv("utf-8", "iso-8859-3", "ocasión");
		$var199 = iconv("utf-8", "iso-8859-3", "Compañía");
		$var200 = iconv("utf-8", "iso-8859-3", "Régimen");
		$var201 = iconv("utf-8", "iso-8859-3", "Protección");
		$var202 = iconv("utf-8", "iso-8859-3", "DUPRÉE");
		$var203 = iconv("utf-8", "iso-8859-3", "divulgación");
		$var204 = iconv("utf-8", "iso-8859-3", "legislación");
		$var205 = iconv("utf-8", "iso-8859-3", "situación");
		$var206 = iconv("utf-8", "iso-8859-3", "contravención");
		$var207 = iconv("utf-8", "iso-8859-3", "Reclamación");
		$var208 = iconv("utf-8", "iso-8859-3", "perdón");
		$var209 = iconv("utf-8", "iso-8859-3", "condonación");
		$var210 = iconv("utf-8", "iso-8859-3", "previsión");
		$var211 = iconv("utf-8", "iso-8859-3", "omisión");
		$var212 = iconv("utf-8", "iso-8859-3", "perjudicará");
		$var213 = iconv("utf-8", "iso-8859-3", "prohibición");
		$var214 = iconv("utf-8", "iso-8859-3", "inválida");
		$var215 = iconv("utf-8", "iso-8859-3", "únicamente");
		$var216 = iconv("utf-8", "iso-8859-3", "intención");
		$var217 = iconv("utf-8", "iso-8859-3", "permanecerán");
		$var218 = iconv("utf-8", "iso-8859-3", "ilícita");
		$var219 = iconv("utf-8", "iso-8859-3", "demás");
		$var220 = iconv("utf-8", "iso-8859-3", "dirección");
		$var221 = iconv("utf-8", "iso-8859-3", "podrán");

		// Definir el nombre de cada mes segun su valor
		if ($mes_hoy == 1)
		{
			$nomb_mes = 'Enero';
		}
		if ($mes_hoy == 2)
		{
			$nomb_mes = 'Febrero';
		}
		if ($mes_hoy == 3)
		{
			$nomb_mes = 'Marzo';
		}
		if ($mes_hoy == 4)
		{
			$nomb_mes = 'Abril';
		}
		if ($mes_hoy == 5)
		{
			$nomb_mes = 'Mayo';
		}
		if ($mes_hoy == 6)
		{
			$nomb_mes = 'Junio';
		}
		if ($mes_hoy == 7)
		{
			$nomb_mes = 'Julio';
		}
		if ($mes_hoy == 8)
		{
			$nomb_mes = 'Agosto';
		}
		if ($mes_hoy == 9)
		{
			$nomb_mes = 'Septiembre';
		}
		if ($mes_hoy == 10)
		{
			$nomb_mes = 'Octubre';
		}
		if ($mes_hoy == 11)
		{
			$nomb_mes = 'Noviembre';
		}
		if ($mes_hoy == 12)
		{
			$nomb_mes = 'Diciembre';
		}

		// Definicion de variables segun parametros de metodo
		// Nombres
		if(!strlen($nomb_pers))
		{
			$nomb_soli = "";
		}
		else
		{
			$nomb_soli = $nomb_pers;
		}

		// Apellidos
		if(!strlen($apel_pers))
		{
			$apel_soli = "";
		}
		else
		{
			$apel_soli = $apel_pers;
		}

		// Fecha de nacimiento
		if(!strlen($fech_naci))
		{
			$naci_soli = "";
		}
		else
		{
			$naci_soli = $fech_naci;
		}

		// Numero de solicitud
		if(!strlen($soli_nume))
		{
			$soli_docu = "";
		}
		else
		{
			$soli_docu = $soli_nume;
		}

		// Tipo de documento
		if(!strlen($tipo_docu))
		{
			$tipo_iden = "";
		}
		else
		{
			$tipo_iden = $tipo_docu;
		}

		// Numero de documento
		if(!strlen($nume_docu))
		{
			$iden_docu = "";
		}
		else
		{
			$iden_docu = $nume_docu;
		}

		// Departamento de residencia
		if(!strlen($depa_resi))
		{
			$depa_docu = "";
		}
		else
		{
			$depa_docu = $depa_resi;
		}

		// Ciudad de residencia
		if(!strlen($ciud_resi))
		{
			$ciud_docu = "";
		}
		else
		{
			$ciud_docu = $ciud_resi;
		}

		// Zona de vinculación
		if(!strlen($zona_vinc))
		{
			$zona_docu = "";
		}
		else
		{
			$zona_docu = $zona_vinc;
		}

		// Centro poblado
		if(!strlen($cent_pobl))
		{
			$pobl_docu = "";
		}
		else
		{
			$pobl_docu = $cent_pobl;
		}

		// Barrio
		if(!strlen($barr_soli))
		{
			$barr_docu = "";
		}
		else
		{
			$barr_docu = $barr_soli;
		}

		// Direccion de residencia
		if(!strlen($dire_resi))
		{
			$dire_docu = "";
		}
		else
		{
			$dire_docu = $dire_resi;
		}

		// Direccion de entrega
		if(!strlen($dire_entr))
		{
			$entr_docu = "";
		}
		else
		{
			$entr_docu = $dire_entr;
		}

		// Celular
		if(!strlen($celu_soli))
		{
			$celu_docu = "";
		}
		else
		{
			$celu_docu = $celu_soli;
		}

		// numero de whatsapp
		if(!strlen($nume_what))
		{
			$what_docu = "";
		}
		else
		{
			$what_docu = $nume_what;
		}

		// Correo
		if(!strlen($corr_elec))
		{
			$corr_docu = "";
		}
		else
		{
			$corr_docu = $corr_elec;
		}

		// Fecha inicio de proceso
		if(!strlen($inic_proc))
		{
			$inic_docu = "";
		}
		else
		{
			$inic_docu = $inic_proc;
		}

		// Nombre referencia familiar
		if(!strlen($nomb_refe))
		{
			$refe_nomb = "";
		}
		else
		{
			$refe_nomb = $nomb_refe;
		}

		// Ciudad referencia familiar
		if(!strlen($ciud_refe))
		{
			$refe_ciud = "";
		}
		else
		{
			$refe_ciud = $ciud_refe;
		}

		// Celular referencia familiar
		if(!strlen($celu_refe))
		{
			$refe_celu = "";
		}
		else
		{
			$refe_celu = $celu_refe;
		}

		// Parentesco referencia familiar
		if(!strlen($pare_refe))
		{
			$refe_pare = "";
		}
		else
		{
			$refe_pare = $pare_refe;
		}

		// Nombre referencia personal
		if(!strlen($refe_pers))
		{
			$pers_refe = "";
		}
		else
		{
			$pers_refe = $refe_pers;
		}

		// Ciudad referencia personal
		if(!strlen($ciud_pers))
		{
			$pers_ciud = "";
		}
		else
		{
			$pers_ciud = $ciud_pers;
		}

		// Celular referencia personal
		if(!strlen($celu_pers))
		{
			$pers_celu = "";
		}
		else
		{
			$pers_celu = $celu_pers;
		}

		// Firma digital 1
		if(!strlen($firm_dig1))
		{
			$firm_unox = "firmUno.jpg";
		}
		else
		{
			$firm_unox = $firm_dig1;
		}

		// Firma digital 2
		if(!strlen($firm_dig2))
		{
			$firm_dosx = "firmDos.jpg";
		}
		else
		{
			$firm_dosx = $firm_dig2;
		}

		// Auyorizacion de aceptar terminos y condiciones
		if(!strlen($tran_star))
		{
			$star_date = "TRA_START_DATE";
		}
		else
		{
			$star_date = $tran_star;
		}

		// Codigo de referencia parte inferior de del documento
		if(!strlen($refe_codi))
		{
			$codi_refe = "";
		}
		else
		{
			$codi_refe = $refe_codi;
		}

		// Ciclo por recorrer para escribir en archivo PDF
		for ($cont_pagi=0; $cont_pagi<$tota_pagi; $cont_pagi++)
		{
			$pagina = $cont_pagi + 1;
			$pdf->addpage();
			$pdf->image($this->root . '/imagenes/HeaderAzzorti.jpg' , 0 ,0,216,0);
			$pdf->ln(10);

			if($pagina==1)
			{
				$pdf->SetFont("Gilroy_light", "", 11);
				$pdf->SetXY(3, 18);
				$pdf->Cell(0, 5, "$var0 1", 0, 1, "L");
				$pdf->ln(5);
				$pdf->image($this->root . '/imagenes/infoPersonal.jpg' , 0 ,25,216,0);
				$pdf->SetFont("Gilroy_light", "", 11);

				$pdf->SetXY(3, 35);
				$pdf->Cell(48, 6, "Nombres", 0,0, "L");

				$pdf->SetXY(3, 45);
				$pdf->SetFillColor(199);
				$pdf->RoundedRect(3, 43, 48, 9, 2, '1234','D');
				$pdf->CellFitSpace(50,7, "$nomb_soli",0, 0 , 'C' );

				$pdf->SetXY(53, 35);
				$pdf->Cell(58, 6, "Apellidos", 0,0, "L");

				$pdf->SetXY(53, 45);
				$pdf->RoundedRect(53, 43, 48, 9, 2, '1234','D');
				$pdf->CellFitSpace(45,6, "$apel_soli",0, 0 , 'C');

				$pdf->SetXY(103, 35);
				$pdf->Cell(48, 6, "Fecha de Nacimiento", 0,0, "L");
				$pdf->SetXY(103, 45);
				// $pdf->Cell(48, 6, "$naci_soli", 1,0, "C");
				$pdf->RoundedRect(103, 43, 48, 9, 2, '1234','D');
				$pdf->CellFitSpace(50,6, "$naci_soli",0, 0 , 'C');

				$pdf->SetXY(153, 35);
				$pdf->Cell(48, 6, "Solicitud $var8", 0,0, "L");
				$pdf->SetXY(153, 45);
				// $pdf->Cell(48, 6, "$soli_docu", 1,0, "C");
				$pdf->RoundedRect(153, 43, 48, 9, 2, '1234','D');
				$pdf->CellFitSpace(50,6, "$soli_docu",0, 0 , 'C');

				$pdf->SetXY(3, 55);
				$pdf->Cell(35, 6, "Tipo de documento", 0,0, "L");
				$pdf->SetXY(3, 65);
				// $pdf->Cell(35, 6, "$tipo_iden", 1,0, "C");
				$pdf->RoundedRect(3, 63, 35, 9, 2, '1234','D');
				$pdf->CellFitSpace(35,6, "$tipo_iden",0, 0 , 'C');

				$pdf->SetXY(40, 55);
				$pdf->Cell(41, 6, "$var8 de documento", 0,0, "L");
				$pdf->SetXY(40, 65);
				// $pdf->Cell(41, 6, "$iden_docu", 1,0, "C");
				$pdf->RoundedRect(40, 63, 40, 9, 2, '1234','D');
				$pdf->CellFitSpace(40,6, "$iden_docu",0, 0 , 'C');

				$pdf->SetXY(83, 55);
				$pdf->Cell(51, 6, "Departamento de residencia", 0,0, "L");
				$pdf->SetXY(83, 65);
				// $pdf->Cell(51, 6, "$depa_docu", 1,0, "C");
				$pdf->RoundedRect(83, 63, 50, 9, 2, '1234','D');
				$pdf->CellFitSpace(50,6, "$depa_docu",0, 0 , 'C');

				$pdf->SetXY(136, 55);
				$pdf->Cell(42, 6, "Ciudad de residencia", 0,0, "L");
				$pdf->SetXY(136, 65);
				// $pdf->Cell(42, 6, "$ciud_docu", 1,0, "C");
				$pdf->RoundedRect(136, 63, 40, 9, 2, '1234','D');
				$pdf->CellFitSpace(40,6, "$ciud_docu",0, 0 , 'C');

				$pdf->SetXY(180, 55);
				$pdf->Cell(31, 6, "Zona $var5", 0,0, "L");
				$pdf->SetXY(180, 65);
				// $pdf->Cell(31, 6, "$zona_docu", 1,0, "C");
				$pdf->RoundedRect(180, 63, 30, 9, 2, '1234','D');
				$pdf->CellFitSpace(30,6, "$zona_docu",0, 0 , 'C');

				$pdf->SetXY(3, 75);
				$pdf->Cell(48, 6, "Centro poblado", 0,0, "L");
				$pdf->SetXY(3, 85);
				// $pdf->Cell(48, 6, "$pobl_docu", 1,0, "C");
				$pdf->RoundedRect(3, 83, 48, 9, 2, '1234','D');
				$pdf->CellFitSpace(48,6, "$pobl_docu",0, 0 , 'C' );

				$pdf->SetXY(53, 75);
				$pdf->Cell(48, 6, "Barrio", 0,0, "L");
				$pdf->SetXY(53, 85);
				$pdf->RoundedRect(53, 83, 48, 9, 2, '1234','D');
				$pdf->CellFitSpace(48,6, "$barr_docu",0, 0 , 'C');

				$pdf->SetXY(103, 75);
				$pdf->Cell(48, 6, "$var6 de residencia", 0,0, "L");
				$pdf->SetXY(103, 85);
				$pdf->RoundedRect(103, 83, 50, 9, 2, '1234','D');
				$pdf->CellFitSpace(48,6, "$dire_docu",0, 0 , 'C');

				$pdf->SetXY(155, 75);
				$pdf->Cell(48, 6, "$var6 de entrega", 0,0, "L");
				$pdf->SetXY(155, 85);
				$pdf->RoundedRect(155, 83, 55, 9, 2, '1234','D');
				$pdf->CellFitSpace(48,6, "$entr_docu",0, 0 , 'C');

				$pdf->SetXY(3, 95);
				$pdf->Cell(48, 6, "Celular", 0,0, "L");
				$pdf->SetXY(3, 105);
				$pdf->RoundedRect(3, 103, 48, 9, 2, '1234','D');
				$pdf->CellFitSpace(48,6, "$celu_docu",0, 0 , 'C' );

				$pdf->SetXY(53, 95);
				$pdf->Cell(48, 6, "Numero de  WhastApp", 0,0, "L");
				$pdf->SetXY(53, 105);
				$pdf->RoundedRect(53, 103, 48, 9, 2, '1234','D');
				$pdf->CellFitSpace(48,6, "$what_docu",0, 0 , 'C');

				$pdf->SetXY(103, 95);
				$pdf->Cell(48, 6, "Correo $var7", 0,0, "L");
				$pdf->SetXY(103, 105);
				$pdf->RoundedRect(103, 103, 59, 9, 2, '1234','D');
				$pdf->CellFitSpace(48,6, "$corr_docu",0, 0 , 'C');

				$pdf->SetXY(163, 95);
				$pdf->Cell(48, 6, "Fecha inicio proceso", 0,0, "L");
				$pdf->SetXY(163, 105);
				// $pdf->Cell(48, 6, "$inic_docu", 1,0, "C");
				$pdf->RoundedRect(163, 103, 46, 9, 2, '1234','D');
				$pdf->CellFitSpace(48,6, "$inic_docu",0, 0 , 'C');

				$pdf->ln(2);
				$pdf->image($this->root . '/imagenes/Referencias.jpg' , 0 ,117,216,0);

				$pdf->SetXY(3, 126);
				$pdf->Cell(70, 6, "Nombres y Apellido Referencia Familiar", 0,0, "L");
				$pdf->SetXY(3, 136);
				$pdf->RoundedRect(3, 134, 70, 9, 2, '1234','D');
				$pdf->CellFitSpace(70,6, "$refe_nomb",0, 0 , 'C');

				$pdf->SetXY(75, 126);
				$pdf->Cell(40, 6, "Ciudad", 0,0, "L");
				$pdf->SetXY(75, 136);
				$pdf->RoundedRect(75, 134, 40, 9, 2, '1234','D');
				$pdf->CellFitSpace(40,6, "$refe_ciud",0, 0 , 'C');

				$pdf->SetXY(117, 126);
				$pdf->Cell(40, 6, "Celular", 0,0, "L");
				$pdf->SetXY(117, 136);
				$pdf->RoundedRect(117, 134, 40, 9, 2, '1234','D');
				$pdf->CellFitSpace(40,6, "$refe_celu",0, 0 , 'C');

				$pdf->SetXY(159, 126);
				$pdf->Cell(45, 6, "Parentesco", 0,0, "L");
				$pdf->SetXY(159, 136);
				$pdf->RoundedRect(159, 134, 45, 9, 2, '1234','D');
				$pdf->CellFitSpace(45,6, "$refe_pare",0, 0 , 'C');

				$pdf->SetXY(3, 146);
				$pdf->Cell(70, 6, "Nombres y Apellido Referencia Personal", 0,0, "L");
				$pdf->SetXY(3, 156);
				$pdf->RoundedRect(3, 154, 70, 9, 2, '1234','D');
				$pdf->CellFitSpace(70,6, "$pers_refe",0, 0 , 'C');

				$pdf->SetXY(75, 146);
				$pdf->Cell(40, 6, "Ciudad", 0,0, "L");
				$pdf->SetXY(75, 156);
				$pdf->RoundedRect(75, 154, 40, 9, 2, '1234','D');
				$pdf->CellFitSpace(40,6, "$pers_ciud",0, 0 , 'C');

				$pdf->SetXY(117, 146);
				$pdf->Cell(40, 6, "Celular", 0,0, "L");
				$pdf->SetXY(117, 156);
				$pdf->RoundedRect(117, 154, 45, 9, 2, '1234','D');
				$pdf->CellFitSpace(45,6, "$pers_celu",0, 0 , 'C');

				$pdf->image($this->root . '/imagenes/autorizacion.jpg' , 0 ,167,216,0);
				$pdf->ln(25);
				$pdf->SetXY(7, 175);
				// $pdf->image($this->root . '/imagenes/ley1266_.jpg' , 0 ,176,216,0);
				$pdf->Multicell(0, 5, "La asesora independiente otorga su consentimiento expreso e irrevocable a la $var1 Industrias Inca S.A.S. para:", 0, 'J');
				$pdf->ln(3);
				$pdf->SetXY(7, 187);
				$pdf->Multicell(0, 5, "1. Consultar en cualquier tiempo, en cualquier central de $var11 u operador de la $var11 (como Experian Colombia S.A, Transunion, y similares) toda la $var11 relevante para conocer su $var10 como deudor, su capacidad de pago o para valorar el riesgo de concederle un $var9.", 0, 'J');
				$pdf->SetXY(7, 203);
				$pdf->Multicell(0, 5, "2. Reportar a cualquier central de $var11 los datos referidos al cumplimiento e incumplimiento, si lo hubiere, de las obligaciones crediticias de la asesora independiente o de sus deberes legales de contenido patrimonial.", 0, 'J');
				$pdf->SetXY(7, 220);
				$pdf->Multicell(0, 5, "3. Conservar, tanto en la $var1 como en las centrales de $var11 u operador, la $var11 indicada en los puntos 1,2,3 con las debidas actualizaciones.", 0, 'J');
				$pdf->SetXY(7, 232);
				$pdf->Multicell(0, 5, "4. Suministrar a las centrales de $var11 arriba mencionadas o a otros operadores de $var11 similares datos relativos a las solicitudes de $var9, $var12 como otros atinentes a sus relaciones comerciales y financieras.", 0, 'J');
				$pdf->SetXY(7, 249);
				$pdf->Multicell(0, 5, "5. Revisar ante las centrales de $var11 los datos de contacto e $var13 ($var6, $var14, celular y correo $var7) suministrados a la $var1 para $var15 de los mismos, con la finalidad de evitar la $var16 de identidad permitiendo $var12 el adecuado desarrollo de la actividad comercial con Industrias Inca S.A.S", 0, 'J');
				$pdf->SetXY(7, 268);
				$pdf->Multicell(0, 5, "$codi_refe", 0, "R");
			}

			if($pagina==2)
			{
				$pdf->ln(10);
				$pdf->image($this->root . '/imagenes/autorizacion.jpg' , 0 ,20,216,0);
				$pdf->ln(25);

				$pdf->SetFont("Gilroy_light", "", 11);
				$pdf->SetXY(9, 30);
				$pdf->Multicell(0, 5, "Usted como Titular de los datos $var17 ejercer en cualquier momento el derecho de Habeas Data que implica la facultad de corroborar en cualquier tiempo ante la $var1 y las centrales de $var11 que los datos sean veraces, pertinentes, completos, actualizados, de calidad y exactos de conformidad con lo $var18 en la ley colombiana en materia de Habeas Data y a que, en caso de que no lo sea y que su $var19 sea fundada, se haga la $var20 correspondiente y se le informe de ello.", 0, 'J');
				$pdf->SetXY(9, 60);
				$pdf->Multicell(0, 5, "$var21. La asesora autoriza de manera clara, expresa, previa y voluntaria a la $var1 para consultar $var11 comercial del negocio, $var11 sobre el estado de sus obligaciones con la $var1 y que le comunique de forma previa el reporte de la $var11 negativa a las centrales de $var11 sobre el incumplimiento de sus obligaciones crediticias, a $var22 de mensajes enviados a su celular ($var23 SMS, whatsapp o llamada $var24 o mensaje de voz o texto) o al que suministre en el formulario de $var25, $var12 como a su $var6 de correo $var7 y/o a su $var6 personal que se encuentra registrada en los sistemas de la $var1 y por cualquier otro previsto en la Ley.", 0, 'J');
				$pdf->SetXY(9, 100);
				$pdf->SetFont("Gilroy_light", "B", 11);
				$pdf->Multicell(0, 5, "PARA $var26 $var78 CONSULTE LA $var28 DE TRATAMIENTO DE DATOS PERSONALES EN EL SIGUIENTE LINK: www.azzorti.co ", 0, 'J');
				$pdf->SetFont("Gilroy_light", "", 11);


				$pdf->SetTextColor(255,0,0);
				$pdf->SetXY(9, 113);
				$pdf->Cell(45, 6, "Esta $var66 de ley 1266 de 2008 (habeas data financiero) fue aceptada [$star_date]", 0,0, "L");

				$pdf->SetTextColor(0,0,0);
				$pdf->SetXY(9, 120);
				$pdf->Cell(45, 6, "Nombres", 0,0, "L");
				$pdf->SetXY(9, 127);
				// $pdf->Cell(45, 6, "$auto_docu", 1,0, "C");
				$pdf->RoundedRect(9, 126, 45, 9, 2, '1234','D');
				$pdf->CellFitSpace(45,7, "$nomb_soli",0, 0 , 'C');

				$pdf->SetXY(58, 120);
				$pdf->Cell(55, 6, "Apellidos", 0,0, "L");
				$pdf->SetXY(58, 127);
				// $pdf->Cell(55, 6, "$apel_docu", 1,0, "C");
				$pdf->RoundedRect(58, 126, 55, 9, 2, '1234','D');
				$pdf->CellFitSpace(55,7, "$apel_soli",0, 0 , 'C');

				$pdf->SetXY(117, 120);
				$pdf->Cell(45, 6, "Tipo de documento", 0,0, "L");
				$pdf->SetXY(117, 127);
				// $pdf->Cell(45, 6, "$tipo_auto", 1,0, "C");
				$pdf->RoundedRect(117, 126, 45, 9, 2, '1234','D');
				$pdf->CellFitSpace(45,7, "$tipo_iden",0, 0 , 'C');

				$pdf->SetXY(166, 120);
				$pdf->Cell(45, 6, "$var8 de documento", 0,0, "L");
				$pdf->SetXY(166, 127);
				// $pdf->Cell(45, 6, "$iden_auto", 1,0, "C");
				$pdf->RoundedRect(166, 126, 45, 9, 2, '1234','D');
				$pdf->CellFitSpace(45,7, "$iden_docu",0, 0 , 'C');

				$pdf->image($this->root . '/imagenes/datosPersonales.jpg' , 0 ,137,216,0);

				$pdf->SetXY(9, 144);
				$pdf->Multicell(0, 5, "Manifiesto que INDUSTRIAS INCA S.A.S, (en adelante AZZORTI y/o la $var1 y/o el RESPONSABLE), sociedad comercial, debidamente constituida en Colombia mediante Escritura $var30 $var8 1040, otorgada en la $var31 Primera de $var32 en fecha 27 de marzo de 1956 a la cual le corresponde el $var8 de $var13 Tributaria, NIT, 860.001.777-9, con domicilio Principal AUT. MEDELLIN KM 7.5 P. IND. CELTA BG 84-85 FUNZA, me ha informado, de manera expresa que es la entidad Responsable del Tratamiento de mis datos personales en mi calidad de ASESORA INDEPENDIENTE DE LA $var1, de conformidad con lo dispuesto por la Ley 1581 de 2012, el Decreto 1377 de 201, Ley 1266 de 2008 y de$var27 disposiciones concordantes y complementarias vigentes sobre la materia. ", 0, 'J');

				$pdf->SetXY(9, 186);
				$pdf->Multicell(0, 5, "Adicionalmente me ha informado que en su $var33 web www.azzorti.co se encuentra publicada la $var29 de Tratamiento de Datos Personales que cobija los datos de la Asesora Independiente y en la cual se informa que mis datos se $var34 en el marco de dicha $var29 y la Ley.", 0, 'J');

				$pdf->SetXY(9, 203);
				$pdf->SetFont("Gilroy_light", "B", 11);
				$pdf->Multicell(0, 5, "DATOS PERSONALES QUE RECOLECTA", 0, 'C');
				$pdf->SetFont("Gilroy_light", "", 11);

				$pdf->SetXY(9, 211);
				$pdf->Multicell(0, 5, "Datos $var35 (como nombre y $var2); Datos semiprivados (como $var6, correo $var7, celular, datos financieros para el pago); Datos sensibles (como datos relacionados con la salud, $var36 sexual y datos $var37, es decir, aquellos datos personales que permiten, a $var22 de $var38 $var39 para el efecto, medir y analizar una serie de $var40 $var41 que son $var42 en cada persona para poder comprobar su identidad, como por ejemplo, y sin limitares, a la huella dactilar, imagen, $var43 faciales, firma y voz).", 0, 'J');

				$pdf->SetXY(9, 245);
				$pdf->Multicell(0, 5, "Entiendo que dentro de los datos que se recolectan se encuentran datos sensibles, es decir, aquellos que afectan la intimidad del Titular y pueden dar lugar a que sea discriminado, como los datos $var37, por ejemplo la imagen facial y la huella. Este tipo de datos constituyen una $var44 especial de datos personales y requieren una $var45 reforzada.", 0, 'J');

				$pdf->SetXY(9, 265);
				$pdf->Multicell(0, 5, "$codi_refe", 0, "R");
			}

			if($pagina==3)
			{
				$pdf->ln(10);
				$pdf->image($this->root . '/imagenes/datosPersonales.jpg' , 0 ,20,216,0);
				$pdf->ln(25);
				$pdf->SetFont("Gilroy_light", "B", 11);
				$pdf->SetXY(9, 30);
				$pdf->Multicell(0, 5, "FINALIDADES DEL TRATAMIENTO", 0, 'C');
				$pdf->SetFont("Gilroy_light", "", 11);
				$pdf->SetXY(9, 39);
				$pdf->Multicell(0, 5, "Los datos recogidos de las asesoras independientes tienen como finalidad el desarrollo y $var46 integral de la $var47 comercial, y en consecuencia, el cumplimiento adecuado de las obligaciones de orden legal, contractual, judicial, administrativo, y las $var29s, instructivos, aplicativos y de$var27 decisiones de la $var48. Por lo tanto, me hacen $var49 las finalidades de Tratamiento mi $var11 personal la cual es o $var50 utilizada para efectos de:", 0, 'J');

				$pdf->SetXY(9, 68);
				$pdf->Multicell(0, 5, "a) El Vendedor recolecta de la Asesora Independiente $var11 de $var51 personal necesaria para el desarrollo de la $var47 comercial que se acuerda en el presente contrato con la finalidad general de desarrollar dicha $var47 comercial y con el $var52 de mantener el manejo y control de los procesos administrativos, comerciales y $var53 derivados de las ventas que $var54.", 0, 'J');

				$pdf->SetXY(9, 90);
				$pdf->Multicell(0, 5, "(b) Recolectar de cualquier fuente toda la $var11 que el Vendedor requiera sobre la Asesora Independiente, entre otras la necesaria para llenar la solicitud de $var9, el $var55 y de$var27 formas utilizadas por el Vendedor en sus bases de datos.", 0, 'J');

				$pdf->SetXY(9, 108);
				$pdf->Multicell(0, 5, "(c) Almacenar dicha $var11 en sus bases de datos.", 0, 'J');

				$pdf->SetXY(9, 115);
				$pdf->Multicell(0, 5, "(d) Utilizar dicha $var11 para asignarle clave de acceso a la $var33 Web del Vendedor.", 0, 'J');

				$pdf->SetXY(9, 122);
				$pdf->Multicell(0, 5, "(e) Poner en $var56 dicha $var11 para que sea verificada, consultada y/o utilizada con fines relacionados con la actividad del Vendedor, tanto por sus empleados en desarrollo de sus funciones como por terceras personas, incluyendo, transportistas, entidades financieras y centrales de riesgo.", 0, 'J');

				$pdf->SetXY(9, 139);
				$pdf->Multicell(0, 5, "(f) Reportar y/o consultar el incumplimiento de las obligaciones contractuales de la Asesora Independiente, frente al vendedor y/o frente a cualquier persona natural o $var57 de naturaleza $var30 o privada, a (o en) las centrales de riesgo.", 0, 'J');

				$pdf->SetXY(9, 156);
				$pdf->Multicell(0, 5, "(g) Enviarle comunicaciones (incluyendo todas aquellas establecidas en las leyes 1266 de 2008 y 1581 de 2012) a $var22 de correo $var58 directo, correo $var7 o llamada $var24 grabada o mensaje de texto o voz, a las direcciones y $var8s suministrados por la Asesora Independiente, incluida la $var59, previa al reporte a las centrales de riesgo del incumplimiento de las obligaciones de la Asesora Independiente.", 0, 'J');

				$pdf->SetXY(9, 182);
				$pdf->Multicell(0, 5, "(h) Que se le $var17 enviar mensajes $var23 $var14 al $var8 suministrado por la Asesora Independiente, o que consulte el mensaje de voz o texto que se $var60 al mismo $var8.", 0, 'J');

				$pdf->SetXY(9, 193);
				$pdf->Multicell(0, 5, "(i) Poner en $var56 dicha $var11 para que sea verificada, consultada y/o utilizada por los transportistas en la parte que sea necesaria para cumplir con la entrega de los pedidos.", 0, 'J');

				$pdf->SetXY(9, 203);
				$pdf->Multicell(0, 5, "(j) Poner en $var56 dicha $var11 para que sea verificada, consultada y/o utilizada por la $var1 que tenga contratada el Vendedor en la parte que sea necesaria para cumplir con el escaneo de los pedidos.", 0, 'J');

				$pdf->SetXY(9, 218);
				$pdf->Multicell(0, 5, "(k) Poner en $var56 dicha $var11 para que sea verificada, consultada y/o utilizada por las entidades financieras y/o entidades de recaudo en la parte que sea necesaria para recibir los pagos que $var61 la Asesora Independiente.", 0, 'J');

				$pdf->SetXY(9, 234);
				$pdf->Multicell(0, 5, "(l) Ser ubicada mediante $var62 y que los datos de contacto nombres, apellidos, $var6 y $var14 sean compartidos con clientes potenciales de los productos de AZZORTI.", 0, 'J');

				$pdf->SetXY(9, 245);
				$pdf->Multicell(0, 5, "(m) Que me sea enviada $var11 en medios $var41, de audio o digitales (correo $var7, mensajes de texto, WhatsApp, redes sociales, etc.) sobre los productos y las actividades comerciales, de cobranza y recaudos de AZZORTI.", 0, 'J');

				$pdf->SetXY(9, 265);
				$pdf->Multicell(0, 5, "$codi_refe", 0, "R");
			}

			if($pagina==4)
			{
				$pdf->ln(10);
				$pdf->image($this->root . '/imagenes/datosPersonales.jpg' , 0 ,20,216,0);
				$pdf->ln(25);
				$pdf->SetFont("Gilroy_light", "", 11);

				$pdf->SetXY(9, 30);
				$pdf->Multicell(0, 5, "(n) Para entregar $var11 a las autoridades competentes en ejercicio de sus funciones.", 0, 'J');

				$pdf->SetXY(9, 36);
				$pdf->Multicell(0, 5, "(o) En general, para el desarrollo integral de la $var47 comercial existente con la $var1.", 0, 'J');

				$pdf->SetFont("Gilroy_light", "B", 11);
				$pdf->SetXY(9, 45);
				$pdf->Multicell(0, 5, "TIPO DE TRATAMIENTO", 0, 'C');

				$pdf->SetFont("Gilroy_light", "", 11);
				$pdf->SetXY(9, 53);
				$pdf->Multicell(0, 5, "El Tratamiento de mis datos personales se $var54 manualmente o automatizado a $var22 de mecanismos digitales, en la nube, plataformas $var63, servidores externos o internos, y/o sistemas $var7s, o sistemas $var37, directamente por el Responsable o por medio de Encargados del Tratamiento.", 0, 'J');

				$pdf->SetFont("Gilroy_light", "B", 11);
				$pdf->SetXY(9, 73);
				$pdf->Multicell(0, 5, "DERECHOS DEL TITULAR ($var64 8 DE LEY 1581 DE 2012)", 0, 'C');
				$pdf->SetFont("Gilroy_light", "", 11);

				$pdf->SetXY(9, 80);
				$pdf->Multicell(0, 5, "a. Conocer y acceder a sus datos personales que hayan sido objeto de un tratamiento", 0, 'J');

				$pdf->SetXY(9, 85);
				$pdf->Multicell(0, 5, "b. Actualizar y rectificar a sus datos personales que hayan sido objeto de un tratamiento.", 0, 'J');

				$pdf->SetXY(9, 90);
				$pdf->Multicell(0, 5, "c. Suprimir la $var66 para el tratamiento de sus datos personales, cuando en el tratamiento de estos no se hayan respetado los principios establecidos en la Ley 1581 de 2012, siempre que no exista impedimento legal o contractual para hacerlo.", 0, 'J');

				$pdf->SetXY(9, 106);
				$pdf->Multicell(0, 5, "d. Solicitar prueba de la $var66 otorgada para el tratamiento de sus datos personales.", 0, 'J');

				$pdf->SetXY(9, 111);
				$pdf->Multicell(0, 5, "e. Abstenerse de responder las preguntas sobre datos sensibles o sobre datos de las $var67 y $var68 y adolescentes.", 0, 'J');

				$pdf->SetXY(9, 121);
				$pdf->Multicell(0, 5, "f. Presentar quejas por $var69 del $var70 de datos personales ante la autoridad competente, Superintendencia de Industria y Comercio (SIC).", 0, 'J');

				$pdf->SetFont("Gilroy_light", "U", 11);
				$pdf->SetXY(9, 135);
				$pdf->Multicell(0, 5, "Nota. Estos derechos $var221 ser ejercidos directamente por usted como Titular de los datos, su apoderado o su causahabiente, $var71 sea el caso.", 0, 'J');

				$pdf->SetFont("Gilroy_light", "B", 11);
				$pdf->SetXY(9, 150);
				$pdf->Multicell(0, 5, "COMPROMISO DE VERACIDAD Y $var72", 0, 'C');
				$pdf->SetFont("Gilroy_light", "", 11);

				$pdf->SetXY(9, 158);
				$pdf->Multicell(0, 5, "La Asesora independiente garantiza la veracidad y exactitud de la $var11 personal que proporciona al Vendedor y se compromete a mantenerla actualizada.", 0, 'J');

				$pdf->SetFont("Gilroy_light", "B", 11);
				$pdf->SetXY(9, 173);
				$pdf->Multicell(0, 5, "CANALES PARA EJERCER SUS DERECHOS", 0, 'C');
				$pdf->SetFont("Gilroy_light", "", 11);

				$pdf->SetXY(9, 180);
				$pdf->Multicell(0, 5, "La $var29 de Tratamiento se encuentra disponible en la $var33 web: www.azzorti.co.", 0, 'J');

				$pdf->SetXY(9, 188);
				$pdf->Multicell(0, 5, "Para la $var73 de requerimientos relacionados con el tratamiento de mis datos personales y el ejercicio de los derechos mencionados en esta $var66 la $var1 tiene dispuesto el canal habeas data correo: atencion.privacidad@azzorti.com.co , $var74 $var24 de servicio al cliente: 2906077a $var22 del cual se $var75 mis consultas quejas y reclamos en los plazos $var18s en la Ley 1581 del 2012 $var65 14 y 15 y de$var27 normas que lo complementan, aclaren, modifiquen o sustituyan, vigentes. ", 0, 'J');

				$pdf->SetFont("Gilroy_light", "B", 11);
				$pdf->SetXY(9, 220);
				$pdf->Multicell(0, 5, "$var79 DE TRATAMIENTO LUEGO DE HABER $var76 Y ENTENDIDO EL TEXTO", 0, 'C');
				$pdf->SetFont("Gilroy_light", "", 11);

				$pdf->SetXY(9, 228);
				$pdf->Multicell(0, 5, "Manifiesto que la presente $var66 me fue solicitada y puesta de presente antes de entregar mis datos y que la suscribo de forma libre y voluntaria una vez $var77 en su totalidad y para el cumplimiento de las finalidades arribas descritas para el RESPONSABLE: INDUSTRIAS INCA S.A.S. - AZZORTI", 0, 'J');

				$pdf->SetXY(9, 245);
				$pdf->Cell(45, 6, "Nombres", 0,0, "L");
				$pdf->SetXY(9, 255);
				// $pdf->Cell(45, 6, "$auto_docu", 1,0, "L");
				$pdf->RoundedRect(9, 254, 45, 9, 2, '1234','D');
				$pdf->CellFitSpace(45,7, "$nomb_soli",0, 0 , 'C');

				$pdf->SetXY(58, 245);
				$pdf->Cell(45, 6, "Apellidos", 0,0, "L");
				$pdf->SetXY(58, 255);
				// $pdf->Cell(45, 6, "$apel_docu", 1,0, "L");
				$pdf->RoundedRect(58, 254, 45, 9, 2, '1234','D');
				$pdf->CellFitSpace(45,7, "$apel_soli",0, 0 , 'C');

				$pdf->SetXY(107, 245);
				$pdf->Cell(45, 6, "Tipo de documento", 0,0, "L");
				$pdf->SetXY(107, 255);
				// $pdf->Cell(45, 6, "$tipo_auto", 1,0, "L");
				$pdf->RoundedRect(107, 254, 45, 9, 2, '1234','D');
				$pdf->CellFitSpace(45,7, "$tipo_iden",0, 0 , 'C');

				$pdf->SetXY(156, 245);
				$pdf->Cell(45, 6, "$var8 de documento", 0,0, "L");
				$pdf->SetXY(156, 255);
				// $pdf->Cell(45, 6, "$iden_auto", 1,0, "L");
				$pdf->RoundedRect(156, 254, 45, 9, 2, '1234','D');
				$pdf->CellFitSpace(45,7, "$iden_docu",0, 0 , 'C');

				$pdf->SetXY(156, 265);
				$pdf->Multicell(0, 5, "$codi_refe", 0, "R");
			}

			if($pagina==5)
			{
				$pdf->ln(10);
				$pdf->image($this->root . '/imagenes/compraventa.jpg' , 0 ,20,216,0);
				$pdf->ln(25);
				$pdf->SetFont("Gilroy_light", "", 11);

				$pdf->SetXY(9, 28);
				$pdf->Multicell(0, 5, "Entre los suscritos, Industrias Inca S.A.S, sociedad existente conforme a las leyes de la $var80 de Colombia, domiciliada en la ciudad de Funza, representada por Samuel Vainberg Bercovici, mayor de edad, identificado conforme aparece al pie de su firma, en su calidad de representante legal, quien se $var81 el Vendedor, de una parte y, de la otra, la persona identificada al final de este documento, mayor de edad, identificada conforme aparece al pie de su firma; quien se $var81 la 'Asesora Independiente' y conjuntamente con el Vendedor, las 'Partes'; han decidido celebrar el presente contrato de compraventa (el 'Contrato'), previas las siguientes. CONSIDERACIONES: * Que el Vendedor es una $var1 dedicada a la $var82 y $var83 de diferentes productos en el mercado colombiano. * Que la Asesora Independiente es una persona que puede llevar a cabo de forma $var84 e independiente la $var83 de los productos que adquiera del Vendedor. * Que la Asesora Independiente declara conocer y aceptar el $var85, el Plan de Asesora Independiente, la $var29 de $var9, la $var29 de $var86 y la $var29 de Tratamiento de Datos Personales del Vendedor. En consecuencia, las Partes acuerdan: Primera. Definiciones: Para efectos de este Contrato se establecen las siguientes definiciones que $var87 el significado que a $var88 se indica: Asesora Independiente: Tiene el significado que se le asigna en el encabezado de este Contrato, siendo la persona que, en virtud de este Contrato, compra los Productos del Vendedor, exclusivamente para su posterior reventa a terceros o a consumidores finales. De conformidad con el numeral 11 del $var89 5 de la ley 1480 de 2011, la Asesora Independiente ostenta la calidad de proveedor frente a sus clientes o consumidores finales. Asesora Independiente Activa: Es la persona natural o $var57 que se encuentra activa en la base de datos del Vendedor y que realiza cierto $var8 de compras en los tiempos y en las condiciones definidas en el Plan de Asesora Independiente. La calidad de Asesora Independiente Activa se $var90 $var71 el Plan de Asesora Independiente del Vendedor. Consumidor: Toda persona natural o $var57 que, como destinatario final, adquiera, disfrute o utilice un determinado producto, cualquiera que sea su naturaleza para la $var91 de una necesidad propia, privada, familiar o $var92 y empresGilroy_light, cuando no $var93 ligada $var94 a su actividad $var95. Se $var96 incluido en el concepto de Consumidor el de usuario. Para los efectos del presente Contrato, los clientes de la Asesora Independiente ostentan la calidad de Consumidores. La Asesora Independiente reconoce y acepta que, en $var97 de la $var47 comercial que pacta en este Contrato y la finalidad mercantil para la cual adquiere los productos, no es consumidora respecto de los Productos que adquiere en virtud y bajo las condiciones de ese Contrato. $var98: $var99 de ventas determinado por el Vendedor, libremente y de tiempo en tiempo, que establece un portafolio de productos, ofertas y unas promociones o planes de incentivos aplicables en dicho periodo. $var85: $var100 impresa o virtual preparada por el Vendedor dirigida a las Asesoras Independientes, para uso exclusivo de $var101 como material comercial de referencia, en el que aparecen todos los productos o servicios ofrecidos por el Vendedor disponibles para la venta en una determinada $var98 y los precios sugeridos para su reventa a terceros. Escala de Descuentos para Reventa: Tabla, elaborada por el Vendedor, en la que se establecen los porcentajes de descuento para reventa ofrecidos exclusivamente a Asesoras Independientes para cada $var74 de producto que se ofrece en el $var85, y los descuentos adicionales que tienen algunas $var74s de acuerdo con el monto total del pedido de productos que adquiera la Asesora Independiente para su reventa a terceros o consumidores finales. La Escala de Descuentos para Reventa $var17 ser modificada libremente y de manera unilateral por el Vendedor y hace parte del Plan de Asesora Independiente. Plan de Asesora Independiente: Es el conjunto de beneficios que el Vendedor le reconoce a la Asesora Independiente por el nivel de compra de productos o servicios que le hace la Asesora Independiente al Vendedor. El Plan de Asesora Independiente $var17 ser modificado de tiempo en tiempo por el Vendedor sin necesidad de $var66 previa de la Asesora Independiente y se encuentra publicado en la $var33 web del Vendedor. $var29 de $var86: Es el documento que contiene, entre otros, el alcance y los aspectos $var102 de las $var86s de los Productos, el cual se encuentra publicado en la $var33 web del Vendedor.", 0, "J");

				$pdf->SetXY(9, 265);
				$pdf->Multicell(0, 5, "$codi_refe", 0, "R");
			}

			if($pagina==6)
			{
				$pdf->ln(10);
				$pdf->image($this->root . '/imagenes/compraventa.jpg' , 0 ,20,216,0);
				$pdf->ln(25);
				$pdf->SetFont("Gilroy_light", "", 11);
				$pdf->SetXY(9, 28);
				$pdf->Multicell(0, 5, "$var29 de $var9: Es el documento que regula los $var103 y condiciones del cupo de $var9 que el Vendedor, por mera liberalidad, le puede otorgar a la Asesora Independiente, el cual se encuentra publicado en la $var33 web del Vendedor. $var29 de Tratamiento de Datos Personales: Es el documento que regula el almacenamiento y tratamiento de los datos personales de la Asesora Independiente, el cual se encuentra publicado en la $var33 web del Vendedor. Producto: Tiene el significado que se le asigna en el literal (a) de la $var104 segunda de este Contrato. Segunda. Objeto: El presente Contrato tiene por objeto: * Regular los $var103 de la venta de los productos y servicios del Vendedor a la Asesora Independiente, bajo las marcas o signos distintivos del Vendedor (los 'Productos'). Los Productos $var105 listados en el $var85, el cual $var50 modificado para cada $var98. * Que la Asesora Independiente adquiera los Productos del Vendedor exclusivamente para la reventa en Colombia, por cuenta y riesgo de la Asesora Independiente. Tercera. $var106 y vigencia: El presente Contrato se $var107 por $var108 indefinido, pero $var17 terminarse por cualquiera de las causales descritas en la $var104 $var109 cuarta de este Contrato. Cuarta. No exclusividad: El Vendedor no concede exclusividad a la Asesora Independiente en Colombia, $var110 el Vendedor el derecho de comercializar o distribuir los Productos por si mismo o por medio de otros terceros. Quinta. $var111 de compra: La solicitud de los Productos $var112 ser realizada por medio de $var111 de compra emitidas por la Asesora Independiente al Vendedor $var113, $var114 o virtualmente a $var22 de la $var33 web del Vendedor, cumpliendo con los siguientes requisitos: * Las $var111 de compra de los Productos $var112n emitirse por una $var115 $var116 y en las fechas establecidas de conformidad con las $var29s internas del Vendedor, la cual $var17 variar cuando el Vendedor lo estime conveniente y sin que medie $var66 por parte de la Asesora Independiente. En caso que la orden de compra sea inferior a la $var115 $var116, el Vendedor $var17 decidir libremente si vende o no los Productos a la Asesora Independiente. * La simple $var117 de la orden de compra por parte del Vendedor no supone que $var118 se obligue al despacho, en la medida que las ventas de Productos a la Asesora Independiente $var119 sujetas a la disponibilidad de inventario o a las $var120 internas del Vendedor, quien $var17 declinar o rechazar tales $var111 de compra. * El Vendedor $var129 factura por cada una de las $var111 de compra realizadas por la Asesora Independiente, las cuales $var121 remitidas y canceladas conforme se establece en la $var104 sexta. $var21 Primero. Los Productos solicitados $var121 suministrados de acuerdo a los plazos de entrega que determine el Vendedor y dentro de los $var122 del cupo de $var9 que el Vendedor le haya concedido a la Asesora Independiente (de ser el caso). $var21 Segundo. En caso que la orden de compra sea virtual, la Asesora Independiente declara que conoce y que acepta la $var29 de acceso y uso de la $var33 web del Vendedor y se acoge a ella. Sexta. Condiciones de compra: Las siguientes son las condiciones de compra de los Productos: * Por la compra de los Productos, la Asesora Independiente $var123 el precio de acuerdo a: (i) la cantidad solicitada en cada orden de compra, (ii) el $var85 y (iii) la Escala de Descuentos para Reventa vigente al momento de la orden de compra.* La Asesora Independiente $var112 pagar el precio de los Productos dentro del plazo establecido en la factura. * En desarrollo del presente Contrato, el Vendedor $var17, a su arbitrio, otorgarle o quitarle el cupo de $var9 a la Asesora Independiente. El cupo de $var9 en $var136 caso $var137 desembolso de dinero efectivo en favor de la Asesora Independiente, pues se trata de una $var138 del precio de los productos que adquiere la Asesora Independiente exclusivamente para la reventa. * Como $var139 del pago de sus obligaciones $var140 derivadas de este Contrato o del cupo de $var9 que el Vendedor otorgue a la Asesora Independiente exclusivamente para el pago del precio de los Productos que adquiera para reventa, la Asesora Independiente, mediante el presente Contrato, otorga a favor del Vendedor un $var55 con espacios dejados en blanco junto con su respectiva carta de instrucciones. $var21 Primero. La Asesora Independiente reconoce, acepta y declara, que los Productos que adquiera en desarrollo de este Contrato, los adquiere exclusivamente para su reventa a terceros o consumidores finales y que, por lo mismo, los plazos para pagos, los cupos de $var9 o descuentos que obtenga del ", 0, "J");

				$pdf->SetXY(9, 268);
				$pdf->Multicell(0, 5, "$codi_refe", 0, "R");
			}

			if($pagina==7)
			{
				$pdf->ln(10);
				$pdf->image($this->root . '/imagenes/compraventa.jpg' , 0 ,20,216,0);
				$pdf->ln(25);
				$pdf->SetFont("Gilroy_light", "", 11);
				$pdf->SetXY(9, 28);
				$pdf->Multicell(0, 5, "Vendedor, y de$var27 derechos contractuales pactados en este Contrato, se le otorgan a la Asesora Independiente $var141 y exclusivamente en virtud de una $var47 mercantil y no de consumo. $var21 Segundo. La Asesora Independiente reconoce, acepta y declara, que el cupo de $var9 que solicite y/o le sea concedido por el Vendedor, no es un $var9 de consumo, sino un cupo de $var9 otorgable exclusivamente para que la Asesora Independiente pueda diferir, en el plazo que le sea conferido, el pago de los Productos que le compre al Vendedor para su reventa a terceros, en desarrollo de sus actividades $var140 independientes como Asesora Independiente y de conformidad con lo establecido en este Contrato. $var21 Tercero. La Asesora Independiente acepta y declara, que los $var103 y condiciones del cupo de $var9 se encuentran regulados en la $var29 de $var9 del Vendedor. Dentro de esos $var103 y condiciones se encuentran los gastos administrativos de cobranza que $var142 ser asumidos y cancelados por la Asesora Independiente de conformidad con lo descrito en la $var29 de $var9 del Vendedor. $var21 Cuarto. La Asesora Independiente acepta que, de tiempo en tiempo y de manera unilateral, el Vendedor pueda modificar el Plan de Asesora Independiente, el $var85 y las $var143. $var21 Quinto. Independientemente del sitio donde se encuentre ubicada la Asesora Independiente y del mecanismo por medio del cual se expida la orden de compra, la venta correspondiente se $var96 perfeccionada en el municipio de Funza (Cundinamarca), domicilio principal del Vendedor y donde se definen y concretan los elementos de la venta de Productos como son precio, forma de pago, condiciones de despacho y entrega de Productos. $var21 Sexto. La Asesora Independiente acepta que el transporte, la $var124 y manejo para el $var125 y la entrega de los Productos hasta la $var6 de entrega, $var221 ser realizados por el Vendedor de forma directa o contratados con terceros en nombre y $var126 de la Asesora, $var71 lo estime $var27 conveniente el Vendedor. En consecuencia, la Asesora Independiente da poder al Vendedor para que este: (i) Celebre en su nombre el contrato de transporte o de suministro transporte de los Productos hasta la $var6 de entrega, con la empresa de transporte que elija, (ii) Emita todos los documentos necesarios para perfeccionar el contrato y para precisar las condiciones del servicio de transporte de cada uno de los $var125s que deba realizar y, (iii) Celebre en su nombre los contratos necesarios de $var124 y manejo de los Productos con la empresa que elija. $var127. Precio de Reventa: El precio de reventa de los Productos $var50 fijado libremente por la Asesora Independiente, en observancia de las condiciones de mercado. No obstante, el Vendedor $var17, mediante el $var85 o por cualquier otro medio, comunicar a la Asesora Independiente y al $var128 en general el precio de reventa sugerido de los Productos. Octava. Obligaciones del Vendedor: * El Vendedor se obliga a entregar los Productos en el domicilio de la Asesora Independiente o en la $var6 que para el efecto indique la Asesora Independiente en la respectiva solicitud de $var9. * Suministrar los Productos y la factura comercial de acuerdo con las respectivas $var111 de compra. El Vendedor $var129 factura por cada orden de compra. * El Vendedor $var130 la responsabilidad que pudiere surgir por la calidad de los Productos de conformidad con la ley 1480 de 2011. * El Vendedor $var131 la $var11 que considere necesaria para que la Asesora Independiente conozca, de forma suficiente, los Productos y el manejo de los mismos conforme a los $var119dares de calidad requeridos para conservar sus propiedades y cualidades. Novena. Obligaciones de la Asesora Independiente: * Pagar el precio de los Productos, en los plazos y condiciones acordadas y recibir los Productos de conformidad con lo pactado. * Pagar al Vendedor el valor del transporte, $var124 y manejo para el $var125 y la entrega de los Productos hasta la $var6 de entrega, ya sea porque el Vendedor le $var132 dichos servicios a nombre propio o porque los $var132 por mandato de la empresa transportadora de conformidad con el poder conferido por la Asesora Independiente en el $var21 sexto de la $var104 Sexta del presente contrato. * Asumir todos los riesgos de $var133 o $var135 de los Productos desde el momento en que $var134 hayan sido entregados $var71 lo acordado. * Atender las peticiones razonables de $var11 que realice el Vendedor. * Mantener indemne y libre de cualquier tipo de $var135 al Vendedor, en virtud de reclamaciones de terceros, judiciales o extrajudiciales, originadas en virtud de hechos dolosos o negligentes de la Asesora Independiente. * Asumir de manera directa los riesgos propios de la $var46 ", 0, "J");

				$pdf->SetXY(9, 268);
				$pdf->Multicell(0, 5, "$codi_refe", 0, "R");
			}

			if($pagina==8)
			{
				$pdf->ln(10);
				$pdf->image($this->root . '/imagenes/compraventa.jpg' , 0 ,20,216,0);
				$pdf->ln(25);
				$pdf->SetFont("Gilroy_light", "", 11);
				$pdf->SetXY(9, 28);
				$pdf->Multicell(0, 5, "del objeto del Contrato. * Firmar los documentos comerciales que le expida el Vendedor y que correspondan a Productos efectivamente entregados, en $var144 de que acepta la $var145 de pagarlos. Si quien recibe el pedido en la $var6 indicada por la Asesora Independiente es una persona distinta de la Asesora Independiente, se $var96 que $var146 persona $var147 autorizada por la Asesora Independiente para recibir, verificar y dejar constancia de su conformidad o inconformidad con la entrega de los Productos. * Actualizar sus datos de contacto e informarle inmediatamente al Vendedor cualquier $var148 de los mismos, en especial de los siguientes: $var6 de residencia, $var6 de entrega de pedido, $var149 $var150 fijo, $var149 de $var14 celular y correo $var7. En caso que la Asesora Independiente no informe dichos cambios al Vendedor, se $var151 como vigente, para las comunicaciones que deba enviarle el Vendedor, la $var11 original que el Vendedor tenga en sus bases de datos. * En caso que el Vendedor le haya asignado un cupo de $var9, pagar los Productos en los $var103 establecidos por el Vendedor en la factura correspondiente. En todo caso, la Asesora Independiente declara y acepta que el Vendedor $var17 cancelar el cupo de $var9 cuando lo estime conveniente. * Pagar los materiales de $var152 o Kit de $var152 que para el efecto haya $var153 el Vendedor. * Las de$var27 que se deriven de este Contrato y de la ley. $var21 Primero. La Asesora Independiente acepta que el Vendedor $var17 enviar y facturar muestras de Productos. En caso de que la Asesora Independiente no desee las muestras enviadas, $var12 lo $var154 al Vendedor, el que $var155 la $var156 y $var157 la $var158 realizada. Pasados veinte (20) $var4 calendario de recibidas las muestras sin que la Asesora Independiente manifieste su rechazo y las entregue efectivamente al Vendedor, se $var96 que $var146 las acepta irrevocablemente y $var112 pagarlas dentro de los plazos establecidos por el Vendedor en sus facturas. $var21 Segundo. Las Partes convienen que cada entrega de los Productos constituye una venta en firme, y que, en consecuencia, el Vendedor no $var155 $var156 alguna, salvo en los casos de Productos con defectos de $var82 advertidos por la Asesora Independiente y notificados por $var146 al Vendedor tal como lo establece la $var29 de $var86 del Vendedor, documento que forma parte integral de este Contrato, o por errores de despacho del Vendedor. $var21 Tercero. Las Partes acuerdan que ante el incumplimiento de cualquiera de las obligaciones descritas en esta $var104, el Vendedor $var17 declarar como exigibles y vencidas todas las obligaciones que bajo el presente Contrato, o por $var159 de las Partes, $var160 sometidas a plazo, y todas las de$var27 sumas pagaderas conforme al mismo, aspecto que no $var161 de $var162 de demanda, protesto u otro aviso posterior o requerimiento de cualquier naturaleza, a todo lo cual renuncia expresamente la Asesora Independiente. $var163. Riesgos y responsabilidades: La Asesora Independiente es directamente responsable por llevar a cabo la reventa y $var164 de los Productos a nombre y por cuenta propia, haciendo uso de sus propios medios y adoptando siempre todas las medidas necesarias y suficientes para distinguirse frente a terceros como un empresario independiente. En el evento de cualquier $var133 de los Productos por cualquier causa, dicha $var133 $var165 por cuenta de la Asesora Independiente como propietaria de los mismos. $var163 Primera. $var86s: Los $var103 y condiciones de las $var166 de los Productos se encuentran en la $var29 de $var86 que la Asesora Independiente declara conocer y aceptar. $var163 Segunda. Marcas: La Asesora Independiente reconoce que el Vendedor es propietario o licenciatario exclusivo de las marcas o signos distintivos usados para identificar los Productos, $var12 como de las denominaciones comerciales, logos, etiquetas y $var167 relacionados con los mismos. Cualquier uso no autorizado de la propiedad intelectual relacionada con los Productos o del Vendedor, por parte de la Asesora Independiente, constituye un incumplimiento grave del presente Contrato. $var163 Tercera. Publicidad: El Vendedor $var168 todos los programas y formas de publicidad a nivel nacional e internacional con arbitrio exclusivo sobre los conceptos creativos, materiales y medios de $var59 utilizados en dichos programas y la $var169 y $var170 de los mismos. $var163 Cuarta. $var171: El Contrato $var172 $var173 por: (a) mutuo acuerdo; (b) incumplimiento de cualquiera de las Partes no subsanado en cinco (5) $var4 $var174 contados a partir de la fecha en que la Parte afectada requiere por escrito a la Parte incumplida;", 0, "J");

				$pdf->SetXY(9, 268);
				$pdf->Multicell(0, 5, "$codi_refe", 0, "R");
			}

			if($pagina==9)
			{
				$pdf->ln(10);
				$pdf->image($this->root . '/imagenes/compraventa.jpg' , 0 ,20,216,0);
				$pdf->ln(25);
				$pdf->SetFont("Gilroy_light", "", 11);
				$pdf->SetXY(9, 28);
				$pdf->Multicell(0, 5, "(c) de manera unilateral por cualquiera de las Partes mediante aviso escrito dado a la otra Parte; (d) ante la ocurrencia de un evento de fuerza mayor o caso fortuito en los $var103 del $var89 64 del $var175 Civil que imposibilite cumplir este Contrato en un plazo igual o superior a dos (2) meses consecutivos; (e) por la apertura de $var176 penal a cualquiera de las Partes, (f) cuando la Asesora Independiente pierda su calidad de Asesora Independiente Activa, $var71 el Plan de Asesora Independiente del Vendedor, y (g) por la inexactitud o falta de veracidad de las declaraciones consagradas en la $var104 $var177 cuarta de este Contrato. $var21. La Asesora Independiente declara y acepta expresamente que no $var151 derecho alguno a $var178, $var156, resarcimiento o reconocimiento $var179 de ninguna clase, a la $var180 del presente Contrato, alegando posicionamiento de los Productos o apertura del mercado. $var163 Quinta. Contratista independiente: Las Partes declaran que no existe $var181 laboral entre ellas ni entre el personal de cada una de ellas. Cada una de las Partes dispone de $var182 y libertad $var183, administrativa, directiva para la $var46 del presente Contrato y $var121 las $var184 responsables del personal que requieran para su $var46, lo cual $var185 en su propio nombre, por su cuenta y riesgo, sin que ninguna adquiera responsabilidad por tales actos o contratos ejecutados por la otra Parte. $var21. La Asesora Independiente declara y acepta expresamente que bajo ninguna circunstancia $var17 asumir funciones propias de la agencia comercial, conforme se define en el $var175 de Comercio de Colombia, y se abs$var151 en todo momento de actuar en nombre y $var126 del Vendedor. $var163 Sexta. $var186: La Asesora Independiente no $var17 ceder su $var187 en el Contrato, sin el consentimiento previo y escrito del Vendedor. El Vendedor $var17 hacerlo libremente sin necesidad de $var66 de la Asesora Independiente. $var163 $var127. Notificaciones: Salvo aquellas comunicaciones que $var71 $var118 Contrato $var17 hacer el Vendedor en su $var33 web o en cualquier otro medio virtual elegido por el Vendedor, las comunicaciones a las Partes en $var47 con este Contrato se $var188: * Al Vendedor: Industrias Inca S.A.S. Destinatario: Departamento de Ventas $var6:Carrera 68 B # 10 A-97 $var32, D.C. $var189: 446 42 64 Email: indinca@AZZORTI.com.co * A la Asesora Independiente: $var71 los datos registrados al pie de su firma. $var21 Primero. La Asesora Independiente se obliga a dar acuse de recibo de manera inmediata a cualquiera de las comunicaciones enviadas por el Vendedor. $var21 Segundo. La oficina abierta al $var128 del Vendedor $var147 ubicada en la Carrera 68 B # 10 A-97 de la ciudad de $var32. $var163 Octava. Mecanismo de $var190 de controversias: En el evento que surja una diferencia entre las Partes originada en el presente Contrato, $var146s se $var191 para tratar de solucionarla de manera directa. Si vencido el $var108 de treinta (30) $var4 contados a partir de la fecha de la $var59 en la que una de las Partes comunica a la otra Parte la existencia de la diferencia y la convoca en una fecha cierta para solucionarla y dicha $var192 no se lleva a cabo o no se ponen de acuerdo en el plazo indicado, las Partes $var121 libres de acudir a la $var193 ordinaria a solucionar dichas diferencias. $var163 Novena. Soporte $var58 del Contrato: Las Partes convienen que transcurridos diez (10) $var194 desde la fecha de $var195 de este Contrato sin que la Asesora Independiente $var61 alguna compra al Vendedor, $var118 $var17 destruir el soporte $var58 del presente Contrato y de todos los documentos asociados al mismo, entre ellos, el $var55. $var196. $var197 DE CONFIDENCIALIDAD Y ADECUADO TRATAMIENTO DE DATOS PERSONALES La $var11 personal de los asesores independientes, de los clientes y en general, la $var11 relativa a todos los datos de personas naturales a las que tenga acceso la asesora independiente y que conozca con $var198 de la $var47 comercial que tiene con INDUSTRIAS INCA S.A.S. AZZORTI y que sea requerida para el desarrollo del contrato suscrito entre las partes o que conozca con $var198 a las mismas, $var112 ser tratada de acuerdo con los $var119dares establecidos por el la $var199, es decir, de acuerdo con la $var29 de Tratamiento de Datos Personales de INDUSTRIAS INCA S.A.S. AZZORTI y de conformidad con el $var200 de $var201 de Datos Personales, Ley 1581 de 2012, Decreto 1377 de 2013, Ley 1266 de 2008, $var12 como todas las normas que la desarrollen, complementen y adicionen. LA ASESORA INDEPENDIENTE $var112 conocer y cumplir a cabalidad las disposiciones contenidas en la $var29 de Datos Personales desarrollada por INDUSTRIAS INCA S.A.S. $var202 para el Tratamiento de la $var11 personal de la ", 0, "J");

				$pdf->SetXY(9, 268);
				$pdf->Multicell(0, 5, "$codi_refe", 0, "R");
			}

			if($pagina==10)
			{
				$pdf->ln(10);
				$pdf->image($this->root . '/imagenes/compraventa.jpg' , 0 ,20,216,0);
				$pdf->ln(25);
				$pdf->SetFont("Gilroy_light", "", 11);
				$pdf->SetXY(9, 28);
				$pdf->Multicell(0, 5, "que tenga conocimiento en desarrollo o con $var198 del contrato que se suscribe. Los datos personales de asesores, clientes y cualquier persona y en general cualquier dato de persona natural que a los que acceda el asesor en cumplimiento del contrato o que conozca con $var198 de la $var47 comercial, $var112n ser tratados exclusivamente para el cumplimiento del contrato suscrito con INDUSTRIAS INCA S.A.S. $var202. En consecuencia, La Asesora $var112 abstenerse de realizar directamente, o a $var22 de terceros, un uso diferente de la $var11 personal de los clientes o persona natural en general. LA ASESORA $var112 abstenerse de comunicar a terceros o usar en su provecho o en el de terceros, la $var11 que por $var97 de su cargo haya podido conocer, la cual es de naturaleza reservada por lo que es consciente de $var118 deber de confidencialidad y que la $var203 puede ocasionar perjuicios a INDUSTRIAS INCA S.A.S. AZZORTI. LA ASESORA se obliga a tratar con especial cuidado la $var11 que tenga el $var51 sensible como la relacionada con la salud de las personas, la $var11 de menores o los datos $var37, los cuales tienen $var45 especial en la $var204 vigente y por ende, requieren de especial diligencia y cuidado en su tratamiento. La $var11 personal recabada de los asesores, clientes o de cualquier persona natural $var112 ser administrada, almacenada y conservada bajo condiciones de seguridad que eviten su consulta o acceso por parte de terceros no autorizados. LA ASESORA tiene un deber de custodia respecto a la $var11 que le ha sido encomendada por lo que $var112 informar oportunamente a INDUSTRIAS INCA S.A.S. AZZORTI acerca de cualquier $var205 que atente o represente una amenaza para la confidencialidad, seguridad e integridad de la $var11 personal de los clientes. En consecuencia, LA ASESORA inmediatamente tenga conocimiento de la brecha de seguridad, tiene el deber informar a la $var199 al correo atencion.privacidad@AZZORTI.com.co para que $var146 junto pueda atender el incidente de seguridad, $var71 los lineamientos establecidos por la $var199 para el efecto. La $var206 a alguna de las obligaciones relativas al tratamiento de la $var11 durante la vigencia del contrato $var50 considerada justa causa de $var180 del contrato Sin embargo, en el evento en que el incumplimiento a alguna de las obligaciones respecto del tratamiento de la $var11 ocurra, o sea conocido, de manera posterior a la $var180 del $var181 contractual, INDUSTRIAS INCA S.A.S $var202 $var17 iniciar las acciones legales y administrativas a las que haya lugar. $var196 Primera. No $var207: Ninguna renuncia de una Parte a reclamar el incumplimiento de una $var145 derivada de este Contrato, $var50 considerada como un $var208 o $var209 futura o permanente de dicha $var145 o de cualquier otra $var210 de este Contrato. La demora u $var211 de una Parte en el ejercicio de un derecho, en manera alguna $var212 el ejercicio de ese derecho en el futuro. $var196 Segunda. Estipulaciones Independientes: Si cualquier $var108, $var210, $var145 o $var213 de este Contrato es considerada o declarada $var214, nula o inejecutable, tal dis$var187 $var50 modificada por las Partes $var215 en la medida en que sea necesario para hacerla ejecutable en forma consistente con la $var216 de las Partes y los de$var27 $var103, previsiones, obligaciones o prohibiciones de este Contrato $var217 con pleno vigor y efecto. $var196 Tercera. Declaraciones: La Asesora Independiente declara que: (a) los recursos y dineros destinados para el pago de sus obligaciones crediticias y/o de los pedidos realizados al Vendedor, $var12 como las ganancias y/o cualquier recurso que derive de su actividad comercial, no provienen directa o indirectamente (ni $var121 destinados para) lavado de activos, $var138 del terrorismo ni ninguna otra actividad $var218; (b) no pertenece a $var136 grupo terrorista o al margen de la ley, y (c) no se encuentra reportada en ninguna lista internacional expedida por el Consejo de Seguridad de las Naciones Unidas ni en las de$var27 que tengan $var51 vinculante para Colombia por actividades relacionadas con lavados de activos y/o $var138 del terrorismo. $var21. La Asesora Independiente conoce y acepta que las declaraciones consagradas en esta $var104 son ciertas tanto al momento de la firma de este Contrato como durante toda su vigencia.", 0, "J");

				// $pdf->Multicell(0, 5, "ANEXO", 0, "J");
				// $pdf->SetXY(9, 190);
				// $pdf->Multicell(0, 5, "$var79 PARA EL TRATAMIENTO DE DATOS PERSONALES", 0, "J");
				// $pdf->SetFont("Gilroy_light", "", 11);
				// $pdf->SetXY(9, 198);
				// $pdf->Multicell(0, 5, "Manifiesto que INDUSTRIAS INCA S.A.S, (en adelante 'AZZORTI' y/o la '$var48' y/o el 'RESPONSABLE'), sociedad comercial, debidamente constituida en Colombia mediante Escritura $var30 $var149 1040, otorgada en la $var31 Primera de $var32 en fecha 27 de Marzo de 1956 a la cual le corresponde el $var8 de $var13 Tributaria, NIT, 860.001.777-9, con domicilio Principal AUT. MEDELLIN KM 7.5 P. IND. CELTA BG 84-85 FUNZA, me ha informado, de manera expresa que es la entidad Responsable del Tratamiento de mis datos personales en mi calidad de ASESORA INDEPENDIENTE DE LA $var48, de conformidad con lo dispuesto por la Ley 1581 de 2012, el Decreto 1377 de 201, Ley 1266 de 2008 y $var219 disposiciones concordantes y complementarias vigentes sobre la materia.", 0, "J");
				// $pdf->SetXY(9, 240);
				// $pdf->Multicell(0, 5, "Adicionalmente me ha informado que en su $var33 web www.AZZORTI.co se encuentra publicada la $var29 de Tratamiento de Datos Personales que cobija los datos de la Asesora Independiente y en la cual se informa que mis datos se $var34 en el marco de dicha $var29 y la Ley.", 0, "J");

				$pdf->SetXY(9, 265);
				$pdf->Multicell(0, 5, "$codi_refe", 0, "R");
			}

			if($pagina==11)
			{
				$pdf->ln(10);
				$pdf->image($this->root . '/imagenes/compraventa.jpg' , 0 ,20,216,0);
				$pdf->ln(25);
				$pdf->SetFont("Gilroy_light", "B", 11);
				$pdf->SetXY(9, 28);
				$pdf->Multicell(0, 5, "ANEXO", 0, "J");
				$pdf->SetXY(9, 35);
				$pdf->Multicell(0, 5, "$var79 PARA EL TRATAMIENTO DE DATOS PERSONALES", 0, "J");

				$pdf->SetFont("Gilroy_light", "", 11);
				$pdf->SetXY(9, 43);
				$pdf->Multicell(0, 5, "Manifiesto que INDUSTRIAS INCA S.A.S, (en adelante 'AZZORTI' y/o la '$var48' y/o el 'RESPONSABLE'), sociedad comercial, debidamente constituida en Colombia mediante Escritura $var30 $var149 1040, otorgada en la $var31 Primera de $var32 en fecha 27 de Marzo de 1956 a la cual le corresponde el $var8 de $var13 Tributaria, NIT, 860.001.777-9, con domicilio Principal AUT. MEDELLIN KM 7.5 P. IND. CELTA BG 84-85 FUNZA, me ha informado, de manera expresa que es la entidad Responsable del Tratamiento de mis datos personales en mi calidad de ASESORA INDEPENDIENTE DE LA $var48, de conformidad con lo dispuesto por la Ley 1581 de 2012, el Decreto 1377 de 201, Ley 1266 de 2008 y $var219 disposiciones concordantes y complementarias vigentes sobre la materia.", 0, "J");
				$pdf->SetXY(9, 84);
				$pdf->Multicell(0, 5, "Adicionalmente me ha informado que en su $var33 web www.AZZORTI.co se encuentra publicada la $var29 de Tratamiento de Datos Personales que cobija los datos de la Asesora Independiente y en la cual se informa que mis datos se $var34 en el marco de dicha $var29 y la Ley.", 0, "J");

				$pdf->SetXY(9, 100);
				$pdf->Multicell(0, 5, "DATOS PERSONALES QUE RECOLECTA", 0, "L");

				$pdf->SetFont("Gilroy_light", "", 11);
				$pdf->SetXY(9, 106);
				$pdf->Multicell(0, 5, "Datos $var35 (como nombre y $var2); Datos semiprivados (como $var6, correo $var7, celular, datos financieros para el pago); Datos sensibles (como datos relacionados con la salud, $var36 sexual y datos $var37, es decir, aquellos datos personales que permiten, a $var22 de $var38 $var39 para el efecto, medir y analizar una serie de $var40 $var41 que son $var42 en cada persona para poder comprobar su identidad, como por ejemplo, y sin limitares, a la huella dactilar, imagen, $var43 faciales, firma y voz).", 0, 'J');

				$pdf->SetXY(9, 137);
				$pdf->Multicell(0, 5, "Entiendo que dentro de los datos que se recolectan se encuentran datos sensibles, es decir, aquellos que afectan la intimidad del Titular y pueden dar lugar a que sea discriminado, como los datos $var37, por ejemplo la imagen facial y la huella. Este tipo de datos constituyen una $var44 especial de datos personales y requieren una $var45 reforzada.", 0, 'J');

				$pdf->SetFont("Gilroy_light", "B", 11);
				$pdf->SetXY(9, 159);
				$pdf->Multicell(0, 5, "FINALIDADES DEL TRATAMIENTO", 0, "L");

				$pdf->SetFont("Gilroy_light", "", 11);
				$pdf->SetXY(9, 165);
				$pdf->Multicell(0, 5, "Los datos recogidos de las asesoras independientes tienen como finalidad el desarrollo y $var46 integral de la $var47 comercial, y en consecuencia, el cumplimiento adecuado de las obligaciones de orden legal, contractual, judicial, administrativo, y las $var29s, instructivos, aplicativos y de$var27 decisiones de la $var48. Por lo tanto, me hacen $var49 las finalidades de Tratamiento mi $var11 personal la cual es o $var50 utilizada para efectos de:", 0, 'J');

				$pdf->SetXY(9, 191);
				$pdf->Multicell(0, 5, "a) El Vendedor recolecta de la Asesora Independiente $var11 de $var51 personal necesaria para el desarrollo de la $var47 comercial que se acuerda en el presente contrato con la finalidad general de desarrollar dicha $var47 comercial y con el $var52 de mantener el manejo y control de los procesos administrativos, comerciales y $var53 derivados de las ventas que $var54.", 0, 'J');

				$pdf->SetXY(9, 212);
				$pdf->Multicell(0, 5, "(b) Recolectar de cualquier fuente toda la $var11 que el Vendedor requiera sobre la Asesora Independiente, entre otras la necesaria para llenar la solicitud de $var9, el $var55 y de$var27 formas utilizadas por el Vendedor en sus bases de datos.", 0, 'J');

				$pdf->SetXY(9, 228);
				$pdf->Multicell(0, 5, "(c) Almacenar dicha $var11 en sus bases de datos.", 0, 'J');

				$pdf->SetXY(9, 234);
				$pdf->Multicell(0, 5, "(d) Utilizar dicha $var11 para asignarle clave de acceso a la $var33 Web del Vendedor.", 0, 'J');

				$pdf->SetXY(9, 240);
				$pdf->Multicell(0, 5, "(e) Poner en $var56 dicha $var11 para que sea verificada, consultada y/o utilizada con fines relacionados con la actividad del Vendedor, tanto por sus empleados en desarrollo de sus funciones como por terceras personas, incluyendo, transportistas, entidades financieras y centrales de riesgo.", 0, 'J');

				$pdf->SetXY(9, 255);
				$pdf->Multicell(0, 5, "(f) Reportar y/o consultar el incumplimiento de las obligaciones contractuales de la Asesora Independiente, frente al vendedor y/o frente a cualquier persona natural o $var57 de naturaleza $var30 o privada, a (o en) las centrales de riesgo.", 0, 'J');

				$pdf->SetXY(9, 265);
				$pdf->Multicell(0, 5, "$codi_refe", 0, "R");
			}

			if($pagina==12)
			{
				$pdf->ln(10);
				$pdf->image($this->root . '/imagenes/compraventa.jpg' , 0 ,20,216,0);
				$pdf->ln(25);
				$pdf->SetFont("Gilroy_light", "", 11);
				$pdf->SetXY(9, 28);
				$pdf->Multicell(0, 5, "(g) Enviarle comunicaciones (incluyendo todas aquellas establecidas en las leyes 1266 de 2008 y 1581 de 2012) a $var22 de correo $var58 directo, correo $var7 o llamada $var24 grabada o mensaje de texto o voz, a las direcciones y $var8s suministrados por la Asesora Independiente, incluida la $var59, previa al reporte a las centrales de riesgo del incumplimiento de las obligaciones de la Asesora Independiente.", 0, 'J');

				$pdf->SetXY(9, 54);
				$pdf->Multicell(0, 5, "(h) Que se le $var17 enviar mensajes $var23 $var14 al $var8 suministrado por la Asesora Independiente, o que consulte el mensaje de voz o texto que se $var60 al mismo $var8.", 0, 'J');

				$pdf->SetXY(9, 65);
				$pdf->Multicell(0, 5, "(i) Poner en $var56 dicha $var11 para que sea verificada, consultada y/o utilizada por los transportistas en la parte que sea necesaria para cumplir con la entrega de los pedidos.", 0, 'J');

				$pdf->SetXY(9, 76);
				$pdf->Multicell(0, 5, "(j) Poner en $var56 dicha $var11 para que sea verificada, consultada y/o utilizada por la $var1 que tenga contratada el Vendedor en la parte que sea necesaria para cumplir con el escaneo de los pedidos.", 0, 'J');

				$pdf->SetXY(9, 92);
				$pdf->Multicell(0, 5, "(k) Poner en $var56 dicha $var11 para que sea verificada, consultada y/o utilizada por las entidades financieras y/o entidades de recaudo en la parte que sea necesaria para recibir los pagos que $var61 la Asesora Independiente.", 0, 'J');

				$pdf->SetXY(9, 108);
				$pdf->Multicell(0, 5, "(l) Ser ubicada mediante $var62 y que los datos de contacto nombres, apellidos, $var6 y $var14 sean compartidos con clientes potenciales de los productos de AZZORTI.", 0, 'J');

				$pdf->SetXY(9, 119);
				$pdf->Multicell(0, 5, "(m) Que me sea enviada $var11 en medios $var41, de audio o digitales (correo $var7, mensajes de texto, WhatsApp, redes sociales, etc.) sobre los productos y las actividades comerciales, de cobranza y recaudos de AZZORTI.", 0, 'J');

				$pdf->SetXY(9, 135);
				$pdf->Multicell(0, 5, "(n) Para entregar $var11 a las autoridades competentes en ejercicio de sus funciones.", 0, 'J');

				$pdf->SetXY(9, 141);
				$pdf->Multicell(0, 5, "(o) En general, para el desarrollo integral de la $var47 comercial existente con la $var1.", 0, 'J');

				$pdf->SetFont("Gilroy_light", "B", 11);
				$pdf->SetXY(9, 150);
				$pdf->Multicell(0, 5, "TIPO DE TRATAMIENTO", 0, 'L');

				$pdf->SetFont("Gilroy_light", "", 11);
				$pdf->SetXY(9, 158);
				$pdf->Multicell(0, 5, "El Tratamiento de mis datos personales se $var54 manualmente o automatizado a $var22 de mecanismos digitales, en la nube, plataformas $var63, servidores externos o internos, y/o sistemas $var7s, o sistemas $var37, directamente por el Responsable o por medio de Encargados del Tratamiento.", 0, 'J');

				$pdf->SetFont("Gilroy_light", "B", 11);
				$pdf->SetXY(9, 175);
				$pdf->Multicell(0, 5, "DERECHOS DEL TITULAR ($var64 8 DE LEY 1581 DE 2012)", 0, 'L');

				$pdf->SetFont("Gilroy_light", "", 11);
				$pdf->SetXY(9, 182);
				$pdf->Multicell(0, 5, "Conocer y acceder a sus datos personales que hayan sido objeto de un tratamiento", 0, 'J');

				$pdf->SetXY(9, 188);
				$pdf->Multicell(0, 5, "Actualizar y rectificar a sus datos personales que hayan sido objeto de un tratamiento.", 0, 'J');

				$pdf->SetXY(9, 194);
				$pdf->Multicell(0, 5, "Suprimir la $var66 para el tratamiento de sus datos personales, cuando en el tratamiento de estos no se hayan respetado los principios establecidos en la Ley 1581 de 2012, siempre que no exista impedimento legal o contractual para hacerlo.", 0, 'J');

				$pdf->SetXY(9, 210);
				$pdf->Multicell(0, 5, "Solicitar prueba de la $var66 otorgada para el tratamiento de sus datos personales.", 0, 'J');

				$pdf->SetXY(9, 217);
				$pdf->Multicell(0, 5, "Abstenerse de responder las preguntas sobre datos sensibles o sobre datos de las $var67 y $var68 y adolescentes.", 0, 'J');

				$pdf->SetXY(9, 228);
				$pdf->Multicell(0, 5, "Presentar quejas por $var69 del $var70 de datos personales ante la autoridad competente, Superintendencia de Industria y Comercio (SIC).", 0, 'J');

				$pdf->SetFont("Gilroy_light", "U", 11);
				$pdf->SetXY(9, 240);
				$pdf->Multicell(0, 5, "Nota. Estos derechos $var221 ser ejercidos directamente por usted como Titular de los datos, su apoderado o su causahabiente, $var71 sea el caso.", 0, 'J');

				$pdf->SetXY(9, 265);
				$pdf->Multicell(0, 5, "$codi_refe", 0, "R");
			}

			if($pagina==13)
			{
				$pdf->SetFont("Gilroy_light", "B", 11);
				$pdf->SetXY(9, 20);
				$pdf->Multicell(0, 5, "COMPROMISO DE VERACIDAD Y $var72", 0, 'L');

				$pdf->SetFont("Gilroy_light", "", 11);
				$pdf->SetXY(9, 28);
				$pdf->Multicell(0, 5, "La Asesora independiente garantiza la veracidad y exactitud de la $var11 personal que proporciona al Vendedor y se compromete a mantenerla actualizada.", 0, 'J');

				$pdf->SetFont("Gilroy_light", "B", 11);
				$pdf->SetXY(9, 40);
				$pdf->Multicell(0, 5, "CANALES PARA EJERCER SUS DERECHOS", 0, 'L');

				$pdf->SetFont("Gilroy_light", "", 11);
				$pdf->SetXY(9, 48);
				$pdf->Multicell(0, 5, "La $var29 de Tratamiento se encuentra disponible en la $var33 web: www.azzorti.co.", 0, 'J');

				$pdf->SetXY(9, 55);
				$pdf->Multicell(0, 5, "Para la $var73 de requerimientos relacionados con el tratamiento de mis datos personales y el ejercicio de los derechos mencionados en esta $var66 la $var1 tiene dispuesto el canal habeas data correo: atencion.privacidad@azzorti.com.co , $var74 $var24 de servicio al cliente: 2906077a $var22 del cual se $var75 mis consultas quejas y reclamos en los plazos $var18s en la Ley 1581 del 2012 $var65 14 y 15 y de$var27 normas que lo complementan, aclaren, modifiquen o sustituyan, vigentes. ", 0, 'J');

				$pdf->SetFont("Gilroy_light", "B", 11);
				$pdf->SetXY(9, 84);
				$pdf->Multicell(0, 5, "$var79 DE TRATAMIENTO LUEGO DE HABER $var76 Y ENTENDIDO EL TEXTO", 0, 'J');

				$pdf->SetFont("Gilroy_light", "", 11);
				$pdf->SetXY(9, 92);
				$pdf->Multicell(0, 5, "Manifiesto que la presente $var66 me fue solicitada y puesta de presente antes de entregar mis datos y que la suscribo de forma libre y voluntaria una vez $var77 en su totalidad y para el cumplimiento de las finalidades arribas descritas para el RESPONSABLE: INDUSTRIAS INCA S.A.S. - AZZORTI", 0, 'J');

				$pdf->SetFont("Gilroy_light", "", 11);
				$pdf->SetXY(9, 115);
				$pdf->Multicell(0, 5, "Por Azzorti,", 0, 'L');

				$pdf->ln(10);
				// $pdf->image($this->root . '/imagenes/'.$firm_unox.'', 9 ,125,83,0);

				$pdf->SetXY(122, 115);
				$pdf->Multicell(0, 5, "Por la asesora independiente,", 0, 'L');

				$pdf->ln(10);
				// $pdf->image($this->root . '/imagenes/'.$firm_dosx.'', 120 ,125,80,50);

				$pdf->SetXY(9, 265);
				$pdf->Multicell(0, 5, "$codi_refe", 0, "R");
			}
			$pdf->SetFont("Gilroy_light", "B", 7);
			$pdf->ln(2);
		}
    $pdf->output($ruta);
    $urlx_fina = "https://".$_SERVER["SERVER_NAME"].URL_TEMPORALES.$nomb_arch;
    return $urlx_fina;
  }
}
?>