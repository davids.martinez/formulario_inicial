<?php

namespace Api\Controllers;

include_once(DIRECTORIO . "/Controllers/Controller.php");
include_once(DIRECTORIO . "/Models/Localizacion.php");
include_once(DIRECTORIO . "/Models/Asesora.php");
include_once(DIRECTORIO . "/Models/Experian.php");

use Api\Controllers\Controller;
use Api\Models\Localizacion;
use Api\Models\Asesora;
use Api\Models\Experian;

/**
 * @author Steven Martinez <davids.martinez@azzorti.co>
 */

class ExperianController extends Controller
{
    /**
     * Modelo de la asesora
     *
     * @var Asesora
     */
    private $asesora;
    /**
     * Modelo de localizacion
     *
     * @var Localizacion
     */
    private $local;
    /**
     * MOdelo de experian
     *
     * @var Experian
     */
    private $experian;
    public function __construct(){
        $this->asesora = new Asesora;
        $this->local = new Localizacion;
        $this->experian = new Experian;
    }
    /**
     * Flujo inicial de validacion de identidad experian POST
     *
     * @param Array $request
     * @return void
     */
    public function post_vali_celu($request){
        /* Validaciones */
        $nume_iden = $request["nume_iden"];
        $this->vali_cart($nume_iden);
        $this->veri_vali_iden($nume_iden);

        /* servicio */
        $respuesta = $this->experian->validaIdentidad(
            $request["celu_terc"],
            $request["fech_expe"],
            $request["nume_iden"],
            $request["apel_terc"]);

        switch ($respuesta[5]){
            case "error_general":
                $this->resp_error($respuesta[1]);
                break;
            case "error_validacion":
                $this->resp_error($respuesta[3][0]);
                break;
            case "error_apellido":
                $this->resp_error($respuesta[1][0]);
                break;
            case "error":
                $this->resp_error($respuesta[1][0]);
                break;
            case "flujo_otp":
                $datos = array(
                    "proceso" => "flujo_otp",
                    "datos" => $this->experian->data_audi["nume_tran"]
                );
                $this->experian->inse_audi();
                $this->respuesta(200,"Proceso validación OTP",$datos);
                break;
            case "flujo_pregunta":
                $datos = array(
                    "proceso" => "preguntas_seguridad",
                    "preguntas" => $respuesta[4]
                );
                $this->experian->inse_audi();
                $this->respuesta(200,"Proceso validación de preguntas",$datos);
                break;
            default:
                $this->respuesta(403, "Respuesta no definida por el servidor");
        }
    }
    /**
     * Envia preguntas de seguridad al servicio para validacion
     *
     * @param Array $request
     * @return void
     */
    public function envi_preg($request){
        $request["nume_iden"] = trim ($request["nume_iden"]);

        $xml = $this->arma_preg($request["preguntas"]);
        $this->experian->data_audi["nume_iden"] = $request["nume_iden"];

        $respuesta = $this->experian
            ->validaPreguntas($xml,$request["transaccion"]);
        switch ($respuesta[5]){
            case "error_general":
                $this->resp_error($respuesta[1]);
                break;
            case "error_validacion":
                $this->resp_error($respuesta[3][0]);
                break;
            case "error_apellido":
                $this->resp_error($respuesta[1][0]);
                break;
            case "error":
                $this->resp_error($respuesta[1][0]);
                break;
            case "validacion_exitosa":
                $resp_string = (String) $respuesta[3][0];
                $this->experian->data_audi["respuesta"] = $resp_string;

                $this->experian->inse_audi();

                $dato_unio = $this->experian->get_nomb_apel_dire($request["nume_iden"]);

                $this->respuesta(200,(String) $resp_string, $dato_unio[0]);
                break;
            default:
                $this->respuesta(403, "Respuesta no definida por el servidor");
        }
    }
    /**
     * Confirma el celular ingresado por el usuario
     *
     * @param Array $request
     * @return void
     */
    public function post_conf_celu($request){
        $this->experian->data_audi["nume_iden"] = $request["nume_iden"];
        $respuesta= $this->experian->validaTelefono(
            $request["indi_tran"],
            $request["nume_celu"]);

        switch($respuesta[5]){
            case "error_validacion":
                $this->resp_error($respuesta[3][0]);
                break;
            case "error_general":
                $this->resp_error($respuesta[1]);
                break;
            case "error":
                $this->resp_error($respuesta[1][0]);
                break;
            case "valida_otp":
                $this->experian->data_audi["respuesta"] = $request["nume_celu"];

                $this->experian->inse_audi();
                $this->respuesta(200,"Celular verificado");
                break;
            default:
                $this->respuesta(403, "Respuesta no definida por el servidor");
        }

    }
    /**
     * Confirma el pin enviado al celular del usuario
     *
     * @param Array $request
     * @return void
     */
    public function confirma_pin($request){
            $this->experian
                ->data_audi["nume_iden"] = $request["nume_iden"];
            $respuesta = $this->experian
                ->validaOTP($request["indi_tran"],
                            $request["codi_otp"]);

            switch($respuesta[5]){
                case "error_validacion":
                    $this->resp_error($respuesta[3][0],$respuesta[5]);
                    //$this->respuesta(403,(string)$respuesta[3][0]);
                    break;
                case "error_general":
                    $this->resp_error($respuesta[1],$respuesta[5]);
                    //$this->respuesta(403,(string)$respuesta[1]);
                    break;
                case "error":
                    $this->resp_error($respuesta[1][0],$respuesta[5]);
                    //$this->respuesta(403,(string)$respuesta[1][0]);
                    break;
                case "validacion_exitosa":
                    $res = (String) $respuesta[3][0];
                    $this->experian->data_audi["respuesta"] = $res;

                    $this->experian->inse_audi();
                    $this->respuesta(200,$res);
                    break;
                case "flujo_otp":
                    $nume_tran = $this->experian->data_audi["nume_tran"];
                    $this->respuesta(201, "Por favor confirme nuevamente los datos",$nume_tran);
                    break;
                default:
                    $this->respuesta(403, "Respuesta no definida por el servidor");
            }
    }
    /**
     * Genera respuesta al cliente e inserta datos de auditoria
     *
     * @param string $respuesta mensaje a enviar al cliente
     * @param mixed $datos datos adicionales a enviar
     * @return void
     */
    private function resp_error($respuesta,$datos=null){
        $resp_string = (String) $respuesta;
        $this->experian->data_audi["respuesta"] = $resp_string;

        $this->experian->inse_audi();
        if($datos === null){
            $this->respuesta(403,$resp_string);
        }else{
            $this->respuesta(403,$resp_string,$datos);
        }
    }
    /**
     * Valida si la asesora existe o no en la base de datos
     *
     * @param Array $request
     * @return void
     */
    public function vali_ases($request){
        $nume_iden= isset($request["nume_iden"]) ? $request["nume_iden"] : "";
        if(strlen($nume_iden) > 0){
            $ases = $this->asesora->vali_ases($nume_iden);
            $ases_proc = $this->asesora->vali_ases_proc($nume_iden);
            if(!empty($ases)){
                $this->respuesta(201,"La asesora con documento $nume_iden esta activa.");
            }
            if(!empty($ases_proc)){
                $this->respuesta(201,"La solicitud de la asesora se encuentra en proceso.");
            }
        }

        $this->respuesta(200,"listo");

    }
    /**
     * Obtiene lista de ciudades
     *
     * @return void
     */
    public function get_ciud()
    {
        $ciud = $this->local->get_ciud();
        $this->respuesta(200,"listo",$ciud);
    }
    /**
     * Obtiene tipos de documento
     *
     * @return void
     */
    public function get_tipo_docu(){
        $tipo_docu = $this->asesora->get_tipo_docu();
        $this->respuesta(200,"listo",$tipo_docu);
    }
    /**
     * Genera el esquema xml de las preguntas enviadas por el usuario para
     * enviar al servicio
     *
     * @param Array $preguntas
     * @return void
     */
    private function arma_preg($preguntas){

        $xml = "";
        foreach($preguntas as $pregunta){
            $atributo = $pregunta["@attributes"];
            $cabeceraXml= "&lt;Question Id='{$atributo["Id"]}' QuestionId='{$atributo["QuestionId"]}' Text='{$atributo['Text']}' Type='{$atributo['Type']}' IsDummy='{$atributo['IsDummy']}' isQuestionTextHasListTypeDataElement='{$atributo['isQuestionTextHasListTypeDataElement']}' ListAnswerIndex='{$atributo['ListAnswerIndex']}' ExpirationTime='{$atributo['ExpirationTime']}' TimeTakenToAnswer='{$atributo['TimeTakenToAnswer']}'&gt;";
            $pieXml = "&lt;/Question&gt;";
            $respuestaXml  = "";
            foreach ($pregunta["Answer"] as $index => $respuesta) {
                $index_pregunta = (int) $pregunta["respuesta"];
                $seleccionado = $index_pregunta === $index ? "true" : "false";
                $respuesta = str_replace("&", "&amp;amp;", $respuesta);
                $respuestaXml = $respuestaXml . "&lt;Answer IsSelected='{$seleccionado}' CorrectAnswer='false'&gt;{$respuesta}&lt;/Answer&gt;";
            }
            $xml = $xml . $cabeceraXml . $respuestaXml . $pieXml;
        }
        //caracteres especiales
        $xmlFina = "
        &lt;ExamDetails xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' Title='Object Mapping' Language='es-CO' MatchKeyId='0000' ErrorGeneratingQA='false' TimeTakenToAnswer=''&gt;
            &lt;Questions&gt;
                $xml
            &lt;/Questions&gt;
        &lt;/ExamDetails&gt;
        ";
        //print_r($xml);
        return $xmlFina;
    }
    /**
     * Validaciones campos formulario
     *
     * @param Array $request
     * @return void
     */
    public function validaciones($request){
        switch ($request["tipo"]){
            case "email":
                $email = $this->asesora->vali_emai($request["email"]);
                if($email){
                    $this->respuesta(400,
                        "El correo {$request["email"]} ya existe por favor ingrese uno diferente."
                    );
                }
                break;
            case "nume_iden":
                $nume_iden = $this->asesora->vali_nume_iden($request["nume_iden"]);
                if($nume_iden){
                    $this->respuesta(400,
                        "El numero de identifiación {$request["nume_iden"]} ya se encuentra en proceso."
                    );
                }
                break;
            case "nume_celu":
                $nume_celu = $this->asesora->vali_celu($request["nume_celu"]);
                if($nume_celu){
                    $this->respuesta(400,
                        "El numero telefonico celular: {$request["nume_celu"]} ya existe por favor verifique."
                    );
                }
                break;
        }
        $this->respuesta(200, "{$request["tipo"]} listo");
    }
    /**
     * Validaciones de saldo cartera
     *
     * @param [type] $nume_iden
     * @return void
     */
    private function vali_cart($nume_iden){
        $nume_iden=trim($nume_iden);
        $this->experian->sald_cart($nume_iden);
        if (!empty($sald_cart)) {
            $mensaje = "Pareciera que tienes un saldo
                de cartera con la compañia si tienes mas dudas por favor dirigete
                a  servicioalcliente@dupree.com.co o comunicate al 571 290-6077";
            $this->respuesta(403,$mensaje);

        }
    }
    /**
     * Valida si ya se realizo validación en el servicio transunion
     *
     * @param int $nume_iden
     * @return void
     */
    private function veri_vali_iden($nume_iden){
        $vali_iden = $this->experian->veri_vali_iden($nume_iden);
        $regi = (int)$vali_iden[0]["cantidad"];

        if($regi != 0){
            $dato_unio = $this->experian->get_nomb_apel_dire($nume_iden);
            $this->respuesta(202,"La identidad ya se valido anteriormente",$dato_unio[0]);
        }
    }
    /**
     * Valida si existe lider
     *
     * @param Array $request
     * @return void
     */
    public function vali_lide($request){
        $vali = $this->asesora->vali_lide($request["nume_lide"]);
        if($vali){
            $this->respuesta(400,"No se encuentra lider");
        }
    }
}
