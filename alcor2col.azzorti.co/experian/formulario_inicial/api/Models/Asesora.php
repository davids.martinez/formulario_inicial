<?php namespace Api\Models;
    require_once(DIRECTORIO . '/../conexion/conexion.php');

    use Conexion;

    class Asesora{
        /**
         * Conexion vase de datos informix.
         *
         * @var Conexion
         */
        private $conexion;
        /**
         * Inicializa la conexion
         */
        public function __construct()
        {
            $this->conexion = new Conexion();
        }
        /**
         * Optiene los tipos de documentos
         *
         * @return Array
         */
        public function get_tipo_docu(){
            $getConexion= $this->conexion->getConexion();
            $query = "select
                        cons_docu,
                        nomb_docu
                    from
                        tipo_docu
                    where
                        cons_docu in ('3', '5')
                    order by
                        cons_docu asc;";
            $arre_docu = $getConexion->ejecutar_consulta($query);

            return $arre_docu;
        }
        /**
         * Valida si la asesora esta activa
         *
         * @param int $nume_iden
         * @return Array
         */
        public function vali_ases($nume_iden){
            $getConexion = $this->conexion->getConexion();
            $query="
                select
                    nume_iden
                from
                    tab_clie
                where
                    nume_iden = '$nume_iden'
                    and esta_acti <> 'INAC';";
            $arre_ases = $getConexion->ejecutar_consulta($query);
            return $arre_ases;
        }
        /**
         * Valida si la asesora se encuentra en proceso
         *
         * @param int $nume_iden
         * @return Array
         */
        public function vali_ases_proc($nume_iden){
            $getConexion = $this->conexion->getConexion();
            $query="
                select
                    nume_iden
                from
                    proc_refe
                where
                    nume_iden = '$nume_iden'";
            $arre_ases = $getConexion->ejecutar_consulta($query);
            return $arre_ases;
        }
        /**
         * Valida si ya esta registrado un email
         *
         * @param string $emai
         * @return bool
         */
        public function vali_emai($emai){
            $emai=trim($emai);
            $getConexion = $this->conexion->getConexion();
            $query = "
                select
                    count(*) as cantidad
                from
                    pre_ases_expe
                where
                    corr_ases = '$emai'
                    AND ESTA_ASES <> 'VALIDAIDENTIDAD'";
            $datos = $getConexion->ejecutar_consulta($query);

            return (int)$datos[0]["cantidad"]=== 0 ? false : true;
        }
        /**
         * Valida si existe cedula.
         *
         * @param int $nume_iden
         * @return bool
         */
        public function vali_nume_iden($nume_iden){
            $nume_iden=trim($nume_iden);
            $getConexion = $this->conexion->getConexion();
            $query = "SELECT
                    count(*) as cantidad
                from
                    pre_ases_expe
                where
                    nume_iden = '$nume_iden'
                    AND ESTA_ASES <> 'VALIDAIDENTIDAD'";
            $datos = $getConexion->ejecutar_consulta($query);
            return (int)$datos[0]["cantidad"]=== 0 ? false : true;

        }
        /**
         * Valida si existe celular
         *
         * @param [type] $nume_celu
         * @return bool
         */
        public function vali_celu($nume_celu){
            $nume_celu=trim($nume_celu);
            $getConexion = $this->conexion->getConexion();
            $query = "
                select
                    count(*) as cantidad
                from
                    pre_ases_expe
                where
                    celu_ases = '$nume_celu'
                    AND ESTA_ASES <> 'VALIDAIDENTIDAD'";
            $datos = $getConexion->ejecutar_consulta($query);
            return (int)$datos[0]["cantidad"]=== 0 ? false : true;

        }
        /**
         * Valida si existe lider
         *
         * @param int $nume_lide
         * @return bool
         */
        public function vali_lide($nume_lide){
            $nume_lide=trim($nume_lide);
            $getConexion = $this->conexion->getConexion();
            $query = "
                select
                count(*) as cantidad
            from
                tab_lide as l,
                tab_clie as c
            WHERE
                l.cons_terc = c.cons_terc
                and l.acti_esta = 'ACT'
                and c.nume_iden = '$nume_lide'";
            $datos = $getConexion->ejecutar_consulta($query);
            return (int)$datos[0]["cantidad"]=== 0 ? true : false;
        }

    }
