<!-- inicio footer -->
<div class="panel-default" id="content1" name="content1">
  <form id="footer" name="footer" style="display:block" action="" method="post" id="footer">
    <div class="container" style="width: 100%">
      <div style="align: left;font-kerning: none;color: black;font-size: 17px">
        Respaldamos el crecimiento integral de la mujer nos apasiona la realizaci&oacute;n de sus sueños, la mejora de su nivel de vida y el de su familia.
      </div>
      <br>
      <img alt='logo' src='imagenes/azzorti.png' title='Azzorti' style='display:block; margin:auto; width: 40%'>

  </form>
</div>
<!-- fin footer -->
</form>

<div class="modal-body" style="display:block">
  <div class='table-responsive'>
    <table class="table table-sm">

      <tbody id='moda_data'>

      </tbody>
    </table>
  </div>
</div>

<footer class="footer">
    <center><p style="position: relative">Copyright © AZZORTI.</p></center>
</footer>
<script src="estilos/js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>
<!-- Latest compiled and minified JavaScript -->
<script
  src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
  integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
  crossorigin="anonymous"></script>
<script src="estilos/js/bootstrap-toggle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.8.0/sweetalert2.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script src="estilos/js/library/wait-me.min.js"></script>
<script src="estilos/js/step-bootstrap.js"></script>
<script src="estilos/js/step-pregunta.js"></script>
<script src="estilos/js/funciones.js"></script>
<script src="estilos/js/funciones-eventos.js"></script>
<script src="estilos/js/eventos.js"></script>