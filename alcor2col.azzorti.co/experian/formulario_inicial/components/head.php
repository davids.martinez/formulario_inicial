<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="UTF-8">
  <title>FORMULARIO</title>
  <link href="estilos/css/bootstrap-reboot.min.css" rel="stylesheet" type="text/css" />
  <link href="estilos/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="estilos/css/font/fuente.css" rel="stylesheet" type="text/css" />
  <link href="estilos/css/seguimiento.css" rel="stylesheet" type="text/css" />
  <link href="estilos/css/solicitud.css" rel="stylesheet" type="text/css" />
  <link href="estilos/css/loader.css" rel="stylesheet" type="text/css" />
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="estilos/css/bootstrap-toggle.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.8.0/sweetalert2.min.css" rel="stylesheet" />
  <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
  <link href="estilos/css/step-bootstrap.css" rel="stylesheet" type="text/css" />
  <link href="estilos/css/wait-me.min.css" rel="stylesheet" />
  <link href="estilos/css/estilos.css" rel="stylesheet" />
  <link href="estilos/css/checkbox.css" rel="stylesheet" />
</head>