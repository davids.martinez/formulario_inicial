<!-- Modal celular -->
<div class="modal fade" id="modal-experian" tabindex="-1" role="dialog" aria-labelledby="modal-experian">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Validar identidad</h4>
          </div>
          <div class="modal-body">
            <div class="stepwizard">
              <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step col-xs-6">
                  <a href="#step-1" type="button" class="btn btn-success btn-circle">1</a>
                  <p><small>Validar celular</small></p>
                </div>
                <div class="stepwizard-step col-xs-6">
                  <a href="#step-2" type="button" class="btn disabled btn-default btn-circle">2</a>
                  <p><small>Codigó OTP</small></p>
                </div>
              </div>
            </div>
            <form role="form">
              <div class="panel panel-primary setup-content" id="step-1">
                <div class="panel-heading">
                  <h3 class="panel-title">Confirmación de teléfono</h3>
                </div>
                <div class="panel-body">
                  <div class="form-group">
                    <label class="control-label">Teléfono celular</label>
                    <input type="text" id="ex_celu_terc" onkeyup="this.value=Numeros(this.value)" class="form-control" placeholder="Numero teléfonico" />
                  </div>
                  <button class="btn btn-primary pull-right" id="step-1-btn" type="button">Continuar</button>
                </div>
              </div>
              <div class="panel panel-primary setup-content" id="step-2">
                <div class="panel-heading">
                  <h3 class="panel-title">Introduce el código enviado a tu celular</h3>
                </div>
                <div class="panel-body">
                  <div class="form-group">
                    <label class="control-label">Código</label>
                    <input type="text" id="ex_otp" onkeyup="this.value=Numeros(this.value)" class="form-control" placeholder="Código OTP" />
                  </div>
                  <button class="btn btn-primary pull-right" id="step-2-btn" type="button">Confirmar</button>
                </div>
              </div>
            </form>
          </div>
        </div><!-- /.modal-content -->
      </div>
    </div>
</div>
<!-- fin modal celular -->