<form id="pestana4" style="display:none" name="pestana4" href="pedidos/pedidos/formulario_inicial" method="POST">
	<div class="container" style="width: 100%">
		<table border='0' width='100%' cellspacing='0' cellpadding='0' class='x-panel-header2'>
			<br>
			<div class="row">
				<div class="col-xs-6">
					<img alt='logo' src='imagenes/banner_final.jpg' title='logol' style='display:block; margin:auto ;width: 100%;'>
				</div>

				<div class="col-xs-6" style="color: black; font-size:8vw	">
						<div>
							<h4 style="text-align: center; margin-bottom:40px"><b>Querida asesora estas a un paso de finalizar el proceso de inscripción.</b></h4>

							<p class="parrafo1">Por favor verifica tu WhatsApp, se te ha enviado un link para que firmes la documentación necesaria para ser asesora Azzorti.</p>
							<p class="parrafo1">Por ultimo tu gerente de zona asignada se pondrá en contacto contigo para darte la bienvenida a esta gran familia!</p>
							<p class="parrafo1">Cualquier duda que tengas envía un correo a :</p>
							<p class="parrafo1">
								<a href="mailto:servicioalcliente@azzorti.co">servicioalcliente@azzorti.co</a>
								 o Comunícate al
								<a href="tel:+6012906077">+601 290-6077</a>
							<p>
						</div>
					<center>
						<input type="submit" name="finalizar" id="finalizar" value="Finalizar" class="btn btn-md btn-success" />
					</center>
				</div>
			</div>
			<br>
		</table>
	</div>
</form>