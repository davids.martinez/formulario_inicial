<!-- Inicio pestana 2 -->
<form id="pestana2" style="display:none" name="pestana2" id="pestana2">
	<div class="container" style="width: 100%">
		<table border='0' width='100%' cellspacing='0' cellpadding='0' class='x-panel-header2'>
    <div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Fecha de nacimiento<span style="color:red;">*</span></label>
					<input id="datepicker" class="form-control" />
				</div>
				<input type="hidden" name="edad" id="edad" value="">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Genero<span style="color:red;">*</span></label>
					<select id="MF88" name="MF88" aria-invalid="false" aria-describedby="nf-error-5" class="form-control is-valid" aria-labelledby="nf-label-field-5" required="">
						<option value="" selected="selected">SELECCIONE UNA OPCION</option>
						<option value="F">Femenino</option>
						<option value="M">Masculino</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Departamento de residencia<span style="color:red;">*</span></label>
					<select name="DPT12" id="DPT12" required="true" class="form-control">
						<option value="" selected="selected">SELECCIONE UNA OPCION</option>
						<?php foreach ($arre_dpto as $key => $registro) { ?>
							<option value="<?php echo $registro["cons_dpto"]; ?>"><?php echo $registro["nomb_dpto"]; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Ciudad de residencia<span style="color:red;">*</span></label>
					<select name="CIU13" id="CIU13" required="true" class="form-control">
						<option value="">SELECCIONE UNA OPCION</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Correo electronico<span style="color:red;">*</span></label>
					<input type="text" name="CCE17" id="CCE17" class="form-control">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-warning" role="alert">
						<strong>Referencia familiar</strong>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Nombres y apellidos<span style="color:red;">*</span></label>
					<input type="text" name="ref_familiar_nombre" id="ref_familiar_nombre" class="form-control" onkeyup="this.value=Mayusculas(this.value)" onblur="this.value=Mayusculas(this.value)" onchange="this.value=Solotexto(this.value)">
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Celular<span style="color:red;">*</span></label>
					<input type="text" name="ref_familiar_celular" id="ref_familiar_celular" class="form-control" onkeyup="this.value=Numeros(this.value)">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Departamento<span style="color:red;">*</span></label>
					<select name="ref_fami_depa" id="ref_fami_depa" required="true" class="form-control">
						<option value="" selected="selected">SELECCIONE UNA OPCION</option>
						<?php foreach ($arre_dpto as $key => $registro) { ?>
							<option value="<?php echo $registro["cons_dpto"]; ?>"><?php echo $registro["nomb_dpto"]; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Ciudad<span style="color:red;">*</span></label>
					<select name="ref_fami_ciud" id="ref_fami_ciud" required="true" class="form-control">
						<option value="">SELECCIONE UNA OPCION</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Parentesco<span style="color:red;">*</span></label>
					<select  name="ref_familiar_parentesco" id="ref_familiar_parentesco" required="true" class="form-control">
						<option value="" selected="selected">SELECCIONE UNA OPCION</option>
						<?php foreach ($arre_pare as $key => $registro) { ?>
							<option value="<?php echo $registro["codi_pare"]; ?>"><?php echo $registro["nomb_pare"]; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-warning" role="alert">
						<strong>Referencia personal</strong>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Nombres y apellidos<span style="color:red;">*</span></label>
					<input type="text" name="ref_personal_nombre" id="ref_personal_nombre" class="form-control" onkeyup="this.value=Mayusculas(this.value)" onblur="this.value=Mayusculas(this.value)" onchange="this.value=Solotexto(this.value)">
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Celular<span style="color:red;">*</span></label>
					<input type="text" name="ref_personal_celular" id="ref_personal_celular" class="form-control">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Departamento<span style="color:red;">*</span></label>
					<select name="ref_pers_depa" id="ref_pers_depa" required="true" class="form-control">
						<option value="" selected="selected">SELECCIONE UNA OPCION</option>
						<?php foreach ($arre_dpto as $key => $registro) { ?>
							<option value="<?php echo $registro["cons_dpto"]; ?>"><?php echo $registro["nomb_dpto"]; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Ciudad<span style="color:red;">*</span></label>
					<select name="ref_pers_ciud" id="ref_pers_ciud" required="true" class="form-control">
						<option value="">SELECCIONE UNA OPCION</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-8" style="width: 100%">
					<label for="validation01">&#191;Te recomendo alguien? <span style="color:red;">*</span></label>
					<label class="radio-inline"><input type="radio" onChange="sele_lide()" value='SI' id="togg_sele" name="togg_sele">SI</label>
					<label class="radio-inline"><input type="radio" onChange="sele_lide()" value='NO' id="togg_sele" name="togg_sele" checked>NO</label>
				</div>
			</div>
			<div class="row" id='cedu_lide'>
				<div class="col-xs-8" style="width: 100%">
					<label for="validation01">Cedula lider referente<span style="color:red;">*</span></label>
					<input type="text" name="RF99" id="RF99" style="width: 100%" class="form-control">
				</div>
			</div>
			<div>
				<div class="col-md-12 text-center">
          			<input type="submit" name="anterior2" id="anterior2" value="Anterior" class="btn btn-md btn-success" />
					<input type="submit" name="siguiente2" id="siguiente2" value="Siguiente" class="btn btn-md btn-success" />
				</div>
			</div>
    <br>
		</table>
	</div>
</form>
<!-- Inicio pestana 2 -->
