<?php
$arre_ltr = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
if ($tipo_via == "AV")
  $sele_a = "selected";
if ($tipo_via == "CL")
  $sele_c = "selected";
if ($tipo_via == "KR")
  $sele_ca = "selected";
if ($tipo_via == "DG")
  $sele_d = "selected";
if ($tipo_via == "TV")
  $sele_t = "selected";
if ($tipo_via == "CQ")
  $sele_cq = "selected";
if ($tipo_via == "VRD")
  $sele_v = "selected";
if ($tipo_via == "SM")
  $sele_sm = "selected";
if ($tipo_via == "CA")
  $sele_ca = "selected";
if ($tipo_via == "LT")
  $sele_lt = "selected";
if ($tipo_via == "KM")
  $sele_km = "selected";
if ($tipo_via == "AGP")
  $sele_agp = "selected";
if ($dire_bis == "BIS")
  $sele_bis = "selected";
if ($dire_cuad == "SUR")
  $sele_s = "selected";
if ($dire_cuad == "ESTE")
  $sele_e = "selected";
?>
<!-- inicio pestana 3 -->
<form id="pestana3" style="display:none" name="pestana3" method="POST">
  <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <label for="validation01">Barrio <span style="color:red;">*</span></label>
          <select name="BAR15" id="BAR15" required="true" class="form-control">
            <option value="">SELECCIONE UNA OPCION</option>
            <?php foreach ($arre_barr as $key => $registro) { ?>
              <option value="<?php echo $registro["cons_barr"]; ?>"><?php echo $registro["nomb_barr"]; ?></option>
            <?php } ?>
          </select>
          <input type="text" style="display:none" name="barr_nuev" id="barr_nuev" class="form-control">
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 label-dire">
          <label for="validation01">Dirección <span style="color:red;">*</span></label>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">

          <div class="form-group">
            <select name='tipo_via' id='tipo_via' onChange="direccion()" id="tipo_via" class="form-control is-valid" value="<?= $tipo_via ?>">
              <option value="">Seleccione</option>
              <option value="AV" <?= $sele_a ?>>Avenida</option>
              <option value="CL" <?= $sele_c ?>>Calle</option>
              <option value="KR" <?= $sele_ca ?>>Carrera</option>
              <option value="DG" <?= $sele_d ?>>Diagonal</option>
              <option value="TV" <?= $sele_t ?>>Transversal</option>
              <option value="CQ" <?= $sele_cq ?>>Circular</option>
              <option value="VRD" <?= $sele_v ?>>Vereda</option>
              <option value="SM" <?= $sele_sm ?>>Super manzana</option>
              <option value="CA" <?= $sele_ca ?>>Casa</option>
              <option value="LT" <?= $sele_lt ?>>Lote</option>
              <option value="KM" <?= $sele_km ?>>Kilómetro</option>
              <option value="AGP" <?= $sele_agp ?>>Agrupación</option>
            </select>
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-1 p-dire">
          <div class="form-group">
            <input name='nume_via' id='nume_via' class="form-control is-valid" onChange="direccion()" onDblClick=" selecciona_value(this)" onkeyup="this.value=NumText(this.value)" value='<?= $nume_via ?>' type='TEXT' size='33' maxlength='10' autocomplete="off" required="">
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-1 p-dire">
          <div class="form-group">
            <select name='letr_via' id='letr_via' onChange="direccion(this.value)" class="form-control is-valid">
              <option value=""> - </option>
              <?php for ($i = 0; $i < count($arre_ltr); $i++) { ?>
                <?php if ($arre_ltr[$i] == $letr_via) { ?>
                  <option value="<?= $arre_ltr[$i] ?>">
                    <?= $arre_ltr[$i] ?>
                  </option>
                <?php } else { ?>
                  <option value="<?= $arre_ltr[$i] ?>">
                    <?= $arre_ltr[$i] ?>
                  </option>
                <?php } ?>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2 p-dire">
          <div class="form-group">
            <select name='dire_bis' id='dire_bis' onChange="direccion()" class="form-control is-valid">
              <option value=""> - </option>
              <option value="BIS" <?= $sele_bis ?>> BIS </option>
            </select>
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-1 p-dire">
          <div class="form-group form-inline">
          <label>#</label>
            <input type="text" id="dire_num1" name="dire_num1" size="2" onChange="direccion()" onKeyPress="if(event.keyCode=='13') { dire_num2.focus()}" onkeyup="this.value=NumText(this.value)" value="<?= $dire_num1 ?>" class="form-control is-valid" autocomplete="off" />
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-1 p-dire">
          <div class="form-group">
            <select name='dire_letr' id='dire_letr' onChange="direccion()" class="form-control is-valid">
              <option value=""> - </option>
              <?php for ($i = 0; $i < count($arre_ltr); $i++) { ?>
                <?php if ($arre_ltr[$i] == $dire_letr) { ?>
                  <option value="<?= $arre_ltr[$i] ?>">
                    <?= $arre_ltr[$i] ?>
                  </option>
                <?php } else { ?>
                  <option value="<?= $arre_ltr[$i] ?>">
                    <?= $arre_ltr[$i] ?>
                  </option>
                <?php } ?>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-1 p-dire">
          <div class="form-group">
            <input type="text" name="dire_num2" id="dire_num2" size="2" onChange="direccion()" onKeyPress="if(event.keyCode=='13') {dire_comp.focus()}" onkeyup="this.value=NumText(this.value)" value="<?= $dire_num2 ?>" class="form-control is-valid" autocomplete="off" />
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
          <div class="form-group">
            <select name='dire_cuad' id='dire_cuad' onChange="direccion()" class="form-control is-valid">
              <option value=""> - </option>
              <option value="SUR" <?= $sele_s ?>>SUR</option>
              <option value="ESTE" <?= $sele_e ?>>ESTE</option>
              <option value="OESTE">OESTE</option>
              <option value="NORTE">NORTE</option>
              <option value="OCCIDENTE">OCCIDENTE</option>
            </select>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <label for="validation01">Complemento </label>
          <input type="text" name="complemento" id="complemento" placeholder="torre,apartamento,manzana" onkeyup="direccion()" onChange="direccion()" class="form-control">
          <input type="hidden" name="dire_fina" id="dire_fina" onkeyup="direccion()" class="form-control">
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <label for="validation01">&#191;La direcci&oacuten de entrega es igual a la direccion de domicilio? <span style="color:red;">*</span></label>
          <select name='DRE904' id='DRE904' onChange="dire_entre()" class="form-control is-valid">
            <option value="Si">Si</option>
            <option value="No">No</option>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <label for="validation01">Direcci&oacuten de entrega </label>
          <input type="text" name="DRE905" id="DRE905" class="form-control">
        </div>
        <div class="row">
          <div class="col-xs-12 m-top-30">
            <center>
              <input type="submit" name="anterior3" id="anterior3" value="Anterior" class="btn btn-md btn-success" />
              <input type="submit" name="siguiente3" id="siguiente3" value="Siguiente" class="btn btn-md btn-success" />
            </center>
          </div>
        </div>
      </div>
      <!-- type="hidden" -->
      <input type="hidden" name="aprobacion" id="aprobacion" value="">
  </div>
</form>
<!-- fin pestana 3 -->