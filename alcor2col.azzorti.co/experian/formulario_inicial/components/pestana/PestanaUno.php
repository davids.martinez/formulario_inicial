<?php
$nume_iden = strlen($_GET["nume_iden"]) > 0 ? $_GET["nume_iden"] : $nume_iden;
?>
<!-- pestaña 1 -->
<form id="pestana1" style="display:block" name="pestana1" id="pestana1" method="POST">
	<div class="container" style="width: 100%">
		<table border='0' width='100%' cellspacing='0' cellpadding='0' class='x-panel-header2'>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Tipo de documento <span style="color:red;">*</span></label>
					<select name="TD1" id="TD1" required="true" class="form-control">
						<option value="" selected="selected">SELECCIONE UNA OPCION</option>
						<?php foreach ($arre_docu as $key => $registro) { ?>
							<option value="<?php echo $registro["cons_docu"]; ?>"><?php echo $registro["nomb_docu"]; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">N&uacutemero Identificacion <span style="color:red;">*</span></label>
						<input id='NI2' name='NI2' class="form-control is-valid" onDblClick=" selecciona_value(this) " onkeyup="this.value=Numeros(this.value)" value='<?php $nume_iden ?>' type='text' size='11' maxlength='20' required autocomplete="off">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Fecha de expedici&oacuten <span style="color:red;">*</span></label>
					<input id="fech_expe" class="form-control" />
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Celular <span style="color:red;">*</span></label>
					<input type="text" name="CEL14" onkeyup="this.value=Numeros(this.value)" id="CEL14" class="form-control">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Primer Nombre <span style="color:red;">*</span></label>
					<input type="text" id='PN7' required="true" onkeyup="this.value=Mayusculas(this.value)" onblur="this.value=Mayusculas(this.value)" onchange="this.value=Solotexto(this.value)" name='PN7' class="form-control">
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Segundo nombre</label>
					<input type="text" id='SN8' onkeyup="this.value=Mayusculas(this.value)" onblur="this.value=Mayusculas(this.value)" onchange="this.value=Solotexto(this.value)" name='SN8' class="form-control">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Primer Apellido <span style="color:red;">*</span></label>
					<input type="text" id='PA9' onkeyup="this.value=Mayusculas(this.value)" onblur="this.value=Mayusculas(this.value)" onchange="this.value=Solotexto(this.value)" name='PA9' class="form-control">
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Segundo Apellido</label>
					<input type="text" id='SA10' onkeyup="this.value=Mayusculas(this.value)" onblur="this.value=Mayusculas(this.value)" onchange="this.value=Solotexto(this.value)" name='SA10' class="form-control">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<label class="check">
						<input type="checkbox" id="poli-trat"/>
						<div class="box"></div>
					</label>
					<label class="label-check">Leí y acepto
						<a href="https://azzorti.co/wp-content/uploads/2020/03/POLITICA-DE-TRATAMIENTOS.pdf" target="_blank">la política de tratamiento de datos Personales </a>
					</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<label class="check">
						<input type="checkbox" id="cons-fina"/>
						<div class="box"></div>
					</label>
					<label class="label-check">Leí y acepto
						<a href="https://azzorti.co/wp-content/uploads/2022/Autorizacion_consulta_y_verificacion_de_datos_en_centrales_de_informacion.pdf" target="_blank">consulta financiera </a>
					</label>
				</div>
			</div>
			<div class="form-inline">
				<input id="proc" name="proc" type="hidden" value="<?php echo $_GET["proc"]; ?>">
				<input id="token" name="token" type="hidden" value="<?php echo $_GET["token"]; ?>">
				<input id="id" name="id" type="hidden" value="<?php echo $_GET["id"]; ?>">
				<input id="cedula" name="cedula" type="hidden" value="<?php echo $_GET["nume_iden"]; ?>">
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<input type="button" id="btn-experian" value="Siguiente" class="btn btn-md btn-success" />
				</div>
			</div>
	</div>
	</table>
	</div>
</form>
<!-- fin pestaña 1. -->