$(document).ready(function() {
    valores_iniciales();

    $("#btn-experian").on("click", function(){
        btn_experian_click();
        return false;
    });

    $('#siguiente2').on('click', function() {
        siguiente_click();
        return false;
    });

    $('#siguiente3').on('click', function() {
        siguiente1_click();
        return false;
    });

    $('#anterior2').on('click', function() {
      $('#pestana1').css('display', 'block');
      $('#pestana2').css('display', 'none');
      cambia_banner("1");
      return false;
    });

    $('#anterior3').on('click', function() {
      $('#pestana2').css('display', 'block');
      $('#pestana3').css('display', 'none');
      return false;
    });


    //valida la cedula existente

    $("#NI2").change(function(e) {
      e.preventDefault();
      identificacion = $("#NI2").val();
      campo = $("#NI2");
      veri_input(campo,"nume_iden");
      $.post("utilidades/fun_validar.php", {
        identificacion: identificacion
      }, function(data) {
        if (data.length) {
          swal({
            text: "cedula ya existente",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#449d44',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK'
          });
          // $("#NI2").val("");

        }
      });

      $("#NI2").prop('selectedIndex', 0);
    });
    $("#CEL14").change(function(e) {
      e.preventDefault();
      campo = $(this);
      veri_input(campo,"nume_celu");
    });
    $("#CCE17").change(function(e) {
      e.preventDefault();
      campo = $(this);
      veri_input(campo,"email");
    });
    $("#RF99").on("change", function(e){
      lide_change();
    }).on('keypress',function(e) {
      if(e.which == 13) {
        e.preventDefault();
        lide_change();
      }
    });




    //trae los ciudades de acuerdo al departamento seleccionado
    $("#DPT12").change(function() {
      departamento_change("#DPT12","#CIU13");
    });
    $("#ref_fami_depa").change(function() {
      departamento_change("#ref_fami_depa","#ref_fami_ciud");
    });
    $("#ref_pers_depa").change(function() {
      departamento_change("#ref_pers_depa","#ref_pers_ciud");
    });
    //trae los barrios de acuerdo a la ciudad
    $("#CIU13").change(function() {
      ciudad_change();
    });

    //trae los barrios de acuerdo a la ciudad
    $("#BAR15").change(function() {
      var barrio = $('#BAR15').val();

      if (barrio == 'otro') {
        $('#barr_nuev').css('display', 'block');
      } else {
        $('#barr_nuev').css('display', 'none');
      }

    });
});