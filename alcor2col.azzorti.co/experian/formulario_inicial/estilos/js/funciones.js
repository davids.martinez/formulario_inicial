//armador de la direccion
function direccion() {
  var tipo_via = $("#tipo_via").val();
  var nume_via = $("#nume_via").val();
  var letr_via = $("#letr_via").val();
  var dire_bis = $("#dire_bis").val();
  var dire_num1 = $("#dire_num1").val();
  var dire_letr = $("#dire_letr").val();
  var dire_num2 = $("#dire_num2").val();
  var dire_cuad = $("#dire_cuad").val();

  var direccion = $("#dire").val();
  var direccion = $.trim(direccion);
  var complemento = $("#complemento").val();
  var complemento = $.trim(complemento);
  var data =
    tipo_via +
    " " +
    nume_via +
    " " +
    letr_via +
    " " +
    dire_bis +
    " " +
    dire_num1 +
    " " +
    dire_letr +
    " " +
    dire_num2 +
    " " +
    dire_cuad +
    " " +
    complemento;
  var data = $.trim(data);

  $("#dire_fina").val(data);
  var direccion_fina = $("#dire_fina").val();
  var dire_entre = $("#DRE904").val();
  if (dire_entre == "Si") {
    $("#DRE905").prop("disabled", true);
    $("#dire_fina").val(data);
    $("#DRE905").val(data);
    return false;
  } else {
    $("#DRE905").prop("disabled", false);
    $("#DRE905").val("");
    $("#dire_fina").val(data);
    return false;
  }
}

/* metodo para el cambio de la direccion de entrega */
function dire_entre() {
  var dire_entre = $("#DRE904").val();
  var direccion = $("#dire_fina").val();
  if (dire_entre == "Si") {
    $("#DRE905").prop("disabled", true);
    $("#DRE905").val(direccion);
    return false;
  } else {
    $("#DRE905").prop("disabled", false);
    $("#DRE905").val("");
    return false;
  }
}

/* metodo que muestra el campo de cedula lider */
function sele_lide() {
  var togg_sele = $("input:radio[name=togg_sele]:checked").val();
  if (togg_sele == "SI") {
    $("#cedu_lide").show();
  } else {
    $("#cedu_lide").hide();
  }
}

//Retornar valor convertido a mayusculas
function Mayusculas(tx) {
  return tx.toUpperCase();
}
//valida solo numeros
function Numeros(string) {
  //Solo numeros
  var out = "";
  var filtro = "1234567890"; //Caracteres validos

  //Recorrer el texto y verificar si el caracter se encuentra en la lista de validos
  for (var i = 0; i < string.length; i++)
    if (filtro.indexOf(string.charAt(i)) != -1)
      //Se añaden a la salida los caracteres validos
      out += string.charAt(i);

  //Retornar valor filtrado
  return out;
}
//validar campos de text
function NumText(string) {
  //solo letras y numeros
  var out = "";
  //Se añaden las letras validas
  var filtro =
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_ "; //Caracteres validos

  for (var i = 0; i < string.length; i++)
    if (filtro.indexOf(string.charAt(i)) != -1) out += string.charAt(i);
  return out;
}

//validar campos de text
function Solotexto(string) {
  //solo letras
  var out = "";
  //Se añaden las letras validas
  var filtro = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "; //Caracteres validos

  for (var i = 0; i < string.length; i++)
    if (filtro.indexOf(string.charAt(i)) != -1) out += string.charAt(i);
  return out;
}
/* Valida el correo electronico */
function isValidEmailAddress(emailAddress) {
  var pattern = new RegExp(
    /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
  );
  return pattern.test(emailAddress);
}

function soli(data,tipo="POST") {
  return new Promise(function (resolve, reject) {
    if(tipo === "POST"){
      data = JSON.stringify(data);
    }
    let host = $(location).attr("host");
    let baseUrl = `https://${host}/experian/formulario_inicial/api/solicitud.php`;
    let config = {
      type: tipo,
      url: baseUrl,
      data: data,
      beforeSend: function () {
        $("body").waitMe({
          effect: "bounce",
          text: "",
          bg: "rgba(255,255,255,0.7)",
          color: "#fedd3c",
          textPos: "vertical",
        });
      },
      complete: function () {
        $("body").waitMe("hide");
      },
    }
    if(tipo === "POST"){
      config["contentType"]  = "application/json; charset=utf-8";
      config["dataType"] = "json";
    }
    $.ajax(config)
      .done(function (respuesta, textoEstado, estado) {
        resp_gene = {
          respuesta: respuesta,
          estado: estado.status,
        };
        resolve(resp_gene);
      })
      .fail(function (err) {
        respuesta = err.responseJSON;
        if (err.status === 403 || err.status === 400) {
          swal({
            text: respuesta.descripcion,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#449d44",
            confirmButtonText: "Aceptar",
          }).then(function (result) {
            reject(err);
          });
        } else if (err.status === 500) {
          swal({
            text: "Ha ocurrido un error en el servidor.",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#449d44",
            confirmButtonText: "Aceptar",
          });
        }
      });
  });
}

function modal_experian(proceso) {
  if (proceso === "abrir") {
    $("#modal-experian").modal({
      backdrop: "static",
      keyboard: false,
    });
  } else if (proceso === "cerrar") {
    $("#modal-experian").modal("toggle");
  }
}

function modal_preguntas(proceso) {
  if (proceso === "abrir") {
    $("#modal-preguntas").modal({
      backdrop: "static",
      keyboard: false,
    });
  } else if (proceso === "cerrar") {
    $("#modal-preguntas").modal("toggle");
  }
}
function seleccion_proceso(info) {
  switch (info.proceso) {
    case "preguntas_seguridad":
      preguntas = info.preguntas;
      try {
        $.experian.preguntas = preguntas.Question;
        crear_preguntas(preguntas);
        $.experian.transaccion = preguntas.Transaccion;
      } catch (error) {
        console.error(error);
      }
      modal_preguntas("abrir");
      break;
    case "flujo_otp":
      $.experian.indi_tran = info.datos;
      modal_experian("abrir");
      break;
    default:
      mensaje("error", "no se ha definido este proceso");
  }
}
function mensaje_accion(tipo, mensaje) {
  return new Promise(function (resolve, reject) {
    swal({
      text: mensaje,
      type: tipo,
      confirmButtonColor: "#449d44",
      confirmButtonText: "OK",
    }).then(function (data) {
      resolve(data);
    });
  });
}
function calcularEdad() {
  var fecha = $(this).val();
  var hoy = new Date();
  var match = /(\d+)\/(\d+)\/(\d+)/.exec(fecha);
  var fecha_fina = match[3] + "/" + match[2] + "/" + match[1];
  var fecha_nuev = new Date(fecha_fina);
  var cumpleanos = new Date(fecha_nuev);
  var edad = hoy.getFullYear() - cumpleanos.getFullYear();
  var m = hoy.getMonth() - cumpleanos.getMonth();
  if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
    edad--;
  }
  $("#edad").val(edad);
}
function mensaje(tipo,mensaje){
  swal({
      text: mensaje,
      type: tipo,
      confirmButtonColor: '#449d44',
      confirmButtonText: 'OK'
  });
}

function steep_2(){
  $("#pestana1").hide();
  $("#pestana2").show();
  $("#step-one").removeClass("active");
  $("#step-two").addClass("active");
}
function fill_datos(respuesta){

  $cedula = $("#NI2");
  $celular = $("#CEL14")
  $fech_expe = $("#fech_expe");
  $nombre1 = $("#PN7");
  $nombre2 = $("#SN8");
  $apellido1 = $("#PA9");
  $apellido2 = $("#SA10");

  $tipo_docu = $("#TD1");
  $politica = $("#poli-trat");
  $confinan = $("#cons-fina");

  $cedula.val(respuesta.nume_iden);
  $fech_expe.val(respuesta.fech_expe);
  $celular.val(respuesta.celu_ases);
  $nombre1.val(respuesta.prim_nomb);
  $nombre2.val(respuesta.segu_nomb);
  $apellido1.val(respuesta.prim_apel);
  $apellido2.val(respuesta.segu_apel);

  $cedula.attr("disabled", true);
  $fech_expe.attr("disabled", true);
  $nombre1.attr("disabled", true);
  $nombre2.attr("disabled", true);
  $apellido1.attr("disabled", true);
  $apellido2.attr("disabled", true);
  $politica.attr("disabled", true);
  $confinan.attr("disabled", true);
  $celular.attr("disabled", true);
  $tipo_docu.attr("disabled", true);
}
function cambia_banner(id){
  $("#logo").attr("src", `imagenes/banner_${id}.jpg`);
}
