<?php
require 'utilidades/get_departamento.php';
require 'utilidades/get_ciudad.php';
require 'utilidades/get_documentos.php';
require 'utilidades/get_parentesco.php';
?>
<!DOCTYPE html>
<html lang="es">

<?php include('components/head.php');?>

<body>
  <?php include('components/modal/VerificaCelular.php');?>
  <?php include('components/modal/PreguntasSeguridad.php');?>
  <?php include('components/header/header.php') ?>
  <?php include('components/pestana/PestanaUno.php') ?>
  <?php include('components/pestana/PestanaDos.php') ?>
  <?php include('components/pestana/PestanaTres.php') ?>
  <?php include('components/pestana/PestanaCuatro.php') ?>
  <?php include('components/carga/Cargando.php') ?>
  <?php include('components/footer/footer.php') ?>
</body>
</html>