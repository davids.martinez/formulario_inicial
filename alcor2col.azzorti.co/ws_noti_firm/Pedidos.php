<?php
/*Clase que maneja la logica del servicio*/
require ("Consultas.php");
class Pedidos{
  private $log = null;
  private $log_proc = null;
  private $curl = null;
  private $cant_resp = 4;

  /*
  * Constructor de la clase en el cual se instancia el modelo y la clase curl
  * @param $file resource variable con el resource del archivo log
  */
  public function __construct($file)
  {
    $this->model = new Consultas();
  }

  /*
  * Obtiene la campaña actual
  * @return codi_camp integer campaña actual
  */
  public function enviamensaje($data)
  {
    $APIurl = 'https://api.chat-api.com/instance113881/';
		$token = 'eize4u6k7a9pij0p';
    $data = [
				'phone' => $data['nume_celu'],
				'body' => $data['mens_envi']
		];
    $ruta_arch='';
    $nume_celu=$data['phone'];
    $mens_envi=$data['body'];
    $ruta = $APIurl."SendMessage?token=".$token;
		// print_r($ruta);
    
		$ch = curl_init($ruta);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$res = curl_exec($ch);
		//valida conexion con servidor
		$esta_cone = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$tiem_cone = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
		//captura errores curl
		$curl_erro = curl_error($ch);
		curl_close($ch);
		$respuesta = json_decode($res);
    // die(print_r($respuesta,true));
    
    if($respuesta->sent==1)
    {
      $obse_envi='Envio mensaje correcto al numero destino';
    }
    else{
      $obse_envi='No se envio el mensaje por inconsistencias';
    }

    $this->model->inse_regi_envi($nume_celu,$mens_envi,$ruta_arch,$obse_envi);
  }

   public function enviaarchivo($data)
  {
    $APIurl = 'https://eu124.chat-api.com/instance113881/';
		$token = 'eize4u6k7a9pij0p';
    $data = [
				'body' => $data['ruta'],
				'filename'=> $data['nomb_arch'],
				'phone' => $data['nume_celu']
	   ];
    $mens_envi=$data['filename'];
    $nume_celu=$data['phone'];
    $ruta_arch=$data['body'];
    $ruta = $APIurl."sendFile?token=".$token;
		$ch = curl_init($ruta);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$res = curl_exec($ch);
		//valida conexion con servidor
		$esta_cone = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$tiem_cone = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
		//captura errores curl
		$curl_erro = curl_error($ch);
		curl_close($ch);
		$respuesta = json_decode($res);
    die(print_r($respuesta,true));
    if($respuesta->sent==1)
    {
      $obse_envi='Envio mensaje correcto al numero destino';
    }
    else{
      $obse_envi='No se envio el mensaje por inconsistencias';
    }

    $this->model->inse_regi_envi($nume_celu,$mens_envi,$ruta_arch,$obse_envi);
  }


  /*
  * Metodo que devuelve mensaej del servicio en formato json
  * @param datos array arreglo con datos a devolver
  *
  */
  public function generaMensaje(array $datos)
  {
    header('Content-type:application/json;charset=utf-8');
    $mensaje = [];
    $mensaje["estado"]="S";
    $mensaje["datos"]=$datos;
    $resu_fina= json_encode($mensaje);
    die($resu_fina);

  }
  public function decodificaJSON($json)
  {
    $datos = json_decode($json);
    switch (json_last_error()) {
      case JSON_ERROR_DEPTH:
        $this->generaError('Error a decodificar JSON  - Excedido tamano maximo de la pila');
      break;
      case JSON_ERROR_STATE_MISMATCH:
        $this->generaError('Error a decodificar JSON  - Desbordamiento de buffer o los modos no coinciden');
      break;
      case JSON_ERROR_CTRL_CHAR:
        $this->generaError('Error a decodificar JSON  - Encontrado caracter de control no esperado');
      break;
      case JSON_ERROR_SYNTAX:
        $this->generaError('Error a decodificar JSON  - Error de sintaxis, JSON mal formado');
      break;
      case JSON_ERROR_UTF8:
        $this->generaError('Error a decodificar JSON  - Caracteres UTF-8 malformados, posiblemente codificados de forma incorrectao');
      break;
    }

    return $datos;
  }

  public function creaArchivoLog($codi_camp,$nume_iden)
  {
    $nomb_arch   = $codi_camp . "_" . $nume_iden . ".txt";
    // $archivo_log = "/inf2/pedidos/$codi_camp/$nomb_arch";
    $archivo_log = RUTA_TEMPORALES."pruebas_log.txt";

    /*$ruta_carp = "/inf2/pedidos/$codi_camp/";
    if (!is_dir($ruta_carp)) {
        mkdir($ruta_carp);
    }*/

    $this->log = fopen($archivo_log, "a");
  }

  public function consultaCodigos($nume_iden,$cod_prod,$codi_camp,$cons_terc)
  {
		if(!strlen(trim($cons_terc)))
		{
			fwrite($this->log_proc,"Cliente No Existe -> $nume_iden".PHP_EOL);
			$this->generaError("Cliente No Existe",["documento"=>$nume_iden]);
		}else{
			$datos = $this->model->consultaCodigos($nume_iden,$cod_prod,$codi_camp);
			if(count($datos)>0)
			{
				$arr_codigo[0] = "OK";
				$this->generaMensaje($arr_codigo);
			}else{
				//fwrite($this->log_proc,"Sin datos del cliente -> $nume_iden".PHP_EOL);
				$this->generaError("No Se Encontraron productos para el codigo ingresado");
			}
		}
  }


  public function escribeLog($cadena)
  {
    if(is_resource($this->log))
    {
      fwrite($this->log,$cadena);
    }
  }

  public function __destruct()
  {
    if(is_resource($this->log))
    {
      fclose($this->log);
    }
  }




}
?>
