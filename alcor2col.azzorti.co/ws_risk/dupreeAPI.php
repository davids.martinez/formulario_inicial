<?php

/**
 * @author david_neira
 */
class dupreeAPI {

  public function API() {
    header('Content-Type: application/JSON');
    $stdlog = fopen(RUTA_TEMPORALES . "cone_pg.txt", "w");
    require_once ("Conexion_Pdo.php");
    include 'dupreeDB.php';
    $conector_ifx = new conexion_pdo("Informix", "Produccion", $stdlog);
    //$conector_ifx = new conexion_pdo("Informix","ProduccionColombia",$stdlog);

    $obj = new dupreeDB($conector_ifx, $base_ifx);


    $method = $_SERVER['REQUEST_METHOD'];
    $modulo = $_GET['modulo'];
    $file = fopen(RUTA_TEMPORALES . "log_movil.txt", "a");
    fwrite($file, "data " . print_r($REQUEST, true) . " modulo " . $modulo . "\n");

    switch ($method) {
      case 'GET'://consulta
        echo 'GET';

        break;
      case 'POST'://inserta
        $data = json_decode(file_get_contents('php://input'), true);
        //facturas
        if ($modulo == "firma_documento") {
          echo $obj->generaCertificado($data);
        }
        //Consulta facturas
        if ($modulo == "notifica_usuario") {
          echo $obj->notificaUsuario($data);
        }

        if($modulo == "envia_otp"){
          echo $obj->recibeEnviaOTP($data);
		      //echo $obj_prov->recibeEnviaOTP($data);
        }
        break;
      case 'PUT'://actualiza
        echo 'PUT';
        break;
      case 'DELETE'://elimina
        echo 'DELETE';
        break;
      default://metodo NO soportado
        echo 'METODO NO SOPORTADO';
        break;
    }
  }

}

//end class
?>