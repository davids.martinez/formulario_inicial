<?php

include("../ws_noti_firm/Pedidos.php");

/**
 * @author david_neira
 */
class dupreeDB {

    /**
     * Ruta del archivo log
     *
     * @var string
     */
    private $logFile = RUTA_TEMPORALES . "noti_form_log.txt";

    /**
     * Ruta temporal del pdf
     *
     * @var string
     */
    private $ruta_pdf = "https://alcor2col.azzorti.co/temporales/";
    //private $ruta_pdf = "https://alcor.dupree.co/temporales/";

    /**
     * Ruta permanente del pdf
     *
     * @var string
     */
    private $ruta_perm_pdf = RUTA_ARCHIVOS . "clientes/vinculacion/";

    /**
     * Servicio whatsapp
     *
     * @var Pedidos
     */
    private $whatsapp;

    /**
     * Registra conexión de base de datos y carga el servicio de whatsapp
     *
     * @param conexion_pdo $conexion
     * @param string $database
     */
    function __construct($conexion, $database) {
        $this->conexion = $conexion;
        $this->database = $database;

        $arch = "";
        $this->whatsapp = new Pedidos($arch);
    }

    /*
     * Consulta saldo
     */

    public function servidorEnlinea($data) {
        return 200;
    }

    public function generaCertificado($data) {
        $file = fopen(RUTA_TEMPORALES . "cert_ases.txt", "a");

        $nume_iden = "51749229";

        $consulta = "select prim_nomb,segu_nomb,prim_apel,segu_apel,nume_iden,celu_ases as celu_terc from pre_ases_expe where nume_iden='$nume_iden';";
        $resu = $this->conexion->ejecutar_consulta($consulta);
        $nomb_ases = trim($resu[0]["prim_nomb"] . " " . $resu[0]["segu_nomb"] . " " . $resu[0]["prim_apel"] . " " . $resu[0]["segu_apel"]);
        $celu_ases = trim($resu[0]["celu_terc"]);

        $toke_docu = "a77d2LtuEJ4l/1stdqXddjntIMSOUlkxHkn4qEOF0IA=";
		

        $headers = array(
            "Token: $toke_docu",
            "Content-Type: application/json");

        $mail_ases = "davidsoin@hotmail.com";

        $ruta_arch = file_get_contents(RUTA_TEMPORALES . "soli_cred.pdf");
        //pruebas
        $clave="Dneira*1324";
        //producción
        //$clave = "Dneira9865*";

        $json_firm = '{
            "Usuario": "Dneira",
            "Clave": "' . $clave . '",
            "Firmantes": [
                {
                    "Identificacion": "' . $nume_iden . '",
                    "TipoIdentificacion": "Cedula de ciudadania",
                    "Nombre": "' . $nomb_ases . '",
                    "Correo": "' . $mail_ases . '",
                    "NroCelular": "' . $celu_ases . '"
                }
            ],
            "ArchivosPDF": [
                {
                    "Nombre": "Solicitud ' . $nume_iden . '",
                    "Documento_base64": "' . base64_encode($ruta_arch) . '"
                }
            ]
            } ';

        fwrite($file, "Json : " . $json_firm . "\n");
        //produccion
        //$url = "https://www.firmaplus.co/risk/api";
        //pruebas
		$url = "https://www.firmaplus.co/FirmaPlusPruebas/api/signer";

        $ch_es = curl_init();
        curl_setopt($ch_es, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch_es, CURLOPT_URL, $url);
        curl_setopt($ch_es, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch_es, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch_es, CURLOPT_POST, true);
        curl_setopt($ch_es, CURLOPT_POSTFIELDS, $json_firm);
        curl_setopt($ch_es, CURLOPT_HTTPHEADER, $headers);
        $tiem_cone = curl_getinfo($ch_es, CURLINFO_TOTAL_TIME);

        if (curl_error($ch_es)) {
            echo curl_error($ch_es);
        }
        $response = curl_exec($ch_es);
        $response = json_decode($response);
        fwrite($file, "Log certificado: " . print_r($response, true) . "\n");
        $code_erro = $response->Code;

        if ($code_erro == 0) {
            http_response_code(404);
            $mensaje = '{"error":"' . $response->Message . '"}';
            return $mensaje;
        }

        $mensaje = '{"mensaje":"Documento enviado con exito"}';
        return $mensaje;
    }

    /*
     * Notificacion usuario
     */
    public function notificaUsuario($data) {
        $getHeaders = apache_request_headers();
		
		
        $file = fopen(RUTA_TEMPORALES . "cert_ases.txt", "w");
        fwrite($file, "Log envio notificacion: " . print_r($data, true) . "\n");

        //$token_empresa = "85j0v49uc68qtank610mshrxfy37aifgd65bw1860olze2p93";
		$token_empresa = "a77d2LtuEJ4l/1stdqXddjntIMSOUlkxHkn4qEOF0IA=";

        $NroReferencia = $data['NroReferencia'];
        $NroSolicitud = $data['NroSolicitud'];
        $NroTransaccion = $data['NroTransaccion'];
        $Firmante = $data['Firmante'];
        $DocumentosPDF = $data['DocumentosPDF'];
        fwrite($file, "Firmante: " . print_r($Firmante, true) . "\n");
        fwrite($file, "DocumentosPDF: " . print_r($DocumentosPDF, true) . "\n");
		fwrite($file, "contadorFirmante: " . print_r(count($Firmante), true) . "\n");
		

        for ($i = 0; $i < count($Firmante); $i++) {
            $Identificacion = $Firmante[$i]["Identificacion"];
            fwrite($file, "Identificacion: " . $Identificacion . "\n");
            $TipoIdentificacion = $Firmante[$i]["TipoIdentificacion"];
            $Nombre = $Firmante[$i]["Nombre"];
            $Correo = $Firmante[$i]["Correo"];
            $NroCelular = $Firmante[$i]["NroCelular"];
            $FotoBase64 = $Firmante[$i]["FotoBase64"];
            $EFirmaID = $Firmante[$i]["EFirmaID"];
            $Fecha = $Firmante[$i]["Fecha"];
            $TipoDocumento = $DocumentosPDF[$i]["TipoDocumento"];
            $DocumentoFirmado = $DocumentosPDF[$i]["DocumentoFirmado"];
            //decodifica archivo
            $desc_arch = base64_decode($DocumentoFirmado);
            //proveedores
            $consulta = "select * from tab_ctrl_apro_prov_web  where nume_iden_dilg='$Identificacion';";
            $resu_prov = $this->conexion->ejecutar_consulta($consulta);
            fwrite($file, "Consulta Proveedor: " . $consulta . "\n");

            if (count($resu_prov) > 0) {

                $nume_iden_dilg = $Identificacion;
                $ano_carga = date('Y');
                $fecha_carga = date('Y') . "-" . date('m') . "-" . date('d');
                //Cuando son varios archivos firmados por 1 sola persona
                for ($d = 0; $d < count($DocumentosPDF); $d++) {
                    $NombDocumento = $DocumentosPDF[$d]["TipoDocumento"];
                    $name_arch = $NombDocumento . "_firm.pdf";
                    $DocumentoFirmado = $DocumentosPDF[$d]["DocumentoFirmado"];
                    //decodifica archivo
                    $desc_arch = base64_decode($DocumentoFirmado);
                    file_put_contents(RUTA_ARCHIVOS . "proveedores/$Identificacion/$ano_carga/$name_arch", $desc_arch);
                }

                $consulta = "begin work";
                $this->conexion->ejecutar_consulta($consulta);
				
                $consulta = "update tab_ctrl_apro_prov_web  set arch_anti_corr_firm_prov = 'X',hora_firm_arch =current, apro_firm_prov = 'X',hora_firm_prov=current ,obse_apro='El proveedor firma los documentos',acti_esta='FIRM' where nume_iden_dilg = '$Identificacion' and acti_esta = 'APR'; ";
                $this->conexion->ejecutar_consulta($consulta);
                fwrite($file, "Consulta Proveedor: " . $consulta . "\n");

				
					//Nuevo --Enviar correo al responsable UEN y oficial de cumplimiento
                    $query = "select token from tab_ctrl_apro_prov_web where nume_iden_dilg = '$Identificacion' and acti_esta = 'FIRM'";
                    fwrite($file, "query1: " . print_r($query, true) . "\n");
                    $resu = $this->conexion->ejecutar_consulta($query);
                    $token = isset($resu[0]['token']) ? trim($resu[0]['token']) : "";
					
					$query = "update tab_gene_link_prov set acti_esta='COM' where token = '$token' and nume_iden = '$Identificacion'";
					fwrite($file, "query2: " . print_r($query, true) . "\n");
                    $this->conexion->ejecutar_consulta($query);

                    $correos = "oficial.cumplimiento@azzorti.co";
                    $query = "select mail_usua_gene_link from tab_gene_link_prov where token = '$token' and nume_iden = '$Identificacion'";
                    fwrite($file, "query3: " . print_r($query, true) . "\n");
                    $resu = $this->conexion->ejecutar_consulta($query);
                    $mail_usua_gene_link = isset($resu[0]['mail_usua_gene_link']) ? trim($resu[0]['mail_usua_gene_link']) : "";
                    if (!empty($mail_usua_gene_link)) {
                        $correos .= "," . trim($mail_usua_gene_link);
                    }
					
					
					require_once $_SERVER['DOCUMENT_ROOT'] . "/form_regi_prov_sap/firmaDigital.php";
					$envia = new firmaDigital("","");
					$envia->envia_correo_firmaDocu($correos,$nume_iden_dilg);
					
				
				$consulta = "commit work";
                $this->conexion->ejecutar_consulta($consulta);
 

                //Nuevo, registrar datos de la tabla temporal a la tabla proveedor
                /* $query = "select cons_prov_web from tab_ctrl_apro_prov_web where nume_iden_dilg = '$Identificacion' and acti_esta = 'APR'";
                  $resu = $this->conexion->ejecutar_consulta($query);
                  $cons_prov_web = isset($resu[0]['cons_prov_web']) ? trim($resu[0]['cons_prov_web']) : "";

                  require_once($_SERVER['DOCUMENT_ROOT']."/form_regi_prov_sap/firmaDigital.php");

                  $proveedor = new firmaDigital($this->conexion,$cons_prov_web);

                  $proveedor->registra_proveedor(); */
            } else {
                //cliente
                $nomb_pdf = "$Identificacion.pdf";
                $nomb_docu = "{$NroSolicitud}_{$Identificacion}";

                file_put_contents(RUTA_TEMPORALES . $nomb_pdf, $desc_arch);
                $consulta = "begin work";
                $resu = $this->conexion->ejecutar_consulta($consulta);
                $consulta = "update pre_ases_expe set codi_apro = 'APR' where nume_iden = '$Identificacion';                        ";

                $resu = $this->conexion->ejecutar_consulta($consulta);
                fwrite($file, "Consulta : " . $consulta . "\n");
                $consulta = "commit work";
                $resu = $this->conexion->ejecutar_consulta($consulta);
                //enviar ws

                $celu_terc = $Firmante[0]["NroCelular"];
                $mensaje = "Gracias por firmar el documento, *Adjuntamos una copia*.";
                $url = $this->ruta_pdf . $nomb_pdf;

                $this->guardaPdf($NroSolicitud, $Identificacion, $desc_arch);

                $this->envi_msn_what($celu_terc, $mensaje);
                $this->envi_arch_what($celu_terc, $url, $nomb_docu);
            }
        }
		

					


        fwrite($file, "Token empresa:" . $token_empresa . " - Token: " . print_r($getHeaders['Token'], true) . "\n");
        if ($token_empresa != $getHeaders['Token']) {
            http_response_code(404);
            $mensaje = '{"error":"Usuario no autorizado"}';
            return $mensaje;
        }

        http_response_code(200);
        $mensaje = '{"mensaje":"Notificacion enviada con exito"}';
		


        return $mensaje;
    }

    /**
     * Recibe datos del servicio Risk firma digital y envia el codigo
     * al correo y a whatsapp
     *
     * @param Array $data
     * @return string
     */
    public function recibeEnviaOTP($data) {
        /* $data = '{
          "NroReferencia": "1021",
          "NroSolicitud": 1001,
          "Identificacion": "1013651642",
          "TipoIdentificacion": "CC",
          "Nombre": "nombre completo",
          "Correo": "dsmartinezva@gmail.com",
          "NroCelular": "3185551972",
          "OTP": 5243
          }';

          $data = json_decode($data,true); */
        $data = array_map("trim", $data);
        $this->escribeLog("Request envio otp", $data);

        $mensaje = "Para continuar con el proceso escribe el siguiente codigo:\n\n" .
                "*{$data["OTP"]}* \n\n" .
                "Con la devolución de este código confirmo que " .
                "*AUTORIZO LA CONSULTA DE MIS DATOS FINANCIEROS Y DE CONTACTO ANTE CENTRALES " .
                "DE INFORMACIÓN POR PARTE DE INDUSTRIAS INCA S.A.S. para convertirme en " .
                "asesora de la compañía y _declaro que leí y entendí la autorización integral_ " .
                "que me fue informada por la gerente de zona* \n\n *Equipo Azzorti*";

        //proveedores --NUEVO OSCAR 10 FEBRERO 2022
        $consulta = "select cons_prov_web from tab_ctrl_apro_prov_web where nume_iden_dilg='" . trim($data["Identificacion"]) . "';";
        $resu_prov = $this->conexion->ejecutar_consulta($consulta);

        //VALIDA SI EL NUMERO DE INDETIFICACION EXISTE EN LA TABLA DE CONTRO ES PORQUE ES UN PROVEEDOR, SOLO ENVIAR CODIGO AL CORREO
        $proveedor = true;
        if (count($resu_prov) == 0) {
            $this->envi_msn_what($data["NroCelular"], $mensaje);
            $this->inse_audi_expe($data);
            $proveedor = false;
        }

        $this->envi_corr_otp($data, $proveedor);

        http_response_code(200);
        $mensaje = '{"mensaje":"Codigo otp enviado con exito"}';
        return $mensaje;
    }

    /**
     * Registra la info recibida en la tabla auditoria
     *
     * @param Array $data
     * @return void
     */
    private function inse_audi_expe($data) {
        $consulta = "begin work";
        $this->conexion->ejecutar_consulta($consulta);
        $quer = "insert into
        audi_expe(
                TRAN_PROC,
                NUME_IDEN,
                NUME_TRAN,
                TRAN_FLUJ,
                TRAN_RESP,
                CODI_CAMP,
                FECH_SOLI
            )
        VALUES
            (
                'risk_otp',
                '{$data["Identificacion"]}',
                '{$data["NroSolicitud"]}',
                '{$data["NroCelular"]}',
                '{$data["OTP"]}',
                '',
                current
            );";
        $this->conexion->ejecutar_consulta($quer);
        $consulta = "commit work";
        $this->conexion->ejecutar_consulta($consulta);
    }

    /**
     * Envia un mensaje de whatsapp
     *
     * @param string $celu
     * @param string $mens
     * @return void
     */
    private function envi_msn_what($celu, $mens) {
        $msnWhat = array(
            "nume_celu" => "+57$celu",
            "mens_envi" => $mens
        );
        $this->escribeLog("Envio mensaje whatsapp", $msnWhat);
        $this->whatsapp->enviamensaje($msnWhat);
    }

    /**
     * Enviar un archivo por whatsapp
     *
     * @param string $celu Numero telefono destinatario
     * @param [type] $url_arch Url http del archivo a enviar
     * @return void
     */
    private function envi_arch_what($celu, $url_arch, $nomb_arch) {
        $msnWhat = array(
            "ruta" => "$url_arch",
            "nomb_arch" => $nomb_arch,
            "nume_celu" => "+57$celu"
        );
        $this->escribeLog("Envio archivo whatsapp", $msnWhat);
        $this->whatsapp->enviaarchivo($msnWhat);
    }

    /**
     * Envia correo electronico con el codigo OTP de la firma digital.
     *
     * @param [type] $data
     * @return void
     */
    private function envi_corr_otp($data, $proveedor = false) {
        include_once ("cadena.php");

        $corr = array(
            "de" => "No responder <noresponder@dupree.com.co>",
            "asunto" => "Codigo de confirmación",
            "correo" => $data["Correo"],
            "nombre" => $data["Nombre"],
            "nume_iden" => $data["Identificacion"],
            "OTP" => $data["OTP"],
            "solicitud" => $data["NroSolicitud"]
        );

        ob_start();
        //VALIDA SI EL NUMERO DE INDETIFICACION EXISTE EN LA TABLA DE CONTRO ES PORQUE ES UN PROVEEDOR, INSERTA PLANTILLA DE PROVEDOR
        if ($proveedor) {
            include($_SERVER['DOCUMENT_ROOT'] . "/form_regi_prov_sap/mail_code.php");
        } else {
            include("template/mail.php");
        }

        $corr_plan = ob_get_clean();
        ob_end_flush();

        $this->escribeLog("Envio codigo otp correo", $corr);
        enviar_correo($corr["de"], trim($data["Correo"]), $corr["asunto"], $corr_plan, $archivos);
    }

    /**
     * Escribe en el archivo log
     *
     * @param string $titulo Titulo de la cabecera
     * @param mixed $info Información a imprimir
     * @return void
     */
    public function escribeLog($titulo, $info) {
        $file = fopen($this->logFile, "a");
        $hora = date("m-d-Y h:i:sa");
        $cabeAscii = "\n*----------------------------------------------------------------------------*\n";
        $cabeAscii .= "*  {$titulo} - {$hora} \n";
        $cabeAscii .= "*----------------------------------------------------------------------------*\n";
        fwrite($file, $cabeAscii);
        fwrite($file, print_r($info, true));
        fclose($file);
    }

    /**
     * Guarda el documento firmado en la ruta permanente
     *
     * @param string $nombre
     * @param Base64 $archivo
     * @return void
     */
    public function guardaPdf($solicitud, $nume_iden, $archivo) {
        $solicitud = trim($solicitud);
        $nume_iden = trim($nume_iden);

        $quer = "select codi_camp as codi_camp from pre_ases_expe where nume_iden='$nume_iden'";
        $camp = $this->conexion->ejecutar_consulta($quer)[0]["codi_camp"];
        $camp = trim($camp);

        $quer = "UPDATE pre_ases_expe SET tran_firm = '$solicitud' WHERE nume_iden='$nume_iden'";
        $this->conexion->ejecutar_consulta($quer);

        $fold_base = "{$this->ruta_perm_pdf}$camp";

        if (!file_exists($fold_base)) {
            mkdir($fold_base, 0777, true);
        }
        $nomb_arch = "{$solicitud}_{$nume_iden}.pdf";
        $this->escribeLog("Guarda pdf en archivos", $fold_base . "/$nomb_arch");

        file_put_contents($fold_base . "/$nomb_arch", $archivo);
    }

}

?>
