<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require ("src/Respuesta.php");

/*
  /* @author           erika_lancheros
  /* @name             Form_veri_pre_ases.php
  /* @version          1.0
  /* @abstract         Clase Form_veri_pre_ases
  /*
  /***** */

class Form_veri_pre_ases extends CI_Controller {
    /**
     * Retorna respuesta http
     */
    use Respuesta;

    private $ruta_pdf = "https://alcor2col.azzorti.co/archivos/clientes/vinculacion/";
    //private $ruta_pdf = "https://alcor.dupree.co/archivos/clientes/vinculacion/";

    public function __construct() {
        parent::__construct();
        $this->base = base_url();
        $this->segment = "form_veri_pre_ases";
        $this->var_pin = $this->globals->post();
        $this->load->model($this->segment . '/M_' . $this->segment);
        foreach ($this->var_pin as $keys => $values) {
            $this->$keys = $values[0];
        }
        /*
         * Variables formulario
         */
        $this->load->library("libGeneral");
        $this->lib = new CI_LibGeneral();
        if (isset($_REQUEST["campos"]))
            $this->lib->variables($_REQUEST["campos"]);
    }
    /**
     * Pinta la información en los datagrid
     *
     * @return void
     */
    public function pre_carga() {
        $filt = '';
        $array = $this->M_form_veri_pre_ases->permisos_internet();
        $codi_zona = isset($array[1]) ? $array[1] : 951;
        $trae_camp = $this->M_form_veri_pre_ases->trae_camp();
        foreach ($trae_camp as $camp_actu) {
            $codi_camp = $camp_actu['codi_camp'];
            $codi_camp = trim($codi_camp);
            $nume_iden = $camp_actu['nume_iden'];
            $nume_iden = trim($nume_iden);
            $codi_zona = $camp_actu['zona_geo'];
            $codi_zona = trim($codi_zona);
            $codi_cort = $camp_actu['codi_cort'];
            $codi_cort = trim($codi_cort);

            //cuenta regresiva
            $cuen_regr = $this->M_form_veri_pre_ases->vali_cuen_regr($codi_camp, $codi_zona, $codi_cort);
            foreach ($cuen_regr as $cuen_regrf) {
                $fecha_inicial = $cuen_regrf['fech_ca21'];
                $fecha_inicial = date("m/d/Y", strtotime($fecha_inicial));
                // fecha actual
                $fecha_final = date("m/d/Y");
                $dias = (strtotime($fecha_inicial) - strtotime($fecha_final)) / 86400;
                $dias = abs($dias);
                $dias = floor($dias);
                $this->M_form_veri_pre_ases->actu_cuen_regr($codi_camp, $nume_iden, $dias);
            }
        }

        /* Metodo que muestra los datos de la tabla pre_ases_expe */
        $data_pre = $this->M_form_veri_pre_ases->trae_data_temp($filt);

        $mensaje['pinta_segmento']['segmento0']['values'] = $data_pre;

        $data_expe = $this->M_form_veri_pre_ases->trae_data_expe();
        $mensaje['pinta_segmento']['segmento1']['values'] = $data_expe;


        /* inserción de clientes en la tabla tab_clie */
        // $this->M_form_veri_pre_ases->inse_tab_clie();

        mensaje($mensaje);
    }

    public function pre_proc_prin() {
        //Your code here and enjoy!!!
    }

    public function guardar_general() {
        //Your code here and enjoy!!!
    }

    public function pre_actu_deta() {
        //Your code here and enjoy!!!
    }

    public function pos_actu_deta() {
        //Your code here and enjoy!!!
    }

    public function pre_dele_deta() {
        //Your code here and enjoy!!!
    }

    public function pos_dele_deta() {
        //Your code here and enjoy!!!
    }

    /* Metodo que realiza el cargue de datos por campaña seleccionada */

    // public function codi_camp()
    // {
    // $codi_camp=$this->var_codi_camp['valo_camp'];
    // if(!empty($codi_camp)){
    //Metodo que trae los datos de la tabña ára realizar la georeferenciación
    // }
    // }
    /* Metodo que realiza la consulta por cortes */
    public function btn_busc() {
        $codi_zona = $this->var_codi_zona['valo_camp'];
        $codi_cort = $this->var_codi_cort['valo_camp'];
        $filt = '';
        if (!empty($codi_zona)) {
            $filt .= "and pre_ases_expe.zona_geo='$codi_zona'";
        }
        if (!empty($codi_cort)) {
            $filt .= "and pre_ases_expe.codi_cort ='$codi_cort '";
        }

        /* Metodo que muestra los datos de la tabla pre_ases_expe */
        $data_pre = $this->M_form_veri_pre_ases->trae_data_temp($filt);
        if (empty($data_pre)) {
            $filt = '';
            $mensaje["var_pin"][0]["codi_zona"] = "";
            $mensaje["var_pin"][0]["codi_cort"] = "";
            $mensaje['mensaje'] = "No existen registros con los parametros ingresados";
            $data_pre = $this->M_form_veri_pre_ases->trae_data_temp($filt);
            $mensaje['pinta_segmento']['segmento0']['values'] = $data_pre;
            mensaje($mensaje);
        } else {
            $mensaje['pinta_segmento']['segmento0']['values'] = $data_pre;
            mensaje($mensaje);
        }
    }

    /* Metodo que realiza la aprobación de las asesoras */

    public function apro_ases() {
        $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : "";
        $nume_iden = isset($_POST['nume_iden']) ? $_POST['nume_iden'] : "";
        $codi_camp = isset($_POST['codi_camp']) ? $_POST['codi_camp'] : "";
        $zona = isset($_POST['zona']) ? $_POST['zona'] : "";
        $sect = isset($_POST['sect']) ? $_POST['sect'] : "";
        $cedu_lide = isset($_POST['cedu_lide']) ? $_POST['cedu_lide'] : "";

        $zona = trim($zona);
        $sect = trim($sect);
        $cedu_lide = trim($cedu_lide);

        $this->vali_data_sect($cedu_lide,$zona,$sect);

        //$datos = $this->M_form_veri_pre_ases->Vali_exis_bloq($nume_iden);

        // $bloqueos=isset($datos[0]['obse_data'])?$datos[0]['obse_data']:"";
        // $bloqueos=trim($bloqueos);
        // if(empty($bloqueos))
        // {
        // $mensaje['error']="valide por favor los bloqueos asociados del documento $nume_iden ";
        // mensaje($mensaje);
        // }
        // else if($bloqueos=='existe')
        // {
        // $mensaje['error']="el documento $nume_iden tiene bloqueos";
        // mensaje($mensaje);
        // }
        // else
        // {
        //Metodo que realiza la insercion a la temporal
        $this->M_form_veri_pre_ases->inse_temp($nume_iden, $codi_camp,$cedu_lide,$zona,$sect);
        $mensaje["confirmar"]["titulo"] = "Desea Aprobar La Solicitud Del Cliente<br>$nombre";
        $mensaje["confirmar"]["accion"] = "aprob_ases";
        $mensaje["confirmar"]["cancelar"] = "elim_data_temp";
        mensaje($mensaje);
        // }
    }

    /* Metodo que realiza la eliminacion de datos de la tabla temporal cancelar la aprobacion de la asesora */

    public function elim_data_temp() {
        $this->M_form_veri_pre_ases->elim_temp();
    }

    /* Metodo que realiza la probación de la asesora , genera el codigo de seguridad */

    public function aprob_ases() {
        $data_apro = $this->M_form_veri_pre_ases->vali_data_apro();
        $data_apro_trim = array_map("trim", $data_apro[0]);

        $nume_iden = isset($data_apro_trim['ni2']) ? $data_apro_trim['ni2'] : "";
        $cedu_lide = isset($data_apro_trim['cedu_lide']) ? $data_apro_trim['cedu_lide'] : "";
        $zona = isset($data_apro_trim['zona_gere']) ? $data_apro_trim['zona_gere'] : "";
        $sect = isset($data_apro_trim['sect_gere']) ? $data_apro_trim['sect_gere'] : "";

       /*  $corr_ases = isset($data_apro_trim['cce17']) ? $data_apro_trim['cce17'] : "";
        $nombre = isset($data_apro_trim['pn7']) ? $data_apro_trim['pn7'] : "";
        $telefono = isset($data_apro_trim['cel14']) ? $data_apro_trim['cel14'] : "";
        $dire_terc = isset($data_apro_trim['dir11']) ? $data_apro_trim['dir11'] : "";
        $nomb_ciud = isset($data_apro_trim['ciu13']) ? $data_apro_trim['ciu13'] : "";
        $nomb_barr = isset($data_apro_trim['bar15']) ? $data_apro_trim['bar15'] : "";
        $codi_zona = isset($data_apro_trim['znc901']) ? $data_apro_trim['znc901'] : "";
        $codi_camp = isset($data_apro_trim['codi_camp']) ? $data_apro_trim['codi_camp'] : ""; */

        //campos lider



        // $codi_camp=$this->var_codi_camp['valo_camp'];

        /*$codigo = $this->genera_codigo(4);

        require("curl/autoload.php");
        $enlace = rawurlencode("https://pedidos2col.azzorti.co/pedidos/pedidos/experian/codigo_verificacion/");
        $texto = rawurlencode("Buen dia $nombre, su código de verificación de pre-inscripción es $codigo termina tu resgitro en el siguiente enlace $enlace<br>Muchas gracias por su atención<br>Cordialmente<br>Dupree by azzorti.");

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://servicioweb2col.azzorti.co/envioSMS/envioSimpre/$telefono/$texto/57",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));
        $response = curl_exec($curl);

        curl_close($curl);
        $datos = json_decode($response);
        if (!empty($datos)) {
            $this->M_form_veri_pre_ases->apro_ases($codigo, $nume_iden);
            include_once("cadena.php");
            $mail_ases = "$corr_ases";
            $asunto = html_entity_decode("Código de verificación");
            $mensaje_email = html_entity_decode("Buen dia <br> <br> $nombre, su código de verificación de pre-inscripción es $codigo termina tu resgitro en el siguiente enlace https://pedidos2col.azzorti.co/pedidos/pedidos/experian/codigo_verificacion/");
            $mensaje_email .= html_entity_decode("<p>Muchas gracias por su atención</p>");
            $mensaje_email .= "<p>Cordialmente</p>";
            $mensaje_email .= "<p>Dupree by azzorti.</p>";
            $de = "No responder <noresponder@dupree.com.co>";
            $body_top = "";
            $cuerpo = $body_top . $mensaje_email;
            enviar_correo($de, $mail_ases, $asunto, $cuerpo);
            $filt = '';
            $data_pre = $this->M_form_veri_pre_ases->trae_data_temp($filt);
            $mensaje['pinta_segmento']['segmento0']['values'] = utf8_converter($data_pre);
            $mensaje['mensaje'] = "Se realizo la aprobación correctamente";
            mensaje($mensaje);
        } else {
            $mensaje['error'] = "Se presento error en el envio del codigo";
            mensaje($mensaje);
        }*/
        $this->M_form_veri_pre_ases->inse_tab_clie($nume_iden,$cedu_lide,$zona,$sect);
        $mensaje['mensaje'] = "Datos guardados con exito.";
        mensaje($mensaje);
    }

    /**
     * Carga la modal para seleccionar rechazos.
     * Rechaza la solicitud de credito.
     */
    public function rech_docu() {
        extract($_POST);
        $camp_conf = (!isset($camp_conf)) ? 0 : $camp_conf;

        if ($camp_conf == 0) {
            $_SESSION["codi_camp"] = $codi_camp;
            $_SESSION["nume_iden"] = $nume_iden;
            $_SESSION["nombre"] = $nombre;
            $html = $this->M_form_veri_pre_ases->CargaModal($codi_camp, $nume_iden, $nombre);
            $mensaje["mensaje"] = $html;
            $mensaje["confirmar"]["titulo"] = "$html";
            $mensaje["confirmar"]["accion"] = "rech_docu";
            $mensaje["confirmar"]["cancelar"] = "";
        } else {
            $filt = '';
            $mensaje["mensaje"] = $this->M_form_veri_pre_ases->RechazaSolicitud($_SESSION["codi_camp"], $_SESSION["nume_iden"]);
            $data_pre = $this->M_form_veri_pre_ases->trae_data_temp($filt);
            $mensaje['pinta_segmento']['segmento0']['values'] = utf8_converter($data_pre);
        }
        mensaje($mensaje);
    }

    /**
     * Inserta/Borra el rechazo de la tabla temporal.
     */
    public function proc_rech() {
        extract($_POST);
        $this->M_form_veri_pre_ases->CargaRechazo($codi_camp, $nume_iden, $codi_rech, $ctrl);
    }

    /* Metodo que genera el codigo de seguridad
     * @param $longitud  integer longitud del codigo de seguridad
     */

    function genera_codigo($longitud) {
        $caracteres = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
        $codigo = '';

        for ($i = 1; $i <= $longitud; $i++) {
            $codigo .= $caracteres[$this->numero_aleatorio(0, 9)];
        }
        return $codigo;
    }

    function numero_aleatorio($ninicial, $nfinal) {
        $numero = rand($ninicial, $nfinal);
        return $numero;
    }

    /* metodo que realiza la apertura de la modal de sanciones */

    public function modal_sanci() {
        $acti_usua = codi_usua();
        $nume_iden = isset($_POST['nume_iden']) ? $_POST['nume_iden'] : "";
        $nombres = isset($_POST['nombres']) ? $_POST['nombres'] : "";
        $apellidos = isset($_POST['apellidos']) ? $_POST['apellidos'] : "";
        $cons_terc = isset($_POST['nume_iden']) ? $_POST['nume_iden'] : "";
        $nombres = trim($nombres);
        $apellidos = trim($apellidos);

        $mensaje['location'] = '/desarrollo/cgis/cons_clie.php?proceso=GENERAR&nume_iden=' . $nume_iden . '&programa=experian';
        mensaje($mensaje);
    }

    /* Metodo que realiza el cargue del archivo de respuesta por parte de experian */

    public function carg_arch() {
        $carg_arch_plan = $this->var_carg_arch['valo_camp'];
        if (strpos($carg_arch_plan, ".xls") !== false) {
            $cargue = $this->M_form_veri_pre_ases->carg_arch_plan($carg_arch_plan);
            $mensaje["limpiar"] = "Inserción de datos correcta";
            $filt = '';
            $data_pre = $this->M_form_veri_pre_ases->trae_data_temp($filt);
            $mensaje['pinta_segmento']['segmento0']['values'] = $data_pre;
            $data_expe = $this->M_form_veri_pre_ases->trae_data_expe();
            $mensaje['pinta_segmento']['segmento1']['values'] = $data_expe;
        } else {
            $mensaje["error"] = "Unicamente se aceptan archivos en formato <b>xls</b>, verifique";
        }
        mensaje($mensaje);
    }
    /**
     * Consulta lider
     *
     * @return Array cedula nombre y zona
     */
    public function carg_lide(){
        $request = $_POST;
        $request = array_map("trim",$request);

        $vali_cedu = $request["filt"]==="cedula";
        $vali_sect = $request["filt"]==="zona_sect";
        if($vali_cedu){
            $lide = $this->M_form_veri_pre_ases->cons_lide_cedu($request);
            $resp["error"] = "Cedula de lider no existe o no petenece a la zona {$request['zona']}";
        }else if($vali_sect){
            $lide = $this->M_form_veri_pre_ases->cons_lide_sect($request);
            $resp["error"] = "Para la zona: {$request['zona']} y Sector: {$request['sect']} no se encontrar registros.";
        }


        if(count($lide) !== 0){
            $resp["mensaje"] = $lide[0];
        }

        $this->respuesta(200,$resp);
    }
    private function vali_data_sect($cedu_lide,$zona,$sect){
        $mensaje["error"] = "";
        if(strlen($cedu_lide) === 0){
            $mensaje["error"] = "Debe ingresar cedula lider";
        }else if(strlen($zona) === 0){
            $mensaje["error"] = "Debe ingresar zona";
        }else if(strlen($sect) === 0){
            $mensaje["error"] = "Debe ingresar sector de la asesora";
        }

        if(strlen($mensaje["error"])!== 0){
            mensaje($mensaje);
            die();
        }
    }
}
