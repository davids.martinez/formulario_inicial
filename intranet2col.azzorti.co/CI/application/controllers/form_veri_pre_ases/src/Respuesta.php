<?php
/**
 * Trait que maneja las respuestas al cliente
 */
trait Respuesta{
    /**
     * Genera una respuesta al navegador
     *
     * @param int $codigo
     * @param mixed $respuesta
     * @param boolean $json
     * @return void
     */
    private function respuesta($codigo,$respuesta,$json=true){
        if($json){
            header('Content-Type: application/json');
            http_response_code($codigo);
        }

        mensaje($respuesta);

        die();
    }
}
?>