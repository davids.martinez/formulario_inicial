<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


/* * *** */
/*
  /* @author           erika_lancheros
  /* @name             M_form_veri_pre_ases.php
  /* @version          1.0
  /* @abstract         Clase M_form_veri_pre_ases
  /*
  /***** */

class M_form_veri_pre_ases extends CI_model {
    /**
     * Ruta archivo PDF firmado
     *
     * @var string
     */
    private $rutaPdf = "/disco1/disco2/archivos/alcor2col.azzorti.co/clientes/vinculacion/";
    //private $rutaPdf = "/disco1/disco2/archivos/alcor.dupree.co/clientes/vinculacion/";

    public function __construct() {
        parent::__construct();
        $this->ifx_inca = $this->load->database('ifx_inca', TRUE);
        $this->var_pin = $this->globals->post();
        foreach ($this->var_pin as $keys => $values) {
            $this->$keys = $values[0];
        }
        /*
         * Variables formulario
         */
        $this->load->library("libGeneral");
        $this->lib = new CI_LibGeneral();
        if (isset($_REQUEST["campos"]))
            $this->lib->variables($_REQUEST["campos"]);
    }

    /* Metodo que trae los valores de la tabla pre_ases_expe con las preinscripciones de las asesoras
     * @param $codi_camp campaña insertada por el usuario
     * @param $codi_zona integer zona de georeferenciación de la asesora
     * @return  $data_pre array retorna los valores de las asesoras preinscritas
     */

    public function trae_data_temp($filt) {
        $query = "delete from  temp_rech_ases ";
        $this->ifx_inca->query($query);

        $query = "
            select
                trim(p.codi_camp) || '/' || p.TRAN_FIRM || '_' || trim(p.nume_iden) || '.pdf' as pdf,
                ''::char(15) as docu_lide,
                ''::char(50) as nomb_lide,
                p.ZONA_GEO zona,
                CASE
                    when p.ZONA_GEO <> '999' then SUBSTR(p.zona_sect, 4)
                END as secc,
                p.codi_camp as codi_camp,
                p.fech_envi as fech_envi,
                CASE
                    WHEN td.cons_docu = 3 THEN 'CC'
                    WHEN td.cons_docu = 5 THEN 'CE'
                    ELSE ''
                END AS TD1,
                p.nume_iden as NI2,
                p.prim_nomb as PN7,
                p.segu_nomb as SN8,
                p.prim_apel as PA9,
                p.segu_apel as SA10,
                p.corr_ases as CCE17,
                p.fech_naci as FN6,
                p.sexo as MF88,
                d.nomb_dpto as DPT12,
                c.nomb_ciud as CIU13,
                p.celu_ases as CEL14,
                p.LIDE_REFE as RF99,
                p.dire_ases as DIR11,
                b.nomb_barr as BAR15,
                p.dire_entre as DRE905,
                '<span class=\"glyphicon glyphicon-remove-sign\" aria-hidden=\"true\" style=\"padding-left: 5px;\"></span>' camp_rech,
                '<span class=\"glyphicon glyphicon-ok-sign\" aria-hidden=\"true\" style=\"padding-left: 5px;\"></span>' camp_apro,
                '<span class=\"glyphicon glyphicon-eye-open\" aria-hidden=\"true\" style=\"padding-left: 60px;align-content:center;\"></span>' clie_sanc,
                p.cuen_regre as cuen_regr,
                p.zona_geo as znc901,
                p.vali_geo as vali_geo,
                '$' || '' || round(p.sald_cart, 2) as cart_cast,
                CASE
                    WHEN td.cons_docu = 3 THEN 'N/A'
                    WHEN td.cons_docu = 5 THEN 'SI'
                    ELSE ''
                END AS pedi_prep,
                b.cons_barr
            from
                pre_ases_expe as p,
                tab_dpto as d,
                tab_ciud as c,
                tipo_docu as td,
                tab_barr as b
            where
                p.cons_dpto = d.cons_dpto
                and p.cons_ciud = c.cons_ciud
                and td.cons_docu = p.tipo_docu
                and b.cons_barr = p.nomb_barr
                and p.esta_fina is null
                AND p.ESTA_ASES <> 'VALIDAIDENTIDAD'
                $filt into temp ases_expe_1;
        ";

        $data_pre = $this->ifx_inca->query($query);
        $query = "
            select
                e1.NI2 as nume_iden,
                cl.nume_iden as docu_lide1,
                trim(cl.nomb_terc) || ' ' || trim(cl.apel_terc) as nomb_lide1,
                SUBSTR(b.codi_terr, 1, 3) as zona1,
                SUBSTR(b.codi_terr, -3) as secc1
            from
                ases_expe_1 e1,
                tab_barr b,
                tab_clie cl
            where
                e1.cons_barr = b.cons_barr
                and b.CONS_LIDE = cl.cons_terc
                and e1.zona = '999'
            UNION ALL
            select
                e1.NI2 as nume_iden,
                cl.nume_iden as docu_lide1,
                trim(cl.nomb_terc) || ' ' || trim(cl.apel_terc) as nomb_lide1,
                s.codi_zona as zona1,
                s.codi_sect as secc1
            from
                ases_expe_1 e1,
                tab_clie cl,
                tab_sect s
            where
                e1.zona <> '999'
                and s.cons_lide = cl.cons_terc
                and s.codi_zona = e1.zona
                and s.codi_sect = e1.secc into temp ases_expe_2;";
        $data_pre = $this->ifx_inca->query($query);
        $query = "
            MERGE INTO ases_expe_1 e1
            USING ases_expe_2 e2
            ON e1.NI2 = e2.nume_iden
            WHEN MATCHED THEN
                UPDATE SET
                    e1.docu_lide = e2.docu_lide1,
                    e1.nomb_lide = e2.nomb_lide1,
                    e1.zona=e2.zona1,
                    e1.secc=e2.secc1;";
        $data_pre = $this->ifx_inca->query($query);
        $query="select * from ases_expe_1;";
        $data_pre = $this->ifx_inca->query($query);
        $data_pre = $data_pre->result_array();

        for($i=0;$i<count($data_pre);$i++){
            $cedu = trim($data_pre[$i]['ni2']);
            $arch = $this->rutaPdf . $data_pre[$i]['pdf'];
            $exis = file_exists($arch);

            if($exis){
                $data_pre[$i]['pdf'] = "
                <span
                    class='glyphicon glyphicon-eye-open'
                    aria-hidden='true'
                    style='padding-left: 5px;'
                    data-cedu='{$data_pre[$i]['pdf']}'
                ></span>";
            }else{
                $data_pre[$i]['pdf'] = "";
            }
        }
        return $data_pre;
    }

    /**
     * Aprueba la solicitud de credito.
     * @param $codi_camp integer  Campana
     * @param $nume_iden char(15) Cedula
     */
    public function ApruebaSolicitud($codi_camp, $nume_iden,$cedu_lide,$zona,$sect) {
        /**
         * Inclusion Funciones consecutivo y geo
         */
        $conexion1 = array(
            "iden_cone" => $this->ifx_inca->conn_id,
            "ruta_clase" => $_SERVER['DOCUMENT_ROOT'] . "/funciones/fun_calcula_digi_veri.php"
        );
        $this->load->library("/Generales/Cgis");
        new CI_Cgis($conexion1, array());
        $conexion2 = array(
            "iden_cone" => $this->ifx_inca->conn_id,
            "ruta_clase" => $_SERVER['DOCUMENT_ROOT'] . "/funciones/fun_calcula_doble_digito.php"
        );
        $this->load->library("/Generales/Cgis");
        new CI_Cgis($conexion2, array());
        $conexion3 = array(
            "iden_cone" => $this->ifx_inca->conn_id,
            "ruta_clase" => $_SERVER['DOCUMENT_ROOT'] . "/funciones/fun_halla_codi_terc.php"
        );
        $this->load->library("/Generales/Cgis");
        new CI_Cgis($conexion3, array());
        $conexion4 = array(
            "iden_cone" => $this->ifx_inca->conn_id,
            "ruta_clase" => $_SERVER['DOCUMENT_ROOT'] . "/funciones/fun_geo_clie.php"
        );
        $this->load->library("/Generales/Cgis");
        new CI_Cgis($conexion4, array());

        global $cons_barr;

        $query = "BEGIN WORK";
        $this->ifx_inca->query($query);

        $query = "SET LOCK MODE TO WAIT";
        $this->ifx_inca->query($query);

        $query = "lock table tab_clie in exclusive mode ;";
        $this->ifx_inca->query($query);

        $query = "lock table referido in exclusive mode ;";
        $this->ifx_inca->query($query);

        $query = "select max(cons_terc) +1 as cons_terc from tab_clie;";
        $resu = $this->ifx_inca->query($query);
        $resu = $resu->result_array();

        //valores
        $cons_terc = $resu[0]['cons_terc'];
        $acti_usua = codi_usua();
        $fech_sist = fech_hora_sist_ifx();

        $query = "select * from tab_clie where nume_iden='$nume_iden';";
        $resu = $this->ifx_inca->query($query);
        $resu = $resu->result_array();
        $tuplas_clie = count($resu);
        if ($tuplas_clie > 0) {
            $esta_form = 1;
        } else {
            $esta_form = 0;
        }

        if ($esta_form == 0) {
            $query = "select * from pre_ases_expe WHERE codi_camp='$codi_camp' AND nume_iden='$nume_iden' ;";
            $resu = $this->ifx_inca->query($query);
            $resu = $resu->result_array();

            //valores
            $acti_esta = "A";
            $acti_hora = "current";
            $actu_dato = "0";
            $actu_soli = "S";
            $prim_apel = $resu[0]['prim_apel'];
            $prim_apel = trim($prim_apel);
            $segu_apel = $resu[0]['segu_apel'];
            $segu_apel = trim($segu_apel);
            $apel_terc = $prim_apel . " " . $segu_apel;
            $auto_port = "N";
            $cons_barr = $resu[0]['nomb_barr'];
            $cons_barr_entr = $cons_barr;
            $cedu_refe = $resu[0]['lide_refe'];

            if ($cons_barr_entr == 0) {
                $cons_barr_entr = $cons_barr;
            }
            $camp_ingr = $codi_camp;
            $cata_camp = 0;
            $celu_ter1 = $resu[0]['celu_ases'];
            $celu_entr = $celu_ter1;
            $celu_ter2 = "";
            $cons_ciud_entr = $resu[0]["cons_ciud"];
            $clav_ases = $nume_iden;
            $codi_desp = $resu[0]['zona_geo'];

            //lider barrios
            $query_l = "select * from  tab_barr where cons_barr='$cons_barr';";
            $resu_l = $this->ifx_inca->query($query_l);
            $resu_l = $resu_l->result_array();
            $query_li = "select cons_terc from tab_clie where nume_iden='$cedu_lide'";
            $resu_li = $this->ifx_inca->query($query_li);
            $resu_li = $resu_li->result_array();
            $cons_lide = $resu_li[0]['cons_terc'];
            $codi_terr = $zona . $sect;
            $codi_zona = $zona;
            $nomb_barr = $resu_l[0]['nomb_barr'];

            global $codi_terc;
            $codi_terc = $codi_terr;

            fun_halla_codi_terc($codi_terr, $cons_terc);
            $cond_pago = 0;
            $cons_bloq = 0;
            $cons_carg = 0;
            $cons_ciud = $resu[0]["cons_ciud"];
            $cupo_cred = $this->cupoCredito($cons_terc, $codi_zona);
            $codi_dest = "R";
            $dife_actu = 0;
            $dife_maxi = 0;

            global $nume_iden, $digi_colm, $digi_cona;
            $nume_iden = trim($resu[0]["nume_iden"]);
            fun_calcula_digi_veri($nume_iden);
            fun_calcula_doble_digito($nume_iden);
            $dire_entr = trim($resu[0]["dire_entre"]);
            $dire_terc = trim($resu[0]["dire_ases"]);

            $esta_acti = "ING";
            $codi_esta = "S";
            $fech_ingr = date("m/d/Y");
            $date = date_create($resu[0]["fech_naci"]);
            $fech_nuev = date_format($date, "m/d/Y");
            $fech_naci = explode("/", trim($fech_nuev));
            $mess_naci = $fech_naci[0];

            if ($mess_naci > 12)
                $fech_naci = $fech_naci[1] . "/" . $fech_naci[0] . "/" . $fech_naci[2];
            else
                $fech_naci = $fech_naci[0] . "/" . $fech_naci[1] . "/" . $fech_naci[2];

            $inte_mora = 0;
            $mail_ases = trim($resu[0]["corr_ases"]);
            $nive_estu = 0;
            $nive_ingr = 1;
            $nomb_barr = $nomb_barr;
            $prim_nomb = trim($resu[0]["prim_nomb"]);
            $segu_nomb = trim($resu[0]["segu_nomb"]);
            $nomb_terc = $prim_nomb . " " . $segu_nomb;
            $nomb_vere = "";
            $nume_hijo = "";
            $nume_pedi = 0;
            $tele_terc = $resu[0]['celu_ases'];

            if (strlen($tele_terc) == 0) {
                $tele_terc = 0;
            }

            $tipo_gene = "F";
            $vere_entr = "";
            $actu_soli = "S";
            $tipo_via = $resu[0]['tipo_via'];
            if ($tipo_via == "CALLE")
                $tipo_via = "CL";
            if ($tipo_via == "AVENI")
                $tipo_via = "AV";
            if ($tipo_via == "CARRE")
                $tipo_via = "KR";
            if ($tipo_via == "DIAGO")
                $tipo_via = "DG";
            if ($tipo_via == "TRANS")
                $tipo_via = "TV";
            if (trim($tipo_via) == "OTRO")
                $tipo_via = "OT";

            $nume_via = trim($resu[0]['nume_via']);
            $letr_via = trim($resu[0]['letr_via']);
            $dire_num1 = trim($resu[0]['dire_num1']);
            $dir_letr = trim($resu[0]['dir_letr']);
            $dire_num2 = trim($resu[0]['dire_num2']);
            $dire_cuad = trim($resu[0]['dire_cuad']);
            $dire_comp = trim($resu[0]['dire_comp']);
            $query_c = "select * from  tab_ciud where cons_ciud='$cons_ciud'";
            $resu_c = $this->ifx_inca->query($query_c);
            $resu_c = $resu_c->result_array();
            $nomb_ciud = trim($resu_c[0]['nomb_ciud']);
            $cons_soli = "";
            $codi_sect = $sect;

            $indi_redi = "PENDIENTE";
            if (strlen($dire_entr) == 0) {
                $dire_entr = $dire_terc;
            }

            $query = "INSERT INTO
                tab_clie (
                    cons_terc,
                    acti_esta,
                    acti_hora,
                    acti_usua,
                    actu_dato,
                    actu_soli,
                    apel_terc,
                    auto_port,
                    auto_rete,
                    barr_entr,
                    camp_ingr,
                    cata_camp,
                    celu_entr,
                    celu_ter1,
                    celu_ter2,
                    ciud_entr,
                    clav_ases,
                    codi_desp,
                    codi_padr,
                    codi_terc,
                    codi_zona,
                    cond_pago,
                    cons_barr,
                    cons_bloq,
                    cons_carg,
                    cons_ciud,
                    cons_lide,
                    cupo_cred,
                    dest_entr,
                    dife_actu,
                    dife_maxi,
                    digi_bogo,
                    digi_colm,
                    digi_colo,
                    digi_cona,
                    dire_entr,
                    dire_terc,
                    esta_acti,
                    esta_civi,
                    fech_ingr,
                    fech_naci,
                    inte_mora,
                    mail_ases,
                    nive_educ,
                    nive_ingr,
                    nomb_barr,
                    nomb_terc,
                    nomb_vere,
                    nuev_terr,
                    nume_hijo,
                    nume_iden,
                    nume_pedi,
                    regi_trib,
                    tele_entr,
                    tele_ter2,
                    tele_terc,
                    tipo_docu,
                    tipo_gene,
                    vere_entr,
                    imag_asig,
                    imag_fech,
                    usua_imag,
                    tipo_via,
                    nume_via,
                    letr_via,
                    dire_num1,
                    dire_letr,
                    dire_num2,
                    dire_cuad,
                    dire_comp,
                    cons_soli,
                    fech_soli,
                    codi_sect
                )
                VALUES
                (
                    '$cons_terc',
                    '$acti_esta',
                    $acti_hora,
                    '$acti_usua',
                    '$actu_dato',
                    '$actu_soli',
                    '$apel_terc',
                    '$auto_port',
                    'N',
                    '$cons_barr_entr',
                    '$camp_ingr',
                    '$cata_camp',
                    '$celu_entr',
                    '$celu_ter1',
                    '$celu_ter2',
                    '$cons_ciud_entr',
                    '$clav_ases',
                    '$codi_desp',
                    '$cons_lide',
                    '$codi_terc',
                    '$codi_zona',
                    '$cond_pago',
                    '$cons_barr',
                    '$cons_bloq',
                    '$cons_carg',
                    '$cons_ciud',
                    '$cons_lide',
                    '$cupo_cred',
                    '$codi_dest',
                    '$dife_actu',
                    '$dife_maxi',
                    '$digi_cona',
                    '$digi_colm',
                    '$digi_cona',
                    '$digi_cona',
                    '$dire_entr',
                    '$dire_terc',
                    '$esta_acti',
                    '$codi_esta',
                    '$fech_ingr',
                    '$fech_naci',
                    '$inte_mora',
                    '$mail_ases',
                    '$nive_estu',
                    '$nive_ingr',
                    '$nomb_barr',
                    '$nomb_terc',
                    '$nomb_vere',
                    '$codi_terr',
                    '$nume_hijo',
                    '$nume_iden',
                    '$nume_pedi',
                    'S',
                    '$tele_entr',
                    '0',
                    '$tele_terc',
                    'C',
                    '$tipo_gene',
                    '$vere_entr',
                    'S',
                    current,
                    '$acti_usua',
                    '$tipo_via',
                    '$nume_via',
                    '$letr_via',
                    '$dire_num1',
                    '$dir_letr',
                    '$dire_num2',
                    '$dire_cuad',
                    '$dire_comp',
                    '$cons_soli',
                    '$fech_soli',
                    '$codi_sect'
                );";
            $this->ifx_inca->query($query);
            // DIE("HOLA");

            $codi_zona = $resu[0]['zona_geo'];

            //Georreferenciacion
            $query = "SELECT codi_zona FROM reco_zona_ciud where codi_zona='" . $codi_zona . "';";
            $resu_zona = $this->ifx_inca->query($query);
            $resu_zona = $resu_zona->result_array();

            //lider geo
            if (count($resu_zona) > 0) {
                $query = "update tab_clie set acti_esta='A' where cons_terc='$cons_terc';";
                $resu = $this->ifx_inca->query($query);
                //geo_cliente($dire_terc, $codi_camp, $cons_terc, "$acti_esta", "APP_MOVIL");
                $indi_p42 = "0000";
                //$codi_terc = recodificacion($dire_terc, $nomb_ciud, 1, $cons_terc, $indi_p42);
            }

            $query_r = "select cons_terc as cons_refe from tab_clie where nume_iden='" . $cedu_refe . "';";
            $resu_r = $this->ifx_inca->query($query_r);
            $resu_r = $resu_r->result_array();
            $cons_refe = $resu_r[0]['cons_refe'];

            //referido
            $query_r = "select codi_refe from referido where codi_refe='$cons_terc';";
            $resu_r = $this->ifx_inca->query($query_r);
            $resu_r = $resu_r->result_array();

            if (count($resu_r) == 0) {
                $query = "INSERT INTO referido ( camp_cdgo , codi_refe , cons_terc , fech_grab ,
	  opci_prem , usua_grab , indi_redi )  VALUES ( \"$codi_camp\", \"$cons_terc\",
	  \"$cons_refe\", \"$fech_ingr\", \"1\", \"$acti_usua\" , \"$indi_redi\"); ";
                $this->ifx_inca->query($query);
            } else {
                $query = "UPDATE referido SET cons_terc='$cons_refe', camp_cdgo='$codi_camp' WHERE codi_refe='$cons_terc' ;";
                $this->ifx_inca->query($query);
            }
        } else if ($esta_form == 1) {
            $query = "select * from tab_clie where nume_iden='$nume_iden';";
            $resu = $this->ifx_inca->query($query);
            $resu = $resu->result_array();
            $query_s = "select * from pre_ases_expe WHERE codi_camp='$codi_camp' AND nume_iden='$nume_iden';";
            $resu_s = $this->ifx_inca->query($query_s);
            $resu_s = $resu_s->result_array();

            //valores
            //resu_s datos de nueva solicitud

            $cons_terc = $resu[0]['cons_terc'];

            $acti_esta = "M";
            $acti_hora = "current";
            $actu_dato = $resu[0]['actu_dato'];
            $actu_soli = "S";

            //valores
            $prim_apel = $resu_s[0]['prim_apel'];
            $prim_apel = trim($prim_apel);
            $segu_apel = $resu_s[0]['segu_apel'];
            $segu_apel = trim($segu_apel);
            $apel_terc = $prim_apel . " " . $segu_apel;
            $auto_port = "N";
            $cons_barr = $resu_s[0]['nomb_barr'];
            $cons_barr_entr = $cons_barr;
            $cedu_refe = $resu_s[0]['lide_refe'];
            if ($cons_barr_entr == 0) {
                $cons_barr_entr = $cons_barr;
            }
            $camp_ingr = $resu[0]['camp_ingr'];
            $cata_camp = $resu[0]['cata_camp'];
            $celu_ter1 = $resu_s[0]['celu_ases'];
            $celu_entr = $celu_ter1;
            $celu_ter2 = "";
            $cons_ciud_entr = $resu_s[0]["cons_ciud"];
            $clav_ases = $resu[0]['clav_ases'];
            $codi_terc = $resu[0]['codi_terc'];

            $query_l = "select * from  tab_barr where cons_barr='$cons_barr'";
            $resu_l = $this->ifx_inca->query($query_l);
            $resu_l = $resu_l->result_array();

            $query_li = "select cons_terc from tab_clie where nume_iden='$cedu_lide'";
            $resu_li = $this->ifx_inca->query($query_li);
            $resu_li = $resu_li->result_array();
            $cons_lide = $resu_li[0]['cons_terc'];
            $codi_terr = $zona . $sect;
            $codi_zona = $zona;
            $nomb_barr = $resu_l[0]['nomb_barr'];

            global $codi_terc;
            $codi_terc = $codi_terr;


            fun_halla_codi_terc($codi_terr, $cons_terc);
            $cond_pago = $resu[0]['cond_pago'];
            $cons_bloq = $resu[0]['cons_bloq'];
            $cons_carg = $resu[0]['cons_carg'];
            $cons_ciud = $resu_s[0]['cons_ciud'];
            $cupo_cred = $resu[0]['cupo_cred'];
            $digi_colm = $resu[0]['digi_colm'];
            $digi_cona = $resu[0]['digi_cona'];
            $codi_dest = "R";
            $dife_actu = $resu[0]['dife_actu'];
            $dife_maxi = $resu[0]['dife_maxi'];
            $dire_entr = trim($resu_s[0]["dire_entre"]);
            $dire_terc = trim($resu_s[0]["dire_ases"]);
            $esta_acti = $resu[0]['esta_acti'];
            $codi_esta = $resu[0]['esta_civi'];

            $fech_ingr = date("m/d/Y");
            $date = date_create($resu[0]["fech_naci"]);
            $fech_nuev = date_format($date, "m/d/Y");
            $fech_naci = explode("/", trim($fech_nuev));

            $mess_naci = $fech_naci[0];
            if ($mess_naci > 12)
                $fech_naci = $fech_naci[1] . "/" . $fech_naci[0] . "/" . $fech_naci[2];
            else
                $fech_naci = $fech_naci[0] . "/" . $fech_naci[1] . "/" . $fech_naci[2];

            $inte_mora = $resu[0]['inte_mora'];
            $mail_ases = $resu_s[0]['corr_ases'];
            $nive_estu = 0;
            $nive_ingr = $resu[0]['nive_ingr'];
            $nomb_barr = $nomb_barr;
            $prim_nomb = trim($resu_s[0]["prim_nomb"]);
            $segu_nomb = trim($resu_s[0]["segu_nomb"]);
            $nomb_terc = $prim_nomb . " " . $segu_nomb;
            $nomb_terc = $nomb_terc;
            $nomb_vere = "";
            $nume_hijo = "";
            $nume_pedi = $resu[0]['nume_pedi'];
            $tele_terc = $resu_s[0]['celu_ases'];
            $tipo_gene = $resu[0]['tipo_gene'];

            $vere_entr = "";
            $actu_soli = "S";
            $tipo_via = $resu_s[0]['tipo_via'];
            if ($tipo_via == "CALLE")
                $tipo_via = "CL";
            if ($tipo_via == "AVENI")
                $tipo_via = "AV";
            if ($tipo_via == "CARRE")
                $tipo_via = "KR";
            if ($tipo_via == "DIAGO")
                $tipo_via = "DG";
            if ($tipo_via == "TRANS")
                $tipo_via = "TV";
            if (trim($tipo_via) == "OTRO")
                $tipo_via = "OT";
            if (trim($tipo_via) == "CIRCU")
                $tipo_via = "CQ";

            $nume_via = $resu_s[0]['nume_via'];
            $letr_via = $resu_s[0]['letr_via'];
            $dire_num1 = $resu_s[0]['dire_num1'];
            $dir_letr = $resu_s[0]['dire_letr'];
            $dire_num2 = $resu_s[0]['dire_num2'];
            $dire_cuad = $resu_s[0]['dire_cuad'];
            $dire_comp = $resu_s[0]['dire_comp'];
            $query_c = "select * from  tab_ciud where cons_ciud='$cons_ciud'";
            $resu_c = $this->ifx_inca->query($query_c);
            $resu_c = $resu_c->result_array();
            $nomb_ciud = trim($resu_c[0]['nomb_ciud']);
            $cons_soli = "";
            $codi_sect = $sect;
            $indi_redi = "PENDIENTE";

            if (strlen($dire_entr) == 0) {
                $dire_entr = $dire_terc;
            }
            $para_lide = ",codi_padr=\"$cons_lide\",cons_lide=\"$cons_lide\"";
            $codi_zona = $zona;
            /* //Si es zona geo no respeta cons_barr si es zona barrios deja cons_barr
            //Georreferenciacion
            $query = "SELECT codi_zona FROM reco_zona_ciud where codi_zona='" . $codi_zona . "'";
            $resu_zona = $this->ifx_inca->query($query);
            $resu_zona = $resu_zona->result_array();

            if (count($resu_zona) == 0) {
                $para_lide = ",codi_padr=\"$cons_lide\",cons_lide=\"$cons_lide\"";
                //zona barrio
                $codi_zona = $zona;
            } */

            $codi_desp = $codi_zona;

            if ($fech_naci == "//") {
                $fech_naci = "";
            }
            $query = "UPDATE
                    tab_clie
                SET
                    acti_esta = '$acti_esta',
                    acti_hora = current,
                    acti_usua = '$acti_usua',
                    actu_dato = '$actu_dato',
                    actu_soli = '$actu_soli',
                    apel_terc = '$apel_terc',
                    auto_port = '$auto_port',
                    auto_rete = 'N',
                    barr_entr = '$cons_barr_entr',
                    camp_ingr = '$codi_camp',
                    cata_camp = '$cata_camp',
                    celu_entr = '$celu_entr',
                    celu_ter1 = '$celu_ter1',
                    celu_ter2 = '$celu_ter2',
                    ciud_entr = '$cons_ciud_entr',
                    clav_ases = '$clav_ases',
                    codi_desp = '$codi_desp'
                    $para_lide,
                    codi_terc = '$codi_terc',
                    codi_zona = '$codi_zona',
                    cond_pago = '$cond_pago',
                    cons_barr = '$cons_barr',
                    cons_bloq = '$cons_bloq',
                    cons_carg = '$cons_carg',
                    cons_ciud = '$cons_ciud',
                    cupo_cred = '$cupo_cred',
                    dest_entr = '$codi_dest',
                    dife_actu = '$dife_actu',
                    dife_maxi = '$dife_maxi',
                    digi_bogo = '$digi_cona',
                    digi_colm = '$digi_colm',
                    digi_colo = '$digi_cona',
                    digi_cona = '$digi_cona',
                    dire_entr = '$dire_entr',
                    dire_terc = '$dire_terc',
                    esta_acti = 'ACT',
                    esta_civi = '$codi_esta',
                    fech_ingr = '$fech_ingr',
                    fech_naci = '$fech_naci',
                    inte_mora = '$inte_mora',
                    mail_ases = '$mail_ases',
                    nive_educ = '$nive_estu',
                    nive_ingr = '$nive_ingr',
                    nomb_barr = '$nomb_barr',
                    nomb_terc = '$nomb_terc',
                    nomb_vere = '$nomb_vere',
                    nuev_terr = '$codi_terr',
                    nume_hijo = '$nume_hijo',
                    nume_iden = '$nume_iden',
                    cons_soli = '$cons_soli',
                    fech_soli = '$fech_soli',
                    nume_pedi = '$nume_pedi',
                    regi_trib = 'S',
                    tele_entr = '$tele_entr',
                    tele_ter2 = '0',
                    tele_terc = '$tele_terc',
                    tipo_docu = 'C',
                    tipo_gene = '$tipo_gene',
                    vere_entr = '$vere_entr',
                    usua_imag = '$codi_usua',
                    imag_asig = '$actu_soli',
                    imag_fech = '$fech_hora_sist_ifx',
                    tipo_via = '$tipo_via',
                    nume_via = '$nume_via',
                    letr_via = '$dire_bis $letr_via',
                    dire_num1 = '$dire_num1',
                    dire_letr = '$dire_letr',
                    dire_num2 = '$dire_num2',
                    dire_cuad = '$dire_cuad',
                    dire_comp = '$dire_comp'
                WHERE
                    1 = 1
                    and cons_terc = '$cons_terc'";
            $this->ifx_inca->query($query);

            // $this->ifx_inca->query($query);
            $query = "SELECT codi_zona FROM reco_zona_ciud where codi_zona='" . $codi_zona . "'";
            $resu_zona = $this->ifx_inca->query($query);
            $resu_zona = $resu_zona->result_array();
            //lider geo
            if (count($resu_zona) > 0) {
                $query = "update tab_clie set acti_esta='A' where cons_terc='$cons_terc'";
                $resu = $this->ifx_inca->query($query);
                //geo_cliente($dire_terc, $codi_camp, $cons_terc, "$acti_esta", "APP_MOVIL");
                $indi_p42 = "0000";
                //$codi_terc = recodificacion($dire_terc, $nomb_ciud, 1, $cons_terc, $indi_p42);
                $para_lide = "";
            }

            //auditoria
            $query = "DELETE FROM audi_tab_clie WHERE 1=1 and cons_terc='$cons_terc'";
            $this->ifx_inca->query($query);
            $query = "INSERT INTO audi_tab_clie (cons_terc,tipo_docu,codi_terc,nume_iden,codi_padr,codi_desp,cons_ciud,codi_zona,nomb_terc,apel_terc,dire_terc,nomb_barr,tele_terc,fech_ingr,camp_ingr,fech_naci,cupo_cred,cons_bloq,inte_mora,cond_pago,digi_cona,digi_bogo,digi_colo,digi_colm,auto_rete,regi_trib,acti_usua,acti_hora,acti_esta,esta_acti,cons_carg,nume_pedi,dife_maxi,dife_actu,tele_ter2,celu_ter1,celu_ter2,cons_barr,nuev_ciud,nuev_terr,cons_lide,nuev_lide,tipo_gene,mail_ases,clav_ases,fech_modi,usua_modi,tipo_proc,imag_asig,imag_fech,pree_pedi,tipo_ases,usua_imag,nomb_vere,nive_educ,nive_ingr,esta_civi,nume_hijo,dire_entr,barr_entr,ciud_entr,vere_entr,tele_entr,celu_entr,dest_entr,auto_port,cata_camp,tota_ppp,nume_dist,actu_dato,actu_soli,cons_soli,fech_soli )
        SELECT cons_terc,tipo_docu,codi_terc,nume_iden,codi_padr,codi_desp,cons_ciud,codi_zona,nomb_terc,apel_terc,dire_terc,nomb_barr,tele_terc,fech_ingr,camp_ingr,fech_naci,cupo_cred,cons_bloq,inte_mora,cond_pago,digi_cona,digi_bogo,digi_colo,digi_colm,auto_rete,regi_trib,acti_usua,acti_hora,acti_esta,esta_acti,cons_carg,nume_pedi,dife_maxi,dife_actu,tele_ter2,celu_ter1,celu_ter2,cons_barr,nuev_ciud,nuev_terr,cons_lide,nuev_lide,tipo_gene,mail_ases,clav_ases,'$fech_sist','$acti_usua','ACTUALIZACION',imag_asig,imag_fech,pree_pedi,tipo_ases,usua_imag,nomb_vere,nive_educ,nive_ingr,esta_civi,nume_hijo,dire_entr,barr_entr,ciud_entr,vere_entr,tele_entr,celu_entr,dest_entr,auto_port,cata_camp,tota_ppp,nume_dist,actu_dato,actu_soli,cons_soli,fech_soli FROM tab_clie WHERE cons_terc='$cons_terc'";
            $this->ifx_inca->query($query);

            //referido
            $query_r = "select cons_terc as cons_refe from tab_clie where nume_iden='" . $cedu_refe . "';";
            $resu_r = $this->ifx_inca->query($query_r);
            $resu_r = $resu_r->result_array();
            $cons_refe = $resu_r[0]['cons_refe'];

            $query_r = "select codi_refe from referido where codi_refe='$cons_terc';";
            $resu_r = $this->ifx_inca->query($query_r);
            $resu_r = $resu_r->result_array();

            if (count($resu_r) == 0) {
                $query = "INSERT INTO referido ( camp_cdgo , codi_refe , cons_terc , fech_grab , opci_prem , usua_grab , indi_redi )  VALUES ( \"$codi_camp\", \"$cons_terc\", \"$cons_refe\", \"$fech_ingr\", \"1\", \"$acti_usua\" , \"$indi_redi\") ";
                $this->ifx_inca->query($query);
            } else {
                $query = "UPDATE referido SET cons_terc='$cons_refe', camp_cdgo='$codi_camp' WHERE codi_refe='$cons_terc' ;";
                $this->ifx_inca->query($query);
            }

            //Quita sanciones de inactividad

            $query = "INSERT INTO tab_desb select cons_terc,'$codi_camp',codi_sanc,'$acti_usua',acti_hora,'$acti_usua','$fech_ingr','ACTUALIZACION APLICACION MOVIL' from cli_sanc where cons_terc='$cons_terc' and codi_sanc in (7,10);";
            $this->ifx_inca->query($query);

            $query = "DELETE FROM cli_sanc WHERE cons_terc ='$cons_terc' and codi_sanc in (7,10);";
            $this->ifx_inca->query($query);
        }

        $query = "select * from pre_ases_expe WHERE codi_camp='$codi_camp' AND nume_iden='$nume_iden';";
        $resu = $this->ifx_inca->query($query);
        $resu = $resu->result_array();

        $query = "DELETE FROM cli_refe WHERE 1=1 and cons_terc='$cons_terc'";
        $this->ifx_inca->query($query);

        $codi_ref1 = '';
        $codi_ref2 = '';
        // $codi_ref1 = $resu[0]['codi_pare_ref1'];
        // $codi_ref2 = $resu[0]['codi_pare_ref2'];
        // if ($codi_ref1 == "-1" && $codi_ref2 == "-1") {
        // $codi_ref1 = 3;
        // $codi_ref2 = 3;
        // }
        //familiar
        $query = "INSERT INTO cli_refe (  cons_terc,tipo_refe,nomb_refe,celu_refe,tele_refe,codi_pare,cons_ciud) VALUES
		('$cons_terc','P','" . $resu[0]['nomb_apell_ref1'] . " " . $resu[0]['prim_apell_ref1'] . "','" . $resu[0]['celu_ref1'] . "','" . $resu[0]['tele_ref1'] . "','" . $codi_ref1 . "','" . $cons_ciud . "')";
        $this->ifx_inca->query($query);
        //personal
        $query = "INSERT INTO cli_refe (  cons_terc,tipo_refe,nomb_refe,celu_refe,tele_refe,codi_pare,cons_ciud) VALUES
		('$cons_terc','P','" . $resu[0]['nomb_apel_ref2'] . " " . $resu[0]['prim_apel_ref2'] . "','" . $resu[0]['celu_ref2'] . "','" . $resu[0]['tele_ref2'] . "','" . $codi_ref2 . "','" . $cons_ciud . "')";
        $this->ifx_inca->query($query);

        // sincroniza datos
      /*  $url_destino = 'https://alcor.dupree.co/dupreeWS/panel/actualiza_estado';
        $ch = curl_init($url_destino);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        // tiempo de espera
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        // TRUE para hacer un HTTP POST normal. Este POST del tipo application/x-www-form-urlencoded, el mÃ¡s comÃºn en formularios HTML.
        curl_setopt($ch, CURLOPT_POST, 1);
        // CURLOPT_POSTFIELDS codificarÃ¡ los datos como multipart/form-data
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'Params={"obse_rech":"' . $nomb_rech . '","nume_iden":"' . $nume_iden . '","esta_prei":"AUTORIZADO"}');
        // TRUE para devolver el resultado de la transferencia como string del valor de curl_exec() en lugar de mostrarlo directamente.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);*/

         $query = "update pre_ases_expe  set esta_fina='APROBADO' WHERE codi_camp='$codi_camp' AND nume_iden='$nume_iden';";
        $resu = $this->ifx_inca->query($query);

        $query = "COMMIT WORK";
        $this->ifx_inca->query($query);

        // $query = "ROLLBACK";
        // $this->ifx_inca->query($query);
    }

    /* Metodo que trae las campañas para los valores de la tabla pre_ases_expe para cuenta regresiva */

    public function trae_camp() {
        $query = "select codi_camp,nume_iden,zona_geo,codi_cort from pre_ases_expe";
        $camp_actu = $this->ifx_inca->query($query);
        $camp_actu = $camp_actu->result_array();
        return $camp_actu;
    }

    /* Metodo que realiza la inserción de los clientes en la tabla tab_clie */

    public function inse_tab_clie($nume_iden,$cedu_lide,$zona,$sect) {
       //valida si ya verifico identidad y firma digital
        $query = "select * from pre_ases_expe where nume_iden='$nume_iden' and apro_ases='APR' and codi_apro='APR'";

        $data_apro = $this->ifx_inca->query($query);
        $data_apro = $data_apro->result_array();

        if(count($data_apro)==0){
            $mensaje['error'] = "La asesora aun no ha completado el proceso de validacion incluyendo firma digital.";
            mensaje($mensaje);
        }

        foreach ($data_apro as $apro) {
            $nume_iden = $apro['nume_iden'];
            //$subestado = $apro['subestado'];
            $codi_camp = $apro['codi_camp'];
            $this->ApruebaSolicitud($codi_camp, $nume_iden,$cedu_lide,$zona,$sect);
        }

    }

    function cupoCredito($cons_terc, $codi_zona) {

        $query_cupo = "SELECT limit 18 codi_camp FROM tab_camp order by codi_camp desc into temp cli1;";
        $this->ifx_inca->query($query_cupo);
        $query_cupo = "select min(codi_camp) min,max(codi_camp) max from cli1;";
        $resu_cupo = $this->ifx_inca->query($query_cupo);
        $arr_cupo = $resu_cupo->result_array();
        $min_camp = trim($arr_cupo[0]['min']);
        $max_camp = trim($arr_cupo[0]['max']);
        $query_cant = "SELECT count(distinct codi_camp) as cant FROM fac_glob WHERE cons_terc='$cons_terc' and codi_camp between '$min_camp' and '$max_camp' and cons_sucu=9 and acti_esta not in ('DEV','dev')";
        $resu_cant = $this->ifx_inca->query($query_cant);
        $arr_cant = $resu_cant->result_array();
        $cant_fact = trim($arr_cupo[0]['cant']);

        $query_z = "SELECT 1 FROM zon_code WHERE codi_zona='$codi_zona'";
        $resu_z = $this->ifx_inca->query($query_z);
        $arr_z = $resu_z->result_array();
        $tuplas_z = count($arr_z);
        if ($tuplas_z == 0)
            $ctrl_zona = 0;
        else
            $ctrl_zona = 1;

        if ($cant_fact == 0) {
            if ($ctrl_zona == 0)
                $cupo_cred = "400000";
            else
                $cupo_cred = "270000";
        } else if ($cant_fact == 1) {
            if ($ctrl_zona == 0)
                $cupo_cred = "550000";
            else
                $cupo_cred = "350000";
        } else if ($cant_fact == 2) {
            if ($ctrl_zona == 0)
                $cupo_cred = "650000";
            else
                $cupo_cred = "450000";
        } else if ($cant_fact == 3) {
            if ($ctrl_zona == 0)
                $cupo_cred = "800000";
            else
                $cupo_cred = "600000";
        } else {
            $query_f = "SELECT limit 3 codi_camp, sum(tota_publ) tota_publ FROM fac_glob WHERE 1=1 and cons_sucu=9 and cons_terc='$cons_terc' and acti_esta not in ('DEV','dev') group by 1 order by codi_camp desc into temp x1;";
            $this->ifx_inca->query($query_f);
            $query_f = "SELECT avg(tota_publ)*2 as prom_dato FROM x1";
            $resu_f = $this->ifx_inca->query($query_f);
            $arr_dato_f = $resu_f->result_array();
            $prom_dato = trim($arr_dato_f[0]['prom_dato']);
            if ($prom_dato < 800000) {
                if ($ctrl_zona == 0)
                    $cupo_cred = "1300000";
                else
                    $cupo_cred = "800000";
            } else
                $cupo_cred = round($prom_dato);
        }
        return $cupo_cred;
    }

    /* Metoto que realiza la consulta de transacciones realizadas por experian */

    public function trae_data_expe() {
        $query = "select distinct CASE
       WHEN TD1='Cédula de Ciudadania' THEN 'CC'
     --  WHEN TD1=5 THEN 'CE'
       ELSE 'CE'
      END AS  tipo_docu,NI2 as nume_iden,PN7 ||''||SN8 ||''||PA9 ||''||SA10 as nomb_ases,
	   SUBESTADO as estado,TRA_STATE_NAME as obse_valo from data_retor_expe";
        $data_expe = $this->ifx_inca->query($query);
        $data_expe = $data_expe->result_array();
        return $data_expe;
    }

    /* Metodo que realiza la inserción del dato a aprobar en la temporal
     * @paramm $nume_iden integer numero de documento a aprobar
     * @paramm $codi_camp integer numero de campaña a aprobar
     */

    public function inse_temp($nume_iden, $codi_camp,$cedu_lide,$zona,$sect) {
        $nume_iden=trim($nume_iden);
        $codi_camp=trim($codi_camp);

        $query = "drop table if exists data_valo_ases";
        $this->ifx_inca->query($query);
        $query = "
            select
                pre_ases_expe.codi_camp as codi_camp,
                pre_ases_expe.fech_envi as fech_envi,
                tipo_docu.nomb_docu as TD1,
                pre_ases_expe.nume_iden as NI2,
                pre_ases_expe.prim_nomb as PN7,
                pre_ases_expe.segu_nomb as SN8,
                pre_ases_expe.prim_apel as PA9,
                pre_ases_expe.segu_apel as SA10,
                pre_ases_expe.corr_ases as CCE17,
                pre_ases_expe.fech_naci as FN6,
                pre_ases_expe.sexo as MF88,
                tab_dpto.nomb_dpto as DPT12,
                tab_ciud.nomb_ciud as CIU13,
                pre_ases_expe.celu_ases as CEL14,
                pre_ases_expe.LIDE_REFE as RF99,
                pre_ases_expe.dire_ases as DIR11,
                pre_ases_expe.nomb_barr as BAR15,
                pre_ases_expe.dire_entre as DRE905,
                pre_ases_expe.cuen_regre as cuen_regr,
                pre_ases_expe.zona_geo as znc901,
                pre_ases_expe.vali_geo as vali_geo,
                '$cedu_lide' as cedu_lide,
                '$zona' as zona_gere,
                '$sect' as sect_gere,
                '$' || '' || round(pre_ases_expe.sald_cart, 2) as cart_cast,
                'SI' as pedi_prep
            from
                pre_ases_expe,
                tab_dpto,
                tab_ciud,
                tipo_docu,
                tab_barr
            where
                pre_ases_expe.cons_dpto = tab_dpto.cons_dpto
                and pre_ases_expe.cons_ciud = tab_ciud.cons_ciud
                and tipo_docu.cons_docu = pre_ases_expe.tipo_docu
                and tab_barr.cons_barr = pre_ases_expe.nomb_barr
                and pre_ases_expe.codi_camp = '$codi_camp'
                and pre_ases_expe.nume_iden = '$nume_iden'
                AND pre_ases_expe.ESTA_ASES <> 'VALIDAIDENTIDAD' into data_valo_ases;
            ";

        $this->ifx_inca->query($query);
    }

    /* Metodo que realiza el borrado de la tabla temporal del dato a aprobar */

    public function elim_temp() {
        $query = "delete from data_valo_ases";
        $this->ifx_inca->query($query);
    }

    /* Metodo que consulta el dato a aprobar */

    public function vali_data_apro() {
        $query = "select * from data_valo_ases";
        $data_ases = $this->ifx_inca->query($query);
        $data_ases = $data_ases->result_array();
        return $data_ases;
    }

    /* Metodo que realiza la aprobación de la asesora
     * @param $codigo integer codigo de aprobacion para asesora
     * @param $nume_iden integer numero de documento de asesora
     */

    public function apro_ases($codigo, $nume_iden) {
        $query = "select * from pre_ases_expe where nume_iden!='$nume_iden' and codi_apro='$codigo'";
        $codi_exis = $this->ifx_inca->query($query);
        $codi_exis = $codi_exis->result_array();
        if (!empty($codi_exis)) {
            $mensaje['error'] = "Genere nuevamente el codigo de aprobación para la asesora";
            mensaje($mensaje);
        } else {
            $query = "update pre_ases_expe set apro_ases='APR' ,codi_apro='$codigo' where nume_iden='$nume_iden'";
            $data_ases = $this->ifx_inca->query($query);
            $query = "update pre_ases_expe set esta_fina='APROBADO' WHERE nume_iden='$nume_iden'";
            $this->ifx_inca->query($query);
        }
    }

    /**
     * Carga la modal con los conceptos de rechazo.
     * @param $codi_camp integer  Campana
     * @param $nume_iden char(15) Cedula
     */
    public function CargaModal($codi_camp, $nume_iden, $nombre) {
        $nomb_ases = $nombre;
        $nume_iden = trim($nume_iden);

        $html = "<p>Asesora : <b>$nomb_ases</b><br/></p><table border='0' align='center'>";

        $query = "SELECT codi_rech,trim(nomb_rech) nomb_rech FROM nuev_rech_expe WHERE acti_esta in ('APP','ACT') order by nomb_rech asc;";
        $resul = $this->ifx_inca->query($query);
        $arreg = $resul->result_array();
        for ($i = 0; $i < count($arreg); $i++) {
            $codi_rech = $arreg[$i]['codi_rech'];
            $nomb_rech = $arreg[$i]['nomb_rech'];

            $html .= "<tr><td><input id='rech$i' type='checkbox' value='$codi_rech' onchange=\"proc_rech($i,this.value,$codi_camp,'$nume_iden');\">$nomb_rech</td></tr><tr></tr>";
        }

        $html .= "</table><br /><p><b>Seleccione el motivo del rechazo</b></p>";
        return $html;
    }

    /**
     * Carga/Borra el rechazo en la tabla temporal.
     * @param $codi_camp integer  Campana
     * @param $nume_iden char(15) Identificacion
     * @param $codi_rech smallint Codigo Rechazo
     * @param $ctrl      smallint Control Borrar/Insertar
     */
    public function CargaRechazo($codi_camp, $nume_iden, $codi_rech, $ctrl) {
        $codi_usua = codi_usua();
        if ($ctrl == 0)
            $query = "INSERT INTO temp_rech_ases  (codi_camp,nume_iden,codi_rech,acti_usua) VALUES ('$codi_camp','$nume_iden','$codi_rech','$codi_usua');";
        else
            $query = "DELETE FROM temp_rech_ases  WHERE codi_camp='$codi_camp' AND nume_iden='$nume_iden' AND codi_rech='$codi_rech' AND acti_usua='$codi_usua';";
        $this->ifx_inca->query($query);
    }

    /**
     * Rechaza la solicitud de credito.
     * Inserta rechazo.
     * @param $codi_camp integer  Campana
     * @param $nume_iden char(15) Cedula
     */
    public function RechazaSolicitud($codi_camp, $nume_iden) {
        $codi_usua = codi_usua();
        $query = "SELECT codi_rech FROM temp_rech_ases  WHERE codi_camp='$codi_camp' AND nume_iden='$nume_iden';";
        $resul = $this->ifx_inca->query($query);
        $arreg = $resul->result_array();
        if (count($arreg) > 0) {
            $query = "BEGIN WORK";
            $this->ifx_inca->query($query);

            /* --ACTUALIZA SOLICITUD-- */

            $query = "delete from rech_nugl WHERE codi_camp='$codi_camp' AND nume_timb='$nume_iden';";
            $this->ifx_inca->query($query);
            $query = "delete from rech_nude WHERE codi_camp='$codi_camp' AND nume_timb='$nume_iden';";
            $this->ifx_inca->query($query);

            /* --INSERTA RECHAZO-- */
            $query = "INSERT INTO rech_nugl (nume_timb,codi_camp,codi_zona,nume_iden,nomb_terc,nume_idco,acti_usua,acti_hora,camp_proc)
      SELECT DISTINCT
      t1.nume_iden,t1.codi_camp,t1.zona_geo,t1.nume_iden,trim(prim_nomb)||' '||trim(segu_nomb)||' '||trim(prim_apel)
      ||' '||trim(segu_apel) as nomb_ases,
      '0' nume_idco,'$codi_usua' acti_usua,current::datetime year to second acti_hora,t2.codi_camp
      FROM pre_ases_expe t1, tab_camp t2
     WHERE 1=1
     AND t1.codi_camp='$codi_camp'
      AND t1.nume_iden='$nume_iden'
      AND today BETWEEN t2.fech_inic AND t2.fech_fina;";
            $this->ifx_inca->query($query);

            $codi_rech_in = "";

            for ($i = 0; $i < count($arreg); $i++) {
                $codi_rech = trim($arreg[$i]["codi_rech"]);
                $codi_rech_in .= "$codi_rech,";

                $query_d = "select * from rech_nude WHERE codi_camp='$codi_camp' AND nume_timb='$nume_iden';";
                $resu_d = $this->ifx_inca->query($query_d);
                $arreg_d = $resu_d->result_array();

                //if(count($arreg_d)==0){
                $query = "INSERT INTO rech_nude (nume_timb,codi_camp,codi_rech) VALUES ('$nume_iden','$codi_camp','$codi_rech');";
                $this->ifx_inca->query($query);
                //}
            }

            $codi_rech_in = trim($codi_rech_in, ",");

            $query = "SELECT codi_rech,trim(nomb_rech) nomb_rech FROM nuev_rech_expe  WHERE codi_rech in ($codi_rech_in) order by nomb_rech asc;";
            $resul = $this->ifx_inca->query($query);
            $arreg = $resul->result_array();

            $nomb_rech = '';

            for ($i = 0; $i < count($arreg); $i++) {
                $nomb_rech .= trim($arreg[$i]['nomb_rech']) . ", ";
            }

            $nomb_rech = trim($nomb_rech, ", ");

            //sincroniza datos
            $url_destino = 'https://alcor.dupree.co/dupreeWS/panel/actualiza_estado';
            $ch = curl_init($url_destino);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            //tiempo de espera
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            //TRUE para hacer un HTTP POST normal. Este POST del tipo application/x-www-form-urlencoded, el mÃ¡s comÃºn en formularios HTML.
            curl_setopt($ch, CURLOPT_POST, 1);
            //CURLOPT_POSTFIELDS codificarÃ¡ los datos como multipart/form-data
            curl_setopt($ch, CURLOPT_POSTFIELDS, 'Params={"obse_rech":"' . $nomb_rech . '","nume_iden":"' . $nume_iden . '","esta_prei":"RECHAZADO"}');
            //TRUE para devolver el resultado de la transferencia como string del valor de curl_exec() en lugar de mostrarlo directamente.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $res = curl_exec($ch);

            if ($res == 1) {
                $query = "update pre_ases_expe set esta_fina='RECHAZADO' WHERE nume_iden='$nume_iden'";
                $this->ifx_inca->query($query);

                $query = "COMMIT WORK";
                $this->ifx_inca->query($query);
                $query = "delete from  temp_rech_ases ";
                $this->ifx_inca->query($query);
                $mensaje = "Asesora <b>$nume_iden</b> Rechazada Correctamente!!";
            } else {
                $query = "ROLLBACK";
                $this->ifx_inca->query($query);

                $mensaje = "Por favor intente nuevamente.";
            }
        } else
            $mensaje["cursor"] = "codi_camp";

        return $mensaje;
    }

    /* Metodo que rrealiza la actualizacion de la cuenta regresiva de los 21 dias
     * @param $codi_camp integer codigo de campaña insertada
     * @param $codi_zona integer zona de las asesoras preinscritas para posterior actualizacion de dias de cuenta regresiva
     * @return $cuen_regr integer cuenta regresiva para los datos consultados
     */

    public function vali_cuen_regr($codi_camp, $codi_zona, $codi_cort) {
        $query = "select distinct fech_ca21 from tab_cron_vent where codi_camp='$codi_camp' and codi_cort='$codi_cort';";
        $cuen_regr = $this->ifx_inca->query($query);
        $cuen_regr = $cuen_regr->result_array();
        return $cuen_regr;
    }

    /* Metodo que actualiza la cuenta regresiva
     * @param $codi_camp integer codigo de campaña insertada
     * @param $dias integer dias trasncurridos en la cuenta regresiva de 21 dias
     */

    public function actu_cuen_regr($codi_camp, $nume_iden, $dias) {

        if ($dias > 25) {
            $query = "select max(codi_camp)+1 as codi_camp from tab_camp;";
            $sig_camp = $this->ifx_inca->query($query);
            $sig_camp = $sig_camp->result_array();
            $camp_sigu = isset($sig_camp[0]['codi_camp']) ? $sig_camp[0]['codi_camp'] : "";
            $query = "update pre_ases_expe set cuen_regre='$dias',codi_camp='$camp_sigu'
		where codi_camp='$codi_camp' and nume_iden='$nume_iden';";
            $this->ifx_inca->query($query);
        } else {
            $query = "update pre_ases_expe set cuen_regre='$dias'where codi_camp='$codi_camp'
		and nume_iden='$nume_iden'; ";
            $this->ifx_inca->query($query);
        }
    }

    /*
     * funcion que retorna el numero de zona segun usuario autenticado
     */

    function permisos_internet() {
        $codi_usua = codi_usua();
        //zona 104
        // $codi_usua = "seudi_salazar";
        //zona 128
        // $codi_usua = "cristina_cardenas";
        //averiguar usuario por zona by erika_lancheros
        // select tab_zona.codi_zona,tab_empl_temp.codi_usua  from tab_empl_temp,tab_vend,tab_zona where tab_empl_temp.nume_iden=tab_vend.cedu_vend and (tab_vend.cons_vend=tab_zona.cons_vend or tab_vend.cons_vend=cons_apo1 or tab_vend.cons_vend=cons_apo2 or tab_vend.cons_vend=cons_apo3 or tab_vend.cons_vend=cons_apo4) and tab_zona.codi_zona='128'
        $cons = "select * from usua_inte where codi_usua='$codi_usua'";
        $resu9 = $this->ifx_inca->query($cons);
        $arr_dato = $resu9->result_array();
        $tuplas = count($arr_dato);

        if ($tuplas == 0) {
            // Evalua si es Gerente Regional o Gerente Lider
            $cons9 = "select tab_area.codi_area from tab_empl_temp,tab_clie,tab_area where tab_empl_temp.codi_usua='$codi_usua' and tab_empl_temp.nume_iden=tab_clie.nume_iden and (tab_clie.cons_terc=tab_area.cons_terc or tab_clie.cons_terc=cons_lid1 or tab_clie.cons_terc=cons_lid2 or tab_clie.cons_terc=cons_lid3 or tab_clie.cons_terc=cons_lid4)";
            $resu9 = $this->ifx_inca->query($cons9);
            $arr_dato9 = $resu9->result_array();
            $tuplas9 = count($arr_dato9);
            if ($tuplas9 > 0) {
                $codi_area = trim($arr_dato9[0]['codi_area']);
                $esta_perf = array(
                    "R",
                    $codi_area
                );
                return $esta_perf;
            } else {
                // Evalua si es Gerente Zonal y de apoyo
                $cons9 = "select tab_zona.codi_zona from tab_empl_temp,tab_vend,tab_zona where tab_empl_temp.codi_usua='$codi_usua' and tab_empl_temp.nume_iden=tab_vend.cedu_vend and (tab_vend.cons_vend=tab_zona.cons_vend or tab_vend.cons_vend=cons_apo1 or tab_vend.cons_vend=cons_apo2 or tab_vend.cons_vend=cons_apo3 or tab_vend.cons_vend=cons_apo4)";
                $resu9 = $this->ifx_inca->query($cons9);
                $arr_dato9 = $resu9->result_array();
                $tuplas9 = count($arr_dato9);

                if ($tuplas9 > 0) {
                    $codi_zona = trim($arr_dato9[0]['codi_zona']);
                    $esta_perf = array(
                        "Z",
                        $codi_zona
                    );

                    return $esta_perf;
                } // Evalua si es Lider
                else {
                    $cons9 = "select  tab_clie.cons_terc,tab_lide.acti_esta from tab_clie,tab_lide where nume_iden='$codi_usua' and tab_lide.cons_terc=tab_clie.cons_terc ";
                    $resu9 = $this->ifx_inca->query($cons9);
                    $arr_dato9 = $resu9->result_array();
                    $tuplas9 = count($arr_dato9);
                    if ($tuplas9 > 0) {
                        $codi_padr = trim($arr_dato9[0]['cons_terc']);
                        $esta_perf[0] = "L";
                        $esta_perf[1] = $codi_padr;
                        return $esta_perf;
                    }
                }
            }
            $resu9 = $this->ifx_inca->query($cons9);
        }
    }

    /*  valida existencia de bloqueos */

    public function Vali_exis_bloq($nume_iden) {
        $query = "select * from data_bloq where nume_iden='$nume_iden'";
        $resultado = $this->ifx_inca->query($query);
        $arreglo = $resultado->result_array();
        return $arreglo;
    }

    /* Metodo que realiza la validacion de sanciones por documento seleccionado */

    public function consu_sanc($cons_terc) {
        $query = "select * from cli_sanc where cons_terc='$cons_terc';";
        $res = $this->ifx_inca->query($query);
        $sanc_usua = $res->result_array();
        return $sanc_usua;
    }

    /*
     * Metodo que realiza el cargue de resapuesta de experian
     * @param $carg_arch_plan string contiene nombre del archivo
     * @return
     */

    public function carg_arch_plan($carg_arch_plan) {
        $codi_usua = codi_usua();
        //sesion actual activa
        $iden_sess = id_session();
        //sesion de guardado en experian
        $id_session = '';
        $TRA_ID = "";
        $FLO_ID = "";
        $FLO_VERSION = "";
        $FLO_STATE = "";
        $PASO = "";
        $TRA_START_DATE = "";
        $TRA_END_DATE = "";
        $TRA_STATE_NAME = "";
        $TDE_STATE_DEC_1 = "";
        $TDE_RESULT_DEC_1 = "";
        $TEV_STATE = "";
        $TEV_RESULT = "";
        $SMS_STATE = "";
        $SMS_RESULT = "";
        $EMAIL_STATE = "";
        $EMAIL_RESULT = "";
        $CONFIR_COD_TRX = "";
        $UTM_SOURCE = "";
        $UTM_MEDIUM = "";
        $UTM_CAMPAIGN = "";
        $TRA_RTM = "";
        $RTM_DATE1 = "";
        $RTM_STEP1 = "";
        $RTM_DATE2 = "";
        $RTM_STEP2 = "";
        $RTM_DATE3 = "";
        $RTM_STEP3 = "";
        $RTM_DATE4 = "";
        $RTM_STEP4 = "";
        $RTM_DATE5 = "";
        $RTM_STEP5 = "";
        $HASH_MD5 = "";
        $HASH_SHA_1 = "";
        $Hash_sha_256 = "";
        $Firma_Valida = "";
        $Subestado = "";
        $CUPO_SUGERIDO = "";
        $CAUSAL = "";
        $DECISION = "";
        $TD1 = "";
        $NI2 = "";
        $PN7 = "";
        $SN8 = "";
        $PA9 = "";
        $SA10 = "";
        $FN6 = "";
        $MF88 = "";
        $DPT12 = "";
        $CIU13 = "";
        $CEL14 = "";
        $CCE17 = "";
        $RF99 = "";
        $BAR15 = "";
        $DIR11 = "";
        $DRE904 = "";
        $DRE905 = "";
        $FED3 = "";
        $ZNC900 = "";
        $ZNC901 = "";
        $TDD1 = "";
        $TYC18 = "";
        $CEL906 = "";
        $CEL907 = "";
        $RFPN82 = "";
        $RFSN83 = "";
        $RFPA84 = "";
        $RFSA85 = "";
        $RFDE86 = "";
        $RFCI87 = "";
        $RFPA86 = "";
        $RFCE90 = "";
        $RPPN103 = "";
        $RPSN104 = "";
        $RPPA105 = "";
        $RPSA106 = "";
        $RFDE86 = "";
        $RPCI109 = "";
        $RPCE110 = "";
        $CIU14 = "";
        $CIU15 = "";
        $this->load->library("ExcelReader");
        $this->load->library("ExcelReader");
        $this->excelReader = New Spreadsheet_Excel_Reader();
        $this->excelReader->read(RUTA_TEMPORALES . usua_sis() . "_$carg_arch_plan", false);
        $filas = $this->excelReader->sheets[0]['numRows'];

        /* verifica la cantidad de columans insertadas en el documento */

        $columnas = $this->excelReader->sheets[0]['numCols'];
        /* crea la tabla en tiempo de ejecucion que contiene el error del documento */

        $tabl_temp = "temp_retor_expe$codi_usua";
        $values = "";
        $query = "DROP TABLE IF EXISTS $tabl_temp;";
        $this->ifx_inca->query($query);

        $query = "CREATE TABLE $tabl_temp(
		cons_tabl serial,
		TRA_ID character(50),
		FLO_ID character(50),
        FLO_VERSION character(50),
        FLO_STATE character(50),
        PASO character(50),
        TRA_START_DATE character(50),
        TRA_END_DATE character(50),
        TRA_STATE_NAME character(50),
        TDE_STATE_DEC_1 character(50),
        TDE_RESULT_DEC_1 character(50),
        TEV_STATE character(50),
        TEV_RESULT character(50),
        SMS_STATE character(50),
        SMS_RESULT character(50),
        EMAIL_STATE character(50),
        EMAIL_RESULT character(50),
        CONFIR_COD_TRX character(50),
        UTM_SOURCE character(50),
        UTM_MEDIUM character(50),
        UTM_CAMPAIGN character(50),
        TRA_RTM character(50),
        RTM_DATE1 character(50),
        RTM_STEP1 character(50),
        RTM_DATE2 character(50),
        RTM_STEP2 character(50),
        RTM_DATE3 character(50),
        RTM_STEP3 character(50),
        RTM_DATE4 character(50),
        RTM_STEP4 character(50),
        RTM_DATE5 character(50),
        RTM_STEP5 character(50),
        Hash_md5 character(50),
        Hash_sha_1 character(50),
        Hash_sha_256 character(50),
        Firma_Valida character(50),
        Subestado character(50),
        CUPO_SUGERIDO character(50),
        CAUSAL character(50),
        DECISION character(50),
        TD1 character(50),
        NI2 character(50),
        PN7 character(50),
        SN8 character(50),
        PA9 character(50),
        SA10 character(50),
        FN6 character(50),
        MF88 character(50),
        DPT12 character(50),
        CIU13 character(50),
        CEL14 character(50),
        CCE17 character(50),
        RF99 character(50),
        BAR15 character(50),
        DIR11 character(50),
        DRE904 character(50),
        DRE905 character(50),
        FED3 character(50),
        ZNC900 character(50),
        ZNC901 character(50),
        TDD1 character(50),
        TYC18 character(50),
        CEL906 character(50),
        CEL907 character(50),
        RFPN82 character(50),
        RFSN83 character(50),
        RFPA84 character(50),
        RFSA85 character(50),
        RFCI87 character(50),
        RFPA86 character(50),
        RFCE90 character(50),
        RPPN103 character(50),
        RPSN104 character(50),
        RPPA105 character(50),
        RPSA106 character(50),
        RFDE86 character(50),
        RPCI109 character(50),
        RPCE110 character(50),
        CIU14 character(50),
        CIU15 character(50),
        session character(50),
		acti_hora datetime year to second,
		acti_usua character (50),
		iden_sess character varying (50),
		obse_erro character varying(200)
		);";

        $this->ifx_inca->query($query);
        /* inserta en la tabla los datos con error en vacio */
        for ($i = 2; $i <= $filas; $i++) {
            $TRA_ID = (!isset($this->excelReader->sheets[0]['cells'][$i][1])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][1]);
            $TRA_ID = preg_replace('([^A-Za-z0-9])', '', $TRA_ID);
            $FLO_ID = (!isset($this->excelReader->sheets[0]['cells'][$i][2])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][2]);
            $FLO_ID = preg_replace('([^A-Za-z0-9])', '', $FLO_ID);
            $FLO_VERSION = (!isset($this->excelReader->sheets[0]['cells'][$i][3])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][3]);
            $FLO_VERSION = preg_replace('([^A-Za-z0-9])', '', $FLO_VERSION);
            $FLO_STATE = (!isset($this->excelReader->sheets[0]['cells'][$i][4])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][4]);
            $FLO_STATE = preg_replace('([^A-Za-z0-9])', '', $FLO_STATE);
            $PASO = (!isset($this->excelReader->sheets[0]['cells'][$i][5])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][5]);
            $PASO = preg_replace('([^A-Za-z0-9])', '', $PASO);
            $TRA_START_DATE = (!isset($this->excelReader->sheets[0]['cells'][$i][6])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][6]);
            $TRA_START_DATE = preg_replace('([^A-Za-z0-9])', '', $TRA_START_DATE);
            $TRA_END_DATE = (!isset($this->excelReader->sheets[0]['cells'][$i][7])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][7]);
            $TRA_END_DATE = preg_replace('([^A-Za-z0-9])', '', $TRA_END_DATE);
            $TRA_STATE_NAME = (!isset($this->excelReader->sheets[0]['cells'][$i][8])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][8]);
            $TRA_STATE_NAME = preg_replace('([^A-Za-z0-9])', '', $TRA_STATE_NAME);
            $TDE_STATE_DEC_1 = (!isset($this->excelReader->sheets[0]['cells'][$i][9])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][9]);
            $TDE_STATE_DEC_1 = preg_replace('([^A-Za-z0-9])', '', $TDE_STATE_DEC_1);
            $TDE_RESULT_DEC_1 = (!isset($this->excelReader->sheets[0]['cells'][$i][10])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][10]);
            $TDE_RESULT_DEC_1 = preg_replace('([^A-Za-z0-9])', '', $TDE_RESULT_DEC_1);
            $TEV_STATE = (!isset($this->excelReader->sheets[0]['cells'][$i][11])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][11]);
            $TEV_STATE = preg_replace('([^A-Za-z0-9])', '', $TEV_STATE);
            $TEV_RESULT = (!isset($this->excelReader->sheets[0]['cells'][$i][12])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][12]);
            $TEV_RESULT = preg_replace('([^A-Za-z0-9])', '', $TEV_STATE);
            $SMS_STATE = (!isset($this->excelReader->sheets[0]['cells'][$i][13])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][13]);
            $SMS_STATE = preg_replace('([^A-Za-z0-9])', '', $SMS_STATE);
            $SMS_RESULT = (!isset($this->excelReader->sheets[0]['cells'][$i][14])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][14]);
            $SMS_RESULT = preg_replace('([^A-Za-z0-9])', '', $SMS_RESULT);
            $EMAIL_STATE = (!isset($this->excelReader->sheets[0]['cells'][$i][15])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][15]);
            $EMAIL_STATE = preg_replace('([^A-Za-z0-9])', '', $EMAIL_STATE);
            $EMAIL_RESULT = (!isset($this->excelReader->sheets[0]['cells'][$i][16])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][16]);
            $EMAIL_RESULT = preg_replace('([^A-Za-z0-9])', '', $CONFIR_COD_TRX);
            $CONFIR_COD_TRX = (!isset($this->excelReader->sheets[0]['cells'][$i][17])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][17]);
            $CONFIR_COD_TRX = preg_replace('([^A-Za-z0-9])', '', $CONFIR_COD_TRX);
            $UTM_SOURCE = (!isset($this->excelReader->sheets[0]['cells'][$i][18])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][18]);
            $UTM_SOURCE = preg_replace('([^A-Za-z0-9])', '', $UTM_SOURCE);
            $UTM_MEDIUM = (!isset($this->excelReader->sheets[0]['cells'][$i][19])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][19]);
            $UTM_MEDIUM = preg_replace('([^A-Za-z0-9])', '', $UTM_MEDIUM);
            $UTM_CAMPAIGN = (!isset($this->excelReader->sheets[0]['cells'][$i][20])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][20]);
            $UTM_CAMPAIGN = preg_replace('([^A-Za-z0-9])', '', $UTM_CAMPAIGN);
            $TRA_RTM = (!isset($this->excelReader->sheets[0]['cells'][$i][21])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][21]);
            $TRA_RTM = preg_replace('([^A-Za-z0-9])', '', $TRA_RTM);
            $RTM_DATE1 = (!isset($this->excelReader->sheets[0]['cells'][$i][22])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][22]);
            $RTM_DATE1 = preg_replace('([^A-Za-z0-9])', '', $RTM_DATE1);
            $RTM_STEP1 = (!isset($this->excelReader->sheets[0]['cells'][$i][23])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][23]);
            $RTM_STEP1 = preg_replace('([^A-Za-z0-9])', '', $RTM_STEP1);
            $RTM_DATE2 = (!isset($this->excelReader->sheets[0]['cells'][$i][24])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][24]);
            $RTM_DATE2 = preg_replace('([^A-Za-z0-9])', '', $RTM_DATE2);
            $RTM_STEP2 = (!isset($this->excelReader->sheets[0]['cells'][$i][25])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][25]);
            $RTM_STEP2 = preg_replace('([^A-Za-z0-9])', '', $RTM_STEP2);
            $RTM_DATE3 = (!isset($this->excelReader->sheets[0]['cells'][$i][26])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][26]);
            $RTM_DATE3 = preg_replace('([^A-Za-z0-9])', '', $RTM_DATE3);
            $RTM_STEP3 = (!isset($this->excelReader->sheets[0]['cells'][$i][27])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][27]);
            $RTM_STEP3 = preg_replace('([^A-Za-z0-9])', '', $RTM_STEP3);
            $RTM_DATE4 = (!isset($this->excelReader->sheets[0]['cells'][$i][28])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][28]);
            $RTM_DATE4 = preg_replace('([^A-Za-z0-9])', '', $RTM_DATE4);
            $RTM_STEP4 = (!isset($this->excelReader->sheets[0]['cells'][$i][29])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][29]);
            $RTM_STEP4 = preg_replace('([^A-Za-z0-9])', '', $RTM_STEP4);
            $RTM_DATE5 = (!isset($this->excelReader->sheets[0]['cells'][$i][30])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][30]);
            $RTM_DATE5 = preg_replace('([^A-Za-z0-9])', '', $RTM_DATE5);
            $RTM_STEP5 = (!isset($this->excelReader->sheets[0]['cells'][$i][31])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][31]);
            $RTM_STEP5 = preg_replace('([^A-Za-z0-9])', '', $RTM_STEP5);
            $HASH_MD5 = (!isset($this->excelReader->sheets[0]['cells'][$i][32])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][32]);
            $HASH_MD5 = preg_replace('([^A-Za-z0-9])', '', $HASH_MD5);
            $HASH_SHA_1 = (!isset($this->excelReader->sheets[0]['cells'][$i][33])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][33]);
            $HASH_SHA_1 = preg_replace('([^A-Za-z0-9])', '', $HASH_SHA_1);
            $Hash_sha_256 = (!isset($this->excelReader->sheets[0]['cells'][$i][34])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][34]);
            $Hash_sha_256 = preg_replace('([^A-Za-z0-9])', '', $Hash_sha_256);
            $Firma_Valida = (!isset($this->excelReader->sheets[0]['cells'][$i][35])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][35]);
            $Firma_Valida = preg_replace('([^A-Za-z0-9])', '', $Firma_Valida);
            $Subestado = (!isset($this->excelReader->sheets[0]['cells'][$i][36])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][36]);
            $Subestado = preg_replace('([^A-Za-z0-9])', '', $Subestado);
            $CUPO_SUGERIDO = (!isset($this->excelReader->sheets[0]['cells'][$i][37])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][37]);
            $CUPO_SUGERIDO = preg_replace('([^A-Za-z0-9])', '', $CUPO_SUGERIDO);
            $CAUSAL = (!isset($this->excelReader->sheets[0]['cells'][$i][38])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][38]);
            $CAUSAL = preg_replace('([^A-Za-z0-9])', '', $CAUSAL);
            $DECISION = (!isset($this->excelReader->sheets[0]['cells'][$i][39])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][39]);
            $DECISION = preg_replace('([^A-Za-z0-9])', '', $DECISION);
            $TD1 = (!isset($this->excelReader->sheets[0]['cells'][$i][40])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][40]);
            $TD1 = preg_replace('([^A-Za-z0-9])', '', $TD1);
            $NI2 = (!isset($this->excelReader->sheets[0]['cells'][$i][41])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][41]);
            $NI2 = preg_replace('([^A-Za-z0-9])', '', $NI2);
            $PN7 = (!isset($this->excelReader->sheets[0]['cells'][$i][42])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][42]);
            $PN7 = preg_replace('([^A-Za-z0-9])', '', $PN7);
            $SN8 = (!isset($this->excelReader->sheets[0]['cells'][$i][43])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][43]);
            $SN8 = preg_replace('([^A-Za-z0-9])', '', $SN8);
            $PA9 = (!isset($this->excelReader->sheets[0]['cells'][$i][44])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][44]);
            $PA9 = preg_replace('([^A-Za-z0-9])', '', $PA9);
            $SA10 = (!isset($this->excelReader->sheets[0]['cells'][$i][45])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][45]);
            $SA10 = preg_replace('([^A-Za-z0-9])', '', $SA10);
            $FN6 = (!isset($this->excelReader->sheets[0]['cells'][$i][46])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][46]);
            $FN6 = preg_replace('([^A-Za-z0-9])', '', $FN6);
            $MF88 = (!isset($this->excelReader->sheets[0]['cells'][$i][47])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][47]);
            $MF88 = preg_replace('([^A-Za-z0-9])', '', $MF88);
            $DPT12 = (!isset($this->excelReader->sheets[0]['cells'][$i][48])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][48]);
            $DPT12 = preg_replace('([^A-Za-z0-9])', '', $DPT12);
            $CIU13 = (!isset($this->excelReader->sheets[0]['cells'][$i][49])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][49]);
            $CIU13 = preg_replace('([^A-Za-z0-9])', '', $CIU13);
            $CEL14 = (!isset($this->excelReader->sheets[0]['cells'][$i][50])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][50]);
            $CEL14 = preg_replace('([^A-Za-z0-9])', '', $CEL14);
            $CCE17 = (!isset($this->excelReader->sheets[0]['cells'][$i][51])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][51]);
            $CCE17 = preg_replace('([^A-Za-z0-9])', '', $CCE17);
            $RF99 = (!isset($this->excelReader->sheets[0]['cells'][$i][52])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][52]);
            $RF99 = preg_replace('([^A-Za-z0-9])', '', $RF99);
            $BAR15 = (!isset($this->excelReader->sheets[0]['cells'][$i][53])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][53]);
            $BAR15 = preg_replace('([^A-Za-z0-9])', '', $BAR15);
            $DIR11 = (!isset($this->excelReader->sheets[0]['cells'][$i][54])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][54]);
            $DIR11 = preg_replace('([^A-Za-z0-9])', '', $DIR11);
            $DRE904 = (!isset($this->excelReader->sheets[0]['cells'][$i][55])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][55]);
            $DRE904 = preg_replace('([^A-Za-z0-9])', '', $DRE904);
            $DRE905 = (!isset($this->excelReader->sheets[0]['cells'][$i][56])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][56]);
            $DRE905 = preg_replace('([^A-Za-z0-9])', '', $DRE905);
            $FED3 = (!isset($this->excelReader->sheets[0]['cells'][$i][57])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][57]);
            $FED3 = preg_replace('([^A-Za-z0-9])', '', $FED3);
            $ZNC900 = (!isset($this->excelReader->sheets[0]['cells'][$i][58])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][58]);
            $ZNC900 = preg_replace('([^A-Za-z0-9])', '', $ZNC900);
            $ZNC901 = (!isset($this->excelReader->sheets[0]['cells'][$i][59])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][59]);
            $ZNC901 = preg_replace('([^A-Za-z0-9])', '', $ZNC901);
            $TDD1 = (!isset($this->excelReader->sheets[0]['cells'][$i][60])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][60]);
            $TDD1 = preg_replace('([^A-Za-z0-9])', '', $TDD1);
            $TYC18 = (!isset($this->excelReader->sheets[0]['cells'][$i][61])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][61]);
            $TYC18 = preg_replace('([^A-Za-z0-9])', '', $TYC18);
            $CEL906 = (!isset($this->excelReader->sheets[0]['cells'][$i][62])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][62]);
            $CEL906 = preg_replace('([^A-Za-z0-9])', '', $CEL906);
            $CEL907 = (!isset($this->excelReader->sheets[0]['cells'][$i][63])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][63]);
            $CEL907 = preg_replace('([^A-Za-z0-9])', '', $CEL907);
            $RFPN82 = (!isset($this->excelReader->sheets[0]['cells'][$i][64])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][64]);
            $RFPN82 = preg_replace('([^A-Za-z0-9])', '', $RFPN82);
            $RFSN83 = (!isset($this->excelReader->sheets[0]['cells'][$i][65])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][65]);
            $RFSN83 = preg_replace('([^A-Za-z0-9])', '', $RFSN83);
            $RFPA84 = (!isset($this->excelReader->sheets[0]['cells'][$i][66])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][66]);
            $RFPA84 = preg_replace('([^A-Za-z0-9])', '', $RFPA84);
            $RFSA85 = (!isset($this->excelReader->sheets[0]['cells'][$i][67])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][67]);
            $RFSA85 = preg_replace('([^A-Za-z0-9])', '', $RFSA85);
            $RFCI87 = (!isset($this->excelReader->sheets[0]['cells'][$i][68])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][68]);
            $RFCI87 = preg_replace('([^A-Za-z0-9])', '', $RFCI87);
            $RFPA86 = (!isset($this->excelReader->sheets[0]['cells'][$i][69])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][69]);
            $RFPA86 = preg_replace('([^A-Za-z0-9])', '', $RFPA86);
            $RFCE90 = (!isset($this->excelReader->sheets[0]['cells'][$i][70])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][70]);
            $RFCE90 = preg_replace('([^A-Za-z0-9])', '', $RFCE90);
            $RPPN103 = (!isset($this->excelReader->sheets[0]['cells'][$i][71])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][71]);
            $RPPN103 = preg_replace('([^A-Za-z0-9])', '', $RPPN103);
            $RPSN104 = (!isset($this->excelReader->sheets[0]['cells'][$i][72])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][72]);
            $RPSN104 = preg_replace('([^A-Za-z0-9])', '', $RPSN104);
            $RPPA105 = (!isset($this->excelReader->sheets[0]['cells'][$i][73])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][73]);
            $RPPA105 = preg_replace('([^A-Za-z0-9])', '', $RPPA105);
            $RPSA106 = (!isset($this->excelReader->sheets[0]['cells'][$i][74])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][74]);
            $RPSA106 = preg_replace('([^A-Za-z0-9])', '', $RPSA106);
            $RFDE86 = (!isset($this->excelReader->sheets[0]['cells'][$i][75])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][75]);
            $RFDE86 = preg_replace('([^A-Za-z0-9])', '', $RFDE86);
            $RPCI109 = (!isset($this->excelReader->sheets[0]['cells'][$i][76])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][76]);
            $RPCI109 = preg_replace('([^A-Za-z0-9])', '', $RPCI109);
            $RPCE110 = (!isset($this->excelReader->sheets[0]['cells'][$i][77])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][77]);
            $RPCE110 = preg_replace('([^A-Za-z0-9])', '', $RPCE110);
            $CIU14 = (!isset($this->excelReader->sheets[0]['cells'][$i][78])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][78]);
            $CIU14 = preg_replace('([^A-Za-z0-9])', '', $CIU14);
            $CIU15 = (!isset($this->excelReader->sheets[0]['cells'][$i][79])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][79]);
            $CIU15 = preg_replace('([^A-Za-z0-9])', '', $CIU15);
            $id_session = (!isset($this->excelReader->sheets[0]['cells'][$i][80])) ? "" : trim($this->excelReader->sheets[0]['cells'][$i][80]);
            $id_session = preg_replace('([^A-Za-z0-9])', '', $id_session);
            $obse_erro = '';

            // $sql="INSERT INTO $tabl_temp (
            // TRA_ID,FLO_ID ,FLO_VERSION ,FLO_STATE ,PASO ,TRA_START_DATE,TRA_END_DATE ,TRA_STATE_NAME ,
            // TDE_STATE_DEC_1 ,TDE_RESULT_DEC_1 ,TEV_STATE ,TEV_RESULT ,SMS_STATE ,SMS_RESULT ,
            // EMAIL_STATE ,EMAIL_RESULT ,CONFIR_COD_TRX ,UTM_SOURCE ,UTM_MEDIUM ,UTM_CAMPAIGN ,
            // TRA_RTM ,RTM_DATE1 ,RTM_STEP1 ,RTM_DATE2 ,RTM_STEP2 ,RTM_DATE3 ,RTM_STEP3 ,RTM_DATE4 ,
            // RTM_STEP4 ,RTM_DATE5 ,RTM_STEP5 ,Hash_md5 ,Hash_sha_1 ,Hash_sha_256 ,FIRMA_VALIDA ,
            // Subestado , CUPO_SUGERIDO ,CAUSAL ,DECISION ,TD1 ,NI2 ,PN7 ,SN8 ,PA9 ,SA10 ,FN6 ,MF88 ,
            // DPT12 ,CIU13 ,CEL14 ,CCE17 ,RF99 ,BAR15 ,DIR11 ,DRE904 ,DRE905 ,FED3 ,ZNC900 ,ZNC901 ,
            // TDD1 ,TYC18 ,CEL906 ,CEL907 ,RFPN82 ,RFSN83 ,RFPA84 ,RFSA85 ,RFCI87 ,RFPA86 ,RFCE90 ,
            // RPPN103 ,RPSN104 ,RPPA105 ,RPSA106 ,RFDE86 ,RPCI109 ,RPCE110 ,CIU14 ,CIU15,session,
            // acti_hora,acti_usua,iden_sess,obse_erro)
            // values(
            // '$TRA_ID','$FLO_ID','$FLO_VERSION','$FLO_STATE','$PASO','$TRA_START_DATE','$TRA_END_DATE','$TRA_STATE_NAME','$TDE_STATE_DEC_1',
            // '$TDE_RESULT_DEC_1','$TEV_STATE','$TEV_RESULT','$SMS_STATE','$SMS_RESULT','$EMAIL_STATE','$EMAIL_RESULT','$CONFIR_COD_TRX',
            // '$UTM_SOURCE','$UTM_MEDIUM','$UTM_CAMPAIGN','$TRA_RTM','$RTM_DATE1','$RTM_STEP1','$RTM_DATE2','$RTM_STEP2','$RTM_DATE3',
            // '$RTM_STEP3','$RTM_DATE4','$RTM_STEP4','$RTM_DATE5','$RTM_STEP5','$HASH_MD5','$HASH_SHA_1','$Hash_sha_256','$Firma_Valida',
            // '$Subestado','$CUPO_SUGERIDO','$CAUSAL','$DECISION','$TD1','$NI2','$PN7','$SN8','$PA9','$SA10','$FN6','$MF88','$DPT12','$CIU13',
            // '$CEL14','$CCE17','$RF99','$BAR15','$DIR11','$DRE904','$DRE905','$FED3','$ZNC900','$ZNC901','$TDD1','$TYC18','$CEL906','$CEL907',
            // '$RFPN82','$RFSN83','$RFPA84','$RFSA85','$RFCI87','$RFPA86','$RFCE90','$RPPN103','$RPSN104','$RPPA105','$RPSA106','$RFDE86','$RPCI109',
            // '$RPCE110','$CIU14','$CIU15','$id_session',current,'$codi_usua','$iden_sess','$obse_erro');";
            // $this->ifx_inca->query($sql);


            $query = "select * from data_retor_expe where  NI2='$NI2' and tra_id='$TRA_ID';";
            $resu_query = $this->ifx_inca->query($query);
            $resu = $resu_query->result_array();
            if (!empty($resu)) {
                $query = "update data_retor_expe set TRA_ID='$TRA_ID',FLO_ID='$FLO_ID',FLO_VERSION='$FLO_VERSION',FLO_STATE='$FLO_STATE',PASO='$PASO',
			TRA_START_DATE='$TRA_START_DATE',TRA_END_DATE='$TRA_END_DATE',TRA_STATE_NAME='$TRA_STATE_NAME',TDE_STATE_DEC_1='$TDE_STATE_DEC_1',
			TDE_RESULT_DEC_1='$TDE_RESULT_DEC_1',TEV_STATE='$TEV_STATE',TEV_RESULT='$TEV_RESULT',SMS_STATE='$SMS_STATE',
			SMS_RESULT='$SMS_RESULT',EMAIL_STATE='$EMAIL_STATE',EMAIL_RESULT='$EMAIL_RESULT',CONFIR_COD_TRX='$CONFIR_COD_TRX',
			UTM_SOURCE='$UTM_SOURCE',UTM_MEDIUM='$UTM_MEDIUM',UTM_CAMPAIGN='$UTM_CAMPAIGN',
			TRA_RTM='$TRA_RTM',RTM_DATE1='$RTM_DATE1',RTM_STEP1='$RTM_STEP1',RTM_DATE2='$RTM_DATE2',RTM_STEP2='$RTM_STEP2',RTM_DATE3='$RTM_DATE3',
			RTM_STEP3='$RTM_STEP3',RTM_DATE4='$RTM_DATE4',RTM_STEP4='$RTM_STEP4',RTM_DATE5='$RTM_DATE5',RTM_STEP5='$RTM_STEP5',
			HASH_MD5='$HASH_MD5',HASH_SHA_1='$HASH_SHA_1',HASH_SHA_256='$Hash_sha_256',FIRMA_VALIDA='$Firma_Valida',SUBESTADO='$Subestado',
			CUPO_SUGERIDO='$CUPO_SUGERIDO',CAUSAL='$CAUSAL',DECISION='$DECISION',TD1='$TD1',NI2='$NI2',PN7='$PN7',SN8='$SN8',PA9='$PA9',
			SA10='$SA10',FN6='$FN6',MF88='$MF88',DPT12='$DPT12',CIU13='$CIU13',CEL14='$CEL14',CCE17='$CCE17',RF99='$RF99',BAR15='$BAR15',
			DIR11='$DIR11',DRE904='$DRE904',DRE905='$DRE905',FED3='$FED3',ZNC900='$ZNC900',ZNC901='$ZNC901',TDD1='$TDD1',TYC18='$TYC18',
			CEL906='$CEL906',CEL907='$CEL907',RFPN82='$RFPN82',RFSN83='$RFSN83',RFPA84='$RFPA84',RFSA85='$RFSA85',RFCI87='$RFCI87',RFPA86='$RFPA86',
			RFCE90='$RFCE90',RPPN103='$RPPN103',RPSN104='$RPSN104',RPPA105='$RPPA105',RPSA106='$RPSA106',RFDE86='$RFDE86',
			RPCI109='$RPCI109',RPCE110='$RPCE110',CIU14='$CIU14',CIU15='$CIU15', session='$id_session' where NI2='$NI2' and tra_id='$TRA_ID'; ";
                $this->ifx_inca->query($query);
                ;
            } else {
                $query = "insert into data_retor_expe(TRA_ID,FLO_ID ,FLO_VERSION ,FLO_STATE ,PASO ,TRA_START_DATE,TRA_END_DATE ,TRA_STATE_NAME ,
		   TDE_STATE_DEC_1 ,TDE_RESULT_DEC_1 ,TEV_STATE ,TEV_RESULT ,SMS_STATE ,SMS_RESULT ,EMAIL_STATE ,EMAIL_RESULT ,
	       CONFIR_COD_TRX ,UTM_SOURCE ,UTM_MEDIUM ,UTM_CAMPAIGN ,TRA_RTM ,RTM_DATE1 ,RTM_STEP1 ,RTM_DATE2 ,RTM_STEP2 ,RTM_DATE3 ,
           RTM_STEP3 ,RTM_DATE4 ,RTM_STEP4 ,RTM_DATE5 ,RTM_STEP5 ,Hash_md5 ,Hash_sha_1 ,Hash_sha_256 ,FIRMA_VALIDA ,Subestado ,
           CUPO_SUGERIDO ,CAUSAL ,DECISION ,TD1 ,NI2 ,PN7 ,SN8 ,PA9 ,SA10 ,FN6 ,MF88 ,DPT12 ,CIU13 ,CEL14 ,CCE17 ,RF99 ,BAR15 ,
           DIR11 ,DRE904 ,DRE905 ,FED3 ,ZNC900 ,ZNC901 ,TDD1 ,TYC18 ,CEL906 ,CEL907 ,RFPN82 ,RFSN83 ,RFPA84 ,RFSA85 ,RFCI87 ,
           RFPA86 ,RFCE90 ,RPPN103 ,RPSN104 ,RPPA105 ,RPSA106 ,RFDE86 ,RPCI109 ,RPCE110 ,CIU14 ,CIU15,session )
		   values('$TRA_ID','$FLO_ID','$FLO_VERSION','$FLO_STATE','$PASO','$TRA_START_DATE','$TRA_END_DATE','$TRA_STATE_NAME','$TDE_STATE_DEC_1',
          '$TDE_RESULT_DEC_1','$TEV_STATE','$TEV_RESULT','$SMS_STATE','$SMS_RESULT','$EMAIL_STATE','$EMAIL_RESULT','$CONFIR_COD_TRX',
          '$UTM_SOURCE','$UTM_MEDIUM','$UTM_CAMPAIGN','$TRA_RTM','$RTM_DATE1','$RTM_STEP1','$RTM_DATE2','$RTM_STEP2','$RTM_DATE3',
          '$RTM_STEP3','$RTM_DATE4','$RTM_STEP4','$RTM_DATE5','$RTM_STEP5','$HASH_MD5','$HASH_SHA_1','$Hash_sha_256','$Firma_Valida',
          '$Subestado','$CUPO_SUGERIDO','$CAUSAL','$DECISION','$TD1','$NI2','$PN7','$SN8','$PA9','$SA10','$FN6','$MF88','$DPT12','$CIU13',
          '$CEL14','$CCE17','$RF99','$BAR15','$DIR11','$DRE904','$DRE905','$FED3','$ZNC900','$ZNC901','$TDD1','$TYC18','$CEL906','$CEL907',
          '$RFPN82','$RFSN83','$RFPA84','$RFSA85','$RFCI87','$RFPA86','$RFCE90','$RPPN103','$RPSN104','$RPPA105','$RPSA106','$RFDE86','$RPCI109',
          '$RPCE110','$CIU14','$CIU15','$id_session');";
                $this->ifx_inca->query($query);
                ;
            }
            $query = "DROP TABLE IF EXISTS $tabl_temp;";
            $this->ifx_inca->query($query);
        }
    }
    /**
     * Consulta si existe la lider por cedula
     *
     * @param int $request datos formulario
     * @return Array
     */
    public function cons_lide_cedu($request){

        $db =$this->ifx_inca;

        $this->cons_lide_gene($db,$request);

        $db->where("t.nume_iden",$request["cedu_lide"])
            ->from("tab_clie as t,tab_sect as b")
            ->select("trim(b.codi_sect) as secc")
            ->limit(1);

        //$result = $db->get_compiled_select();

        $result = $db->get()->result_array();

        if(count($result) > 0){
            return $result;
        }

        $db->from("tab_clie as t,tab_barr as b")
            ->where("t.nume_iden",$request["cedu_lide"])
            ->select("trim(SUBSTR(b.codi_terr,-3)) as secc")
            ->limit(1);
        //$result = $db->get_compiled_select();

        $result = $db->get()->result_array();

        return $result;
    }
    /**
     * Consulta lider por sector
     *
     * @param array $request datos del formulario
     * @return void
     */
    public function cons_lide_sect($request){

        $db =$this->ifx_inca;

        $this->cons_lide_gene($db,$request);

        $db->from("tab_clie as t,tab_sect as b")
            ->select("trim(b.codi_sect) as secc")
            ->where("b.codi_zona",$request["zona"])
            ->where("b.codi_sect",$request["sect"])
            ->limit(1);

        //$result = $db->get_compiled_select();

        $result = $db->get()->result_array();

        if(count($result) > 0){
            return $result;
        }

        $db->from("tab_clie as t,tab_barr as b")
            ->select("trim(SUBSTR(b.codi_terr,-3)) as secc")
            ->where("trim(SUBSTR(b.codi_terr, -3)) = '{$request["sect"]}'")
            ->limit(1);
        //$result = $db->get_compiled_select();

        $result = $db->get()->result_array();

        return $result;
    }
    /**
     * Consulta base para consultar lider
     *
     * @param Object $db objeto base de datos
     * @param array $request datos formulario
     * @return void
     */
    private function cons_lide_gene($db,$request){
        $db->start_cache();
        $db->select("trim(t.nume_iden) as nume_iden")
            ->select("trim(t.nomb_terc) || ' ' || trim(t.apel_terc) as nomb_comp")
            ->select("trim(b.codi_zona) as zona")
            ->where("t.cons_terc = b.cons_lide")
            ->where("b.codi_zona",$request["zona"]);
        $db->stop_cache();
    }
}
