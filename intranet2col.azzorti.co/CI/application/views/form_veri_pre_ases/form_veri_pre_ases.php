<script type="text/javascript">

 $("#window_1_1").hide();
 $("#window_2_1").hide();

$(document).ready(function(){
    /*
	*funcion para el listado de datos al realizar la pre carga
	*/
  /*
   * Ruta del pdf firmado por la asesora
   */
  let rutaPdf="https://alcor2col.azzorti.co/archivos/clientes/vinculacion/";
  //let rutaPdf="https://alcor.dupree.co/archivos/clientes/vinculacion/";

	$.post("/CI/form_veri_pre_ases/form_veri_pre_ases/pre_carga",function(data)
	{
		/*  evalua si es personal de servio a ventas */
		<?php
		   $codi_usua=codi_usua();
        ?>
		var usuario ='<?php  echo $codi_usua  ?>';
		if(usuario=='erika_lancheros' || usuario=='hernan_gutierrez'  || usuario=='gina_padilla'  || usuario=='claudia_vanegas' ||
		usuario=='andres_chiguazuque' || usuario=='ana_rubio'
		)
		{
		  $("#window_1_1").show();
		}
		mensaje(data);
	},'json');


	/* Realiza la precarga de datos en la pestaña de consultas */

    // $.post("/CI/form_veri_pre_ases/form_veri_pre_ases/pre_carga",function(data)
	// {
		// mensaje(data);
	// },'json');

   /* Metodo que bloquea las primeras columnas */
  $('#make_render_segmento0').jqxGrid('pincolumn','camp_apro');
  $('#make_render_segmento0').jqxGrid('pincolumn','camp_rech');
  /**
   * Evento al editar las columnas
   */
  $("#make_render_segmento0").on('cellendedit', function (event) {
    var args = event.args;
    var nomb_colu = args.datafield;
    var fila = args.rowindex;
    var valor = args.value;
    var grid = $("#make_render_segmento0");
    var zona = grid.jqxGrid('getcelltext', fila, 'zona');
    var data_ante = {
      secc: grid.jqxGrid('getcelltext', fila, 'secc'),
      nomb: grid.jqxGrid('getcelltext', fila, 'nomb_lide'),
      cedu: grid.jqxGrid('getcelltext', fila, 'docu_lide'),
      zona: zona
    };

    zona = zona.trim();

    switch(nomb_colu){
      case 'docu_lide':
        var datos = {
          filt: "cedula",
          zona: zona,
          cedu_lide: valor
        }
        cons_lide(grid,fila,datos,data_ante);
        break;
      case 'secc':
        var datos = {
          filt: "zona_sect",
          zona: zona,
          sect: valor
        }
        console.error(datos);
        cons_lide(grid,fila,datos,data_ante);
        break;
    }
  });

   $("#make_render_segmento0").on("cellclick", function (event){

	  // var campana = $("#codi_camp").val();

    var columna = event.args.datafield;
    var fila = event.args.rowindex;
	  var prim_nomb = $('#make_render_segmento0').jqxGrid('getcelltext', fila, 'pn7');
	  var segu_nomb = $('#make_render_segmento0').jqxGrid('getcelltext', fila, 'sn8');
	  var prim_apel = $('#make_render_segmento0').jqxGrid('getcelltext', fila, 'pa9');
	  var segu_apel = $('#make_render_segmento0').jqxGrid('getcelltext', fila, 'sa10');
	  var campana = $('#make_render_segmento0').jqxGrid('getcelltext', fila, 'codi_camp');
	  var prim_nomb=$.trim( prim_nomb );
	  var segu_nomb=$.trim( segu_nomb );
	  var prim_apel=$.trim(prim_apel);
	  var segu_apel=$.trim( segu_apel );
	  var nombres=prim_nomb+" "+segu_nomb;
	  var apellidos=prim_apel+" "+segu_apel;
	  var apellidos=$.trim( apellidos );
	   var color = $('#color').val();
	  var nombre=prim_apel+" "+segu_apel+" "+prim_nomb+" "+segu_nomb;
      var cedula = $('#make_render_segmento0').jqxGrid('getcelltext', fila, 'ni2');
    //sector y documento de la lider seleccinado por la gerente
    var grid = $("#make_render_segmento0");
    var zona = grid.jqxGrid('getcelltext', fila, 'zona');
    var sect = grid.jqxGrid('getcelltext', fila, 'secc');
    var cedu = grid.jqxGrid('getcelltext', fila, 'docu_lide');

	  if(columna=="camp_apro")
	  {
	    $.ajax(
        {
          url: '/CI/form_veri_pre_ases/form_veri_pre_ases/'+'apro_ases',
          type: 'post',
          data:{
            codi_camp: campana,
            nume_iden: cedula,
            bloqueos:color,
            nombre:nombre,
            zona: zona,
            sect: sect,
            cedu_lide: cedu
          }
        }).done(function(data)
        {
          mensaje(data);
        });

	  }
	  else if (columna=="clie_sanc")
	  {
        $.ajax
		  ({
        type: "POST",
        url: '/CI/form_veri_pre_ases/form_veri_pre_ases/'+'modal_sanci',
        data:{codi_camp: campana, nume_iden: cedula,nombre:nombre,nombres:nombres,apellidos:apellidos,cons_terc:cedula},
        success: function(data)
        {
          mensaje(data);
        },
        dataType: 'json',
        error: function(data)
        {
          alert("error");
        }
      });
	  }
	   else if (columna=="camp_rech")
	  {
	    proc_soli(campana,cedula,'rech_docu',nombre);

	  }
    else if (columna=="pdf"){
      let boto = event.args.value;
      let vali = boto.trim()==="";
      let arch_str = $(boto).data("cedu");
      let arch = rutaPdf + arch_str;
      if(!vali){
        window.open(arch,"Solicitud asesora" + arch ,"popup");
      }
    }
   });

});


function proc_soli(campana,cedula,proceso,nombre)
{
  $.ajax(
  {
    url: '/CI/form_veri_pre_ases/form_veri_pre_ases/'+proceso,
    type: 'post',
    data:{codi_camp: campana, nume_iden: cedula,nombre:nombre}
  }).done(function(data)
  {
    mensaje(data);
    if(proceso=="rech_docu")
      $(".confirm_continue").prop('disabled',true);
  });
}

function cons_lide(grid,fila,datos,data_ante){

  soli(datos,"carg_lide").then(function(data){
    let nomb_lide = "";
    let zona = "";
    let secc = "";
    let docu = "";
    if(data.mensaje !== undefined){
      info = data.mensaje;
      nomb_lide = info.nomb_comp;
      zona = info.zona;
      secc = info.secc;
      docu = info.nume_iden;
    }else if (data.error !== undefined){
      nomb_lide = data_ante.nomb;
      zona = data_ante.zona;
      secc = data_ante.secc;
      docu = data_ante.cedu;
      mensaje(data);
    }
    grid.jqxGrid('setcellvalue',fila,'docu_lide',docu);
    grid.jqxGrid('setcellvalue',fila,'nomb_lide',nomb_lide);
    grid.jqxGrid('setcellvalue',fila,'zona',zona);
    grid.jqxGrid('setcellvalue',fila,'secc',secc);
  });
}

function proc_rech(id,valor,campana,cedula)
{
  vali_check();
  if($("#rech"+id).prop("checked"))
    var ctrl=0;
  else
    var ctrl=1;

  $.ajax(
  {
    url: '/CI/form_veri_pre_ases/form_veri_pre_ases/proc_rech',
    type: 'post',
    data:{codi_camp: campana, nume_iden: cedula, codi_rech: valor, ctrl: ctrl}
  });
}

function vali_check()
{
  var ctrl = 0;
  $("[type='checkbox']").each(function(){
    var propiedad = $(this).is(':checked');

    if(propiedad===true)
      ctrl++;
  });

  if(ctrl==0)
    $(".confirm_continue").prop('disabled',true);
  else
    $(".confirm_continue").prop('disabled',false);
}
/*
 * Realiza una solicitud http
 */
function soli(data,metodo,tipo="POST") {

  return new Promise(function (resolve, reject) {
    let baseUrl = `/CI/form_veri_pre_ases/form_veri_pre_ases/${metodo}`;
    let config = {
      type: tipo,
      url: baseUrl,
      data: data,
      beforeSend: function () {
        $("#loading").html('<div style="background-color: rgb(255, 255, 255) !important;" id="cover_load_method"><div id="cover_img"></div><div id="cover_msg">Cargando...</div></div>');
      },
      complete: function () {
        $("#loading").html("");
      },
    }

    $.ajax(config)
      .done(function (respuesta, textoEstado, estado) {
        resolve(respuesta);
      })
      .fail(function (err) {
        reject(err);
          let msg={
            error : "Ha ocurrido un error en el servidor."
          };
          mensaje(msg);
      });
  });
}
