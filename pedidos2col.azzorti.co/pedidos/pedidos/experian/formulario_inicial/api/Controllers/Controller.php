<?php

namespace Api\Controllers;

include_once(DIRECTORIO . "/Traits/Respuesta.php");

use Api\Traits\Respuesta;

/**
 * @author Steven Martinez <davids.martinez@azzorti.co>
 */

class Controller
{
    use Respuesta;
    /**
     * Valida si existe un parametro en la clase
     *
     * @param string $nombre nombre de la funcion
     * @param Array $argumentos Argumentos de la funcion
     * @return void
     */
    public function __call($nombre, $argumentos)
    {
        $this->respuesta(500,"no existe el parametro {$nombre}");
    }
}
