<?php

namespace Api\Models;

require_once(DIRECTORIO . '/../conexion/conexion.php');

use Conexion;

class Experian {

    /**
     * conexion de la base de datos
     *
     * @var Conexion
     */
    private $conexion;

    /**
     * Informacion a almacenar en la tabla auditoria
     *
     * @var Array
     */
    public $data_audi;

    public function __construct() {
        $this->conexion = new Conexion();
        $this->data_audi = array(
            "proceso" => "",
            "nume_iden" => 0,
            "nume_tran" => 0,
            "flujo" => "",
            "respuesta" => ""
        );
    }

    /*
     * Paso uno peticion inicial identidad
     */

    public function validaIdentidad($nume_celu, $fech_expe, $nume_iden, $apel_terc) {
        //Vacia y llena los campos para insertar a la tabla auditoria
        $this->data_audi["proceso"] = "validaIdentidad";
        $this->data_audi["nume_iden"] = $nume_iden;

        $tipo_iden = "1";
        $file = fopen(RUTA_TEMPORALES . "log_idvision.txt", "a");
        fwrite($file, "Paso 1: ---------------------------------------\n");

        $xml_auto = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
            <soapenv:Header/>
            <soapenv:Body>
                <tem:ExecuteXMLString>
                    <!--Optional:-->
                    <tem:request><![CDATA[<DCRequest xmlns="http://transunion.com/dc/extsvc">
                    <Authentication type="OnDemand">
                        <UserId>IDV_INDUSTRIASINCASAS.DEV1</UserId>
                        <Password>Azzorti2022da$</Password>
                    </Authentication>
                    <RequestInfo>
                        <SolutionSetId>142</SolutionSetId>
                         <SolutionSetVersion>146</SolutionSetVersion>
                        <ExecutionMode>NewWithContext</ExecutionMode>
                    </RequestInfo>
                        <Fields>
                            <Field key="PrimaryApplicant">
                            &lt;Applicant&gt;
                            &lt;IdNumber&gt;' . $nume_iden . '&lt;/IdNumber&gt;
                            &lt;IdType&gt;' . $tipo_iden . '&lt;/IdType&gt;
                            &lt;IdExpeditionDate&gt;' . $fech_expe . '&lt;/IdExpeditionDate&gt;
                            &lt;RecentPhoneNumber&gt;' . $nume_celu . '&lt;/RecentPhoneNumber&gt;
                            &lt;LastName&gt;' . $apel_terc . '&lt;/LastName&gt;
                            &lt;/Applicant&gt;
                            </Field>
                        </Fields>
                    </DCRequest>]]></tem:request>
                        </tem:ExecuteXMLString>
                    </soapenv:Body>
                    </soapenv:Envelope>';

        fwrite($file, "Request: $xml_auto");

        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: http://tempuri.org/IExternalSolutionExecution/ExecuteXMLString",
            "Content-length: " . strlen($xml_auto)
        ); // SOAPAction: your op URL

        $file = fopen(RUTA_TEMPORALES . "log_idvision.txt", "a");
        fwrite($file, "Parametro: " . print_r($data, true) . "\n");
        $ch = curl_init("https://www.transuniondecisioncentreuat.com.mx/TU.IDS.ExternalServices_mex_latam/SolutionExecution/ExternalSolutionExecution.svc?wsdl");
        //Validacion SSL
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_auto);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $esta_cone = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $tiem_cone = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
        $curl_erro = curl_error($ch);
        curl_close($ch);

        //habilitar servicio
        $response = str_replace("&lt;", "<", $response);
        $response = str_replace("&gt;", ">", $response);
        $response = str_ireplace('<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><ExecuteXMLStringResponse xmlns="http://tempuri.org/"><ExecuteXMLStringResult>', "", $response);
        $response = str_ireplace('</ExecuteXMLStringResult></ExecuteXMLStringResponse></s:Body></s:Envelope>', "", $response);
        fwrite($file, "respuesta total valida identidad: " . print_r($response, true) . "\n");
        $xml = simplexml_load_string($response);
        $arre_vali = $this->validaXml($xml);

        return $arre_vali;
    }

    /*
     * Valida telefono
     */

    public function validaTelefono($application_id, $nume_celu) {

        $this->data_audi["proceso"] = "validaTelefono";

        $xml_auto = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
            <soapenv:Header/>
            <soapenv:Body>
                <tem:ExecuteXMLString>
                    <!--Optional:-->
                    <tem:request><![CDATA[<?xml version="1.0" encoding="utf-8"?>
                                    <DCRequest
                                        xmlns="http://transunion.com/dc/extsvc">
                                        <Authentication type="OnDemand">
                                            <UserId>IDV_INDUSTRIASINCASAS.DEV1</UserId>
                                            <Password>Azzorti2022da$</Password>
                                        </Authentication>
                                        <RequestInfo>
                                            <ApplicationId>' . $application_id . '</ApplicationId>
                                            <QueueName>PhoneSelection</QueueName>
                                            <ExecutionMode>Send</ExecutionMode>
                                        </RequestInfo>
                                        <Fields>
                                            <Field key="ValidationMethod">SMS</Field>
                                            <Field key="PhoneType">Mobile</Field>
                                            <Field key="SelectedPhoneNumber">' . $nume_celu . '</Field>
                                        </Fields>
                                    </DCRequest>]]></tem:request>
                        </tem:ExecuteXMLString>
                    </soapenv:Body>
                    </soapenv:Envelope>';

        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: http://tempuri.org/IExternalSolutionExecution/ExecuteXMLString",
            "Content-length: " . strlen($xml_auto)
        ); // SOAPAction: your op URL

        $file = fopen(RUTA_TEMPORALES . "log_idvision.txt", "a");
        fwrite($file, "Paso 2: ---------------------------------------\n");

        $file = fopen(RUTA_TEMPORALES . "log_idvision.txt", "a");
        fwrite($file, "request: " . $xml_auto . "\n");

        $ch = curl_init("https://www.transuniondecisioncentreuat.com.mx/TU.IDS.ExternalServices_mex_latam/SolutionExecution/ExternalSolutionExecution.svc?wsdl");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_auto);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);

        // curl_close($ch);
        $response = str_replace("&lt;", "<", $response);
        $response = str_replace("&gt;", ">", $response);
        $response = str_ireplace('<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><ExecuteXMLStringResponse xmlns="http://tempuri.org/"><ExecuteXMLStringResult>', "", $response);
        $response = str_ireplace('</ExecuteXMLStringResult></ExecuteXMLStringResponse></s:Body></s:Envelope>', "", $response);
        $file = fopen(RUTA_TEMPORALES . "log_idvision.txt", "a");
        fwrite($file, "respuesta xml: " . $response . "\n");
        $xml = simplexml_load_string($response);

        $arre_vali = $this->validaXml($xml);

        return $arre_vali;
    }

    public function validaXml($response) {
        $file = fopen(RUTA_TEMPORALES . "log_idvision.txt", "a");
        fwrite($file, "ContextData: " . print_r($response->ContextData, true) . "\n");
        fwrite($file, "ResponseInfo: " . print_r($response->ResponseInfo, true) . "\n");

        //  print_r($response);

        $resp_info = trim((string) $response->ResponseInfo->CurrentQueue[0]);

        //valida otp
        if ($resp_info == "PhoneSelection") {
            $xml_otp = trim((string) $response->ContextData->Field[3], ",");
            $code_tran = (string) $response->ResponseInfo->ApplicationId;
            $mens_tele = trim((string) $response->ContextData->Field[2]);
            fwrite($file, "Error telefono: " . print_r($mens_tele, true) . "\n");
            //valida telefono incorrecto
            if (strlen($mens_tele) > 0) {
                $xml_otp = array(0 => "error_otp", 1 => "Telefono no encontrado.");
                fwrite($file, "Error telefono: " . print_r($xml_otp, true) . "\n");
            }
        }
        if ($resp_info == "PinVerification_OTPInput") {
            $xml_otp = (string) $response->ResponseInfo->CurrentQueue;
            $code_tran = (string) $response->ResponseInfo->ApplicationId;
            //valida pin
            $pin_corr = trim((string) $response->ContextData->Field[1]);
            $pin_digi = trim((string) $response->ContextData->Field[2]);
            if ($pin_corr != $pin_digi && strlen($pin_corr) == 4 && strlen($pin_digi) == 4) {
                $xml_otp = array(0 => "error_otp", 1 => "Pin incorrecto");
                fwrite($file, "Error otp: " . print_r($xml_otp, true) . "\n");
            }
        } else {
            //nodo validacion preguntas
            //activa IDMReasonCodes
            $vali_flujo = (string) $response->ContextData->Field[11];
            $reason_otp = (string) $response->ContextData->Field[11];
            $reason_otp = htmlspecialchars_decode($reason_otp);
            $xml_fina = simplexml_load_string($reason_otp);
            fwrite($file, "respuesta otp: " . print_r($xml_fina, true) . "\n");

            //nodo validacion rechazo
            $reason_otp = (string) $response->ContextData->Field[12];
            $reason_otp = htmlspecialchars_decode($reason_otp);
            $xml_rech = simplexml_load_string($reason_otp);
            fwrite($file, "respuesta rechazo: " . print_r($xml_rech, true) . "\n");

            //nodo validacion preguntas
            //print_r($response);
            $reason_otp = (string) $response->ContextData->Field[1];
            $code_tran = (string) $response->ResponseInfo->ApplicationId;
            $reason_otp = htmlspecialchars_decode($reason_otp);
            $xml_preg = simplexml_load_string($reason_otp);
            fwrite($file, "respuesta preguntas: " . print_r($xml_preg, true) . "\n");

            //validaciones iniciales
            if (strlen($vali_flujo) == 0) {
                $reason_otp = (string) $response->ContextData->Field[1];
                $code_tran = (string) $response->ResponseInfo->ApplicationId;
                $reason_otp = htmlspecialchars_decode($reason_otp);
                $xml_vali = simplexml_load_string($reason_otp);
                fwrite($file, "validaciones iniciales: " . print_r($xml_vali, true) . "\n");
            }

            $xmlPreguntas = $xml_preg->Questions;
            $xmlPreguntas->Transaccion = $code_tran;
        }//end validacion identidad
        //Numero transaccion
        if (strlen($code_tran) != 0) {
            $this->data_audi["nume_tran"] = $code_tran;
        }
        $xml = $this->OtpArray($xml_fina, $xml_rech, $xmlPreguntas, $xml_vali, $xml_otp);

        return $xml;
    }

    /*
     * Valida OTP
     */

    public function validaOTP($application_id, $pin_number) {
        $this->data_audi["proceso"] = "validaOTP";

        $xml_auto = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
                                <soapenv:Header/>
                                <soapenv:Body>
                                   <tem:ExecuteXMLString>
                                      <!--Optional:-->
                                      <tem:request><![CDATA[<?xml version="1.0" encoding="utf-8"?>
                             <DCRequest
                                 xmlns="http://transunion.com/dc/extsvc">
                             <Authentication type="OnDemand">
                                     <UserId>IDV_INDUSTRIASINCASAS.DEV1</UserId>
                                     <Password>Azzorti2022da$</Password>
                             </Authentication>
                                 <RequestInfo>
                                     <ApplicationId>'.$application_id.'</ApplicationId>
                                     <QueueName>PinVerification_OTPInput</QueueName>
                                     <ExecutionMode>Send</ExecutionMode>
                                 </RequestInfo>
                                 <Fields>
                                     <Field key="PinNumber">'.$pin_number.'</Field>
                                        <Field key="Action">Continue</Field>
                                 </Fields>
                             </DCRequest>]]></tem:request>
                                   </tem:ExecuteXMLString>
                                </soapenv:Body>
                             </soapenv:Envelope>';
        
        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: http://tempuri.org/IExternalSolutionExecution/ExecuteXMLString",
            "Content-length: " . strlen($xml_auto)
        ); // SOAPAction: your op URL

        $file = fopen(RUTA_TEMPORALES . "log_idvision.txt", "a");
        fwrite($file, "request: " . $xml_auto . "\n");

        $ch = curl_init("https://www.transuniondecisioncentreuat.com.mx/TU.IDS.ExternalServices_mex_latam/SolutionExecution/ExternalSolutionExecution.svc?wsdl");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_auto);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);

        curl_close($ch);
        $response = str_replace("&lt;", "<", $response);
        $response = str_replace("&gt;", ">", $response);
        $response = str_ireplace('<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><ExecuteXMLStringResponse xmlns="http://tempuri.org/"><ExecuteXMLStringResult>', "", $response);
        $response = str_ireplace('</ExecuteXMLStringResult></ExecuteXMLStringResponse></s:Body></s:Envelope>', "", $response);
        $file = fopen(RUTA_TEMPORALES . "log_idvision.txt", "a");
        fwrite($file, "respuesta xml: " . $response . "\n");
        $xml = simplexml_load_string($response);
        $file = fopen(RUTA_TEMPORALES . "log_idvision.txt", "a");
        fwrite($file, "respuesta: " . print_r($xml, true) . "\n");

        $arre_vali = $this->validaXml($xml);

        return $arre_vali;
    }

    public function OtpArray($xml, $xml_rech, $xml_preg, $xml_apel, $xml_otp) {
        //codigo bloqueo
        $code_bloq = $xml->Applicant->VelocityCheckData->VelocityCheckReasons->Reason->Code;
        $desc_bloq = $xml->Applicant->VelocityCheckData->VelocityCheckReasons->Reason->Description;
        //rechazo por no coincidencias apellido fecha expedicion
        $code_rech = $xml_rech->IDMReasonCodes->Reason->Code;
        $code_rech_coin = $xml_rech->IDMReasonCodes->Reason->Description;
        //rechazo por apellido
        $code_apel = $xml_apel->InputValReasonCodes->Reason->Code;
        $code_rech_apel = $xml_apel->InputValReasonCodes->Reason->Description;
        if (count($xml_otp) > 0) {
            $desc_erro = "flujo_otp";
            $xml_preg = $xml_otp;
            //confirma otp
            if ($xml_otp == "PinVerification_OTPInput") {
                $desc_erro = "valida_otp";
            }
            //errores otp
            if ($xml_otp[0] == "error_otp") {
                unset($code_bloq);
                unset($code_rech);
                unset($xml_preg);
                $code_rech_apel = $xml_otp[1];
            }
        } else {
            //codigos errores
            if ($code_rech == "700") {
                $desc_erro = "error_validacion";
            } else if ($code_rech == "702") {
                $desc_erro = "validacion_exitosa";
            } else if ($code_bloq == 101 || $code_bloq == 102 || $code_bloq == 100) {
                $desc_erro = "error";
            } else {
                $desc_erro = "flujo_pregunta";
            }
        }
        //Error general
        if (strlen($code_bloq) == 0 && strlen($code_rech) == 0 && count($xml_preg) == 0) {
            $code_bloq = "error_general";
            $desc_bloq = $code_rech_apel . ".";
            $desc_erro = $code_bloq;
        }

        $otp_array = array(
            $code_bloq,
            $desc_bloq,
            $code_rech,
            $code_rech_coin,
            $xml_preg,
            $desc_erro
        );

        $this->data_audi["flujo"] = $desc_erro;

        $file = fopen(RUTA_TEMPORALES . "log_idvision.txt", "a");
        fwrite($file, "arreglo mensajes: " . print_r($otp_array, true) . "\n");

        return $otp_array;
    }

    public function convierteXml($xml) {

        $response = str_replace("&lt;", "<", $xml);
        $response = str_replace("&gt;", ">", $response);

        return $response;
    }

    public function validaPreguntas($xml, $application_id) {
        $this->set_vaci_audi();
        $this->data_audi["proceso"] = "validaPreguntas";
        $this->data_audi["nume_tran"] = $application_id;
        $file = fopen(RUTA_TEMPORALES . "log_idvision.txt", "a");

        if (strlen($application_id) == 0) {
            $mensaje = "Id transaccion erroneo";
            fwrite($file, "error: " . $mensaje . "\n");
            return $mensaje;
        }

        $xml_auto = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
            <soapenv:Header/>
            <soapenv:Body>
               <tem:ExecuteXMLString>
                  <!--Optional:-->
                  <tem:request><![CDATA[<?xml version="1.0" encoding="utf-8"?>
         <DCRequest
             xmlns="http://transunion.com/dc/extsvc">
             <Authentication type="OnDemand">
                <UserId>IDV_INDUSTRIASINCASAS.DEV1</UserId>
         <Password>Azzorti2022da$</Password>
             </Authentication>
             <RequestInfo>
                 <ApplicationId>' . $application_id . '</ApplicationId>
                 <QueueName>ShowExam</QueueName>
                <ExecutionMode>Send</ExecutionMode>
             </RequestInfo>
             <Fields>
                 <Field key="AnswerData">' . $xml . '</Field>
             </Fields>
         </DCRequest>]]></tem:request>
               </tem:ExecuteXMLString>
            </soapenv:Body>
         </soapenv:Envelope>';

        //echo $xml_auto;
        //die();
        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: http://tempuri.org/IExternalSolutionExecution/ExecuteXMLString",
            "Content-length: " . strlen($xml_auto)
        ); // SOAPAction: your op URL

        fwrite($file, "request: " . $xml_auto . "\n");

        $ch = curl_init("https://www.transuniondecisioncentreuat.com.mx/TU.IDS.ExternalServices_mex_latam/SolutionExecution/ExternalSolutionExecution.svc?wsdl");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_auto);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        fwrite($file, "respuesta xml: " . $response . "\n");
        curl_close($ch);

        $response = str_replace("&lt;", "<", $response);
        $response = str_replace("&gt;", ">", $response);
        $response = str_ireplace('<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><ExecuteXMLStringResponse xmlns="http://tempuri.org/"><ExecuteXMLStringResult>', "", $response);
        $response = str_ireplace('</ExecuteXMLStringResult></ExecuteXMLStringResponse></s:Body></s:Envelope>', "", $response);

        $xml = simplexml_load_string($response);
        fwrite($file, "respuesta preguntas: " . print_r($xml, true) . "\n");

        $arre_vali = $this->validaXml($xml);

        return $arre_vali;
        //   print_r($xml);
    }

    /**
     * Vacia el array data audi
     *
     * @return void
     */
    public function set_vaci_audi() {
        $this->data_audi["proceso"] = "";
        $this->data_audi["nume_tran"] = 0;
        $this->data_audi["flujo"] = 0;
        $this->data_audi["respuesta"] = 0;
    }

    /**
     * Inserta en la base de datos la auditoria
     *
     * @return void
     */
    public function inse_audi() {
        $db = $this->conexion->getConexion();
        $datos = $this->data_audi;
        $query = "
            INSERT INTO audi_expe(
                tran_proc,
                nume_iden,
                nume_tran,
                tran_fluj,
                tran_resp,
                codi_camp,
                fech_soli)
            VALUES(
                '{$datos['proceso']}',
                '{$datos['nume_iden']}',
                '{$datos['nume_tran']}',
                '{$datos['flujo']}',
                '{$datos['respuesta']}',
                (select max(codi_camp) from fac_glob),
                CURRENT);
            ";

        $db->ejecutar_consulta($query);
    }

    public function veri_vali_iden($nume_iden) {

        //habilitar validacion de campana produccion
        $db = $this->conexion->getConexion();
        $query = "
            SELECT COUNT(*) as cantidad
            FROM audi_expe
            WHERE
                tran_fluj = 'validacion_exitosa'
                and nume_iden = '$nume_iden';
                --and codi_camp = (select max(codi_camp) from tab_camp)
        ";
        $resul = $db->ejecutar_consulta($query);

        return $resul;
    }

    public function actu_apro_ases($nume_iden){
        $db = $this->conexion->getConexion();
        $resul = $this->veri_vali_iden($nume_iden);
        if ($resul[0]["cantidad"] > 0) {
            $query_upda = "
            update
                pre_ases_expe
            set
                apro_ases = 'APR'
            where
                nume_iden = '$nume_iden'";

            $db->ejecutar_consulta($query_upda);
        }
    }
    public function sald_cart($nume_iden){
        /* Validaciones saldo de cartera */
        $ch = curl_init('https://alcor.dupree.co/dupreeWS/reportes/saldo');
        //Validacion SSL
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        //TRUE para hacer un HTTP POST normal. Este POST del tipo application/x-www-form-urlencoded, el más común en formularios HTML.
        curl_setopt($ch, CURLOPT_POST, 1);
        //CURLOPT_POSTFIELDS codificará los datos como multipart/form-data
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'Params={"id_user":"710a35be0112475e3c7e3362ed188f33","cedula":' . $nume_iden . '}');
        //TRUE para devolver el resultado de la transferencia como string del valor de curl_exec() en lugar de mostrarlo directamente.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($res);
        $sald_cart = isset($data->result) ? $data->result : 0.00;

        return $sald_cart;
    }
}
