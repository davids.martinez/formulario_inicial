<?php namespace Api\Models;

require_once(DIRECTORIO . '/../conexion/conexion.php');

use Conexion;

class Localizacion
{
    /**
     * Variable conexion
     *
     * @var Conexion
     */
    private $conexion;
    /**
     * Crea conexion
     */
    public function __construct()
    {
        $this->conexion = new Conexion();
    }
    /**
     * Optiene las ciudadades
     *
     * @return array
     */
    public function get_ciud()
    {
        $getConexion = $this->conexion->getConexion();
        $query = "select
                    tc.cons_ciud,
                    tc.nomb_ciud
                from
                    tab_ciud tc
                group by
                    tc.cons_ciud,
                    tc.nomb_ciud
                order by
                    tc.nomb_ciud";
        $arre_ciud = $getConexion->ejecutar_consulta($query);

        return $arre_ciud;
    }
}
