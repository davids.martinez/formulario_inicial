<?php namespace Api\Traits;

trait Respuesta{
    /**
     * Genera una respuesta http en formato JSON
     *
     * @param integer $codigo codigo de la respuesta 200,400 etc
     * @param string $descripcion descripcion de la respuesta
     * @param mixed $respuesta informacion adicional de la respuesta
     * @return void
     */
    public function respuesta($codigo,$descripcion,$respuesta=null){
        http_response_code($codigo);
        header('Content-type: application/json; charset=utf-8');

        $response = array(
            "codigo" => $codigo,
            "descripcion" => $descripcion
        );

        if($respuesta != null){
            $response["respuesta"] = $respuesta;
        }

        echo json_encode($response);
        die();
    }
}

