<?php
  define("DIRECTORIO", __DIR__);

  require_once(DIRECTORIO . "/Controllers/ExperianController.php");

  use Api\Controllers\ExperianController;

  /**
   * Tipo de metodo enviado por el cliente (post,get,put)
   */

  $metodo = $_SERVER['REQUEST_METHOD'];

  /**
   * Valida el tipo de solicitud y formatea la informacion enviada por el cliente
   */

  switch($metodo){
    case 'GET':
      $data = $_GET;
      break;
    case 'POST':
      $data = json_decode(file_get_contents('php://input'), true);
      break;
  }
  /**
   * Ejecuta la funcion que se le envie en el parametro proceso
   */
  $proceso = isset($data["proceso"]) ? $data["proceso"] : false;
  $controlador=new ExperianController;
  if($proceso===false){
    $controlador->respuesta(200,"No se ha definido variable proceso");
  }

  call_user_func_array(array($controlador,$proceso),
  array($data));
