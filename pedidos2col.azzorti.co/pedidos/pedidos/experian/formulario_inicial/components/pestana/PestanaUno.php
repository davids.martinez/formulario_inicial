<?php
$nume_iden = strlen($_GET["nume_iden"]) > 0 ? $_GET["nume_iden"] : $nume_iden;
?>
<!-- pestaña 1 -->
<form id="pestana1" style="display:block" name="pestana1" id="pestana1" method="POST">
	<div class="container" style="width: 100%">
		<table border='0' width='100%' cellspacing='0' cellpadding='0' class='x-panel-header2'>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Tipo de documento <span style="color:red;">*</span></label>
					<select name="TD1" id="TD1" required="true" class="form-control">
						<option value="" selected="selected">SELECCIONE UNA OPCION</option>
						<?php foreach ($arre_docu as $key => $registro) { ?>
							<option value="<?php echo $registro["cons_docu"]; ?>"><?php echo $registro["nomb_docu"]; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">N&uacutemero Identificacion <span style="color:red;">*</span></label>
						<input id='NI2' name='NI2' class="form-control is-valid" onDblClick=" selecciona_value(this) " onkeyup="this.value=Numeros(this.value)" value='<?php $nume_iden ?>' type='text' size='11' maxlength='20' required autocomplete="off">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Fecha de expedici&oacuten del documento <span style="color:red;">*</span></label>
					<input id="fech_expe" class="form-control" />
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Celular <span style="color:red;">*</span></label>
					<input type="text" name="CEL14" onkeyup="this.value=Numeros(this.value)" id="CEL14" class="form-control">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Primer Nombre <span style="color:red;">*</span></label>
					<input type="text" id='PN7' required="true" onkeyup="this.value=Mayusculas(this.value)" onblur="this.value=Mayusculas(this.value)" onchange="this.value=Solotexto(this.value)" name='PN7' class="form-control">
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Segundo nombre</label>
					<input type="text" id='SN8' onkeyup="this.value=Mayusculas(this.value)" onblur="this.value=Mayusculas(this.value)" onchange="this.value=Solotexto(this.value)" name='SN8' class="form-control">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Primer Apellido <span style="color:red;">*</span></label>
					<input type="text" id='PA9' onkeyup="this.value=Mayusculas(this.value)" onblur="this.value=Mayusculas(this.value)" onchange="this.value=Solotexto(this.value)" name='PA9' class="form-control">
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Segundo Apellido</label>
					<input type="text" id='SA10' onkeyup="this.value=Mayusculas(this.value)" onblur="this.value=Mayusculas(this.value)" onchange="this.value=Solotexto(this.value)" name='SA10' class="form-control">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<label class="check">
						<input type="checkbox" id="poli-trat"/>
						<div class="box"></div>
					</label>
					<label class="label-check">Leí y acepto
						<a href="https://azzorti.co/wp-content/uploads/2020/03/POLITICA-DE-TRATAMIENTOS.pdf" target="_blank">la política de tratamiento de datos Personales </a>
					</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<label class="check">
						<input type="checkbox" id="cons-fina"/>
						<div class="box"></div>
					</label>
					<label class="label-check">Leí y acepto
						<a href="https://azzorti.co/wp-content/uploads/2021/07/PO-COR-03-V05-Tratamiento-de-Datos-Personales.pdf" target="_blank">consulta financiera </a>
					</label>
				</div>
			</div>
			<div class="form-inline">
				<input id="proc" name="proc" type="hidden" value="<?php echo $_GET["proc"]; ?>">
				<input id="token" name="token" type="hidden" value="<?php echo $_GET["token"]; ?>">
				<input id="id" name="id" type="hidden" value="<?php echo $_GET["id"]; ?>">
				<input id="cedula" name="cedula" type="hidden" value="<?php echo $_GET["nume_iden"]; ?>">
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<input type="button" id="btn-experian" value="Validar" class="btn btn-md btn-success" />
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Fecha de nacimiento<span style="color:red;">*</span></label>
					<input id="datepicker" class="form-control" disabled />
				</div>
				<input type="hidden" name="edad" id="edad" value="">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Genero<span style="color:red;">*</span></label>
					<select id="MF88" name="MF88" aria-invalid="false" aria-describedby="nf-error-5" class="form-control is-valid" aria-labelledby="nf-label-field-5" required="" disabled>
						<option value="" selected="selected">SELECCIONE UNA OPCION</option>
						<option value="F">Femenino</option>
						<option value="M">Masculino</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Departamento de residencia<span style="color:red;">*</span></label>
					<select name="DPT12" id="DPT12" required="true" class="form-control" disabled>
						<option value="" selected="selected">SELECCIONE UNA OPCION</option>
						<?php foreach ($arre_dpto as $key => $registro) { ?>
							<option value="<?php echo $registro["cons_dpto"]; ?>"><?php echo $registro["nomb_dpto"]; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Ciudad de residencia<span style="color:red;">*</span></label>
					<select name="CIU13" id="CIU13" required="true" class="form-control" disabled>
						<option value="">SELECCIONE UNA OPCION</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Correo electronico<span style="color:red;">*</span></label>
					<input type="text" name="CCE17" id="CCE17" class="form-control" disabled>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-warning" role="alert">
						<strong>Referencia familiar</strong>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Nombres y apellidos<span style="color:red;">*</span></label>
					<input type="text" name="ref_familiar_nombre" id="ref_familiar_nombre" class="form-control" onkeyup="this.value=Mayusculas(this.value)" onblur="this.value=Mayusculas(this.value)" onchange="this.value=Solotexto(this.value)" disabled>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Celular<span style="color:red;">*</span></label>
					<input type="text" name="ref_familiar_celular" id="ref_familiar_celular" class="form-control" onkeyup="this.value=Numeros(this.value)" disabled>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Departamento<span style="color:red;">*</span></label>
					<select name="ref_fami_depa" id="ref_fami_depa" required="true" class="form-control" disabled>
						<option value="" selected="selected">SELECCIONE UNA OPCION</option>
						<?php foreach ($arre_dpto as $key => $registro) { ?>
							<option value="<?php echo $registro["cons_dpto"]; ?>"><?php echo $registro["nomb_dpto"]; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Ciudad<span style="color:red;">*</span></label>
					<select name="ref_fami_ciud" id="ref_fami_ciud" required="true" class="form-control" disabled>
						<option value="">SELECCIONE UNA OPCION</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Parentesco<span style="color:red;">*</span></label>
					<select  name="ref_familiar_parentesco" id="ref_familiar_parentesco" required="true" class="form-control" disabled>
						<option value="" selected="selected">SELECCIONE UNA OPCION</option>
						<?php foreach ($arre_pare as $key => $registro) { ?>
							<option value="<?php echo $registro["codi_pare"]; ?>"><?php echo $registro["nomb_pare"]; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-warning" role="alert">
						<strong>Referencia personal</strong>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Nombres y apellidos<span style="color:red;">*</span></label>
					<input type="text" name="ref_personal_nombre" id="ref_personal_nombre" class="form-control" onkeyup="this.value=Mayusculas(this.value)" onblur="this.value=Mayusculas(this.value)" onchange="this.value=Solotexto(this.value)" disabled>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Celular<span style="color:red;">*</span></label>
					<input type="text" name="ref_personal_celular" id="ref_personal_celular" class="form-control" disabled>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Departamento<span style="color:red;">*</span></label>
					<select name="ref_pers_depa" id="ref_pers_depa" required="true" class="form-control" disabled>
						<option value="" selected="selected">SELECCIONE UNA OPCION</option>
						<?php foreach ($arre_dpto as $key => $registro) { ?>
							<option value="<?php echo $registro["cons_dpto"]; ?>"><?php echo $registro["nomb_dpto"]; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<label for="validation01">Ciudad<span style="color:red;">*</span></label>
					<select name="ref_pers_ciud" id="ref_pers_ciud" required="true" class="form-control" disabled>
						<option value="">SELECCIONE UNA OPCION</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-8" style="width: 100%">
					<label for="validation01">&#191;Te recomendo alguien? <span style="color:red;">*</span></label>
					<label class="radio-inline"><input type="radio" onChange="sele_lide()" value='SI' id="togg_sele" name="togg_sele">SI</label>
					<label class="radio-inline"><input type="radio" onChange="sele_lide()" value='NO' id="togg_sele" name="togg_sele" checked>NO</label>
				</div>
			</div>
			<div class="row" id='cedu_lide'>
				<div class="col-xs-8" style="width: 100%">
					<label for="validation01">Cedula lider referente<span style="color:red;">*</span></label>
					<input type="text" name="RF99" id="RF99" style="width: 100%" class="form-control">
				</div>
			</div>
			<div>
				<div class="col-md-12 text-center">
					<input type="submit" name="siguiente" id="siguiente" value="Siguiente" class="btn btn-md btn-success" disabled />
				</div>
			</div>
	</div>
	</table>
	</div>
</form>
<!-- fin pestaña 1. -->