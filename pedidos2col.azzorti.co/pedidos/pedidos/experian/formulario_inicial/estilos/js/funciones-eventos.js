function valores_iniciales() {
  let conf = {
    format: "dd/mm/yyyy",
    uiLibrary: "bootstrap",
  };
  $("#fech_expe").bind('keydown',function(e){
    teclas_fecha(e,this);
  }).datepicker(conf);
  $("#datepicker").bind('keydown',function(e){
    teclas_fecha(e,this);
  }).datepicker(conf);
  $("#datepicker").on("change", calcularEdad);

  /* Metodo que retorna la edad de usuario */
  $.experian = new Object();
  $("#solicitud").css("display", "block");
  $("#DRE905").val("");
  $("#DRE905").prop("disabled", true);
  $("#dire_fina").prop("disabled", true);
  $("#tipo_via").trigger("change", true);
  $("#togg_sele").trigger("change", true);
  $("#aprobacion").val("NO");
}
function teclas_fecha(e,este){

  if (e.which == 13){
    e.preventDefault();
    e.stopImmediatePropagation();
    $(este)
    .parent()
    .parent()
    .next()
    .find("input")
    .focus();
  }
}
function btn_experian_click() {
  var tipo_docu = $("#TD1").val();
  var nume_iden = $("#NI2").val();
  var fech_expe = $("#fech_expe").val();
  var prim_nomb = $("#PN7").val();
  var segu_nomb = $("#SN8").val();
  var prim_apel = $("#PA9").val();
  var segu_apel = $("#SA10").val();
  var celu_terc = $("#CEL14").val();
  var chec_poli_trat = $("#poli-trat").is(":checked");
  var chec_cons_fina = $("#cons-fina").is(":checked");


  if (tipo_docu.length == 0) {
    $("#tipo_docu").val("");
    $("#tipo_docu").focus();
    var error = "Por favor seleccione tipo de documento";
  } else if (nume_iden.length == 0) {
    $("#nume_iden").val("");
    $("#nume_iden").focus();
    error = "Por favor ingrese el numero de identificacion";
  } else if (fech_expe.length == 0) {
    error = "Por favor ingrese fecha de expedición del documento";
  } else if (celu_terc.length == 0) {
    error = "Por favor digite celular de contacto";
  } else if (celu_terc.length != 10) {
    error = "Su Teléfono celular debe tener 10 digitos";
  } else if (prim_nomb.length == 0 && segu_nomb.length == 0) {
    error = "ingrese Nombres";
  } else if (prim_apel.length == 0 && segu_apel.length == 0) {
    error = "ingrese Apellidos";
  } else if (!chec_poli_trat) {
    error =
      "Por favor autorice la politica de tratamiento de datos.";
  }else if (!chec_cons_fina) {
        error =
          "Por favor autorice la consulta financiera.";
  } else {
    error = -1;
  }

  if (error.length > 0) {
    swal({
      text: error,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#449d44",
      cancelButtonColor: "#d33",
      confirmButtonText: "OK",
    });
  } else {
    let data = {
      proceso: "post_vali_celu",
      fech_expe: fech_expe,
      nume_iden: nume_iden,
      celu_terc: celu_terc,
      apel_terc: prim_apel,
    };
    soli(data)
      .then(function (respuesta) {
        switch (respuesta.estado) {
          case 200:
            $.experian.preguntas = respuesta.Question;
            info = respuesta.respuesta.respuesta;
            seleccion_proceso(info);
            break;
          case 202:
            console.log(respuesta);
            mensaje_accion("success", respuesta.respuesta.descripcion)
              .then(function(){
                habilitar_campos();
              });
            break;
          default:
            mensaje("error", "No se ha definido esta respuesta");
        }
      })
      .catch(function (err) {});
  }
  return false;
}
function siguiente_click() {
  var tipo_docu = $("#TD1").val();
  var nume_iden = $("#NI2").val();
  var fech_expe = $("#fech_expe").val();
  var prim_nomb = $("#PN7").val();
  var segu_nomb = $("#SN8").val();
  var prim_apel = $("#PA9").val();
  var segu_apel = $("#SA10").val();
  var sexo = $("#MF88").val();
  var codi_dpto = $("#DPT12").val();
  var codi_ciud = $("#CIU13").val();
  var celu_terc = $("#CEL14").val();
  var corr_elec = $("#CCE17").val();
  var fech_min = "1953-01-01";
  var fech_max = "2003-12-31";
  var fech_naci = $("#datepicker").val();
  var edad = $("#edad").val();
  var togg_sele = $("input:radio[name=togg_sele]:checked").val();
  var ref_fami_nomb =$("#ref_familiar_nombre").val();
  var ref_fami_depa =$("#ref_fami_depa").val();
  var ref_fami_ciud =$("#ref_fami_ciud").val();
  var ref_fami_celu =$("#ref_familiar_celular").val();
  var ref_fami_pare =$("#ref_familiar_parentesco").val();
  var ref_pers_nomb =$("#ref_personal_nombre").val();
  var ref_pers_depa =$("#ref_pers_depa").val();
  var ref_pers_ciud =$("#ref_pers_ciud").val();
  var ref_pers_celu =$("#ref_personal_celular").val();
  var chec_poli_trat = $("#poli-trat").is(":checked");
  var chec_cons_fina = $("#cons-fina").is(":checked");

  cambia_banner("2");

  var cedu_lide = $("#RF99").val();
  if (tipo_docu.length == 0) {
    $("#tipo_docu").val("");
    $("#tipo_docu").focus();
    var error = "Por favor seleccione tipo de documento";
  } else if (nume_iden.length == 0) {
    $("#nume_iden").val("");
    $("#nume_iden").focus();
    error = "Por favor ingrese el numero de identificacion";
  } else if (fech_expe.length == 0) {
    error = "Por favor ingrese fecha de expedición del documento";
  } else if (prim_nomb.length == 0 && segu_nomb.length == 0) {
    error = "ingrese Nombres";
  } else if (prim_apel.length == 0 && segu_apel.length == 0) {
    error = "ingrese Apellidos";
  } else if (fech_naci.length == 0) {
    error = "Por favor ingrese fecha de nacimiento";
  } else if (edad < 18) {
    error = "Edad invalida ,Debes ser mayor de edad";
  } else if (sexo.length == 0) {
    error = "Por favor ingrese genero";
  } else if (codi_dpto.length == 0) {
    error = "Por favor seleccione departamento";
  } else if (codi_ciud.length == 0) {
    error = "Por favor seleccione ciudad";
  } else if (celu_terc.length == 0) {
    error = "Por favor inserte celular de contacto";
  } else if (celu_terc.length != 10) {
    error = "Su Teléfono celular debe tener 10 digitos";
  } else if (corr_elec.length == 0) {
    error = "Por favor inserte correo electronico";
  } else if (!isValidEmailAddress(corr_elec)) {
    error = "correo no valido";
  } else if (ref_fami_nomb.length == 0) {
    error = "Ingrese Nombres y apellidos de su referencia familiar";
  } else if (ref_fami_celu.length == 0) {
    error = "Ingrese teléfono celular de su referencia familiar";
  } else if (ref_fami_celu.length != 10) {
    error = "El Teléfono celular de su referencia personal debe tener 10 digitos";
  } else if (ref_fami_depa.length == 0) {
    error = "Ingrese Departamento de su referencia familiar";
  } else if (ref_fami_ciud.length == 0) {
    error = "Ingrese Ciudad de su referencia familiar";
  } else if (ref_fami_pare.length == 0) {
    error = "Ingrese parentesco de su referencia familiar";
  } else if (ref_pers_nomb.length == 0) {
    error = "Ingrese Nombres y apellidos de su referencia personal";
  } else if (ref_pers_celu.length == 0) {
    error = "Ingrese Teléfono celular de su referencia personal";
  } else if (ref_pers_celu.length != 10) {
    error = "El Teléfono celular de su referencia personal debe tener 10 digitos";
  } else if (ref_pers_depa.length == 0) {
    error = "Ingrese Departamento de su referencia personal";
  } else if (ref_pers_ciud.length == 0) {
    error = "Seleccione Ciudad de su referencia personal";
  } else if (togg_sele == "SI" && cedu_lide.length == 0) {
    error = "Por favor inserte cedula lider";
  } else if (cedu_lide.length > 0 && togg_sele == "NO") {
    error = "marca si en el campo ¿Te recomendo una lider?";
  } else if (!chec_poli_trat) {
    error =
      "Por favor autorice la politica de tratamiento de datos.";
  } else if (!chec_cons_fina) {
    error =
      "Por favor autorice la consulta financiera.";
  } else {
    error = -1;
  }

  if (error.length > 0) {
    swal({
      text: error,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#449d44",
      cancelButtonColor: "#d33",
      confirmButtonText: "OK",
    });
  } else {
    $("#step-two").addClass("active");
    $("#pestana1").css("display", "none");
    $("#solicitud").css("display", "none");
    $("#pestana2").css("display", "block");
  }
}
function siguiente1_click() {
  var tipo_docu = $("#TD1").val();
  var nume_iden = $("#NI2").val();
  var fech_expe = $("#fech_expe").val();
  var prim_nomb = $("#PN7").val();
  var segu_nomb = $("#SN8").val();
  var prim_apel = $("#PA9").val();
  var segu_apel = $("#SA10").val();
  var fech_naci = $("#datepicker").val();
  var sexo = $("#MF88").val();
  var codi_dpto = $("#DPT12").val();
  var codi_ciud = $("#CIU13").val();
  var celu_terc = $("#CEL14").val();
  var corr_elec = $("#CCE17").val();
  var cedu_lide = $("#RF99").val();
  var ref_fami_nomb =$("#ref_familiar_nombre").val();
  var ref_fami_ciud =$("#ref_fami_ciud").val();
  var ref_fami_celu =$("#ref_familiar_celular").val();
  var ref_fami_pare =$("#ref_familiar_parentesco").val();
  var ref_pers_nomb =$("#ref_personal_nombre").val();
  var ref_pers_ciud =$("#ref_pers_ciud").val();
  var ref_pers_celu =$("#ref_personal_celular").val();
  var barrio = $("#BAR15").val();
  var barr_nuev = $("#barr_nuev").val();
  var direccion = $("#dire_fina").val();
  var direccion = $.trim(direccion);
  var aprobacion = $("#aprobacion").val();
  var direccion1 = $("#dire").val();
  var direccion1 = $.trim(direccion1);
  var complemento = $("#complemento").val();
  var complemento = $.trim(complemento);
  var tipo_via = $("#tipo_via").val();
  var tipo_via = $.trim(tipo_via);
  var nume_via = $("#nume_via").val();
  var nume_via = $.trim(nume_via);
  var letr_via = $("#letr_via").val();
  var letr_via = $.trim(letr_via);
  var dire_num1 = $("#dire_num1").val();
  var dire_num1 = $.trim(dire_num1);
  var dir_letr = $("#dire_letr").val();
  var dir_letr = $.trim(dir_letr);
  var dire_num2 = $("#dire_num2").val();
  var dire_num2 = $.trim(dire_num2);
  var dire_cuad = $("#dire_cuad").val();
  var dire_cuad = $.trim(dire_cuad);
  var dire_comp = $("#complemento").val();
  var dire_comp = $.trim(dire_comp);

  var dire_entre = $.trim($("#DRE905").val());
  var dire_entre = $.trim(dire_entre);
  var vali_apro_prep = "NA";
  if ($("input:checkbox[name=check]:checked").val()) {
    var check = "si";
  } else {
    var check = "no";
  }
  var error = -1;

  if (barrio.length == 0 && barrio != "otro") {
    error = "Por favor inserte barrio";
  } else if (barrio == "otro" && barr_nuev.length == 0) {
    error = "por favor seleccione el barrio de residencia";
  } else if (direccion == "") {
    error = "Por favor inserte direccion";
  } else if (dire_entre == "") {
    error = "Por favor inserte direccion de entrega";
  } else if (edad > 75 || tipo_docu == 5 || sexo == "M") {
    // event.preventDefault();
    var vali_apro_prep = "A";
    if (aprobacion == "SI") {
      $("#aprobacion").val("SI");
      error = -1;
    } else {
      swal({
        text: "Querida posible asesora o asesor, para poder continuar debes manejar la opción de: “pedido prepagado�?, ¿deseas continuar? ",
        showCancelButton: true,
        confirmButtonColor: "#4A8C40",
        cancelButtonColor: "#f7505a",
        confirmButtonText: "SI",
        cancelButtonText: "NO",
      }).then((result) => {
        if (Object.values(result) == "true") {
          $("#aprobacion").val("SI");
        } else {
          $("#aprobacion").val("NO");
          swal({
            text: "acepta la opción de: “pedido prepagado“? para continuar",
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#449d44",
            cancelButtonColor: "#d33",
            confirmButtonText: "OK",
          });
        }
      });
    }
    if (aprobacion == "SI") {
      error = -1;
    } else {
      return false;
    }
  }

  if (error.length > 0) {
    swal({
      text: error,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#449d44",
      cancelButtonColor: "#d33",
      confirmButtonText: "OK",
    });
  } else {
    $("#pestana2").css("display", "none");

    $.ajax({
      type: "POST",
      url: "utilidades/Guardar.php",
      data: {
        apro_term: check,
        tipo_docu: tipo_docu,
        nume_iden: nume_iden,
        fech_expe: fech_expe,
        prim_nomb: prim_nomb,
        segu_nomb: segu_nomb,
        prim_apel: prim_apel,
        segu_apel: segu_apel,
        fech_naci: fech_naci,
        sexo: sexo,
        codi_dpto: codi_dpto,
        codi_ciud: codi_ciud,
        celu_terc: celu_terc,
        corr_elec: corr_elec,
        cedu_lide: cedu_lide,
        fami_nomb: ref_fami_nomb,
        fami_codi_ciud: ref_fami_ciud,
        fami_celu: ref_fami_celu,
        fami_pare: ref_fami_pare,
        pers_nomb: ref_pers_nomb,
        pers_codi_ciud: ref_pers_ciud,
        pers_celu: ref_pers_celu,
        barrio: barrio,
        direccion: direccion,
        dire_entre: dire_entre,
        barr_nuev: barr_nuev,
        tipo_via: tipo_via,
        nume_via: nume_via,
        letr_via: letr_via,
        dire_num1: dire_num1,
        dir_letr: dir_letr,
        dire_num2: dire_num2,
        dire_cuad: dire_cuad,
        dire_comp: dire_comp,
        vali_apro_prep: vali_apro_prep,
        aprob_prep: aprobacion,
      },
      beforeSend: function () {
        $("#cargando").show();
      },
      success: function (data) {
        $("#cargando").hide();
        setTimeout(function () {
          if (data.titulo == "mensaje") {
            swal({
              text: data.mensaje,
              type: data.type,
              showCancelButton: false,
              confirmButtonColor: "#4A8C40",
              cancelButtonColor: "#f7505a",
              confirmButtonText: "OK",
            });
            cambia_banner("3");
            $("#step-three").addClass("active");
            $("#solicitud").css("display", "none");
            $("#pestana3").css("display", "block");
          } else if (data.titulo == "informativo") {
            $("#step-three").addClass("active");
            $("#solicitud").css("display", "none");
            $("#pestana3").css("display", "block");
            swal({
              text: data.mensaje,
              type: data.type,
              showCancelButton: false,
              confirmButtonColor: "#4A8C40",
              cancelButtonColor: "#f7505a",
              confirmButtonText: "OK",
            }).then((result) => {
              if (Object.values(result) == "true") {
                swal({
                  text: "Guardado de datos exitoso",
                  type: data.type,
                  showCancelButton: true,
                  confirmButtonColor: "#449d44",
                  cancelButtonColor: "#d33",
                  confirmButtonText: "OK",
                });
                cambia_banner("3");
              }
            });
          } else {
            swal({
              text: data.mensaje,
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#449d44",
              cancelButtonColor: "#d33",
              confirmButtonText: "OK",
            });
            $("#step-two").removeClass("active");
            $("#solicitud").css("display", "block");
            $("#pestana1").css("display", "block");
            cambia_banner("1");
          }
        }, 200);
      },
      dataType: "json",
      error: function (data) {
        $("#cargando").hide();
        swal({
          text: "Fallo el guardado de datos",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#449d44",
          cancelButtonColor: "#d33",
          confirmButtonText: "OK",
        });
      },
    });
  }
}
function departamento_change(dept,ciud) {
    $(ciud).find('option').remove().end().append(
      '<option value="whatever"></option>').val('whatever');
    $(`${dept} option:selected`).each(function() {
      departamento = $(this).val();
      $.post("utilidades/get_ciudad_dupree_alternativo.php", {
        departamento: departamento
      }, function(data) {
        $(ciud).html(data);
      });
    });
    $(ciud).prop('selectedIndex', 0);
}
function ciudad_change(){
    $('#BAR15').find('option').remove().end().append(
        '<option value="whatever"></option>').val('whatever');
      $("#CIU13 option:selected").each(function() {
        ciudad = $(this).val();
        $.post("utilidades/get_barrio_dupree_alternativo.php", {
          ciudad: ciudad
        }, function(data) {
          $("#BAR15").html(data);
        });
      });
      $("#BAR15").prop('selectedIndex', 0);
}
function veri_input(input,proceso){
  let data={
    proceso: "validaciones",
    tipo: proceso,
  }
  data[proceso]=input.val()
  soli(data,"GET")
    .then(function (e) {

    })
    .catch(function(e){
      input.val("");
    });
  //console.log("aqui estoy"+ input.val());
}
function lide_change(){
  let nume_lide = $("#RF99").val();
  let data = {
    proceso: "vali_lide",
    nume_lide: nume_lide,
  };
  soli(data,"GET")
    .catch(function(err){
      $("#RF99").val("");
    });
}
