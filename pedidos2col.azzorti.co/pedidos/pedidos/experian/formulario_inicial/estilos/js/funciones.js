function llen_info(auto){
  //habilita los campos

  //datos experian primera pestaña
  $("#TD1")[0].selectedIndex = auto.pestana1.tipo_docu;
  $("#NI2").val(auto.pestana1.cedula);
  $("#CEL14").val(auto.pestana1.celular);
  $("#fech_expe").val(auto.pestana1.fech_expe);
  $("#PN7").val(auto.pestana1.nombre1);
  $("#SN8").val(auto.pestana1.nombre2);
  $("#PA9").val(auto.pestana1.apellido1);
  $("#SA10").val(auto.pestana1.apellido2);

  if(!auto.habi_preg_seg){
    habilitar_campos();
  }

  //datos post validacion digital
  $("#datepicker").val(auto.pestana1.fecha_naci);
  $("#MF88")[0].selectedIndex = auto.pestana1.genero;
  dpto = $("#DPT12");
  dpto[0].selectedIndex = auto.pestana1.departamento;
  dpto.trigger("change");
  dpto.trigger("change");
  $("#datepicker").trigger("change");
  setTimeout(function(){
    $("#CIU13")[0].selectedIndex = auto.pestana1.ciudad;
    $("#CIU13").trigger("change");
  },1000);
  $("#CCE17").val(auto.pestana1.correo);
  //REFERENCIAS
  $("#ref_familiar_nombre").val(auto.pestana1.ref_nombre);
  $("#ref_fami_depa")[0].selectedIndex = auto.pestana1.ref_dpto;
  $("#ref_fami_depa").trigger("change");
  $("#ref_familiar_celular").val(auto.pestana1.ref_celu);
  $("#ref_familiar_parentesco")[0].selectedIndex = auto.pestana1.ref_pare;
  $("#ref_personal_nombre").val(auto.pestana1.refp_nombre);
  $("#ref_pers_depa")[0].selectedIndex = auto.pestana1.ref_dpto;
  $("#ref_pers_depa").trigger("change");
  $("#ref_personal_celular").val(auto.pestana1.refp_celu);

  setTimeout(function(){
    $("#ref_fami_ciud")[0].selectedIndex = auto.pestana1.ref_ciud;
    $("#ref_pers_ciud")[0].selectedIndex = auto.pestana1.refp_ciud;
  },1000)

  //TERMINOS

  $("#poli-trat").prop( "checked", auto.pestana1.trat_dato);
  $("#cons-fina").prop( "checked", auto.pestana1.cons_fina);

  //segunda pestaña
  setTimeout(function(){
    $("#BAR15")[0].selectedIndex = auto.pestana2.barrio;
    $("#BAR15").trigger("change");
  },1500);

  $("#tipo_via")[0].selectedIndex = auto.pestana2.direccion.tipo_via;
  $("#nume_via").val(auto.pestana2.direccion.nume_via);
  $("#letr_via")[0].selectedIndex = auto.pestana2.direccion.letr_via;
  $("#dire_bis")[0].selectedIndex = auto.pestana2.direccion.dire_bis;
  $("#dire_num1").val(auto.pestana2.direccion.dire_num1);
  $("#dire_letr")[0].selectedIndex = auto.pestana2.direccion.dire_bis;
  $("#dire_num2").val(auto.pestana2.direccion.dire_num2);
  $("#dire_cuad")[0].selectedIndex = auto.pestana2.direccion.dire_cuad;
  $("#complemento").val(auto.pestana2.complemento);
  if(auto.pestana_activa === 2){
    setTimeout(function(){
    $("#siguiente").trigger("click");
  },1000);
  }
  direccion();
}

//armador de la direccion
function direccion() {
  var tipo_via = $("#tipo_via").val();
  var nume_via = $("#nume_via").val();
  var letr_via = $("#letr_via").val();
  var dire_bis = $("#dire_bis").val();
  var dire_num1 = $("#dire_num1").val();
  var dire_letr = $("#dire_letr").val();
  var dire_num2 = $("#dire_num2").val();
  var dire_cuad = $("#dire_cuad").val();

  var direccion = $("#dire").val();
  var direccion = $.trim(direccion);
  var complemento = $("#complemento").val();
  var complemento = $.trim(complemento);
  var data =
    tipo_via +
    " " +
    nume_via +
    " " +
    letr_via +
    " " +
    dire_bis +
    " " +
    dire_num1 +
    " " +
    dire_letr +
    " " +
    dire_num2 +
    " " +
    dire_cuad +
    " " +
    complemento;
  var data = $.trim(data);

  $("#dire_fina").val(data);
  var direccion_fina = $("#dire_fina").val();
  var dire_entre = $("#DRE904").val();
  if (dire_entre == "Si") {
    $("#DRE905").prop("disabled", true);
    $("#dire_fina").val(data);
    $("#DRE905").val(data);
    return false;
  } else {
    $("#DRE905").prop("disabled", false);
    $("#DRE905").val("");
    $("#dire_fina").val(data);
    return false;
  }
}

/* metodo para el cambio de la direccion de entrega */
function dire_entre() {
  var dire_entre = $("#DRE904").val();
  var direccion = $("#dire_fina").val();
  if (dire_entre == "Si") {
    $("#DRE905").prop("disabled", true);
    $("#DRE905").val(direccion);
    return false;
  } else {
    $("#DRE905").prop("disabled", false);
    $("#DRE905").val("");
    return false;
  }
}

/* metodo que muestra el campo de cedula lider */
function sele_lide() {
  var togg_sele = $("input:radio[name=togg_sele]:checked").val();
  if (togg_sele == "SI") {
    $("#cedu_lide").show();
  } else {
    $("#cedu_lide").hide();
  }
}

//Retornar valor convertido a mayusculas
function Mayusculas(tx) {
  return tx.toUpperCase();
}
//valida solo numeros
function Numeros(string) {
  //Solo numeros
  var out = "";
  var filtro = "1234567890"; //Caracteres validos

  //Recorrer el texto y verificar si el caracter se encuentra en la lista de validos
  for (var i = 0; i < string.length; i++)
    if (filtro.indexOf(string.charAt(i)) != -1)
      //Se añaden a la salida los caracteres validos
      out += string.charAt(i);

  //Retornar valor filtrado
  return out;
}
//validar campos de text
function NumText(string) {
  //solo letras y numeros
  var out = "";
  //Se añaden las letras validas
  var filtro =
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_ "; //Caracteres validos

  for (var i = 0; i < string.length; i++)
    if (filtro.indexOf(string.charAt(i)) != -1) out += string.charAt(i);
  return out;
}

//validar campos de text
function Solotexto(string) {
  //solo letras
  var out = "";
  //Se añaden las letras validas
  var filtro = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "; //Caracteres validos

  for (var i = 0; i < string.length; i++)
    if (filtro.indexOf(string.charAt(i)) != -1) out += string.charAt(i);
  return out;
}
/* Valida el correo electronico */
function isValidEmailAddress(emailAddress) {
  var pattern = new RegExp(
    /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
  );
  return pattern.test(emailAddress);
}

function soli(data,tipo="POST") {
  return new Promise(function (resolve, reject) {
    if(tipo === "POST"){
      data = JSON.stringify(data);
    }
    let host = $(location).attr("host");
    let baseUrl = `https://${host}/pedidos/pedidos/experian/formulario_inicial/api/solicitud.php`;
    let config = {
      type: tipo,
      url: baseUrl,
      data: data,
      beforeSend: function () {
        $("body").waitMe({
          effect: "bounce",
          text: "",
          bg: "rgba(255,255,255,0.7)",
          color: "#fedd3c",
          textPos: "vertical",
        });
      },
      complete: function () {
        $("body").waitMe("hide");
      },
    }
    if(tipo === "POST"){
      config["contentType"]  = "application/json; charset=utf-8";
      config["dataType"] = "json";
    }
    $.ajax(config)
      .done(function (respuesta, textoEstado, estado) {
        resp_gene = {
          respuesta: respuesta,
          estado: estado.status,
        };
        resolve(resp_gene);
      })
      .fail(function (err) {
        respuesta = err.responseJSON;
        if (err.status === 403 || err.status === 400) {
          swal({
            text: respuesta.descripcion,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#449d44",
            confirmButtonText: "Aceptar",
          }).then(function (result) {
            reject(err);
          });
        } else if (err.status === 500) {
          swal({
            text: "Ha ocurrido un error en el servidor.",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#449d44",
            confirmButtonText: "Aceptar",
          });
        }
      });
  });
}

function modal_experian(proceso) {
  if (proceso === "abrir") {
    $("#modal-experian").modal({
      backdrop: "static",
      keyboard: false,
    });
  } else if (proceso === "cerrar") {
    $("#modal-experian").modal("toggle");
  }
}

function modal_preguntas(proceso) {
  if (proceso === "abrir") {
    $("#modal-preguntas").modal({
      backdrop: "static",
      keyboard: false,
    });
  } else if (proceso === "cerrar") {
    $("#modal-preguntas").modal("toggle");
  }
}
function seleccion_proceso(info) {
  switch (info.proceso) {
    case "preguntas_seguridad":
      preguntas = info.preguntas;
      try {
        $.experian.preguntas = preguntas.Question;
        crear_preguntas(preguntas);
        $.experian.transaccion = preguntas.Transaccion;
      } catch (error) {
        console.error(error);
      }
      modal_preguntas("abrir");
      break;
    case "flujo_otp":
      $.experian.indi_tran = info.datos;
      modal_experian("abrir");
      break;
    default:
      mensaje("error", "no se ha definido este proceso");
  }
}
function mensaje_accion(tipo, mensaje) {
  return new Promise(function (resolve, reject) {
    swal({
      text: mensaje,
      type: tipo,
      confirmButtonColor: "#449d44",
      confirmButtonText: "OK",
    }).then(function (data) {
      resolve(data);
    });
  });
}
function calcularEdad() {
  var fecha = $(this).val();
  var hoy = new Date();
  var match = /(\d+)\/(\d+)\/(\d+)/.exec(fecha);
  var fecha_fina = match[3] + "/" + match[2] + "/" + match[1];
  var fecha_nuev = new Date(fecha_fina);
  var cumpleanos = new Date(fecha_nuev);
  var edad = hoy.getFullYear() - cumpleanos.getFullYear();
  var m = hoy.getMonth() - cumpleanos.getMonth();
  if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
    edad--;
  }
  $("#edad").val(edad);
}
function mensaje(tipo,mensaje){
  swal({
      text: mensaje,
      type: tipo,
      confirmButtonColor: '#449d44',
      confirmButtonText: 'OK'
  });
}

function habilitar_campos(){
  $("#btn-experian").remove();
  $("#datepicker").prop("disabled", false);
  $("#MF88").prop("disabled", false);
  $("#DPT12").prop("disabled", false);
  $("#CIU13").prop("disabled", false);
  $("#CCE17").prop("disabled", false);
  $("#siguiente").prop("disabled", false);
  $("#ref_familiar_nombre").prop("disabled", false);
  $("#ref_familiar_celular").prop("disabled", false);
  $("#ref_familiar_parentesco").prop("disabled", false);
  $("#ref_personal_nombre").prop("disabled", false);
  $("#ref_personal_celular").prop("disabled", false);
  $("#ref_pers_depa").prop("disabled", false);
  $("#ref_pers_ciud").prop("disabled", false);
  $("#ref_fami_depa").prop("disabled", false);
  $("#ref_fami_ciud").prop("disabled", false);
}
function cambia_banner(id){
  $("#logo").attr("src", `imagenes/banner_${id}.jpg`);
}
