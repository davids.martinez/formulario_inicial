$(document).ready(function () {
    /**
     * Variables elementos del DOM
     */
    let ventanas = $('#modal-experian .setup-content'),
        btn_step_1 = $('#step-1-btn'),
        btn_step_2 = $('#step-2-btn'),
        celu_terc = $("#ex_celu_terc"),
        otp = $("#ex_otp"),
        step_1_href = $("a[href='#step-1']"),
        step_2_href = $("a[href='#step-2']"),
        step_1 =  $("#step-1"),
        step_2 =  $("#step-2"),
        nume_iden = $("#NI2");
    ventanas.hide();
    step_1.show();
    /**
     * Accion del primer paso de la solicitud telefono
     *
     * @return void
     */
    btn_step_1.on('click',function (e) {
        e.preventDefault();
        let error = -1;
        let form_group = celu_terc.parent(".form-group");
        let data = {
            "proceso": "post_conf_celu",
            "indi_tran": $.experian.indi_tran,
            "nume_iden": nume_iden.val(),
            "nume_celu" : celu_terc.val()
        }

        if(celu_terc.val().length === 0) {
            error = "Debe confirmar teléfono celular";

        }else if(celu_terc.val().length != 10) {
            error = "El campo teléfono celular debe contener 10 números";
        }

        if(error != -1){
            mensaje("error", error)
            form_group.addClass("has-error");
            return false;
        }
        form_group.removeClass("has-error");
        soli(data)
            .then(function(datos){
                if(datos.estado === 200){
                    step_1_href.removeClass("btn-success").addClass("btn-default disabled");
                    step_2_href.removeClass("btn-default disabled").addClass("btn-success");
                    step_1.hide();
                    step_2.show();
                }
            }).catch(function(err){
                respuesta = err.responseJSON;
                mensaje("error",`${respuesta.descripcion}`);
            });


    })

    btn_step_2.on("click",function(e){
        e.preventDefault();
        let form_group = otp.parent(".form-group");
        let error = -1;

        let data = {
            "proceso": "confirma_pin",
            "nume_iden": nume_iden.val(),
            "indi_tran": $.experian.indi_tran,
            "codi_otp" : otp.val()
        }
        if(otp.val().length === 0) {
            error = "Debe escribir el código enviado al celular.";
        }else if(otp.val().length != 4){
            error = "El código debe contener 4 dígitos.";
        }

        if(error != -1){
            mensaje("error", error)
            form_group.addClass("has-error");
            otp.val("");
            return false;
        }

        form_group.removeClass("has-error");

        soli(data)
        .then(function(datos){
            if(datos.estado === 200){
                mensaje_accion("success",datos.respuesta.descripcion)
                    .then(function(ventana){
                        modal_experian("cerrar");
                        habilitar_campos();
                    });
            }else if ( datos.estado === 201){
                let nume_tran = datos.respuesta.respuesta;
                $.experian.indi_tran = nume_tran
                mensaje_accion("info",datos.respuesta.descripcion)
                .then(function(ventana){
                    otp.val("");
                    celu_terc.val("");
                    step_1_href.removeClass("btn-default disabled").addClass("btn-success");
                    step_2_href.removeClass("btn-success").addClass("btn-default disabled");
                    step_1.show();
                    step_2.hide();
                });
            }
        }).catch(function(err){
            otp.val("");
            respuesta = err.responseJSON;
            respuestaDefinida = typeof respuesta.respuesta === "undefined";
            if(!respuestaDefinida){
              tipo_error = respuesta.respuesta;
              if(tipo_error==="error_validacion" || tipo_error==="error"){
                window.location.reload();
              }
            }
        });
    });
});