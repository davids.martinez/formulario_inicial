$(document).ready(function () {
    $.respuestas = [];
    $("#modal-preguntas").on("click",".btn-preguntas", function(){
        let nume_iden = $('#NI2').val();
        let ultimoBoton = $("#modal-preguntas .btn-preguntas").last().data("id");
        let idsteep = $(this).data("id");
        let steep = $(`#step-p-${idsteep}`);
        let nextSteep =$(`#step-p-${idsteep + 1}`);
        let cabecera_step = $(`a[href='#step-p-${idsteep}']`);
        let next_cabecera_step = $(`a[href='#step-p-${idsteep + 1}']`);
        let isValid = $(`#step-p-${idsteep} input[name=radio]`).is(":checked");
        let respuesta = $(`#step-p-${idsteep} input[name=radio]:checked`);
        let index = cabecera_step.data("index");

        if(!isValid) {
            steep.children(".panel-body").addClass("has-error");
            mensaje("error",`Por favor seleccione una opcion de la pregunta ${idsteep + 1}`);
            return;
        }

        $.experian.preguntas[index].respuesta = respuesta.val();


        if(ultimoBoton === idsteep){
            envi = {
                "proceso": "envi_preg",
                "nume_iden": nume_iden,
                "preguntas" : $.experian.preguntas,
                "transaccion" : $.experian.transaccion
            }
            soli(envi)
                .then(function(data){
                    if(data.estado == 200){
                        let desc = data.respuesta.descripcion;
                        mensaje_accion("success",desc)
                            .then(function(envio){
                                habilitar_campos();
                                modal_preguntas("cerrar");
                            });
                    }
                })
                .catch(function(err){
                    window.location.reload();
                });
            //mensaje("success", "informaccion enviada correctamente");
            return;
        }

        steep.hide();
        nextSteep.show();
        cabecera_step.removeClass("btn-success");
        cabecera_step.addClass("btn-default disabled");
        next_cabecera_step.removeClass("btn-default disabled");
        next_cabecera_step.addClass("btn-success");

    });
});

function crear_preguntas(preguntas){
    let encabezado = "";
    let contEncabezado = "";
    let form_modal = $("#form-modal-preguntas");
    let selecEncabezado = $("#modal-preguntas .setup-panel");
    let contPie = "";
    let cuerpo = "";
    let cuerpoAcum = "";
    cantidad = preguntas.Question.length - 1;


    $.each(preguntas.Question, function(index,dato){
        let atributos = dato["@attributes"];
        encabezado =
                `${encabezado}
                <div class="stepwizard-step col-xs-2">
                  <a href="#step-p-${index}" data-index="${index}" type="button" class="btn btn-step btn-default disabled btn-circle">${index + 1}</a>
                  <p><small>Pregunta</small></p>
                </div>`;

                contEncabezado  = `
                <div class="panel panel-primary setup-content" id="step-p-${index}">
                    <div class="panel-heading">
                    <h3 class="panel-title">Pregunta ${index + 1}</h3>
                    </div>
                    <div class="panel-body">
                        <h4>${atributos.Text}</h4>`;
                contPie = `<button class="btn btn-primary pull-right btn-preguntas" data-id="${index}" id="step-p-btn-${index}" type="button">Continuar</button>
                    </div>
                </div>`;

        $.each(dato.Answer,function(j,pregunta){
            checkEnd = j === dato.Answer.length - 1;
            cuerpo = `
            ${cuerpo}
                <div class="radio">
                    <label>
                    <input type="radio" name="radio" value="${j}">
                    ${pregunta}
                    </label>
                </div>
                `;
            if(checkEnd){
                cuerpoAcum=`
                    ${cuerpoAcum}
                    ${contEncabezado}
                    ${cuerpo}
                    ${contPie}
                `;
                cuerpo = "";
                if(cantidad === index){
                    form_modal.html(cuerpoAcum);
                    selecEncabezado.html(encabezado);
                    let ventanas = $('#modal-preguntas .setup-content');
                    let btnCabecera = $("#modal-preguntas .setup-panel a").first();
                    btnCabecera.removeClass("btn-default disabled");
                    btnCabecera.addClass("btn-success")
                    ventanas.hide();
                    ventanas.first().show();
                }
            }
        });

    });
}
