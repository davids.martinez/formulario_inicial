Vue.use(window.vuelidate.default);
var required = validators.required;
var minLength = validators.minLength;
var maxLength = validators.maxLength;

new Vue({
    el: "#app",
    vuetify: new Vuetify(),
    mounted: function () {
        this.carga.pagina = false;
        this.carga_tipo_docu();
        this.retr_tipo_docu = _.debounce(this.vali_docu,1000);
    },
    data: {
        paso: 1,
        alerta: {
            texto: "",
            visible: false
        },
        datos: {
            tipo_docu: "",
            nume_iden: null,
        },
        estilos: {
            elevacion: 2,
        },
        rutas: {
            banner: "../imagenes/imag_experian.png",
        },
        seleccion: {
            tipodoc: [{
                text: "Seleccione una opción",
                value: "",
            }, ],
        },
        deshabilitado: {
            tipo_docu: true,
        },
        carga: {
            pagina: true,
            tipo_docu: true,
        },
    },
    validations: {
        datos: {
            tipo_docu: {
                required
            },
            nume_iden:{
                required,
                minLength: maxLength(15)
            }
        }
    },
    watch:{
            "datos.nume_iden": function(){
                this.retr_tipo_docu();
            }
    },
    methods: {
        carga_tipo_docu: function () {
            _this = this;
            axios
                .get("solicitud.php", {
                    params: {
                        proceso: "get_tipo_docu",
                    },
                })
                .then(function (response) {
                    response.data.respuesta.map(function (docu, index) {
                        obj = {
                            text: docu.nomb_docu,
                            value: docu.cons_docu,
                        };
                        _this.seleccion.tipodoc.push(obj);
                        return 0;
                    });
                    _this.deshabilitado.tipo_docu = false;
                    _this.carga.tipo_docu = false;
                })
                .catch(function (error) {
                    console.log(error);
                });

        },
        vali_docu: function() {
            let _this = this;
            let nume_iden = this.datos.nume_iden;
            axios
                .get("solicitud.php", {
                    params: {
                        proceso: "vali_ases",
                        nume_iden: nume_iden
                    },
                })
                .then(function (response) {
                    if(response.status === 201){
                        _this.alerta.visible = true;
                        _this.alerta.texto = response.data.descripcion;
                    }else if(response.status === 200){
                        _this.alerta.visible = false;
                        _this.alerta.texto = "";
                    }

                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        error_mens: function(campo){
            campo = this.$v.datos[campo];
            const errors = [];
            if (!campo.$dirty || campo.invalid) return errors
            !campo.required && errors.push('Este campo es obligatorio')
            !campo.minLength && errors.push('El campo no debe superar los 10 caracteres')
            return errors
        }
    },
});