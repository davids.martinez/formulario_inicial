<?php

/*
 * Georeferenciacion automatica 
 * @autor david_neira 21/05/2014
 */
include("../nusoap/lib/nusoap.php");
include("../fun_halla_codi_terc_secc.php");
require_once("cadena.php");

function geo_cliente($dire_terc, $codi_camp, $cons_terc, $codi_usua, $desc_orig) {

  global $conector_ifx, $base_ifx, $form_deta, $nume_secu, $var_pin, $mensaje_error_php, $error, $fech_hora_sist_ifx, $esta_form;

  $sitidata = "http://172.16.1.8:8080/sitidata/wsSitidataEstandar.php?wsdl";
  $wsdl = "http://172.16.1.8:8080/webservice/webservice.wsdl";
  //Almacena la ruta del Web services en una variable
  $cli_sit = new nusoap_client($sitidata, "wsdl"); //Crea una instancia del webservice Sitidata
  $cliente = new nusoap_client($wsdl, "wsdl"); //Crea una instancia del webservice

  $usuario = 'pruebas';
  $pass = '123456';
  $aprox = 3;
  $ip = "172.16.1.8";
  $puerto = 80;
  if ($fp = @fsockopen($ip, $puerto, $ERROR_NO, $ERROR_STR, (float) 0.5)) {
    fclose($fp);
    //die("server online");  
  } else {
    die("server offline");
    $mensaje_error_php = 1;
  }

  //auditoria   
  $query = "insert into geo_clie_audi select * from geo_clie where cons_terc='$cons_terc'";
  $resureg = ejecuta($conector_ifx, $base_ifx, $query, 0);

  $query = "delete from geo_clie where cons_terc='$cons_terc'";
  $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);

  $query = "select tab_clie.cons_terc,'$dire_terc','$dire_terc',tab_ciud.nomb_ciud,tab_clie.nomb_barr,tab_clie.codi_zona,tab_clie.cons_ciud,tab_clie.codi_terc,tab_clie.acti_esta,codi_terr from tab_clie,tab_ciud, tab_barr where  tab_clie.cons_ciud = tab_ciud.cons_ciud and tab_clie.cons_barr=tab_barr.cons_barr and tab_clie.cons_terc='$cons_terc'";
  $resureg = ejecuta($conector_ifx, $base_ifx, $query, 0);
  $arr_dato = prepara_dato($base_ifx, $resureg);
  $nroreg = filas($base_ifx, $arr_dato, $resureg);

  if ($nroreg == 0) {
    
  } else {
    for ($i = 0; $i < $nroreg; $i++) {
      // Se descompone el resultado de la consulta en variables para pasar como parametros al Web services de georreferenciacion

      $cons_terc = (int) trim(obtiene_dato($base_ifx, $arr_dato, $resureg, $i, 0));
      $dire_terc = remove_accent(trim(obtiene_dato($base_ifx, $arr_dato, $resureg, $i, 1)));
      $dire_terc = preg_replace('[\s+]', ' ', $dire_terc);
      $dire_entr = remove_accent(trim(obtiene_dato($base_ifx, $arr_dato, $resureg, $i, 2)));
      $nomb_ciud = trim(obtiene_dato($base_ifx, $arr_dato, $resureg, $i, 3));
      $nomb_barr = trim(obtiene_dato($base_ifx, $arr_dato, $resureg, $i, 4));
      $cons_ciud = trim(obtiene_dato($base_ifx, $arr_dato, $resureg, $i, 6));
      $terc_codi = trim(obtiene_dato($base_ifx, $arr_dato, $resureg, $i, 7));
      $acti_esta = trim(obtiene_dato($base_ifx, $arr_dato, $resureg, $i, 8));
      $estado_entr = trim(obtiene_dato($base_ifx, $arr_dato, $resureg, $i, 9));
      $codi_zona = trim(obtiene_dato($base_ifx, $arr_dato, $resureg, $i, 5));


      //geo con barrios
      $query_vent = "select indi_barr from tab_ciud_vent where codi_ciud ='$cons_ciud'";
      $resu_vent = ejecuta($conector_ifx, $base_ifx, $query_vent, 0);
      $arr_dato_vent = prepara_dato($base_ifx, $resu_vent);
      $indi_barr = trim(obtiene_dato($base_ifx, $arr_dato_vent, $resu_vent, 0, 0));

      if ($indi_barr == 1) {
        $dire_terc = $dire_terc . " " . $nomb_barr;
      }

      $nomb_ciud = explode("(", $nomb_ciud);
      $barrio = "";
      $nomb_ciud = $nomb_ciud[0];
      $dir_barr_ciudad = array(
          array(
              $dire_terc,
              $barrio,
              $nomb_ciud
          )
      );

      // Se hace llamado a metodo del nusoap instanciado con el web service pasando los parametros de la consultas para la direccion del cliente 

      $params = array(
          "direccion" => $dire_terc,
          "ciudad" => $nomb_ciud,
          "barrio" => $barrio,
          "usuario" => $usuario,
          "clave" => $clave,
          "aprox" => $aprox
      );
      $result = $cliente->call('procesarGeo', array(
          'dir_barr_ciudad' => $dir_barr_ciudad,
          'usuario' => $usuario,
          'pass' => $pass,
          'aprox' => $aprox
      ));

      //datos georeferenciador

      $geo_direc = explode(";", $result);
      $direcciontraducida = $geo_direc[0];
      $fuente = $geo_direc[1];
      $barrio = remove_accent($geo_direc[8]);
      $localidad = remove_accent($geo_direc[10]);
      $cx = $geo_direc[12];
      $cy = $geo_direc[13];
      $zona1 = $geo_direc[16];
      $zona2 = $geo_direc[17];
      $secc1 = $geo_direc[18];
      $esta = $geo_direc[22];


      $geo = "Georreferenciador";

      /*            if ($esta != 'B' and $esta != 'D' and $esta != 'F' and $esta != 'I' and $esta != 'L' and $esta != 'M' and $esta != 'N' and $esta != 'Y') { */

      //datos sitidata

      $arrayResult = $cli_sit->call('enriquecer', $params);
      $direcciontraducida = $arrayResult['geo'][0];
      $codigodireccion = $arrayResult['geo'][3];
      $fuente = $arrayResult['geo'][4];
      $barrio = remove_accent($arrayResult['geo'][11]);
      $localidad = remove_accent($arrayResult['geo'][13]);
      $cx = $arrayResult['geo'][17];
      $cy = $arrayResult['geo'][18];
      if (strlen($cy) == 0) {
        $cy = 0;
      }
      $zona1 = $arrayResult['geo'][22];
      $secc1 = $arrayResult['geo'][23];
      if (strlen($cx) == 0) {
        $cx = 0;
      }
      $esta = $arrayResult['geo'][25];
      $geo = "Sitidata";
      // }

      $arre_geo = array(
          $zona1,
          $secc1,
          $esta
      );

      $barriotraducido = '';
      $codigodireccion = '';
      $codigodireccionen = '';
      $diralterna = '';
      $manzana = '';
      $nivsocio = '';
      $cx_entr = 0;
      $cy_entr = 0;
      $zona1_entr = '';
      $estado_entr = '';
      $barrio_entr = '';
      if ($cons_terc > 0) {
        $veri_geo = "select cons_terc from geo_clie where cons_terc = $cons_terc";
        $res_geoc = ejecuta($conector_ifx, $base_ifx, $veri_geo, 0);
        $arr_datc = prepara_dato($base_ifx, $res_geoc);
        $nroreg1 = filas($base_ifx, $arr_datc, $res_geoc);

        if ($desc_orig != "0") {
          $query = "INSERT INTO audi_tab_clie (cons_terc,tipo_docu,codi_terc,nume_iden,codi_padr,codi_desp,cons_ciud,codi_zona,nomb_terc,apel_terc,dire_terc,nomb_barr,tele_terc,fech_ingr,camp_ingr,fech_naci,cupo_cred,cons_bloq,inte_mora,cond_pago,digi_cona,digi_bogo,digi_colo,digi_colm,auto_rete,regi_trib,acti_usua,acti_hora,acti_esta,esta_acti,cons_carg,nume_pedi,dife_maxi,dife_actu,tele_ter2,celu_ter1,celu_ter2,cons_barr,nuev_ciud,nuev_terr,cons_lide,nuev_lide,tipo_gene,mail_ases,clav_ases,fech_modi,usua_modi,tipo_proc,imag_asig,imag_fech,pree_pedi,tipo_ases,usua_imag,nomb_vere,nive_educ,nive_ingr,esta_civi,nume_hijo,dire_entr,barr_entr,ciud_entr,vere_entr,tele_entr,celu_entr,dest_entr,auto_port,cata_camp) 
					SELECT cons_terc,tipo_docu,codi_terc,nume_iden,codi_padr,codi_desp,cons_ciud,codi_zona,nomb_terc,apel_terc,dire_terc,nomb_barr,tele_terc,fech_ingr,camp_ingr,fech_naci,cupo_cred,cons_bloq,inte_mora,cond_pago,digi_cona,digi_bogo,digi_colo,digi_colm,auto_rete,regi_trib,acti_usua,acti_hora,acti_esta,esta_acti,cons_carg,nume_pedi,dife_maxi,dife_actu,tele_ter2,celu_ter1,celu_ter2,cons_barr,nuev_ciud,nuev_terr,cons_lide,nuev_lide,tipo_gene,mail_ases,clav_ases,current,'$codi_usua','$desc_orig',imag_asig,imag_fech,pree_pedi,tipo_ases,usua_imag,nomb_vere,nive_educ,nive_ingr,esta_civi,nume_hijo,dire_entr,barr_entr,ciud_entr,vere_entr,tele_entr,celu_entr,dest_entr,auto_port,cata_camp 
					FROM tab_clie 
					WHERE cons_terc='$cons_terc'";
          ejecuta($conector_ifx, $base_ifx, $query, 0);
        }

        if ($esta != 'A' && $esta != 'C' && $esta != 'E' && $esta != 'G' && $esta != 'H' && $esta != 'J' && $esta != 'R' && $esta != 'W' && strlen($esta)) {

          if ($nroreg1 == 0) {
            $actu_geo = "insert into geo_clie (cons_terc,dire_terc,codi_zona,cx,cy,estado,direcciontraducida,barriotraducido,codigodireccion,codigodireccionen,fuente,diralterna,manzana,barrio,localidad,nivsocio,zona1,zona2,cx_entr,cy_entr,zona1_entr,estado_entr,barrio_entr,codi_camp,acti_hora,acti_usua,desc_orig) values ($cons_terc,'$dire_terc','$codi_zona',$cx,$cy,'$esta','$direcciontraducida','$barriotraducido','$codigodireccion','$codigodireccionen','$fuente','$diralterna','$manzana','$barrio','','$nivsocio','$zona1','$secc1',$cx_entr,$cy_entr,'$zona1_entr','$estado_entr','$barrio_entr',$codi_camp,current,'$codi_usua','$desc_orig')";

            $query = "delete from geo_inco_clie where cons_terc='$cons_terc'";
            $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
          }

          $upd_estado = "update tab_clie set acti_esta = 'G' where cons_terc = $cons_terc";
          ejecuta($conector_ifx, $base_ifx, $upd_estado, 0);

          $res_geo = ejecuta($conector_ifx, $base_ifx, $actu_geo, 0);
        }
        // Inconsistencias		
        else if ($esta == 'A' or $esta == 'C' or $esta == 'E' or $esta == 'G' or $esta == 'H' or $esta == 'J' or $esta == 'R' or $esta == 'W' or $esta == '') {
          /*
           * Geo interno
           */
          $datos_inte = geo_interno($dire_terc, $nomb_ciud, $nomb_barr);
          $datos_inte = json_decode($datos_inte, true);
          $datos_inte = $datos_inte['geo'];
          $vali_inco = $datos_inte[7];
          $vali_inco = "X99";

          if ($vali_inco != "X99") {

            $zona_geo = $datos_inte[6];
            $codi_secc = $datos[7];
            $esta = "B";
            $barrio = "";
            $secc_dupr = $codi_secc;
            $codi_secc = "0" . $datos_inte[7];

            $actu_geo = "insert into geo_clie (cons_terc,dire_terc,codi_zona,cx,cy,estado,direcciontraducida,barriotraducido,codigodireccion,codigodireccionen,fuente,diralterna,manzana,barrio,localidad,nivsocio,zona1,zona2,cx_entr,cy_entr,zona1_entr,estado_entr,barrio_entr,codi_camp,acti_hora,acti_usua,desc_orig) values ($cons_terc,'$dire_terc','$codi_zona',$cx,$cy,'$esta','$direcciontraducida','$barriotraducido','$codigodireccion','$codigodireccionen','$fuente','$diralterna','$manzana','$barrio','','$nivsocio','$zona1','$secc1',$cx_entr,$cy_entr,'$zona1_entr','$estado_entr','$barrio_entr',$codi_camp,current,'$codi_usua','GEO_INTERNO')";
            $res_geo = ejecuta($conector_ifx, $base_ifx, $actu_geo, 0);

            $query = "delete from geo_inco_clie where cons_terc='$cons_terc'";
            $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);

            $upd_estado = "update tab_clie set acti_esta = 'G' where cons_terc = $cons_terc";
            ejecuta($conector_ifx, $base_ifx, $upd_estado, 0);
          } else {
            $veri_inco = "select cons_terc from geo_inco_clie where cons_terc = $cons_terc";
            $res_inco = ejecuta($conector_ifx, $base_ifx, $veri_inco, 0);
            $arr_dati = prepara_dato($base_ifx, $res_inco);
            $nroreg2 = filas($base_ifx, $arr_dati, $res_geoi);
            if ($nroreg2 == 0) {
              $actu_inco = "insert into geo_inco_clie (cons_terc,dire_inco,dire_nuev,esta_inco,cx,cy,codi_zona,acti_hora,acti_usua,codi_camp,esta_regi,orig_inco) values ($cons_terc,'$dire_terc','','$esta',0,0,'$codi_zona',current,'$codi_usua',$codi_camp,0,'GEO_INTERNO')";
              ejecuta($conector_ifx, $base_ifx, $actu_inco, 0);
            }

            $upd_estado = "update tab_clie set acti_esta = 'A' where cons_terc = $cons_terc";
            ejecuta($conector_ifx, $base_ifx, $upd_estado, 0);
          }
        } else {
          $veri_inco = "select cons_terc from geo_inco_clie where cons_terc = $cons_terc";
          $res_inco = ejecuta($conector_ifx, $base_ifx, $veri_inco, 0);
          $arr_dati = prepara_dato($base_ifx, $res_inco);
          $nroreg2 = filas($base_ifx, $arr_dati, $res_geoi);
          if ($nroreg2 == 0) {
            $actu_inco = "insert into geo_inco_clie (cons_terc,dire_inco,dire_nuev,esta_inco,cx,cy,codi_zona,acti_hora,acti_usua,codi_camp,esta_regi,orig_inco) values ($cons_terc,'$dire_terc','','$esta',0,0,'$codi_zona',current,'$codi_usua',$codi_camp,0,'$desc_orig')";
            ejecuta($conector_ifx, $base_ifx, $actu_inco, 0);
          }

          $upd_estado = "update tab_clie set acti_esta = 'A' where cons_terc = $cons_terc";
          ejecuta($conector_ifx, $base_ifx, $upd_estado, 0);
        }
      }
    }
  }
  return $arre_geo;
}

/*
 * Georreferenciador interno
 */

function geo_interno($direccion, $nomb_ciud, $nomb_barr) {
  global $conector_ifx, $base_ifx;
  //cons_ciud
  /*$query = "select limit 1 cons_ciud from tab_ciud where nomb_ciud matches ('*$nomb_ciud*') order by cons_ciud asc";
  $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
  $arr_dato = prepara_dato($base_ifx, $resu);
  $cons_ciud = trim(obtiene_dato($base_ifx, $arr_dato, $resu, 0, 0));

  //cons_ciud
  $query = "select limit 1 cons_barr from tab_barr where nomb_barr matches ('*$nomb_barr*') order by cons_barr asc";
  $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
  $arr_dato = prepara_dato($base_ifx, $resu);
  $cons_barr = trim(obtiene_dato($base_ifx, $arr_dato, $resu, 0, 0));


  $direccion = str_replace(" ", "+", $direccion);
  $ch = curl_init();
  //Validacion SSL
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_URL, "https://servicioweb.dupree.col/geo_interno/phpdupree2.1/entregable/busqueda.php?ciudad=$cons_ciud&barrio=$cons_barr&txtDireccion=$direccion&enviando=OK");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $resultado = curl_exec($ch);

  $file = fopen(RUTA_ARCHIVOS . "geo_interno.txt", "a");
  fwrite($file, "https://servicioweb.dupree.col/geo_interno/phpdupree2.1/entregable/busqueda.php?ciudad=$cons_ciud&barrio=$cons_barr&txtDireccion=$direccion&enviando=OK" . "\n");
  fwrite($file, $resultado . "\n");
  fclose($file);

  return $resultado;*/
}

function geo_interno2($direccion, $nomb_ciud, $nomb_barr) {
  global $conector_ifx, $base_ifx;
  //cons_ciud
  $query = "select limit 1 cons_ciud from tab_ciud where nomb_ciud matches ('*$nomb_ciud*') order by cons_ciud asc";
  $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
  $arr_dato = prepara_dato($base_ifx, $resu);
  $cons_ciud = trim(obtiene_dato($base_ifx, $arr_dato, $resu, 0, 0));

  //cons_ciud
  $query = "select limit 1 cons_barr from tab_barr where nomb_barr matches ('*$nomb_barr*') order by cons_barr asc";
  $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
  $arr_dato = prepara_dato($base_ifx, $resu);
  $cons_barr = trim(obtiene_dato($base_ifx, $arr_dato, $resu, 0, 0));


  $direccion = str_replace(" ", "+", $direccion);
  $ch = curl_init();
  include_once("/usr/local/apache2/paginas/servicioweb.dupree.col/geo_interno/phpdupree2.1/entregable/busqueda.php");
  /*
    //Validacion SSL
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_URL, "https://servicioweb.dupree.col/geo_interno/phpdupree2.1/entregable/busqueda.php?ciudad=$cons_ciud&barrio=$cons_barr&txtDireccion=$direccion&enviando=OK");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $resultado = curl_exec($ch);

    $file      = fopen(RUTA_ARCHIVOS . "geo_interno.txt", "a");
    fwrite($file,"https://servicioweb.dupree.col/geo_interno/phpdupree2.1/entregable/busqueda.php?ciudad=$cons_ciud&barrio=$cons_barr&txtDireccion=$direccion&enviando=OK" . "\n");
    fwrite($file, $resultado . "\n");
    fclose($file); */

  return $resultado;
}

function georreferencia($direccion, $ciudad, $barrio) {
  global $conector_ifx, $base_ifx, $codi_usua, $form_deta, $nume_secu, $var_pin, $mensaje_error_php, $error, $cons_barr, $cons_terc;


  $query = "select 	
		  tab_clie.nomb_barr 	
		  from	  
		  tab_clie 
		  where 
		  tab_clie.cons_terc='$cons_terc'";
  $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
  $arre_dato = prepara_dato($base_ifx, $resu);
  $barr_geoi = trim(obtiene_dato($base_ifx, $arre_dato, $resu, 0, 0));


  $sitidata = "http://172.16.1.8:8080/sitidata/wsSitidataEstandar.php?wsdl";
  $wsdl = "http://172.16.1.8:8080/webservice/webservice.wsdl";
  //Almacena la ruta del Web services en una variable
  $cli_sit = new nusoap_client($sitidata, "wsdl"); //Crea una instancia del webservice Sitidata
  $cliente = new nusoap_client($wsdl, "wsdl"); //Crea una instancia del webservice

  $usuario = 'pruebas';
  $pass = '123456';
  $aprox = 3;

  $ip = "172.16.1.8";
  $puerto = 80;
  if ($fp = @fsockopen($ip, $puerto, $ERROR_NO, $ERROR_STR, (float) 0.5)) {
    fclose($fp);
    //die("server online");  
  } else {
    die("server offline");
    $mensaje_error_php = 1;
  }

  $esta_geo = 0;


  $dire_terc = $direccion;
  $nomb_ciud = $ciudad;
  $nomb_barr = "";
  $nomb_ciud = explode("(", $nomb_ciud);
  $nomb_ciud = trim($nomb_ciud[0]);

  //geo con barrios

  $query_vent = "select indi_barr from tab_ciud_vent where nomb_ciud matches ('*$nomb_ciud*')";
  $resu_vent = ejecuta($conector_ifx, $base_ifx, $query_vent, 0);
  $arr_dato_vent = prepara_dato($base_ifx, $resu_vent);
  $indi_barr = trim(obtiene_dato($base_ifx, $arr_dato_vent, $resu_vent, 0, 0));

  $query_vent = "select nomb_barr from tab_barr where cons_barr='$cons_barr'";
  $resu_vent = ejecuta($conector_ifx, $base_ifx, $query_vent, 0);
  $arr_dato_vent = prepara_dato($base_ifx, $resu_vent);
  $nomb_barr = trim(obtiene_dato($base_ifx, $arr_dato_vent, $resu_vent, 0, 0));

  if ($indi_barr == 1) {
    $dire_terc = $dire_terc . " " . $nomb_barr;
  }

  $dir_barr_ciudad = array(
      array(
          $dire_terc,
          $nomb_barr,
          $nomb_ciud
      )
  );

  // Se hace llamado a metodo del nusoap instanciado con el web service pasando los parametros de la consultas para la direccion del cliente 

  $params = array(
      "direccion" => $dire_terc,
      "ciudad" => $nomb_ciud,
      "barrio" => $nomb_barr,
      "usuario" => $usuario,
      "clave" => $clave,
      "aprox" => $aprox
  );


  $result = $cliente->call('procesarGeo', array(
      'dir_barr_ciudad' => $dir_barr_ciudad,
      'usuario' => $usuario,
      'pass' => $pass,
      'aprox' => $aprox
  ));


  //Descomponemos el arreglo resultado en variables

  $geo_direc = explode(";", $result);
  $direcciontraducida = $geo_direc[0];
  $fuente = $geo_direc[1];
  $barrio = remove_accent($geo_direc[8]);
  $localidad = remove_accent($geo_direc[10]);
  $cx = $geo_direc[12];
  $cy = $geo_direc[13];
  $zona1 = $geo_direc[16];
  $zona2 = $geo_direc[17];
  $secc1 = $geo_direc[18];
  $esta = $geo_direc[22];

  $geo = "Georreferenciador";

  /* if ($esta != 'B' and $esta != 'D' and $esta != 'F' and $esta != 'I' and $esta != 'L' and $esta != 'M' and $esta != 'N' and $esta != 'Y') { */

  //datos sitidata	     
  $arrayResult = $cli_sit->call('enriquecer', $params);
  $direcciontraducida = $arrayResult['geo'][0];
  $codigodireccion = $arrayResult['geo'][3];
  $fuente = $arrayResult['geo'][4];
  $barrio = remove_accent($arrayResult['geo'][11]);
  $localidad = remove_accent($arrayResult['geo'][13]);
  $cx = $arrayResult['geo'][17];
  $cy = $arrayResult['geo'][18];
  if (strlen($cy) == 0)
    $cy = 0;
  $zona1 = $arrayResult['geo'][21];
  $secc1 = $arrayResult['geo'][23];
  if (strlen($cx) == 0)
    $cx = 0;
  $esta = $arrayResult['geo'][25];
  $geo = "Sitidata";
  //}


  $barriotraducido = '';
  $codigodireccion = '';
  $codigodireccionen = '';
  $diralterna = '';
  $manzana = '';
  $nivsocio = '';
  $cx_entr = 0;
  $cy_entr = 0;
  $zona1_entr = '';
  $estado_entr = '';
  $barrio_entr = '';


  if ($zona1 == '' or $secc1 == '') {
    $zona1 = "999";
    $secc1 = "99";
    $esta_geo = 0;
  } else {
    $esta_geo = 1;
  }

  $query = "select * from tab_sect where codi_zona = '$zona1' and codi_sect = '$secc1'";
  $resureg = ejecuta($conector_ifx, $base_ifx, $query, 0);
  $arr_dato = prepara_dato($base_ifx, $resureg);
  $nroreg = filas($base_ifx, $arr_dato, $resureg);

  if ($nroreg == 0) {
    $sect_info = 'N';
  } else {
    $sect_info = 'S';
  }

  $direcciontraducida = preg_replace('/' . '#' . '/', "", $direcciontraducida);
  //geo interno
  if ($secc1 == '99') {

    $datos_inte = geo_interno($dire_terc, $ciudad, $nomb_barr);
    //$datos_inte = geo_interno("cr+81b+11+d+68", "BOGOTA", "SANTA CATALINA");
    $datos_inte = json_decode($datos_inte, true);
    $datos_inte = $datos_inte['geo'];
    $vali_inco = $datos_inte[7];
    $vali_inco = "X99";

    if ($vali_inco != "X99") {

      $array = array(
          'zona' => trim($datos_inte[6]),
          'sector' => trim($datos_inte[7]),
          'estado' => "B",
          'dire_trad' => trim($datos_inte[1]),
          'origen' => "Geo Interno",
          'cx' => trim($datos_inte[3]),
          'cy' => trim($datos_inte[4]),
          'esta_geo' => 1,
          'sector_informixx' => "S"
      );
      return ($array);
    }
  }

  $array = array(
      'zona' => $zona1,
      'sector' => $secc1,
      'estado' => $esta_geo,
      'dire_trad' => $direcciontraducida,
      'origen' => $geo,
      'cx' => $cx,
      'cy' => $cy,
      'esta_geo' => $esta,
      'sector_informix' => $sect_info
  );
  return ($array);
}

function recodificacion_masiva($dire_terc, $nomb_ciud, $cons_terc, $coor_x, $coor_y, $zona_dupr, $codi_secc_dupr, $lide_ante) {
  global $conector_ifx, $base_ifx, $codi_usua, $form_deta, $nume_secu, $var_pin, $mensaje_error_php, $error;

  $nomb_ciud = trim($nomb_ciud);

  $query = "select 
		  tab_clie.cons_terc, 
		  tab_clie.dire_terc, 
		  tab_clie.codi_terc, 
		  tab_clie.codi_padr, 
		  tab_clie.codi_zona, 
		  tab_clie.nume_iden,
		  tab_clie.nomb_barr 	
		  from	  
		  tab_clie 
		  where 
		  tab_clie.cons_terc='$cons_terc'";
  $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
  $arre_dato = prepara_dato($base_ifx, $resu);
  $tuplas_soli = filas($base_ifx, $arre_dato, $resu);

  $codi_ante = trim(obtiene_dato($base_ifx, $arre_dato, $resu, 0, 2));
  $codi_padr = trim(obtiene_dato($base_ifx, $arre_dato, $resu, 0, 3));
  $nume_iden = trim(obtiene_dato($base_ifx, $arre_dato, $resu, 0, 5));
  $nomb_barr_ante = trim(obtiene_dato($base_ifx, $arre_dato, $resu, 0, 6));

  if ($tuplas_soli > 0) {


    $veri_defe = "select 
		 tab_ciud_vent.coor_cx, 
		 tab_ciud_vent.coor_cy, 
		 cons_lide 
		 from 
		 tab_ciud_vent, tab_sect
 	     where 
		 tab_ciud_vent.codi_zona=tab_sect.codi_zona
		 and tab_ciud_vent.codi_secc=tab_sect.codi_sect
		 and tab_ciud_vent.codi_zona='$zona_dupr' 
		 and tab_ciud_vent.codi_secc='$codi_secc_dupr'
		 and tab_ciud_vent.nomb_alia matches ('*$nomb_ciud*')";
    $resu_defe = ejecuta($conector_ifx, $base_ifx, $veri_defe, 0);
    $arre_defe = prepara_dato($base_ifx, $resu_defe);
    $tuplas_defe = filas($base_ifx, $arre_defe, $resu_defe);
    debuger("tuplas: " . $tuplas_defe);
    debuger("query: " . $veri_defe);
  }

  if ($tuplas_defe > 0) {
    //$lide_ante = trim(obtiene_dato($base_ifx, $arre_defe, $resu_defe, 0, 2));
    $codi_nuev = $zona_dupr . $codi_secc_dupr;

    //Verificacion coordenadas por defecto

    $veri_defe = "select max(codi_camp) from tab_camp";
    $resu_defe = ejecuta($conector_ifx, $base_ifx, $veri_defe, 0);
    $arre_defe = prepara_dato($base_ifx, $resu_defe);
    $codi_camp_defe = trim(obtiene_dato($base_ifx, $arre_defe, $resu_defe, 0, 0));

    $query = "delete from geo_inco_clie where cons_terc='$cons_terc'";
    ejecuta($conector_ifx, $base_ifx, $query, 0);

    $query = "delete from geo_clie where cons_terc='$cons_terc'";
    ejecuta($conector_ifx, $base_ifx, $query, 0);

    $actu_geo = "insert into geo_clie (cons_terc,dire_terc,codi_zona,cx,cy,estado,direcciontraducida,barriotraducido,codigodireccion,codigodireccionen,fuente,diralterna,manzana,barrio,localidad,nivsocio,zona1,zona2,cx_entr,cy_entr,zona1_entr,estado_entr,barrio_entr,codi_camp,acti_hora,acti_usua,desc_orig) values ($cons_terc,'$dire_terc','$zona_dupr','$coor_x','$coor_y','B','$dire_terc','$nomb_barr_ante','$codigodireccion','$codigodireccionen','MASIVO','$dire_terc','$manzana','$barrio','','$nivsocio','$zona_dupr','$codi_secc_dupr',$coor_x,$coor_y,'$zona1_entr','$estado_entr','$barrio_entr',$codi_camp_defe,current,'$codi_usua','GEO_MASIVO')";
    ejecuta($conector_ifx, $base_ifx, $actu_geo, 0);
    debuger($actu_geo);

    $codi_terc = fun_halla_codi_terc_secc($codi_nuev, $posi);

    //actualizacion lider defecto
    $para_lide = ", cons_lide=" . $lide_ante . ", codi_padr='" . $lide_ante . "', codi_zona='" . $zona_dupr . "', codi_desp='" . $zona_dupr . "', nuev_terr='" . $zona_dupr . $codi_secc_dupr . "'";

    $query = "update tab_clie set codi_terc='$codi_terc', codi_sect='$codi_secc_dupr', dire_terc = '$dire_terc', acti_hora=current $para_lide where cons_terc='$cons_terc'";
    ejecuta($conector_ifx, $base_ifx, $query, 0);

    $query_reco = "INSERT INTO tab_reco (cons_terc, codi_ante, codi_nuev, acti_hora, acti_usua, codi_camp)
						  SELECT '$cons_terc','$codi_ante','$codi_terc',current,'$codi_usua',codi_camp 
						  FROM tab_camp 
						  WHERE today between fech_inic and fech_fina";
    ejecuta($conector_ifx, $base_ifx, $query_reco, 0);
  }
}

function recodificacion($dire_terc, $nomb_ciud, $nomb_barr, $cons_terc, $indi_p42) {
  global $conector_ifx, $base_ifx, $codi_usua, $form_deta, $nume_secu, $var_pin, $mensaje_error_php, $error, $codi_terc, $ctrl_modu;


  $nomb_ciud = trim($nomb_ciud);

  $query = "select 
		  tab_clie.cons_terc, 
		  tab_clie.dire_terc, 
		  tab_clie.codi_terc, 
		  tab_clie.codi_padr, 
		  tab_clie.codi_zona, 
		  tab_clie.nume_iden,
		  tab_clie.nomb_barr 	
		  from	  
		  tab_clie 
		  where 
		  tab_clie.cons_terc='$cons_terc'";
  $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
  $arre_dato = prepara_dato($base_ifx, $resu);

  $codi_ante = trim(obtiene_dato($base_ifx, $arre_dato, $resu, 0, 2));
  $codi_padr = trim(obtiene_dato($base_ifx, $arre_dato, $resu, 0, 3));
  $zona_dupr = trim(obtiene_dato($base_ifx, $arre_dato, $resu, 0, 4));
  $nume_iden = trim(obtiene_dato($base_ifx, $arre_dato, $resu, 0, 5));
  $nomb_barr_ante = trim(obtiene_dato($base_ifx, $arre_dato, $resu, 0, 6));

  //zona y seccion soli_cred   

  $query = "select  
    limit 1
    hist_soli_cred.cons_soli[1,3] as codi_zona,
    hist_soli_cred.cons_soli[4,6] as codi_secc,
    acti_hora    
    from   
    hist_soli_cred
    where 
    hist_soli_cred.nume_iden='$nume_iden' 
	and codi_zona !=''
    and tipo_proc='APP' order by acti_hora desc into temp geo$nume_iden;";
  $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);


  $query = "select	
	      soli_cred.codi_terc[1,3] as codi_zona,
		  soli_cred.codi_terc[4,6] as codi_secc		  
		  from	  
		  soli_cred 
		  where 
		  soli_cred.nume_iden='$nume_iden'
		  union    
	      select  
		  codi_zona,
		  codi_secc    
		  from   
		  geo$nume_iden";
  $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
  $arre_dato = prepara_dato($base_ifx, $resu);
  $tuplas_soli = filas($base_ifx, $arre_dato, $resu);

  /* $asunto ="Geo";
    $de = "No responder <noresponder@dupree.com.co>";
    $mandar_email = enviar_correo($de, "david.neira@dupree.com.co", $asunto, $query, $archivos); */

  //zona seccion tab_clie

  if ($ctrl_modu == "tab_clie") {
    $tuplas_soli = 1;
  }

  if ($tuplas_soli > 0) {
    $zona_dupr = trim(obtiene_dato($base_ifx, $arre_dato, $resu, 0, 0));
    $codi_secc_dupr = trim(obtiene_dato($base_ifx, $arre_dato, $resu, 0, 1));

    //modulo tab_clie
    if ($ctrl_modu == "tab_clie") {
      $zona_dupr = substr($codi_terc, 0, 3);
      $codi_secc_dupr = substr($codi_terc, 3, 3);
    }

    $veri_defe = "select 
		 tab_ciud_vent.coor_cx, 
		 tab_ciud_vent.coor_cy, 
		 cons_lide 
		 from 
		 tab_ciud_vent, tab_sect
 	     where 
		 tab_ciud_vent.codi_zona=tab_sect.codi_zona
		 and tab_ciud_vent.codi_secc=tab_sect.codi_sect
		 and tab_ciud_vent.codi_zona='$zona_dupr' 
		 and tab_ciud_vent.codi_secc='$codi_secc_dupr'
		 and tab_ciud_vent.nomb_alia matches ('*$nomb_ciud*')
		 and tab_ciud_vent.indi_cart != 1";
    $resu_defe = ejecuta($conector_ifx, $base_ifx, $veri_defe, 0);
    $arre_defe = prepara_dato($base_ifx, $resu_defe);
    $tuplas_defe = filas($base_ifx, $arre_defe, $resu_defe);
    debuger($veri_defe);
  }

  if ($tuplas_defe > 0) {
    $coor_cx = trim(obtiene_dato($base_ifx, $arre_defe, $resu_defe, 0, 0));
    $coor_cy = trim(obtiene_dato($base_ifx, $arre_defe, $resu_defe, 0, 1));
    $lide_ante = trim(obtiene_dato($base_ifx, $arre_defe, $resu_defe, 0, 2));
    $codi_nuev = $zona_dupr . $codi_secc_dupr;

    //Verificacion coordenadas por defecto

    $veri_defe = "select max(codi_camp) from tab_camp";
    $resu_defe = ejecuta($conector_ifx, $base_ifx, $veri_defe, 0);
    $arre_defe = prepara_dato($base_ifx, $resu_defe);
    $codi_camp_defe = trim(obtiene_dato($base_ifx, $arre_defe, $resu_defe, 0, 0));

    $query = "delete from geo_inco_clie where cons_terc='$cons_terc'";
    ejecuta($conector_ifx, $base_ifx, $query, 0);

    $query = "delete from geo_clie where cons_terc='$cons_terc'";
    ejecuta($conector_ifx, $base_ifx, $query, 0);

    $actu_geo = "insert into geo_clie (cons_terc,dire_terc,codi_zona,cx,cy,estado,direcciontraducida,barriotraducido,codigodireccion,codigodireccionen,fuente,diralterna,manzana,barrio,localidad,nivsocio,zona1,zona2,cx_entr,cy_entr,zona1_entr,estado_entr,barrio_entr,codi_camp,acti_hora,acti_usua,desc_orig) values ($cons_terc,'$dire_terc','$zona_dupr',$coor_cx,$coor_cy,'B','$dire_terc','$nomb_barr_ante','$codigodireccion','$codigodireccionen','NUEVA','$dire_terc','$manzana','$barrio','','$nivsocio','$zona_dupr','$codi_secc_dupr',$coor_cx,$coor_cy,'$zona1_entr','$estado_entr','$barrio_entr',$codi_camp_defe,current,'$codi_usua','GEO_DEFECTO')";
    ejecuta($conector_ifx, $base_ifx, $actu_geo, 0);
    debuger($actu_geo);

    $codi_terc = fun_halla_codi_terc_secc($codi_nuev, $posi);

    //actualizacion lider defecto
    $para_lide = ", cons_lide=" . $lide_ante . ", codi_padr=" . $lide_ante . ", codi_zona=" . $zona_dupr . ", codi_desp='" . $zona_dupr . "', nuev_terr='" . $codi_nuev . "'";

    $query = "update tab_clie set codi_terc='$codi_terc', codi_sect='$codi_secc_dupr', dire_terc = '$dire_terc', acti_hora=current $para_lide where cons_terc='$cons_terc'";
    ejecuta($conector_ifx, $base_ifx, $query, 0);

    $query_reco = "INSERT INTO tab_reco (cons_terc, codi_ante, codi_nuev, acti_hora, acti_usua, codi_camp)
						  SELECT '$cons_terc','$codi_ante','$codi_terc',current,'$codi_usua',codi_camp 
						  FROM tab_camp 
						  WHERE today between fech_inic and fech_fina";
    ejecuta($conector_ifx, $base_ifx, $query_reco, 0);
  } else {


    $datos = georreferencia($dire_terc, $nomb_ciud, $nomb_barr);
    $zona_geo = $datos[zona];
    $codi_secc = $datos[sector];
    $esta = $datos[esta_geo];
    $barrio = $datos[barrio];
    $secc_dupr = $codi_secc;
    $codi_secc = "0" . $codi_secc;

    $flag = 0;
    if ($esta != 'A' && $esta != 'C' && $esta != 'E' && $esta != 'G' && $esta != 'H' && $esta != 'J' && $esta != 'R' && $esta != 'W' && strlen($esta))
      $flag = 1;

    $query = "select codi_sect, cons_lide from tab_sect where codi_sect='$codi_secc' and codi_zona='$zona_geo'";
    $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
    $arr_dato = prepara_dato($base_ifx, $resu);
    $tuplas = filas($base_ifx, $arr_dato, $resu);
    $codi_secc = trim(obtiene_dato($base_ifx, $arr_dato, $resu, 0, 0));
    $geo_lide = trim(obtiene_dato($base_ifx, $arr_dato, $resu, 0, 1));


    // 000010 P63 , 000001 EGRESO, 000000 REINGR	 
    $indi_p42 = substr($indi_p42, 0, 4);

    if ($indi_p42 == "0000" && $flag == 1 && $tuplas > 0) {
      //asesora de la seccion				  	               
      $para_lide = ", cons_lide=" . $geo_lide . ", codi_padr=" . $geo_lide . ", codi_zona=" . $zona_geo . ", codi_desp=" . $zona_geo;

      $codi_nuev = $zona_geo . $codi_secc;
      $codi_terc = fun_halla_codi_terc_secc($codi_nuev, $posi);
      $query = "update tab_clie set codi_terc='$codi_terc', codi_sect='$codi_secc', dire_terc = '$dire_terc', acti_hora=current $para_lide where cons_terc='$cons_terc'";
      $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
      debuger(date("H:i:s") . "- $query");

      $query_inse = "
				  insert 
				  into 
				  temp_reco_secc (
				  nume_iden,
				  codi_terc,
				  codi_ante,
				  codi_zona,
				  codi_secc,
				  cons_terc,
				  acti_hora)
				  values
				  ('$nume_iden',
				  '$codi_terc',
				  '$codi_ante',
				  '$zona_geo',
				  '$codi_secc',
				  '$cons_terc',
				  current)";
      $resu = ejecuta($conector_ifx, $base_ifx, $query_inse, 0);
    } else {
      //si no es p42    
      if ($zona_dupr == $zona_geo) {
        if ($codi_padr == $geo_lide) {
          $codi_nuev = $zona_geo . $codi_secc;
          $codi_terc = fun_halla_codi_terc_secc($codi_nuev, $posi);
          $query = "update tab_clie set codi_terc='$codi_terc', codi_sect='$codi_secc', dire_terc = '$dire_terc', acti_hora=current where cons_terc='$cons_terc'";
          $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
          $query_inse = "
				  insert 
				  into 
				  temp_reco_secc (
				  nume_iden,
				  codi_terc,
				  codi_ante,
				  codi_zona,
				  codi_secc,
				  cons_terc,
				  acti_hora)
				  values
				  ('$nume_iden',
				  '$codi_terc',
				  '$codi_ante',
				  '$zona_geo',
				  '$codi_secc',
				  '$cons_terc',
				   current)";
          $resu = ejecuta($conector_ifx, $base_ifx, $query_inse, 0);
        } else if ($codi_padr != $geo_lide && $flag == 1) {
          //asesora fuera de la seccion
          $codi_secc = "X" . $secc_dupr;
          $codi_nuev = $zona_dupr . $codi_secc;
          $codi_terc = fun_halla_codi_terc_secc($codi_nuev, $posi);

          $query = "update tab_clie set codi_terc='$codi_terc', codi_sect='$codi_secc', dire_terc = '$dire_terc', acti_hora=current where cons_terc='$cons_terc'";
          $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);

          $query_inse = "
				  insert 
				  into 
				  temp_reco_secc (
				  nume_iden,
				  codi_terc,
				  codi_ante,
				  codi_zona,
				  codi_secc,
				  cons_terc,
				  acti_hora)
				  values
				  ('$nume_iden',
				  '$codi_terc',
				  '$codi_ante',
				  '$zona_geo',
				  '$codi_secc',
				  '$cons_terc',
				   current)";
          $resu = ejecuta($conector_ifx, $base_ifx, $query_inse, 0);
        }
      } else if ($zona_dupr != $zona_geo && $flag == 1) {
        //no georeferenciada            
        $codi_nuev = $zona_dupr . "N99";
        $secc_ases = "N99";

        $codi_terc = fun_halla_codi_terc_secc($codi_nuev, $posi);
        $query = "update tab_clie set codi_terc='$codi_terc', codi_sect='$secc_ases', dire_terc = '$dire_terc', acti_hora=current where cons_terc='$cons_terc'";

        $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
        $query_inse = "
				  insert 
				  into 
				  temp_reco_secc (
				  nume_iden,
				  codi_terc,
				  codi_ante,
				  codi_zona,
				  codi_secc,
				  cons_terc,
				  acti_hora)
				  values
				  ('$nume_iden',
				  '$codi_terc',
				  '$codi_ante',
				  '$zona_geo',
				  '$secc_ases',
				  '$cons_terc',
  				   current)";
        $resu = ejecuta($conector_ifx, $base_ifx, $query_inse, 0);
      } else if ($flag == 0) {
        //no georeferenciada	
        //si es recodificacion y no encuentra lider asigna dupree
        if ($nomb_barr == 1)
          $actu_lide = ", codi_padr='20426' , cons_lide='20426' ";

        $codi_nuev = $zona_dupr . "X99";
        $codi_terc = fun_halla_codi_terc_secc($codi_nuev, $posi);
        $query = "update tab_clie set codi_terc='$codi_terc', codi_sect='X99', dire_terc = '$dire_terc', acti_hora=current $actu_lide where cons_terc='$cons_terc'";
        $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
        $query_inse = "
				  insert 
				  into 
				  temp_reco_secc (
				  nume_iden,
				  codi_terc,
				  codi_ante,
				  codi_zona,
				  codi_secc,
				  cons_terc,
				  acti_hora)
				  values
				  ('$nume_iden',
				  '$codi_terc',
				  '$codi_ante',
				  '$codi_zona',
				  'X99',
				  '$cons_terc',
 				   current)";
        $resu = ejecuta($conector_ifx, $base_ifx, $query_inse, 0);
      }
    }
    //si es cliente nuevo no recodifica

    $query_reco = "INSERT INTO tab_reco (cons_terc, codi_ante, codi_nuev, acti_hora, acti_usua, codi_camp)
				  SELECT '$cons_terc','$codi_ante','$codi_terc',current,'$codi_usua',codi_camp 
				  FROM tab_camp 
				  WHERE today between fech_inic and fech_fina";
    ejecuta($conector_ifx, $base_ifx, $query_reco, 0);

    //verificacion lider inca
    $query = "SELECT codi_camp 
				  FROM tab_camp 
				  WHERE today between fech_inic and fech_fina";
    $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
    $arre_dato = prepara_dato($base_ifx, $resu);
    $codi_camp = trim(obtiene_dato($base_ifx, $arre_dato, $resu, 0, 0));

    $query_lide = "SELECT CODI_ZONA,CODI_SECC,CODI_PADR,CONS_TERC,NUME_REFE,NOMB_TERC FROM ACTIVI WHERE CODI_PADR=20426 AND CAMP_REPO=$codi_camp AND CODI_ZONA IN (SELECT CODI_ZONA FROM RECO_ZONA_CIUD) INTO TEMP X1GEO_CLIE WITH NO LOG;";
    ejecuta($conector_ifx, $base_ifx, $query_lide, 0);

    $query_lide = "SELECT DISTINCT X1GEO_CLIE.CODI_ZONA,CODI_SECT,CODI_PADR,X1GEO_CLIE.CONS_TERC FROM X1GEO_CLIE,TAB_SECT WHERE X1GEO_CLIE.CODI_PADR=TAB_SECT.CONS_LIDE AND X1GEO_CLIE.CODI_ZONA=TAB_SECT.CODI_ZONA AND X1GEO_CLIE.CODI_SECC=TAB_SECT.CODI_SECT INTO TEMP X2GEO_CLIE WITH NO LOG; ";
    ejecuta($conector_ifx, $base_ifx, $query_lide, 0);

    $query_lide = "DELETE FROM X1GEO_CLIE WHERE CONS_TERC IN (SELECT CONS_TERC FROM X2GEO_CLIE);";
    ejecuta($conector_ifx, $base_ifx, $query_lide, 0);

    $query_lide = "UPDATE TAB_CLIE SET CODI_PADR = (SELECT CONS_LIDE FROM TAB_SECT WHERE TAB_CLIE.CODI_ZONA=TAB_SECT.CODI_ZONA AND TAB_CLIE.CODI_SECT=TAB_SECT.CODI_SECT) WHERE CONS_TERC IN (SELECT CONS_TERC FROM X1GEO_CLIE) AND CODI_SECT NOT IN ('N99','X99') AND CODI_ZONA IN (SELECT CODI_ZONA FROM RECO_ZONA_CIUD);";
    ejecuta($conector_ifx, $base_ifx, $query_lide, 0);

    $query_lide = "UPDATE TAB_CLIE SET CONS_LIDE = (SELECT CONS_LIDE FROM TAB_SECT WHERE TAB_CLIE.CODI_ZONA=TAB_SECT.CODI_ZONA AND TAB_CLIE.CODI_SECT=TAB_SECT.CODI_SECT) WHERE CONS_TERC IN (SELECT CONS_TERC FROM X1GEO_CLIE) AND CODI_SECT NOT IN ('N99','X99') AND CODI_ZONA IN (SELECT CODI_ZONA FROM RECO_ZONA_CIUD);";
    ejecuta($conector_ifx, $base_ifx, $query_lide, 0);

    $query_lide = "DROP TABLE X1GEO_CLIE";
    ejecuta($conector_ifx, $base_ifx, $query_lide, 0);

    $query_lide = "DROP TABLE X2GEO_CLIE";
    ejecuta($conector_ifx, $base_ifx, $query_lide, 0);
  }
  /*
   * Valida si la zona es diferente
   */
  $query = "select limit 1 codi_ante[1,3],codi_nuev[1,3] from tab_reco where cons_terc='$cons_terc' order by acti_hora desc;";
  $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
  $arr_dato = prepara_dato($base_ifx, $resu);
  $codi_ante = obtiene_dato($base_ifx, $arr_dato, $resu, 0, 0);
  $codi_nuev = obtiene_dato($base_ifx, $arr_dato, $resu, 0, 1);

  if ($codi_ante != $codi_nuev)
    envia_correo($cons_terc, $codi_ante, $codi_nuev);

  return $codi_terc;
}

function arma_direccion($arre_dire, $nomb_ciud) {

  $tuplas = count($arre_dire);
  //cuadrante nivel 2		
  $dire_terc = $arre_dire[0] . " " . $arre_dire[1] . " SUR ";
  for ($i = 2; $i < $tuplas; $i++) {
    $comp_dire .= $arre_dire[$i] . " ";
  }
  $dire_terc = $dire_terc . $comp_dire;
  $datos = georreferencia($dire_terc, $nomb_ciud, $nomb_barr);
  // cuadrante nivel 3
  if ($datos[sector] == "99") {
    $dire_terc = $arre_dire[0] . " " . $arre_dire[1] . " " . $arre_dire[2] . " SUR ";
    for ($i = 3; $i < $tuplas; $i++) {
      $comp_dire_a .= $arre_dire[$i] . " ";
    }
    $dire_terc = $dire_terc . $comp_dire_a;
    $datos = georreferencia($dire_terc, $nomb_ciud, $nomb_barr);
  }
  $dire_terc = trim(str_replace("   ", " ", $dire_terc));
  $dire_terc = trim(str_replace("  ", " ", $dire_terc));
  return $dire_terc;
}

function asigna_p42($codi_camp, $cons_terc) {
  global $conector_ifx, $base_ifx, $codi_usua, $form_deta, $nume_secu, $var_pin, $mensaje_error_php, $error;

  $cont = 0;
  $query = "select limit 5 codi_camp from tab_camp where codi_camp<='$codi_camp' order by codi_camp desc";
  $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
  $arr_dato = prepara_dato($base_ifx, $resu);
  $tuplas = filas($base_ifx, $arr_dato, $resu);
  for ($i = 0; $i < $tuplas; $i++) {
    $camp_fact = trim(obtiene_dato($base_ifx, $arr_dato, $resu, $i, 0));
    $query = "select nume_fact from fac_glob where cons_terc='$cons_terc' and cons_sucu=9 and acti_esta!='DEV' and codi_camp='$camp_fact'";
    $resu_fact = ejecuta($conector_ifx, $base_ifx, $query, 0);
    $arr_dato_fact = prepara_dato($base_ifx, $resu_fact);
    $tuplas_fact = filas($base_ifx, $arr_dato_fact, $arr_dato_fact);

    if ($tuplas_fact == 0)
      $cont .= "0";
    else
      $cont .= "1";
  }
  return $cont;
}

function envia_correo($cons_terc, $zona_ante, $codi_zona) {
  global $conector_ifx, $base_ifx, $codi_usua, $form_deta, $nume_secu, $var_pin, $mensaje_error_php, $error;
  $query = "select limit 1 codi_ante,codi_nuev from tab_reco where cons_terc='$cons_terc' order by acti_hora desc;";
  $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
  $arr_dato = prepara_dato($base_ifx, $resu);
  $codi_ante = obtiene_dato($base_ifx, $arr_dato, $resu, 0, 0);
  $codi_nuev = obtiene_dato($base_ifx, $arr_dato, $resu, 0, 1);
  $zona_nuev = substr($codi_nuev, 0, 3);
  $zona_ante = substr($codi_ante, 0, 3);


  $query = "select codi_terc, nume_iden, trim(nomb_terc)||' '||trim(apel_terc) as nomb_terc, codi_zona, codi_sect, dire_terc, celu_ter1 from tab_clie where cons_terc='$cons_terc';";
  $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
  $arr_dato = prepara_dato($base_ifx, $resu);
  $codi_terc = obtiene_dato($base_ifx, $arr_dato, $resu, 0, 0);
  $nume_iden = obtiene_dato($base_ifx, $arr_dato, $resu, 0, 1);
  $nomb_terc = obtiene_dato($base_ifx, $arr_dato, $resu, 0, 2);
  $codi_secc = obtiene_dato($base_ifx, $arr_dato, $resu, 0, 4);
  $dire_terc = obtiene_dato($base_ifx, $arr_dato, $resu, 0, 5);
  $celu_terc = obtiene_dato($base_ifx, $arr_dato, $resu, 0, 6);

  $asunto = "Dupree-Reporte georeferenciacion";

  $query = "select corr_empl from tab_empl_temp,tab_vend,tab_zona where tab_zona.codi_zona='$zona_ante' and tab_empl_temp.nume_iden=tab_vend.cedu_vend and (tab_vend.cons_vend=tab_zona.cons_vend or tab_vend.cons_vend=cons_apo1 or tab_vend.cons_vend=cons_apo2 or tab_vend.cons_vend=cons_apo3 or tab_vend.cons_vend=cons_apo4);";
  $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
  $arr_dato = prepara_dato($base_ifx, $resu);
  $corr_gere_ante = obtiene_dato($base_ifx, $arr_dato, $resu, 0, 0);

  $mensaje = "<p>
						  <strong>
						  SISTEMA DE GEOREFERENCIACION DUPREE
						  </strong>
						</p>
						<p> Buen dia, le informamos que la siguiente asesora cambio de zona:</p>";
  $mensaje .= "<p>------------------------------------------------------------------------</p>";
  $mensaje .= "<p><b>CEDULA:</b> $nume_iden </br> <b>NOMBRE:</b> $nomb_terc </br> <b>ZONA:</b>  $zona_ante</p>";
  $mensaje .= "<p>------------------------------------------------------------------------</p>";
  $mensaje .= "<p>Cordialmente,</p>";
  $mensaje .= "<p>Direccion de Ventas.</p>";

  $query = "select corr_empl from tab_empl_temp,tab_vend,tab_zona where tab_zona.codi_zona='$codi_zona' and tab_empl_temp.nume_iden=tab_vend.cedu_vend and (tab_vend.cons_vend=tab_zona.cons_vend or tab_vend.cons_vend=cons_apo1 or tab_vend.cons_vend=cons_apo2 or tab_vend.cons_vend=cons_apo3 or tab_vend.cons_vend=cons_apo4);";
  $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
  $arr_dato = prepara_dato($base_ifx, $resu);
  $corr_gere_desp = obtiene_dato($base_ifx, $arr_dato, $resu, 0, 0);

  $mensaje2 = "<p>
						  <strong>
						  SISTEMA DE GEOREFERENCIACION DUPREE
						  </strong>
						</p>
						<p> Buen dia, le informamos que la siguiente asesora ingreso a su zona:</p>";
  $mensaje2 .= "<p>------------------------------------------------------------------------</p>";
  $mensaje2 .= "<p> </br> <b>CEDULA:</b> $nume_iden </br> <b>NOMBRE:</b> $nomb_terc </br> <b>ZONA:</b>  $codi_zona </br> <b>DIRECCION:</b>  $dire_terc </br> <b>CELULAR:</b>  $celu_terc</p>";
  $mensaje2 .= "<p>------------------------------------------------------------------------</p>";
  $mensaje2 .= "<p>Cordialmente,</p>";
  $mensaje2 .= "<p>Direccion de Ventas.</p>";

  $cabeceras = "From: Industrias Inca DUPREE <servicioalcliente@dupree.com.co>\n";
  $cabeceras .= "Reply-To: servicioalcliente@dupree.com.co\n";
  $cabeceras .= "MIME-version: 1.0\n";
  $cabeceras .= "Content-type: multipart/mixed; ";
  $cabeceras .= "boundary=\"Message-Boundary\"\n";
  $cabeceras .= "Content-transfer-encoding: 7BIT\n";
  $body_top = "";
  $cuerpo = $body_top . $mensaje;
  $cuerpo2 = $body_top . $mensaje2;

  $de = "No responder <noresponder@dupree.com.co>";

  $query = "SELECT codi_zona FROM reco_zona_ciud where codi_zona='$codi_zona'";
  $resu = ejecuta($conector_ifx, $base_ifx, $query, 0);
  $arr_dato = prepara_dato($base_ifx, $resu);
  $tuplas = filas($base_ifx, $arr_dato, $resu);

  $mandar_email = enviar_correo($de, "$corr_gere_ante", $asunto, $cuerpo, $archivos);
  $mandar_email = enviar_correo($de, "$corr_gere_desp", $asunto, $cuerpo2, $archivos);
}

?>