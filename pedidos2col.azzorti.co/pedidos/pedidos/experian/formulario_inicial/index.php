<?php
require 'utilidades/get_departamento.php';
require 'utilidades/get_ciudad.php';
require 'utilidades/get_documentos.php';
require 'utilidades/get_parentesco.php';
?>
<!DOCTYPE html>
<html lang="es">

<?php include('components/head.php');?>

<body>
  <?php include('components/modal/VerificaCelular.php');?>
  <?php include('components/modal/PreguntasSeguridad.php');?>
  <?php include('components/header/header.php') ?>
  <?php include('components/pestana/PestanaUno.php') ?>
  <?php include('components/pestana/PestanaDos.php') ?>
  <?php include('components/pestana/PestanaTres.php') ?>
  <?php include('components/carga/Cargando.php') ?>
  <?php include('components/footer/footer.php') ?>
</body>
</html>
<script language="javascript">
  $(document).ready(function(){
    /*
    * Llena los input de forma automatica
    *
    * @property bool habi_preg_set Habilita boton para preguntas de seguridad
    * @property int pestana_activa: Habilita la pestaña uno (1) o (2) del formulario
    * @property object pestana1: Contiene la informacion a autocompletar de la primera pestaña
    * @property object pestana2: Contiene la informacion a autocompletar de la primera pestaña 2
    *
    * nota: Los campos de tipo select se ingresa el numero de indice del campo a seleccionar
    */
    let info={
      habi_preg_seg: false,
      pestana_activa: 2,
      pestana1: {
        tipo_docu: 1,
        cedula: "52880658",
        celular: "3054445555",
        fech_expe: "05/04/2001",
        nombre1:"MARIA",
        nombre2: "FERNANDA",
        apellido1: "LASSO",
        apellido2: "GUTIERREZ",
        fecha_naci: "05/04/1990",
        genero: 1,
        departamento: 4,
        ciudad: 1,
        correo: "correo2@mihost.es",
        ref_nombre: "MARIA FERNANDA AGUILAR",
        ref_dpto: 1,
        ref_ciud: 1,
        ref_celu: "3185555555",
        ref_pare:  2,
        refp_nombre: "CAMILO LOPEZ",
        ref_dpto: 1,
        refp_ciud: 1,
        refp_celu: "3152223255",
        trat_dato: true,
        cons_fina: true,
      },
      pestana2:{
        barrio: 1,
        direccion: {
          tipo_via: 1,
          nume_via: "12",
          letr_via: 1,
          dire_bis: 1,
          dire_num1: "17",
          dire_letr: 1,
          dire_num2: "20",
          dire_cuad: 1
        },
        complemento: "casa"
      }
    }
      //llen_info(info);
  });
</script>