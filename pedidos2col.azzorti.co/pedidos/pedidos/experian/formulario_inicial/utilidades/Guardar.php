<?php
define("DIRECTORIO", __DIR__ . "/../api/");

require_once('../conexion/conexion.php');
require_once('../api/Models/Experian.php');
require 'bd_transacciones.php';

use Api\Models\Experian;
//Controlador servicio transunion
$experian = new Experian();

$conexion = new Conexion();
$getConexion = $conexion->getConexion();

/* se realizan las consultas generales */
$consultas = new consultas();

$acti_usua = "experian";
$tipo_docu = isset($_POST["tipo_docu"]) ? $_POST["tipo_docu"] : "";
$nume_iden = isset($_POST["nume_iden"]) ? $_POST["nume_iden"] : "";
$nume_iden = trim($nume_iden);
$prim_nomb = isset($_POST["prim_nomb"]) ? $_POST["prim_nomb"] : "";
$segu_nomb = isset($_POST["segu_nomb"]) ? $_POST["segu_nomb"] : "";
$prim_apel = isset($_POST["prim_apel"]) ? $_POST["prim_apel"] : "";
$segu_apel = isset($_POST["segu_apel"]) ? $_POST["segu_apel"] : "";
$fech_naci = isset($_POST["fech_naci"]) ? $_POST["fech_naci"] : "";
$fech_naci_n = str_replace('/', '-', $fech_naci);
$date = date_create($fech_naci_n);
$fech_nuev = date_format($date, "m/d/Y");
$fech_expe = isset($_POST["fech_expe"]) ? $_POST["fech_expe"] : "";
$fech_expe_n = str_replace('/', '-', $fech_expe);
$fech_expe_date = date_create($fech_expe_n);
$fech_expe_nuev = date_format($fech_expe_date, "m/d/Y");
$sexo = isset($_POST["sexo"]) ? $_POST["sexo"] : "";
$codi_dpto = isset($_POST["codi_dpto"]) ? $_POST["codi_dpto"] : "";
$codi_ciud = isset($_POST["codi_ciud"]) ? $_POST["codi_ciud"] : "";
$celu_terc = isset($_POST["celu_terc"]) ? $_POST["celu_terc"] : "";
$corr_elec = isset($_POST["corr_elec"]) ? $_POST["corr_elec"] : "";
$cedu_lide = isset($_POST["cedu_lide"]) ? $_POST["cedu_lide"] : "";
$barrio = isset($_POST["barrio"]) ? $_POST["barrio"] : "";
$direccion = isset($_POST["direccion"]) ? $_POST["direccion"] : "";
$dire_entre = isset($_POST["dire_entre"]) ? $_POST["dire_entre"] : "";
$barr_nuev = isset($_POST["barr_nuev"]) ? $_POST["barr_nuev"] : "";
$edad = isset($_POST["edad"]) ? $_POST["edad"] : "";
$tipo_via = isset($_POST["tipo_via"]) ? $_POST["tipo_via"] : "";
$nume_via = isset($_POST["nume_via"]) ? $_POST["nume_via"] : "";
$letr_via = isset($_POST["letr_via"]) ? $_POST["letr_via"] : "";
$dire_num1 = isset($_POST["dire_num1"]) ? $_POST["dire_num1"] : "";
$dir_letr = isset($_POST["dir_letr"]) ? $_POST["dir_letr"] : "";
$dire_num2 = isset($_POST["dire_num2"]) ? $_POST["dire_num2"] : "";
$dire_cuad = isset($_POST["dire_cuad"]) ? $_POST["dire_cuad"] : "";
$dire_comp = isset($_POST["dire_comp"]) ? $_POST["dire_comp"] : "";
$apli_prep = isset($_POST["vali_apro_prep"]) ? $_POST["vali_apro_prep"] : "";
$apro_prep = isset($_POST["aprob_prep"]) ? $_POST["aprob_prep"] : "";
$nomb_refe = isset($_POST["fami_nomb"]) ? $_POST["fami_nomb"] : "";
$ciud_refe = isset($_POST["fami_codi_ciud"]) ? $_POST["fami_codi_ciud"] : "";
$celu_refe = isset($_POST["fami_celu"]) ? $_POST["fami_celu"] : "";
$pare_refe = isset($_POST["fami_pare"]) ? $_POST["fami_pare"] : "";
$refe_pers = isset($_POST["pers_nomb"]) ? $_POST["pers_nomb"] : "";
$ciud_pers = isset($_POST["pers_codi_ciud"]) ? $_POST["pers_codi_ciud"] : "";
$celu_pers = isset($_POST["pers_celu"]) ? $_POST["pers_celu"] : "";
$apro_term = isset($_POST["apro_term"]) ? $_POST["apro_term"] : "";

//Metodo que trae la campaña actual
$arre_camp = $consultas->trae_camp();
$camp = isset($arre_camp[0]['camp']) ? $arre_camp[0]['camp'] : "";

/* Metodo que trae el nombre de la ciudad por consecutivo */
$arre_ciud = $consultas->trae_ciud($codi_ciud);
$ciudad = isset($arre_ciud[0]['nomb_ciud']) ? $arre_ciud[0]['nomb_ciud'] : "";

/* Metodo que trae el nombre de la ciudad por consecutivo ref 1*/
$arre_refe = $consultas->trae_ciud($ciud_refe);
$ciud_refe =explode("(",isset($arre_refe[0]['nomb_ciud']) ? $arre_refe[0]['nomb_ciud'] : "");
$ciud_refe = $ciud_refe[0];

/* Metodo que trae el nombre de la ciudad por consecutivo ref2 */
$arre_pers = $consultas->trae_ciud($ciud_pers);
$ciud_pers =explode("(",isset($arre_pers[0]['nomb_ciud']) ? $arre_pers[0]['nomb_ciud'] : "");
$ciud_pers = $ciud_pers[0];

/* Metodo que trae el nombre de la departamento por consecutivo */

$arre_dpto = $consultas->trae_dpto($codi_dpto);
$nomb_dpto = isset($arre_dpto[0]['nomb_dpto']) ? $arre_dpto[0]['nomb_dpto'] : "";

/* trae el nombre del barrio */
$arre_barr = $consultas->trae_barr($barrio);
$nomb_barr = isset($arre_barr[0]['nomb_barr']) ? $arre_barr[0]['nomb_barr'] : "";
/* Valida si se realizo la validacion de identidad */
$vali_iden = $experian->veri_vali_iden($nume_iden);
$regi = (int)$vali_iden[0]["cantidad"];

if($regi == 0){
    $titulo = 'error';
    $mensaje = 'No se ha validado la identidad de la asesora.';
    $respuesta = ['titulo' => $titulo, 'mensaje' => $mensaje];
    $respuesta = json_encode($respuesta);
    die("$respuesta");
}

/* georeferenciacion */
$WG_VERSION = 2;
require_once("functions/conector_gs.php");
require ('../geo/fun_geo_clie.php');
if (empty($cedu_lide) || $cedu_lide = "") {
    $cedu_lide = '860001777';
} else {
    $cedu_lide = $_POST["cedu_lide"];
}
if ($barrio === "otro" || $barrio === 'otro') {
    $barrio = '315661';
}
$direccion = mb_strtoupper($direccion);
$dire_entre = mb_strtoupper($dire_entre);
$prim_nomb = mb_strtoupper($prim_nomb);
$segu_nomb = mb_strtoupper($segu_nomb);
$prim_apel = mb_strtoupper($prim_apel);
$segu_apel = mb_strtoupper($segu_apel);
/* Metodo que realiza la georeferenciación de la direccción */
$datos = georreferencia($direccion, $ciudad, $nomb_barr);
$codi_zona = $datos[zona];
$codi_sect = $datos[sector];
$esta_geo = $datos[estado];
$barrio_geo = $datos[barrio];
$dire_trad = $datos[dire_trad];
$origen = $datos[origen];
$cx = $datos[cx];
$cy = $datos[cy];

if ($origen == 'Sitidata') {
    $codi_sect = "0" . $datos[sector];
}


/* Validaciones saldo de cartera */
$ch = curl_init('https://alcor.dupree.co/dupreeWS/reportes/saldo');
//Validacion SSL
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
//TRUE para hacer un HTTP POST normal. Este POST del tipo application/x-www-form-urlencoded, el más común en formularios HTML.
curl_setopt($ch, CURLOPT_POST, 1);
//CURLOPT_POSTFIELDS codificará los datos como multipart/form-data
curl_setopt($ch, CURLOPT_POSTFIELDS, 'Params={"id_user":"710a35be0112475e3c7e3362ed188f33","cedula":' . $nume_iden . '}');
//TRUE para devolver el resultado de la transferencia como string del valor de curl_exec() en lugar de mostrarlo directamente.
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$res = curl_exec($ch);
curl_close($ch);
$data = json_decode($res);
$sald_cart = isset($data->result) ? $data->result : 0.00;

if (!empty($sald_cart)) {
    $titulo = 'informativo';
    $mensaje = 'Pareciera que tienes un saldo
		de cartera con la compañia si tienes mas dudas por favor dirigete
		a  servicioalcliente@dupree.com.co o comunicate al 571 290-6077';
    $respuesta = ['titulo' => $titulo, 'mensaje' => $mensaje, 'type' => 'warning'];
    $respuesta = json_encode($respuesta);
}


/* Validacion de estado de asesora */
$ch = curl_init('https://servicioweb.dupree.co/ws_movil/estado_asesora');
//Validacion SSL
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
//TRUE para hacer un HTTP POST normal. Este POST del tipo application/x-www-form-urlencoded, el más común en formularios HTML.
curl_setopt($ch, CURLOPT_POST, 1);
//CURLOPT_POSTFIELDS codificará los datos como multipart/form-data
curl_setopt($ch, CURLOPT_POSTFIELDS, 'Params={"cedula":' . $nume_iden . '}');
//TRUE para devolver el resultado de la transferencia como string del valor de curl_exec() en lugar de mostrarlo directamente.
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$resp = curl_exec($ch);
curl_close($ch);
$esta_ases = $resp;

//valida el corte por la zona de asesora
$arre_cort = $consultas->tra_cort_ases($codi_zona);
$codi_cort = isset($arre_cort[0]['codi_cort']) ? $arre_cort[0]['codi_cort'] : "";

/* obtiene la cuenta regresiva */
$query = "select distinct fech_ca21 from tab_cron_vent where codi_camp='$camp' and codi_cort='$codi_cort';";
$cuen_regr = $getConexion->ejecutar_consulta($query);
if (!empty($cuen_regr)) {
    $fecha_inicial = isset($cuen_regr[0]['fech_ca21']) ? $cuen_regr[0]['fech_ca21'] : "";
    // fecha actual
    $fecha_final = date("m/d/Y");
    $dias = (strtotime($fecha_inicial) - strtotime($fecha_final)) / 86400;
    $dias = abs($dias);
    $dias = floor($dias);
} else {
    $dias = 0;
}
//temporal
$apro_prep = "NO";
if ($apli_prep == 'A' && $apro_prep == 'SI') {
    $exis_docu = $consultas->vali_exis_docu_prep($nume_iden, $apro_prep);
    if (empty($exis_docu)) {
        $consultas->pedi_prep_apro($nume_iden, $apro_prep);
    }
}
//verifica si la zona es zona especial de cartera
// $codi_zona=614;
$zona_carte = $consultas->tra_zona_espe($codi_zona);
$zona_cart = isset($zona_carte[0]['zona_cart']) ? $zona_carte[0]['zona_cart'] : "";
if (empty($zona_cart)) {
    $zona_cart_fina = '2';
} else {
    $zona_cart_fina = '1';
}


$zona_sect_valo = trim($codi_zona) . trim($codi_sect);
// /* valida la existencia del documento en tab_clie*/
// $arre_clie=$consultas->vali_ases($nume_iden);
// if(!empty($arre_clie))
// {
// $titulo='error';
// $mensaje=$arre_clie;
// $respuesta=['titulo'=>$titulo,'mensaje'=>$mensaje];
// $respuesta=json_encode($respuesta);
// die("$respuesta");
// }

/* valida la existencia del numero de documento en la tabla pre_ases_expe */
$query = "select * from pre_ases_expe where nume_iden='$nume_iden'";
$data_ases = $getConexion->ejecutar_consulta($query);
if (!empty($data_ases)) {
    $titulo = 'error';
    $mensaje = 'Ya existe un registro con este documento ' . $nume_iden;
    $respuesta = ['titulo' => $titulo, 'mensaje' => $mensaje];
    $respuesta = json_encode($respuesta);
    die("$respuesta");
}

/* valida la existencia del numero de telefono en la tabla pre_ases_expe */
$query = "select * from pre_ases_expe where celu_ases='$celu_terc'";
$celu_repe = $getConexion->ejecutar_consulta($query);
if (!empty($celu_repe)) {
    $titulo = 'error';
    $mensaje = 'Ya existe el telefono ' . $celu_ases . ' registrado';
    $respuesta = ['titulo' => $titulo, 'mensaje' => $mensaje];
    $respuesta = json_encode($respuesta);
    die("$respuesta");
}
/* valida la existencia del correo electronico en la tabla pre_ases_expe */
$query = "select * from pre_ases_expe where corr_ases='$corr_elec'";
$corr_repe = $getConexion->ejecutar_consulta($query);
if (!empty($corr_repe)) {
    $titulo = 'error';
    $mensaje = 'Ya existe el correo ' . $corr_elec . ' registrado';
    $respuesta = ['titulo' => $titulo, 'mensaje' => $mensaje];
    $respuesta = json_encode($respuesta);
    die("$respuesta");
}

if ($codi_zona == '999') {
    /* si existe error en la georeferenciacion se inserta en el modulo desarrollo/cgis/geo_inco_clie/geo_inco_clie.php con corte X */
    $cx = '';
    $cy = '';
    $consultas->begin();
    $consultas->set_lock_mode();
    $consultas->lock_table("geo_inco_clie");
    /* Metodo que inserta en el modulo geo_inco_clie */
    $consultas->inserta_geo_inco($nume_iden, $direccion, $cx, $cy, $codi_zona, $acti_usua, $camp);
    $consultas->commit();
}
$consultas->begin();
$consultas->set_lock_mode();
$consultas->lock_table("pre_ases_expe");
/* Metodo que realiza la insercion a la tabla pre_ases_expe */

$file = fopen(RUTA_TEMPORALES . "cert_ases.txt", "a");

//envio risk

$toke_docu = "a77d2LtuEJ4l/1stdqXddjntIMSOUlkxHkn4qEOF0IA=";
$headers = array(
    "Token: $toke_docu",
    "Content-Type: application/json");

//$mail_ases = "david.neira@azzorti.co";
$mail_ases = "dsmartinezva@gmail.com";
$celu_terc = "3185551972";
//$celu_terc = "3184691381";
//$mail_ases = "andres.chiguazuque@azzorti.co";

//genera pdf
include("/disco1/paginas/alcor2col.azzorti.co/CGeneraFirm.php");

$clas_bot = new CGeneraFirm(null,"whatsapp");
$nomb_file="solicitud_$nume_iden.pdf";

$nomb_ases = $prim_nomb." ".$segu_nomb;
$corr_elec =$corr_elec;
$celu_ases = trim($celu_terc);
$soli_nume ="SC-".$nume_iden;
$desc_docu ="";
if($tipo_docu==3){
    $desc_docu = "CEDULA";
}else{
    $desc_docu = "C EXTRANJERIA";
}
$inic_proc = date ("Y-m-d h:i:s");

//referencias personales
//$nomb_ases."-".$prim_apel."-".$fech_naci_n."-".$soli_nume."-".$desc_docu."-".$nume_iden."-".$nomb_dpto."-".$ciudad."-".$zona_cart_fina."-".$cent_pobl."-".$nomb_barr."-".$direccion."-".$dire_entre."-".$celu_terc."-".$celu_terc."-".$corr_elec."-".$inic_proc."-".$nomb_refe."-".$ciud_refe."-".$celu_refe."-".$pare_refe."-".$refe_pers."-".$ciud_pers."-".$celu_pers."-".$firm_dig1."-".$firm_dig2."-".$tran_star."-".$refe_codi;
$clas_bot->gene_docu_firm($nomb_ases,$prim_apel,$fech_naci_n,$soli_nume,$desc_docu,$nume_iden,$nomb_dpto,$ciudad,$zona_cart_fina,$cent_pobl,$nomb_barr,$direccion,$dire_entre,$celu_terc,$celu_terc,$corr_elec,$inic_proc,$nomb_refe,$ciud_refe,$celu_refe,$pare_refe,$refe_pers,$ciud_pers,$celu_pers,$firm_dig1,$firm_dig2,$tran_star,$refe_codi);

// Descripcion de parametros
/*$nomb_pers = Nombres.
$apel_pers = Apellidos.
$fech_naci = Fecha de nacimiento.
$soli_nume = Numero de solicitud.
$tipo_docu = Tipo de documento.
$nume_docu = N�mero de documento.
$depa_resi = Departamento de residencia.
$ciud_resi = Ciudad de residencia.
$zona_vinc = Zona de vinculacion.
$cent_pobl = Centro poblado.
$barr_soli = Barrio.
$dire_resi = Direccion de residencia.
$dire_entr = Direccion de entrega.
$celu_soli = Celular.
$nume_what = Numero de whatsapp.
$corr_elec = Correo electronico.
$inic_proc = Fecha inicio proceso.
$nomb_refe = Nombres y apellidos referencia familiar.
$ciud_refe = Ciudad referencia familiar.
$celu_refe = Celular referencia familiar.
$pare_refe = Parentesco referencia familiar.
$refe_pers = Nombres y apellidos referencia personal.
$ciud_pers = Ciudad referencia personal.
$celu_pers = Celular referencia personal.
$firm_dig1 = Por azzorti. (enviar solo el nombre de la imagen, dejarla en alcor.azzorti.co/imagenes/ en formato .jpg resolucion del 70%)
$firm_dig2 = Por la asesora independiente (enviar solo el nombre de la imagen, dejarla en alcor.azzorti.co/imagenes/ en formato .jpg resolucion del 70%)
$tran_star = Aceptaci�n de autorizacion y tratamiento de datos [TRA_START_DATE]
$refe_codi = Referencia o codigo mencionado por Andres (Estoy a la espera que me confirme este codigo a que hace referencia)*/

$ruta_arch = file_get_contents(RUTA_TEMPORALES . $nomb_file);
$json_firm = '{
    "Usuario": "Dneira",
    "Clave": "Dneira*1324",
    "Firmantes": [
        {
            "Identificacion": "' . $nume_iden . '",
            "TipoIdentificacion": "Cedula de ciudadania",
            "Nombre": "' . $nomb_ases . '",
            "Correo": "' . $mail_ases . '",
            "NroCelular": "' . $celu_ases . '"
        }
    ],
    "ArchivosPDF": [
        {
            "Nombre": "Solicitud ' . $nume_iden . '",
            "Documento_base64": "' . base64_encode($ruta_arch) . '"
        }
    ]
    } ';

fwrite($file, "Json : " . $json_firm . "\n");

$url = "http://firmaplus.co/FirmaPlusPruebas/api/Signer";
$ch_es = curl_init();
curl_setopt($ch_es, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch_es, CURLOPT_URL, $url);
curl_setopt($ch_es, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch_es, CURLOPT_TIMEOUT, 20);
curl_setopt($ch_es, CURLOPT_POST, true);
curl_setopt($ch_es, CURLOPT_POSTFIELDS, $json_firm);
curl_setopt($ch_es, CURLOPT_HTTPHEADER, $headers);
$tiem_cone = curl_getinfo($ch_es, CURLINFO_TOTAL_TIME);

if (curl_error($ch_es)) {
    echo curl_error($ch_es);
}

$response = curl_exec($ch_es);
$response = json_decode($response);


fwrite($file, "Log certificado: " . print_r($response, true) . "\n");
$code_erro = $response->Code;

if ($code_erro == 0) {
    http_response_code(404);
    $mensaje = '{"error":"' . $response->Message . '"}';
    die($mensaje);
}

//Envia correo electronico con el hipervinculo para la firma digital
ob_start();
    include("email/mail.php");

    $correo = ob_get_clean();
ob_end_flush();

$de = "No responder <noresponder@dupree.com.co>";
$asunto = "Firma digital";
$archivos ="";

require_once ("cadena.php");

enviar_correo($de, $mail_ases, $asunto, $correo,$archivos);

//Envia mensaje de Whatsapp con el hipervinculo para el servicio firma digital

$mensaje = "Cordial saludo Sr(a): *$nomb_ases*\n
Por favor firma tu documento en este link:\n
{$response->Data->Link}\n

_*Recuerda no compartir este link a terceros*_.\n
Si tienes dudas sobre esta información envía
un correo a :  *servicioalcliente@dupree.com.co*
O comunícate al *+571 290-6077*

Equipo *Azzorti*";
//envio whatsapp
$info_what = array(
    "nume_celu" => "+57$celu_ases",
    "mens_envi" => $mensaje
);

$info_what = json_encode($info_what);

$ch = curl_init('https://alcor2col.azzorti.co/ws_noti_firm/EnvioMensaje/');
//Validacion SSL
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
//TRUE para hacer un HTTP POST normal. Este POST del tipo application/x-www-form-urlencoded, el más común en formularios HTML.
curl_setopt($ch, CURLOPT_POST, 1);
//CURLOPT_POSTFIELDS codificará los datos como multipart/form-data
curl_setopt($ch, CURLOPT_POSTFIELDS, $info_what);
//TRUE para devolver el resultado de la transferencia como string del valor de curl_exec() en lugar de mostrarlo directamente.
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$resp = curl_exec($ch);
curl_close($ch);

/**
 * Inserta registros
 */
 $consultas->inserta_datos(
    $tipo_docu,
    $camp,
    $nume_iden,
    $prim_nomb,
    $segu_nomb,
    $prim_apel,
    $segu_apel,
    $fech_expe_nuev,
    $fech_nuev,
    $sexo,
    $codi_dpto,
    $codi_ciud,
    $celu_terc,
    $corr_elec,
    $cedu_lide,
    $barrio,
    $direccion,
    $dire_entre,
    $dias,
    $codi_zona,
    $cx,
    $cy,
    $barr_nuev,
    $sald_cart,
    $esta_ases,
    $codi_cort,
    $tipo_via,
    $nume_via,
    $letr_via,
    $dire_num1,
    $dir_letr,
    $dire_num2,
    $dire_cuad,
    $dire_comp,
    $zona_cart_fina,
    $zona_sect_valo,
    $apro_term);
$consultas->commit();
$experian->actu_apro_ases($nume_iden);

$error = $consultas->verificaError($result) ;
 if (!empty($error)) {
    $mensaje = $error;
    $type = 'warning';
} else {
    $mensaje = 'Guardado de datos exitoso';
    $type = 'success';
}

if (isset($respuesta)) {
    die("$respuesta");
} else {
    $titulo = 'mensaje';
    $respuesta = ['titulo' => $titulo, 'mensaje' => $mensaje, 'type' => $type];
    $respuesta = json_encode($respuesta);
    die("$respuesta");
}
?>