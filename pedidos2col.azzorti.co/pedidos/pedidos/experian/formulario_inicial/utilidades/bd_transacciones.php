<?php

class consultas {

  public function __construct() {
    require_once('../conexion/conexion.php');
    $conexion = new Conexion();
    $getConexione = $conexion->getConexion();
    $this->conexion = $getConexione;
  }

  /* transacciones */

  public function begin() {
    $query = "begin work;";
    $resu_dato = $this->conexion->ejecutar_consulta($query);
    ;
    $this->verificaError($resu_dato);
  }

  public function commit() {
    $query = "commit work;";
    $resu_dato = $this->conexion->ejecutar_consulta($query);
    ;
    $this->verificaError($resu_dato);
  }

  public function set_lock_mode() {
    $query = "set lock mode to wait;";
    $resu_dato = $this->conexion->ejecutar_consulta($query);
    ;
    $this->verificaError($resu_dato);
  }

  public function lock_table($tabla) {
    $query = "lock table $tabla in exclusive mode;";
    $resu_dato = $this->conexion->ejecutar_consulta($query);
    ;
    $this->verificaError($resu_dato);
  }

  /* Metodos generales */

  /* Metodo que realiza la insercion de datos en la tabla pre_ases_expe */

  public function inserta_datos($tipo_docu, $camp, $nume_iden, $prim_nomb, $segu_nomb, $prim_apel, $segu_apel, $fech_expe, $fech_nuev, $sexo, $codi_dpto, $codi_ciud, $celu_terc, $corr_elec, $cedu_lide, $barrio, $direccion, $dire_entre, $dias, $codi_zona, $cx, $cy, $barr_nuev, $sald_cart, $esta_ases, $codi_cort, $tipo_via, $nume_via, $letr_via, $dire_num1, $dir_letr, $dire_num2, $dire_cuad, $dire_comp, $zona_cart_fina, $zona_sect_valo,$apro_term) {

      $query = "insert into pre_ases_expe(tipo_docu,codi_camp,fech_envi,nume_iden,prim_nomb,segu_nomb,
	prim_apel,segu_apel,fech_expe,fech_naci,sexo,cons_dpto,cons_ciud,celu_ases,corr_ases,lide_refe,nomb_barr,
	dire_ases,dire_entre,cuen_regre,apro_ases,codi_apro,zona_geo,coor_x,coor_y,vali_geo,barr_nuev,
	sald_cart,esta_ases,codi_cort,tipo_via,nume_via,letr_via,dire_num1,dir_letr,dire_num2,
	dire_cuad,dire_comp,zona_espe,zona_sect,apro_term)
	values('$tipo_docu','$camp',current,'$nume_iden','$prim_nomb','$segu_nomb','$prim_apel',
	'$segu_apel','$fech_expe','$fech_nuev','$sexo','$codi_dpto','$codi_ciud','$celu_terc','$corr_elec',
	'$cedu_lide','$barrio','$direccion','$dire_entre','$dias','','','$codi_zona','$cx','$cy',
	'SI','$barr_nuev','$sald_cart','$esta_ases','$codi_cort','$tipo_via',
	'$nume_via','$letr_via','$dire_num1','$dir_letr','$dire_num2','$dire_cuad','$dire_comp',
	'$zona_cart_fina','$zona_sect_valo','$apro_term');";
    $resu_dato = $this->conexion->ejecutar_consulta($query);

    ;
    $this->verificaError($resu_dato);
  }

  /* Metodo que realiza la insercion en el programa desarrollo/cgis/geo_inco_clie/geo_inco_clie.php con corte X */

  public function inserta_geo_inco($nume_iden, $direccion, $cx, $cy, $codi_zona, $acti_usua, $camp) {

     $query = "delete from geo_inco_clie where cons_terc = '$nume_iden';";
    $resu_dato = $this->conexion->ejecutar_consulta($query);

    $query = "insert into geo_inco_clie(cons_terc,dire_inco,dire_nuev,esta_inco,cx,cy,
	codi_zona,acti_hora,acti_usua,codi_camp,esta_regi,usua_modi,obse_usua,orig_inco)
	values('$nume_iden','$direccion','','C','$cx','$cy','$codi_zona',current,'$acti_usua','$camp',
	'1','$acti_usua','','EXPERIAN');";
    $resu_dato = $this->conexion->ejecutar_consulta($query);
    $this->verificaError($resu_dato);
  }

  //Metodo que trae la campa�a actual
  public function trae_camp() {
    $query = "select MAX(codi_camp) as camp from tab_camp;";
    $resu_dato = $this->conexion->ejecutar_consulta($query);
    return $resu_dato;
  }

  /* Metodo que trae el nombre de la ciudad por consecutivo */

  public function trae_ciud($codi_ciud) {
    $query = "select nomb_ciud  from tab_ciud where cons_ciud='$codi_ciud';";
    $resu_dato = $this->conexion->ejecutar_consulta($query);
    return $resu_dato;
  }

  /* Metodo que trae el nombre del departamento */

  public function trae_dpto($codi_dpto) {
    $query = "select nomb_dpto  from tab_dpto where cons_dpto='$codi_dpto';";
    $resu_dato = $this->conexion->ejecutar_consulta($query);
    return $resu_dato;
  }

  /* Metodo que trae el nombre del barrio por consecutivo */

  public function trae_barr($barrio) {
    $query = "select nomb_barr  from tab_barr where cons_barr='$barrio';";
    $resu_dato = $this->conexion->ejecutar_consulta($query);
    return $resu_dato;
  }

  /* Metodo que valida la existencia de la asesora en tab_clie  */

  public function vali_ases($nume_iden) {
    $valor = "";
    $query = "select nume_iden from tab_clie where nume_iden='$nume_iden' and esta_acti <> 'INAC';";
    $resu_dato = $this->conexion->ejecutar_consulta($query);
    if (!empty($resu_dato)) {
      $valor = 'asesora activa en el sistema';
    } else {
      $query = "select nume_iden from proc_refe where nume_iden='$nume_iden'";
      $resu_dato = $this->conexion->ejecutar_consulta($query);
      if (!empty($resu_dato)) {
        $valor = 'La asesora ya esta siendo procesado...';
      }
    }
    return $valor;
  }

  /* Metodo que valida el corte por zona encontrada */

  public function tra_cort_ases($codi_zona) {
    $query = "select codi_cort from tab_zona where codi_zona='$codi_zona';";
    $resu_dato = $this->conexion->ejecutar_consulta($query);
    return $resu_dato;
  }

  /* Metodoq que valida la existencia del documento para el pedido prepagado */

  public function vali_exis_docu_prep($nume_iden, $apro_prep) {
    $query = "select * from pedi_prep_apro where nume_iden='$nume_iden'";
    $resu_dato = $this->conexion->ejecutar_consulta($query);
    return $resu_dato;
  }

  /* Metodo que realiza el almacenamiento del pedido prepagado */

  public function pedi_prep_apro($nume_iden, $apro_prep) {
    $query = "insert into pedi_prep_apro(nume_iden,apro_prep) values ('$nume_iden','$apro_prep')";
    $this->conexion->ejecutar_consulta($query);
  }

  /* Metodo que consulta las zonas especiales de cartera */

  public function tra_zona_espe($codi_zona) {
    $query = "SELECT codi_zona as zona_cart FROM ZON_CODE where codi_zona='$codi_zona'";
    $zona_cart = $this->conexion->ejecutar_consulta($query);
    return $zona_cart;
  }

  /*
   * Metodo que verifica si existe algun error en la consulta
   * @param result mixed resultado de la consulta, si es de tipo string es un error, si es un arreglo la consulta esta bien
   * @return error bool resultado de la evaluacion
   */

  public function verificaError($result) {
    $error = false;
    if (gettype($result) === "string") {
      header("HTTP/1.1 400 Error_consulta");
      // $error = true;
      header('Content-type:application/json;charset=utf-8');
      $error = [];
      $error["estado"] = "E";
      $error["mensaje"] = "Error en Servidor";
      $resu_fina = json_encode($error);
      die($resu_fina);
    }
    return $error;
  }

}

?>