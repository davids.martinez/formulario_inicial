<footer class="footer">
    <center><p style="position: relative">Copyright © DUPR&Eacute;E.</p></center>
</footer>
<script src="estilos/js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>
<!-- Latest compiled and minified JavaScript -->
<script
  src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
  integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
  crossorigin="anonymous"></script>
<script src="estilos/js/bootstrap-toggle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.8.0/sweetalert2.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script src="estilos/js/library/wait-me.min.js"></script>
<script src="estilos/js/step-bootstrap.js"></script>
<script src="estilos/js/step-pregunta.js"></script>
<script src="estilos/js/funciones.js"></script>
<script src="estilos/js/funciones-eventos.js"></script>
<script src="estilos/js/eventos.js"></script>