SELECT
    trim(t.nume_iden) as nume_iden,
    trim(t.nomb_terc) || ' ' || trim(t.apel_terc) as nomb_comp,
    trim(b.codi_zona) as zona,
    trim(b.codi_sect) as secc
FROM
    tab_clie as t,
    tab_sect as b
WHERE
    t.cons_terc = b.cons_lide
    AND b.codi_zona = '328'
    AND b.codi_sect = '000'
LIMIT
    1;

select
    trim(p.codi_camp) || '/' || p.TRAN_FIRM || '_' || trim(p.nume_iden) || '.pdf' as pdf,
    '' :: char(15) as docu_lide,
    '' :: char(50) as nomb_lide,
    p.ZONA_GEO zona,
    CASE
        when p.ZONA_GEO <> '999' then SUBSTR(p.zona_sect, 4)
    END as secc,
    p.codi_camp as codi_camp,
    p.fech_envi as fech_envi,
    CASE
        WHEN td.cons_docu = 3 THEN 'CC'
        WHEN td.cons_docu = 5 THEN 'CE'
        ELSE ''
    END AS TD1,
    p.nume_iden as NI2,
    p.prim_nomb as PN7,
    p.segu_nomb as SN8,
    p.prim_apel as PA9,
    p.segu_apel as SA10,
    p.corr_ases as CCE17,
    p.fech_naci as FN6,
    p.sexo as MF88,
    d.nomb_dpto as DPT12,
    c.nomb_ciud as CIU13,
    p.celu_ases as CEL14,
    p.LIDE_REFE as RF99,
    p.dire_ases as DIR11,
    b.nomb_barr as BAR15,
    p.dire_entre as DRE905,
    '' camp_rech,
    '' camp_apro,
    '' clie_sanc,
    p.cuen_regre as cuen_regr,
    p.zona_geo as znc901,
    p.vali_geo as vali_geo,
    '$' || '' || round(p.sald_cart, 2) as cart_cast,
    CASE
        WHEN td.cons_docu = 3 THEN 'N/A'
        WHEN td.cons_docu = 5 THEN 'SI'
        ELSE ''
    END AS pedi_prep,
    b.cons_barr
from
    pre_ases_expe as p,
    tab_dpto as d,
    tab_ciud as c,
    tipo_docu as td,
    tab_barr as b
where
    p.cons_dpto = d.cons_dpto
    and p.cons_ciud = c.cons_ciud
    and td.cons_docu = p.tipo_docu
    and b.cons_barr = p.nomb_barr
    and p.esta_fina is null into temp ases_expe_1;

SELECT
    DISTINCT Z.CODI_CORT,
    ACTIVI.CODI_ZONA,
    A.NUME_IDEN,
    ACTIVI.NOMB_TERC,
    ACTIVI.CODI_TERC,
    A.DIRE_TERC,
    A.NOMB_BARR,
    ACTIVI.TELE_TERC,
    ACTIVI.NOMB_CIUD,
    ACTIVI.ELITE_2 AS ESTA_ACTI,
    ACTIVI.TOTA_SALD,
    ACTIVI.ACTI_CAM1,
    ACTIVI.VALO_FAL,
    B.NUME_IDEN AS CEDU_LIDE,
    B.CODI_TERC AS CODI_LIDE,
    TRIM(B.NOMB_TERC) || " " || TRIM(B.APEL_TERC) AS NOMB_LIDE,
    C.NUME_IDEN AS CEDU_refe,
    TRIM(C.NOMB_TERC) || " " || TRIM(C.APEL_TERC) AS NOMB_refe,
    ACTIVI.CAMP_REPO,
    ACTIVI.CODI_AREA,
    A.camp_ingr,
    Activi.nume_pedi
FROM
    ACTIVI,
    TAB_CLIE A,
    TAB_CLIE B,
    TAB_CLIE C,
    TAB_ZONA Z
WHERE
    ACTIVI.CONS_TERC IS NOT NULL
    AND ACTIVI.CONS_TERC = A.CONS_TERC
    AND Z.CODI_ZONA = ACTIVI.CODI_ZONA
    and 1 = 1
    AND A.CODI_PADR = B.CONS_TERC
    AND A.cons_lide = C.CONS_TERC
    AND CAMP_REPO = '202116'
    and 1 = 1;

UPDATE
    pre_ases_expe
SET
    tipo_docu = '$tipo_docu',
    codi_camp = '$camp',
    fech_envi = current,
    fech_naci = '$fech_nuev',
    sexo = '$sexo',
    cons_dpto = '$codi_dpto',
    cons_ciud = '$codi_ciud',
    celu_ases = '$celu_terc',
    corr_ases = '$corr_elec',
    lide_refe = '$cedu_lide',
    nomb_barr = '$barrio',
    dire_ases = '$direccion',
    dire_entre = '$dire_entre',
    cuen_regre = '$dias',
    apro_ases = '',
    codi_apro = '',
    zona_geo = '$codi_zona',
    coor_x = '$cx',
    coor_y = '$cy',
    vali_geo = 'SI',
    barr_nuev = '$barr_nuev',
    sald_cart = '$sald_cart',
    esta_ases = '$esta_ases',
    codi_cort = '$codi_cort',
    tipo_via = '$tipo_via',
    nume_via = '$nume_via',
    letr_via = '$letr_via',
    dire_num1 = '$dire_num1',
    dir_letr = '$dir_letr',
    dire_num2 = '$dire_num2',
    dire_cuad = '$dire_cuad',
    dire_comp = '$dire_comp',
    zona_espe = '$zona_cart_fina',
    zona_sect = '$zona_sect_valo',
    apro_term = '$apro_term'
where
    nume_iden = '$nume_iden';

select
    CONS_BARR_HOMO,
    NOMB_BARR_HOMO,
    NOMB_BARR,
    CONS_BARR
from
    tab_barr
where
    cons_ciud = "1"
    and (
        cons_barr_homo <> ""
        or cons_barr_homo is not null
    )
order by
    CONS_BARR_HOMO;

select
    CASE
        WHEN b.cons_barr_homo <> ""
        or b.cons_barr_homo is not null THEN b.cons_barr_homo
        ELSE b.CONS_BARR
    END CONS_BARR,
    CASE
        WHEN b.cons_barr_homo <> ""
        or b.cons_barr_homo is not null THEN b.NOMB_BARR_HOMO
        ELSE b.NOMB_BARR
    END NOMB_BARR,
from
    tab_barr b
where
    b.cons_ciud = "1"
    and (
        b.cons_barr_homo <> ""
        or b.cons_barr_homo is not null
    )
order by
    b.CONS_BARR_HOMO;