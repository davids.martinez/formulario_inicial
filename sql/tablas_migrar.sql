create table pre_ases_expe  (
  id_ases serial ,
  codi_camp character (20) ,
  fech_envi date ,
  tipo_docu integer ,
  nume_iden character (13) ,
  prim_nomb character (200) ,
  segu_nomb character (200) ,
  prim_apel character (200) ,
  segu_apel character (200) ,
  fech_expe date ,
  fech_naci date ,
  sexo character (20) ,
  cons_dpto smallint ,
  cons_ciud smallint ,
  celu_ases character (50) ,
  corr_ases character (50) ,
  lide_refe character (50) ,
  nomb_barr character (200) ,
  barr_nuev character (100) ,
  dire_ases character (200) ,
  dire_entre character (200) ,
  cuen_regre character (200) ,
  zona_geo character (50) ,
  codi_cort character (50) ,
  coor_x character (50) ,
  coor_y character (50) ,
  vali_geo character (20) ,
  barr_dupr character (50) ,
  sald_cart character (20) ,
  tipo_via character (20) ,
  nume_via character (20) ,
  letr_via character (20) ,
  dire_num1 character (20) ,
  dir_letr character (20) ,
  dire_num2 character (20) ,
  dire_cuad character (20) ,
  dire_comp character (20) ,
  zona_espe character (5) ,
  esta_ases character (20) ,
  apro_ases character (50) ,
  codi_apro character (10) ,
  esta_fina character (20) ,
  zona_sect character (20) ,
  tran_firm integer ,
  apro_term character (3)
);

ALTER TABLE pre_ases_expe
    ADD zona_sect character (20),
    ADD tran_firm integer,
    ADD apro_term character (3),
    ADD fech_expe date BEFORE fech_naci ;

create table audi_expe (
  tran_proc character (30) ,
  nume_iden character (13) ,
  nume_tran character (13) ,
  tran_fluj character (30) ,
  tran_resp character varying (100) ,
  codi_camp integer ,
  fech_soli  datetime year to second
);

create table temp_rech_ases (
  codi_camp integer ,
  nume_iden character (15) ,
  codi_rech smallint ,
  acti_usua character (35) ,
  obse_rech character (200)
);
