select
    '' as pdf,
    cl.nume_iden as docu_lide,
    cl.nomb_terc || '' || cl.apel_terc as nomb_lide,
    SUBSTR(b.codi_terr,1,3) as zona,
    SUBSTR(b.codi_terr,-3) as secc,
    p.codi_camp as codi_camp,
    p.fech_envi as fech_envi,
    CASE
        WHEN td.cons_docu = 3 THEN 'CC'
        WHEN td.cons_docu = 5 THEN 'CE'
        ELSE ''
    END AS TD1,
    p.nume_iden as NI2,
    p.prim_nomb as PN7,
    p.segu_nomb as SN8,
    p.prim_apel as PA9,
    p.segu_apel as SA10,
    p.corr_ases as CCE17,
    p.fech_naci as FN6,
    p.sexo as MF88,
    d.nomb_dpto as DPT12,
    c.nomb_ciud as CIU13,
    p.celu_ases as CEL14,
    p.LIDE_REFE as RF99,
    p.dire_ases as DIR11,
    b.nomb_barr as BAR15,
    p.dire_entre as DRE905,
    '<span class=\"glyphicon glyphicon-remove-sign\" aria-hidden=\"true\" style=\"padding-left: 5px;\"></span>' camp_rech,
    '<span class=\"glyphicon glyphicon-ok-sign\" aria-hidden=\"true\" style=\"padding-left: 5px;\"></span>' camp_apro,
    '<span class=\"glyphicon glyphicon-eye-open\" aria-hidden=\"true\" style=\"padding-left: 60px;align-content:center;\"></span>' clie_sanc,
    p.cuen_regre as cuen_regr,
    p.zona_geo as znc901,
    p.vali_geo as vali_geo,
    '$' || '' || round(p.sald_cart, 2) as cart_cast,
    CASE
        WHEN td.cons_docu = 3 THEN 'N/A'
        WHEN td.cons_docu = 5 THEN 'SI'
        ELSE ''
    END AS pedi_prep
from
    pre_ases_expe as p,
    tab_dpto as d,
    tab_ciud as c,
    tipo_docu as td,
    tab_barr as b,
    tab_clie as cl
where
    p.cons_dpto = d.cons_dpto
    and cl.cons_terc = b.cons_lide
    and p.cons_ciud = c.cons_ciud
    and td.cons_docu = p.tipo_docu
    and b.cons_barr = p.nomb_barr
    and p.esta_fina is null